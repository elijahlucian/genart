PImage img;

void setup() {
  size(400, 400);
  img = loadImage("field.jpg");
}

void draw() {
  loadPixels(); 
  // Since we are going to access the image's pixels too  
  img.loadPixels(); 
  for (int y = 0; y < height; y++) {
    for (int x = 0; x < width; x++) {
      int loc = x + y*width;
      
      // The functions red(), green(), and blue() pull out the 3 color components from a pixel.
      float r = red(img.pixels[loc]);
      float g = green(img.pixels[loc]);
      float b = blue(img.pixels[loc]);
      float mX = map(mouseX + frameCount, 0, width, 0, 255);
      float mY = map(mouseY + frameCount, 0, height, 0, 255);
      
      if(b > 190){ b = 10; g = mX; r = mY; }
      if(mousePressed){r = 255-r; g = 255-g; b = 255-g; }
      
      
      // Image Processing would go here
      // If we were to change the RGB values, we would do it here, 
      // before setting the pixel in the display window.
      
      // Set the display pixel to the image pixel
      pixels[loc] =  color(r,g,b);          
    }
  }
  updatePixels();
}
