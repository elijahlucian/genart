Petal[] petals = new Petal[127];

BasicInstrument basic_instrument = new BasicInstrument(1);

Drum kick = new Drum(36);

int[] kicks = {36,38,40};
int[] snares = {41,43,45,47};
int[] hats = {42,44,46};
int[] cymbals = {54,56,58};
int[] toms = {48,50,52};
int[] congas = {53,55,57};
int[] tamb = {51};
int[] cowbell = {49};
int[] fx = {59};
int[] rim = {37};
int[] clap = {39};
int[] percs = {60,61};

HashMap hashmap = new HashMap<Integer, Integer[]>

// initialize arrays [0..127]

float w = 800;
float h = 800;

float drum_x = 0;
float drum_y = 0;
float drum_h = 0;

float box_x = w / 2;
float box_y = h / 8;
float box_size = 100;

float tom_h = 0;
float tom_x = 0;
float tom_y = 0;

float pad_heat = 10;
boolean pad_on = false;

class Petal {
  float life;
  int note;

  Petal(int _note) {
    note = _note + 48;
    life = 150;
  }

  void petal_fade() {
    life *= 0.75;
  }

  void lightUp() {
    life = 150;
  }

  void show(float x,float y,int size) {
    if (life > 2) {
      petal_fade();
    }

    fill(128 + life,97 + life,249 + life,20 + life * 0.5);

    // life / 150

    beginShape();
    curveVertex(x,y);
    curveVertex(x,y);
    curveVertex(x-size/4 * (life / 150), size/2);
    curveVertex(x,size + 50 * noise(frameCount * 0.005));
    curveVertex(x+size/4, size/2);
    curveVertex(x,y);
    curveVertex(x,y);
    endShape();
  }
}

class Flower {
  float x, y, increment, opacity;
  int size, petal_count;

  IntList flower_petals;

  Flower(float _x, float _y, int _petals, int _size) {
    x = _x;
    y = _y;
    petal_count = _petals;
    // petals = new Petal[_petals];

    flower_petals = new IntList();

    for (int i = 0; i < _petals; ++i) {
      // petals[note_iterator] = new Petal(note_iterator);
      flower_petals.append(note_iterator);
      note_iterator++;
    }

    // petals = _petals;
    increment = TAU / _petals;
    size = _size;
    opacity = 0.2;
  }
  
  void update(float time) {
    int trans = petal_count - 5;
    
    rotate(time);
    stroke(253,252,127,30 + (trans * 60));

    for (int petal_index : flower_petals) {
      rotate(increment);
      petals[petal_index].show(x,y,size);
    }

    for (int i = 0; i < petal_count; ++i) {

    }
  }
}

Flower[] flowers = new Flower[3];
import themidibus.*; //Import the library
MidiBus myBus; // The MidiBus
int note_iterator = 48;

float time = 0;

int ionian[] = {0,2,4,5,7,9,11,12,14,16,17,19,21,23};

void setup() {


  for (int i = 0; i < petals.length; ++i) {
    petals[i] = new Petal(i);
  }

  size(800,800, P3D);
  smooth();

  MidiBus.list(); // List all available Midi devices on STDOUT. This will show each device's index and name.

  myBus = new MidiBus(this, "JD-Xi", 3); // (this, input_device, output_device);

  for (int i = 0; i < flowers.length; ++i) {
    flowers[i] = new Flower(0,0,i + 5,600/(i+1));
  }
}

void draw() {



  time = frameCount * 0.001;

  fill(pad_heat, 20);
  rect(0,0,width,height);

  strokeWeight(1);

  float x = sin(-frameCount * 0.01) * 10;
  float y = cos(-frameCount * 0.01) * 10;
  translate(width/2 + x, height/2 + y);

  pushMatrix();

  // piano
  for (Flower flower : flowers) {
    flower.update(time);
  }

  popMatrix();

  // drum thing
  fill(255, 20);
  strokeWeight(5);
  stroke(255);

  arc(drum_h, 400, drum_x, drum_y, 0, TAU);
  if (drum_x > 0) { drum_x *= 0.95; }
  if (drum_y > 0) { drum_y *= 0.95; }
  if (drum_h > 400) { drum_h = -400; }
  drum_h += 1;

  // tom thing
  if (tom_h > 5 ) { 
    arc(tom_x, tom_y, tom_h, tom_h, 0, TAU);
    tom_h *= 0.8;
  } 

  // bassline

  noFill();
  strokeWeight(3);
  box(box_x, box_y, box_size);
  if (box_x > 0){ box_x *= 0.8; } 
  if (box_y > 0){ box_y *= 0.8; }

  if (pad_on) {
    if (pad_heat < 127) { pad_heat *= 2.25; }
  } else {
    if(pad_heat > 10) { pad_heat *= 0.5; }
  }

}

void noteOn(int channel, int pitch, int velocity) {
  
  if ([36,38,40].include? pitch) kick.animate();

  if(pitch == 36) {
    kick.animate()
  } 
  
  switch(channel) {
  
    case 0: // Digital Synth 1
      petals[pitch].lightUp();
      // for (Flower flower : flowers) {
      //   flower.send_note(pitch);
      // }
      break;
    case 1: // Digital Synth 2
      //pads[pitch].play();
      pad_on = true;
      break;
    case 9: // Drums
      // drums[pitch].play();
      drum_switch(pitch, velocity);
      
      break;
    case 2: // Analog Synth

      // synth[pitch].play();
      // box_x += w;
      // box_y += h;
      break;
    default:

      break;
  }
  println(">>_"+Set.of(kicks).contains(pitch));
  
  // println("Channel:"+channel); 
  println("Pitch:"+pitch); // 0 .. 255
  // println("Velocity:"+velocity); // 0 .. 127
}

void drum_switch(int pitch, int velocity) {
  // if (Set.of(36,38).contains(pitch)) {
  //   drum[pitch].note_on();
  //   // box_x += w;
  //   // box_y += h;
  // }
  
  

  switch (pitch) {
    case 36: // kick
      // box_x += w;
      // box_y += h;
      // drum_y += (velocity * 2);
      break;
    case 39: // clap
      drum_y += (velocity * 1);
      break;
    case 41: //snare
      break;
    case 55: //tom
      tom_h = velocity;
      tom_x = random(-400, 400);
      tom_y = random(-400, 400);
      break;
    default:
      // drum_x += velocity;
      // drum_y += velocity;
      break;

  }
}

void noteOff(int channel, int pitch, int velocity) {
  switch(channel) {
    case 1: // Digital Synth 2
      //pads[pitch].play(); 
      pad_on = false;
      println("Pitch:"+pitch); // 0 .. 127
      break;
  }
}
