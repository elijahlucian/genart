// MidiMapping.pde

// Midi mapping for the jdxi

class BasicInstrument {
  int channel, note_index;

  float life, threshold, velocity_multiplier;
  float attack, decay, sustain, release;

  boolean percussive; // either has a key on&off, or it is percussive
  boolean note_off; 
  boolean draw_when_inactive; // persists even when under life threshold

  BasicInstrument(int _note) {
    note_index = _note;
  }

  void node_on(int _velocity) {
    if(percussive) {
      life = _velocity * velocity_multiplier;
    } else {
      // attack
      // decay
      // sustain
    }
  }

  void node_off() {
    note_off = true;
    // release
  }

  void update() {
    if(life > threshold) {
      if(note_off) { life *= release; }
      draw_self();
    }
    if(draw_when_inactive) { draw_self(); }
  }

  void draw_self() {

  }

}

class Drum extends BasicInstrument {
  int channel = 9;
  Drum(int _note){
    super(_note);
  }
  void draw_self() {
  }
}

class Synth extends BasicInstrument {
  int channel = 0;
  Synth(int _note){
    super(_note);
  }
  void draw_self() {
  }
}

class Pad extends BasicInstrument {
  int channel = 1;
  Pad(int _note){
    super(_note);
  }
  void draw_self() {
  }
}

class Bass extends BasicInstrument {
  int channel = 2;
  Bass(int _note){
    super(_note);
  }
  void draw_self() {
  }
}
