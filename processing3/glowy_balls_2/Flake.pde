class Flake {
  float x = random(width);
  float y = random(height);
  
  float max_life = 120;
  float age_rate = random(1,5);
  float age = 0;
  
  boolean mature = false;
  
  float x_direction = random(-1,1);
  float y_direction = random(-1,1);
  
  void new_flake() {
    x = random(width);
    y = random(height);
    x_direction = random(-1,1);
    y_direction = random(-1,1);
    age = 100;
  }
  
  void move() {
   
    if(x > width || x < 0) {
      new_flake();
    }
    
    if(y > height || y < 0) {
      new_flake();
    }
    
    if(mature) {
      age = age - age_rate/2;
      if(age < 5){
        new_flake();
      }
    } else {
     age = age + age_rate/5;
     if(age > max_life) {
      mature = true; 
     }
    }
    
    
    
    x = x + x_direction + random(0.5);
    y = y + y_direction + random(0.5);
    
  }
  
  void show() {
    stroke(188+(age/2),237-(age/2),246-(age/2),age);
    line(x,y,x+random(-3,3)*(age/40*random(1,2)),y+random(-3,3)*(age/40*random(1,2)));
  }
}
