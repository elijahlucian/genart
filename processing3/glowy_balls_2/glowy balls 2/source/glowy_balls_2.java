import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class glowy_balls_2 extends PApplet {

ArrayList<Ball> balls = new ArrayList<Ball>();

public void setup() {
  
  background(52,51,48);
  
  for(int w = 0; w <= width; w += 50){
    for(int h = 0; h <= height; h += 50){
        balls.add(new Ball(w,h));
    }
  }
  
  //balls.add(new Ball(width/2, height/2));
  
  int total = balls.size();
  println("The total number of balls is: " + total);
  
}

public void draw() {
  
  fill(0,8);
  rect(0,0,width,height);
  
  for (Ball ball: balls) {
    ball.show(); 
  }

}
class Ball {

  float x = width/2;
  float y = height/2;
  float shrink_speed = 1.1f;

  float Xdiff = abs(x - mouseX);
  float Ydiff = abs(y - mouseY);
   
  float size_mod = 1;
  float size = 1;
  float mouse_distance = 0;
  float reaction_range = 100;
  
  float diff_avg = (Xdiff + Ydiff) / 2; 
  
  float the_power = pow(1.1f, diff_avg);
   
  float x_size = 1;
  float y_size = 1;
   
   Ball(int _x, int _y) {
    println("new ball created", _x, _y);
    x = _x;
    y = _y;
   }
   
   public void log_shit() {
     println(Xdiff,Ydiff);
   }
   
   
   public void grow() {
     if(size < 200){
     size = size + size_mod;
     }
   }
   
   public void shrink() {
     if (size > 0) {
     size = size - 1 * shrink_speed;
     shrink_speed *= 1.1f;    
     }else{
       size = 1;
       shrink_speed = 1.1f; 
     }

   }
   
   public void check_size() {
     
     // use some math shit to calculate distance. 
     // origin, mouse, ... angle? not really. 
     // you have mouse x,y and origin x,y
     
     // two points. and an angle by way of ok
     // all coords are positive. diff wont be.
     
     // origin: x,y | 10,10 | 120, 90 
     // mouse: x,y | 20,20 | 80, 150
     
     // 10^2,10^2 = ^c
     // Math.sqrt(pow(abs(x_diff), 2) + pow(abs(y_diff), 2)) 
     
     Xdiff = x - mouseX;
     Ydiff = y - mouseY;
     
     mouse_distance = sqrt((pow(Xdiff,2) + pow(Ydiff,2))); 
     
     if (mouse_distance < reaction_range) {

       size_mod = (reaction_range - mouse_distance)/8;
       grow();
     } else {
        shrink();
     }
   }
   
   
   
   public void show() {
     
     check_size();
     stroke(0,0);
     //stroke(188,237,246,20);
     fill(188,237,246,10);  
     
     ellipse(x,y,size,size);
     
     //ellipse(x,y,1+Xsize_mod+random(5),1+Ysize_mod+random(5));
   }
  
}

class Flake {
  float x = random(width);
  float y = random(height);
  
  float max_life = 120;
  float age_rate = random(1,5);
  float age = 0;
  
  boolean mature = false;
  
  float x_direction = random(-1,1);
  float y_direction = random(-1,1);
  
  public void new_flake() {
    x = random(width);
    y = random(height);
    x_direction = random(-1,1);
    y_direction = random(-1,1);
    age = 100;
  }
  
  public void move() {
   
    if(x > width || x < 0) {
      new_flake();
    }
    
    if(y > height || y < 0) {
      new_flake();
    }
    
    if(mature) {
      age = age - age_rate/2;
      if(age < 5){
        new_flake();
      }
    } else {
     age = age + age_rate/5;
     if(age > max_life) {
      mature = true; 
     }
    }
    
    
    
    x = x + x_direction + random(0.5f);
    y = y + y_direction + random(0.5f);
    
  }
  
  public void show() {
    stroke(188+(age/2),237-(age/2),246-(age/2),age);
    line(x,y,x+random(-3,3)*(age/40*random(1,2)),y+random(-3,3)*(age/40*random(1,2)));
  }
}
class Ring {
  
}
  public void settings() {  size(1800,900); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "--present", "--window-color=#666666", "--stop-color=#cccccc", "glowy_balls_2" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
