

class Flower {
  float x, y, increment, opacity;
  int petals, size;

  Flower(float _x, float _y, int _petals, int _size) {
    x = _x;
    y = _y;
    petals = _petals;
    increment = TAU / _petals;
    size = _size;
    opacity = 0.2;
  }
  
  void petal_life() {
  }
  
  void update(float time) {

    int trans = petals - 5;

    fill(128,97,249,20 + (trans * 20));
    stroke(253,252,127,30 + (trans * 60));
    rotate(time);

    for (int i = 0; i < petals; ++i) {
      rotate(increment);

      beginShape();
      curveVertex(x,y);
      curveVertex(x,y);
      curveVertex(x-size/4, size/2);
      curveVertex(x,size + 50 * noise(i + frameCount * 0.005));
      curveVertex(x+size/4, size/2);
      curveVertex(x,y);
      curveVertex(x,y);
      endShape();
    }
  }
}

Flower flower;
Flower flower2;
Flower flower3;

Flower[] flowers = new Flower[3];
import themidibus.*; //Import the library
MidiBus myBus; // The MidiBus


float time = 0;

void setup() {
  size(800,800);
  smooth();

  MidiBus.list(); // List all available Midi devices on STDOUT. This will show each device's index and name.

  myBus = new MidiBus(this, "reface DX", 3); // (this, input_device, output_device);
  

  for (int i = 0; i < flowers.length; ++i) {
    flowers[i] = new Flower(0,0,i + 5,600/(i+1));
  }
}

void draw() {
  time = frameCount * 0.001;

  // background(0);
  fill(0,30);
  rect(0,0,width,height);

  strokeWeight(1);

  float x = sin(-frameCount * 0.01) * 10;
  float y = cos(-frameCount * 0.01) * 10;
  translate(width/2 + x, height/2 + y);

  for (Flower flower : flowers) {
    flower.update(time);
  }

}
