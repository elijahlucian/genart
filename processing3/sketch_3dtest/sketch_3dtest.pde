float x_mid, y_mid, z_mid;
Cube cube;
PImage tex;

class Cube {
  
  float x,y,z,r,rotation, height;
  
  Cube(float _x, float _y, float _z) {
    x = _x;
    y = _y;
    z = _z;
    r = 0;
    height = 200;
    rotation = 0;
  }
  
  void update() { 
    rotation += 0.01;
    height = 200 * sin(rotation);
  }
  
  void show() {

    rotateX(TAU + rotation);
    rotateZ(TAU + rotation / 2);

    stroke(255);
    fill(127);

    box(100);
    
    // rotateY(TAU/6);

  }
   
}

void setup() {
  
  tex = loadImage("tex.jpg");

  size(500,500,P3D);
  x_mid = width/2;
  y_mid = width/2;
  z_mid = -500;
  translate(x_mid, y_mid, z_mid);
  
  cube = new Cube(0,0,100);
  
}

void draw() {
  
  cube.update(); 
  //z_mid++;
  
  background(0);
  
  if (mousePressed) {
    directionalLight(0, 255, 0, 0, -1, 0);
  }
  
  translate(x_mid, y_mid, z_mid);
  
  cube.show();
  
}
