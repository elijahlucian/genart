//Flake f;
Flake[] f = new Flake[100];
float xoff = 0.0;

void setup() {
   
  size(1800,1000);
  background(52,51,48);
  for (int i = 0; i < f.length; i++) {
    f[i] = new Flake();
  }
  
}

void get_mouse(float w, float h) {
  float x = w;
  float y = h;
  
  float Xdiff = abs(x - mouseX);
  float Ydiff = abs(y - mouseY);
  
  float Xsize_mod = 1;
  float Ysize_mod = 1;
  
  float reaction_range = 400;
  
  stroke(188,237,246,20);
  fill(0,0);
  
  
  
  if (Xdiff < reaction_range && Ydiff < reaction_range) {
    Xsize_mod = (reaction_range-Xdiff) / 100;
    Ysize_mod = (reaction_range-Ydiff) / 100;
    stroke(188,237,246,80);
    fill(188,237,246,20);   
  }
  
  
  ellipse(x,y,10*Xsize_mod+random(15),10*Ysize_mod+random(15));
}

void draw() {
  
  fill(0,8);
  rect(0,0,width,height);
  
  for (int w = 0; w <= width; w+=100) {
    for (int h = 0; h <= height; h+=100) {

      get_mouse(w,h);
      
    }
  }

  
  for (int i = 0; i < f.length; i++) {
    f[i].move();
    f[i].show();
  }
}
