class Flake {
  float x = random(width);
  float y = random(height);
  
  float max_life = 120;
  float age_rate = random(1,30);
  float age = 255;
  
  float x_direction = random(-2,2);
  float y_direction = random(-2,2);
  
  void new_flake() {
    x = random(width);
    y = random(height);
    x_direction = random(-1,1);
    y_direction = random(-1,1);
    age = 255;
  }
  
  void move() {
   
    if(x > width || x < 0) {
      new_flake();
    }
    
    if(y > height || y < 0) {
      new_flake();
    }
    
    if(age < 50){
      new_flake();
    }
    
    x = x + x_direction + random(2);
    y = y + y_direction + random(2);
    age = age - age_rate;
  }
  
  void show() {
    stroke(255,249,165,age);
    line(x,y,x+random(-3,3),y+random(-3,3));
  }
}
