

class Boat {

  float x,y,angle;

  Boat(float _x, float _y) {
    x = _x;
    y = _y;
    angle = 0;
  }

  void update() {

  }

  void display() {

  }

}

void setup() {

  size(800,800);
  background(0);

}




void draw() {
  
  float x = 0;
  float y = 0;
  float last_x = 0;
  float last_y = 0;

  // noStroke();

  stroke(255, 100);
  strokeWeight(3);

  

  for (int j = - 150; j < height; j += 100 ) {

    float h = j + 120 * TAU + 42;

    beginShape();
    float grey = (height - j) / 4;
    fill(0,0, grey);
    curveVertex(0,height);
    float frame = frameCount * 0.01;
    
    float j_offset = (j - height * 0.95) * 0.001;

    for (int i = -50 ; i <= width + 50; i += 20) {
      x = i + noise(i + j) * 10;
      
      y = sin((i * 0.000051) * (j * 0.5) + frame + (frame * (h*0.001) )) * 20 + j + (noise(i + frame) * 10);

      // y = sin(i * 0.05 + frame * j * 0.02) * 20 + j + (noise(i + frame) * 10); //sin(j_offset * i * 0.04 + frame) * (noise(i + frame * 2) * 10 - (j / 4)) + j;
      
      curveVertex(x,y);
    }

    curveVertex(width,height);
    curveVertex(0,height);
    curveVertex(0,0);
    endShape();
    
  }

}
