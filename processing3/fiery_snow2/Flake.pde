class Flake {
  float x = random(width);
  float y = random(height);
  float max_life = 320;
  float age_rate = random(1,5);
  float age = 0;
  float speed = random(1,2);
  boolean mature = false;
  float x_direction = set_speed();
  float y_direction = set_speed();
  
  float r = 230+(age/2)+random(-5,5);
  float g = 160-(age/2)+random(-5,5);
  float b = 120-(age/2)+random(-5,5);
  
  
  float set_speed() {
    return random(-0.5,0.5);
  }
  
  void new_flake() {
    
    x = random(width);
    y = random(height);
    x_direction = set_speed();
    y_direction = set_speed();
    age = 0;
    speed = random(1,1.2);
    mature = false;
  }
  
  void move() {
   
    if(x > width || x < 0) {
      new_flake();
    }
    
    if(y > height || y < 0) {
      new_flake();
    }
    
    if(mature) {
      age = age - age_rate/2;
      if(age < 5){
        new_flake();
      }
    } else {
     age = age + age_rate/5;
     if(age > max_life) {
      mature = true; 
     }
    }
    
    x = x + (x_direction + random(0.2)) * speed ;
    y = y + (y_direction + random(0.2)) * speed ;
    speed = speed + 0.01;
    
  }
  
  void show() {
    
    strokeWeight(age/40);
    
    stroke(r,g,b,age);
     
    line(x,y,x+random(-3,3)*(age/60*random(1,1.5)),y+random(-3,3)*(age/60*random(1,1.5)));
  }
}
