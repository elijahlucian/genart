Note[] notes = new Note[127];
Pad[] pads =   new Pad[127];
Drum[] drums = new Drum[5];



import themidibus.*; //Import the library
MidiBus myBus; // The MidiBus

void setup() {
  size(800, 200);
  background(0);
  MidiBus.list(); // List all available Midi devices on STDOUT. This will show each device's index and name.

  Synth synthesizer = new Synth();
  
  for(int i = 0; i < 127; i++){
    pads[i] = new Pad(i);
    notes[i] = new Note(i);
  }

  myBus = new MidiBus(this, "JD-Xi", 0); // (this, input_device, output_device);
  
}

void draw() {
  background(0);

  for(int i = 0; i < notes.length; i++) {
    notes[i].animate();
    notes[i].show();
  }
}

void noteOn(int channel, int pitch, int velocity) {
  
  switch(channel) {
  
    case 0: // Digital Synth 1
      notes[pitch].new_note(pitch);
      break;
    case 1: // Digital Synth 2
      //pads[pitch].play();
      break;
    case 9: // Drums
      // drums[pitch].play();
      break;
    case 2: // Analog Synth
      // synth[pitch].play();
      break;
    default:

      break;
  }
  
  println("Channel:"+channel);
  println("Pitch:"+pitch);
}

void noteOff(int channel, int pitch, int velocity) {
  notes[pitch].stop_note();
}

void controllerChange(int channel, int number, int value) {
  // Receive a controllerChange
  println();
  println("Controller Change:");
  println("--------");
  println("Channel:"+channel);
  println("Number:"+number);
  println("Value:"+value);
}

void delay(int time) {
  int current = millis();
  while (millis () < current+time) Thread.yield();
}
