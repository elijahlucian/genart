// GLOBALS FOR GLOBALY EFFECTS FROM KICK MAYBE? 
// SIZE MULTIPLIERS FROM KICK AND SNARE
// LUMINOSITY FROM CYMBALS
// SOME KIND OF SCREEN SHAKE / TRANSOFRM FOR PERCS

class Drum {

  int[] kicks = {36,38,40};
  int[] snares = {37,39,41,43,45,47};
  int[] hats = {42,44,46};  
  
  int[] percs = {48,49,50,51,52,53,55,57,59,60,61};
  int[] cymbals = {54,56,58};
    
  String drum_kind;
  
  boolean check_kind(int[] arr, int pitch) {
    for(int i: arr) {
         if(pitch == i)
         { 
           return true;
         } 
       }
       return false;
  }
  
  Drum(int pitch) {
    if(pitch < 48) {
       if (check_kind(kicks, pitch)) {
         drum_kind = "kick";
       } else if (check_kind(hats, pitch)) {
         drum_kind = "hat";
       } else {
         drum_kind = "snare";
       }
    } else {
      if (check_kind(cymbals, pitch)) {
        drum_kind = "cymbal";
      } else {
        drum_kind = "perc";
      }
    }
   }
   
    // then set drum coordinates based on drum kind.
    
   
   void play() {
     
   }
   
   void animate() {
     
   }
   
   void show() {
     
   }
  
}
