class Note {
  
  int len;
  int velo;
  int pitch;
  float x, y;
  float y_speed = 1;
  boolean alive = false;
  int octave = -12 * 1;

  float life = 100;

  Note(int pitch) {
    // make this relative to scale
    
    if(pitch > 47 + octave && pitch < 85 + octave){
 
      
      x = float(pitch - 36) / 36 * 800;
    } else {
      x = -1000;
    }
  }
  
 
  
  void new_note(int pitch) {
    alive = true;
    y = 200;
    y_speed = 1; 
    life = 100;
  }
  
  void stop_note() {
   alive = false;
   y = 0;
  }
  
  void animate() {
    if(alive){
      life += 0.1;

    }else{
      
      if(life >= 0){
        life -= 2;
      }
       
    }
  }
  
  void show() {    
      fill((life-50),(life-50),(life-50));
      stroke(life,life,life);
      rect(x,-20,(800/36)-50,240);
  }
}
