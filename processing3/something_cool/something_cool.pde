Note note[] = new Note[8];



import themidibus.*; //Import the library

int[] chord1 = new int[4];
int isMinor = 0;
int isDiminished = 0;
int isDominant = 0;

MidiBus myBus; // The MidiBus

void setup() {
  size(800, 200);
  background(0);
  noStroke();
  MidiBus.list(); // List all available Midi devices on STDOUT. This will show each device's index and name.

  for(int i = 0; i < width; i += 100) {
    fill(random(255),random(255),random(255));
    rect(i, 0, width/7, height);
  }

  // Either you can
  //                   Parent In Out
  //                     |    |  |
  //myBus = new MidiBus(this, 0, 1); // Create a new MidiBus using the device index to select the Midi input and output devices respectively.

  // or you can ...
  //                   Parent         In                   Out
  //                     |            |                     |
  //myBus = new MidiBus(this, "IncomingDeviceName", "OutgoingDeviceName"); // Create a new MidiBus using the device names to select the Midi input and output devices respectively.

  // or for testing you could ...
  //                 Parent  In        Out
  //                   |     |          |
  myBus = new MidiBus(this, 0, "reface DX"); // Create a new MidiBus with no input device and the default Java Sound Synthesizer as the output device.
}

void draw() {
  
  //fill(0,150);
  //rect(0,0,width, height);

  // highlight chord that's playing
  // if mouse has not moved, in one beat, then start chord progression.
  // make note object.

  float mouseChord = map(mouseX, 0, width, 0, 7);
  float bgColor = map(mouseX, 0, width, 0, 255);
  int chordNumber; // = round(mouseChord);
  chordNumber = round(mouseX/100);
  int rootNote = 60;
  int ionian[] = {0,2,4,5,7,9,11,12,14,16,17,19,21,23};
  
  int third = 2;
  int fifth = 4;
  int seventh = 6;
  
  int arp[] = {
               rootNote+ionian[chordNumber%ionian.length],
               rootNote+ionian[(chordNumber+third)%ionian.length],
               rootNote+ionian[(chordNumber+fifth)%ionian.length],
               rootNote+ionian[(chordNumber+seventh)%ionian.length]
             };
  
  int channel = 0;
  int velocity = 100;
  int beat = 60;
  
  for(int i = 0; i < arp.length; i ++) {
    velocity = velocity + i * 6;
    myBus.sendNoteOn(channel, arp[i], velocity); // Send a Midi noteOn
    delay(beat*4);
  }
  
  for(int i = 0; i < arp.length; i ++) {
    velocity = velocity + i * 6;
    myBus.sendNoteOff(channel, arp[i], velocity); // Send a Midi nodeOff
    delay(beat*2);
  }
  
  int number = 0;
  int value = 90;

  myBus.sendControllerChange(channel, number, value); // Send a controllerChange
  println(pitch,":",velocity,":",chordNumber,":",mouseChord);
  
}

void noteOn(int channel, int pitch, int velocity) {
  // Receive a noteOn
  println();
  println("Note On:");
  println("--------");
  println("Channel:"+channel);
  println("Pitch:"+pitch);
  println("Velocity:"+velocity);
}

void noteOff(int channel, int pitch, int velocity) {
  // Receive a noteOff
  println();
  println("Note Off:");
  println("--------");
  println("Channel:"+channel);
  println("Pitch:"+pitch);
  println("Velocity:"+velocity);
}

void controllerChange(int channel, int number, int value) {
  // Receive a controllerChange
  println();
  println("Controller Change:");
  println("--------");
  println("Channel:"+channel);
  println("Number:"+number);
  println("Value:"+value);
}

void delay(int time) {
  int current = millis();
  while (millis () < current+time) Thread.yield();
}
