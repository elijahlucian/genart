int window_size = 600;

class Star {
  float x, y, s, initial_x;
  
  //int window_size = 1000;
  
  Star(float x1, float y1) {
    initial_x = x1;
    x = x1;
    y = y1;
    s = random(10);
  }
  
  void reset() {
    x = random(window_size);
    y = 0;
    s = random(10);
  }
  
  void update() {
    // x += 1;
    y += 4 / s; // + sin(x/2 * 6.18);
    //x = sin(y/2 * 6.18) + x;
    if(y > window_size) {
      reset();
      
    }
  }
  
  void display() {
  
    fill(255/(s/2));
    float len = 10 - s;
    ellipse(x,y,2,len);
  }
  
}

Star[] stars;

void setup() {
  size(600,600); 
  
  stars = new Star[255];
  for(int i = 0; i < 255; i++) {
    stars[i] = new Star(random(window_size),random(window_size));
  }
}

void draw() {
    background(0);
    for(int i = 0; i < 255; i++) {
      stars[i].update();
      stars[i].display();
    }
}
