boolean flip = false;
int mod = 190;

void setup() {
  size(800,800);
  background(255);
  smooth();
}

void draw() {
  float frame = frameCount * 0.03;
  float stroke_it = (frameCount % mod) / 3;
  if (frameCount % mod == 0) {
    flip = !flip;
  }
  
  noStroke();
  rect(0,0,width,height);
  fill(flip ? 0 : 255, 10);

  translate(width/2,height/2);

  if (mousePressed) {
    println("Mouse: " + mouseX + ", " + mouseY);
    println("Time: ", frameCount);
  }

  beginShape();
  // noFill();
  stroke(flip ? 255 : 0);


  strokeWeight(stroke_it);

  for (int i = 0; i < 80; ++i) {
    float x = sin(-i + frame) * 10;
    float y = cos(-i + frame) * 10;
    curveVertex(x * i, y * i);
  }
  endShape();

}
