Ball[] ball = new Ball[100];

void setup() {
   
  size(800,600);
  background(52,51,48);
  
}

void get_mouse(float w, float h) {
  float x = w;
  float y = h;
  
  float Xdiff = abs(x - mouseX);
  float Ydiff = abs(y - mouseY);
  
  float Xsize_mod = 1.1;
  float Ysize_mod = 1.1;
  
  float reaction_range = width/4;
  
  float diff_avg = (Xdiff + Ydiff) / 2; 
  
  float the_power = pow(1.1, diff_avg);
  
  stroke(188,237,246,1);
  fill(0,0);
  
  //println("X:", Xdiff, "Y:", Ydiff, "pow:", the_power);
  
  if (Xdiff < reaction_range && Ydiff < reaction_range) {
    Xsize_mod = pow(1.1, (reaction_range-Xdiff)/4);
    Ysize_mod = pow(1.1, (reaction_range-Ydiff)/4);
  }
  
  // just because it's along the x axis, the y should not be full height. 
  
  stroke(188+Xdiff,237+Ydiff,246+diff_avg,diff_avg/200);
  fill(188,237,246,20);   
  ellipse(x,y,1+Xsize_mod+random(5),1+Ysize_mod+random(5));
}

void draw() {
  
  fill(0,8);
  rect(0,0,width,height);
  
  for(int w = 0; w <= width; w += 50){
    for(int h = 0; h <= height; h += 50){
        ball = new Ball(w,h);
        ball.show();
    }
  }

  
}
