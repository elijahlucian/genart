class Ball {

   float x = width/2;
   float y = height/2;
   
  float Xdiff = abs(x - mouseX);
  float Ydiff = abs(y - mouseY);
   
  float Xsize_mod = 1.1;
  float Ysize_mod = 1.1;
  
  float reaction_range = width/4;
  
  float diff_avg = (Xdiff + Ydiff) / 2; 
  
  float the_power = pow(1.1, diff_avg);
   
   Ball(int _x, int _y) {
    println("new ball created", _x, _y);
    x = _x;
    y = _y;
   }
   
   
   void resize() {
     
     Xdiff = abs(x - mouseX);
     Ydiff = abs(y - mouseY);
     
     diff_avg = (Xdiff + Ydiff) / 2; 
     
     if (Xdiff < reaction_range && Ydiff < reaction_range) {
       
       Xsize_mod = pow(1.1, (reaction_range-Xdiff)/4);
       Ysize_mod = pow(1.1, (reaction_range-Ydiff)/4);
     } else {
        Xsize_mod = 1.1;
        Ysize_mod = 1.1;
   
     }
   }
   
   
   
   void show() {
     
     println(Xdiff,Ydiff);
     
     resize();
     stroke(188+Xdiff,237+Ydiff,246+diff_avg,diff_avg/200);
     fill(188,237,246,20+diff_avg);  
     ellipse(x,y,1+Xsize_mod+random(5),1+Ysize_mod+random(5));
   }
  
}
