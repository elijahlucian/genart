class Drop {
 
  float x = random(width);
  float y = 0;
  
  float speed = 4;
  
  void fall() {
    y = y + speed;
  }
  
  void show() {
    stroke(120);
    line(x,y,x+2,y+4);
  }
  
}
