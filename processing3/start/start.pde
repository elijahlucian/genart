float t = 0;

int w = 100;
int h = 100;

Pixel[] agents = new Pixel[w*h];

void setup() {
  surface.setSize(w, h);
  noLoop();
  fill(255);
  noStroke();
  for (int i = 0; i < agents.length; i++) {
    agents[i] = new Pixel(
      i, 
      1.0*(i%w)/w, 
      1.0*i/w/h
    ); 
  }
}
 
void draw() {
  surface.setLocation(displayWidth/2,0);
  fill(0,30);
  rect(0,0,width,height);
  t += 0.005;
  float w = width / 2;
  float h = height / 2;
  fill(255);
  circle(sin(t) * w /2 + w, h, 10);
  
  loadPixels();
  for(Pixel pix : agents) {
    for(int i = -4; i < 4; i++) {
      Pixel n = agents[lim(i)];
      println(frameCount, i, n.r, n.g, n.b);
    }
    pixels[pix.i] = color(
      pix.r, 
      pix.g, 
      pix.b
     );
  }
  updatePixels();
} 

int lim(int n) {
  if(n < 0) return 0;
  if(n > agents.length) return agents.length;
  return n;
}
