//Flake f;
Flake[] f = new Flake[50];

void setup() {
  size(1800,900);
  background(52,51,48);
  for (int i = 0; i < f.length; i++) {
    f[i] = new Flake();
  }
}

void draw() {
  stroke(0,0);
  fill(0,8);
  rect(0,0,width,height);
  
  
  for (int i = 0; i < f.length; i++) {
    f[i].move();
    f[i].show();
  }
}
