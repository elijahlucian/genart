ArrayList<Ball> balls = new ArrayList<Ball>();

void setup() {
  size(1800,900);
  background(52,51,48);
  
  for(int w = 0; w <= width; w += 50){
    for(int h = 0; h <= height; h += 50){
        balls.add(new Ball(w,h));
    }
  }
  
  //balls.add(new Ball(width/2, height/2));
  
  int total = balls.size();
  println("The total number of balls is: " + total);
  
}

void draw() {
  
  fill(0,8);
  rect(0,0,width,height);
  
  for (Ball ball: balls) {
    ball.show(); 
  }

}
