class Ball {

  float x = width/2;
  float y = height/2;
  float shrink_speed = 1.1;

  float Xdiff = abs(x - mouseX);
  float Ydiff = abs(y - mouseY);
   
  float size_mod = 1;
  float size = 1;
  float mouse_distance = 0;
  float reaction_range = 100;
  
  float diff_avg = (Xdiff + Ydiff) / 2; 
  
  float the_power = pow(1.1, diff_avg);
   
  float x_size = 1;
  float y_size = 1;
   
   Ball(int _x, int _y) {
    println("new ball created", _x, _y);
    x = _x;
    y = _y;
   }
   
   void log_shit() {
     println(Xdiff,Ydiff);
   }
   
   
   void grow() {
     if(size < 200){
     size = size + size_mod;
     }
   }
   
   void shrink() {
     if (size > 0) {
     size = size - 1 * shrink_speed;
     shrink_speed *= 1.1;    
     }else{
       size = 1;
       shrink_speed = 1.1; 
     }

   }
   
   void check_size() {
     
     // use some math shit to calculate distance. 
     // origin, mouse, ... angle? not really. 
     // you have mouse x,y and origin x,y
     
     // two points. and an angle by way of ok
     // all coords are positive. diff wont be.
     
     // origin: x,y | 10,10 | 120, 90 
     // mouse: x,y | 20,20 | 80, 150
     
     // 10^2,10^2 = ^c
     // Math.sqrt(pow(abs(x_diff), 2) + pow(abs(y_diff), 2)) 
     
     Xdiff = x - mouseX;
     Ydiff = y - mouseY;
     
     mouse_distance = sqrt((pow(Xdiff,2) + pow(Ydiff,2))); 
     
     if (mouse_distance < reaction_range) {

       size_mod = (reaction_range - mouse_distance)/8;
       grow();
     } else {
        shrink();
     }
   }
   
   
   
   void show() {
     
     check_size();
     stroke(0,0);
     //stroke(188,237,246,20);
     fill(188,237,246,10);  
     
     ellipse(x,y,size,size);
     
     //ellipse(x,y,1+Xsize_mod+random(5),1+Ysize_mod+random(5));
   }
  
}
