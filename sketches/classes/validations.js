const validate = params => {
  const valid = {
    // indexes
    i: Number,
    z: Number,

    // position
    u: Number,
    v: Number,
    x: Number,
    y: Number,
    angle: Number,

    // health related
    age: Number,
    life: Number,
    limit: Number,

    // canvas specific
    size: Number,
    context: CanvasRenderingContext2D,

    // aesthetics
    color: String,
    palette: Array,
  }

  for (let param in params) {
    if (params[param].constructor != valid[param]) {
      console.log('Invalid Param', param, param.constructor)
      throw Error(`invalid key => ${param}`)
    }
  }
  return params
}

const require = params => {}

export { validate }
