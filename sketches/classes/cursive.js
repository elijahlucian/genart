import { Random } from 'random-js'

export class FracTree2 {
  constructor({
    context,
    u,
    v,
    color,
    transparency,
    angle,
    baseSize,
    palette,
    life,
    limit,
    rand,
    speed = 4,
    generation = 0,
  }) {
    Object.assign(this, {
      context,
      z: u,
      u,
      v,
      color,
      palette,
      transparency,
      angle,
      baseSize,
      life,
      limit,
      rand,
      speed,
      generation,
    })
    this.startingLife = this.life
    this.branches = []
    this.size = baseSize
    this.dead = false
    this.mature = false
    this.iterations = 0
  }

  branch() {
    let {
      context,
      u,
      v,
      color,
      palette,
      transparency,
      baseSize,
      rand,
      limit,
    } = this
    for (let i = 0; i < 3; i++) {
      if (this.rand.chance(0.2 + this.generation / 75)) return
      let n = this.rand.noise3D(u, v, this.generation)
      let outlier = this.rand.chance(0.01) ? this.rand.range(0.2, 0.2) : 0
      // let spread = this.generation / 100 - this.generation / 200
      let spread = 0
      // if (this.angle + n * 0.1 + outlier + spread > 1.6) continue
      this.branches.push(
        new FracTree2({
          angle: (this.angle += n * 0.1 + outlier + spread),
          context,
          u,
          v,
          color: this.rand.pick(palette),
          transparency,
          baseSize,
          palette,
          life:
            context.canvas.width / 500 +
            (rand.value() * context.canvas.width) / 500 +
            this.generation * 2,
          rand,
          limit,
          generation: (this.generation += 1),
        })
      )
    }
  }

  grow() {
    this.branches.forEach(branch => {
      branch.update()
      branch.draw()
    })
  }

  bloom() {
    // draw some kind of flower thing?

    this.context.fillStyle = this.rand.pick(this.palette) + 'd0'

    for (let i = 0; i < 10; i++) {
      const berrySize = this.rand.value() * 30
      this.context.beginPath()
      const n = this.rand.noise3D(
        this.u * (this.generation + 10),
        this.v * (this.generation + 10),
        i / 10
      )
      this.context.arc(
        this.x + Math.sin(n) * berrySize * 3,
        this.y + Math.cos(n) * berrySize * 3,
        berrySize,
        0,
        6.18
      )
      this.context.fill()
    }
  }

  checkNormalBounds() {}

  update() {
    if (this.dead) return
    // grow the current branch in general direction
    // if reaches EOL, branch out if total agent count is not done.

    if (this.v < 0) {
      this.dead = true //this.v = 0
    }

    if (this.mature) {
      this.grow()
      return
    }

    // if (this.rand.chance((0.01 * this.generation) / 40)) this.bloom()

    this.angle +=
      (0.5 *
        (this.startingLife - this.life) *
        0.005 *
        (this.generation % 2 === 0 ? 1 : -1)) /
      2

    this.life -= 0.1

    if (this.life < 0) {
      this.mature = true
      this.branch()
    }

    let { width, height } = this.context.canvas

    let xRatio = 1 / width
    let yRatio = 1 / height

    this.n = this.rand.noise3D(
      this.u,
      this.v,
      this.life * 0.2 + this.generation / 2
    )
    this.u += Math.sin(this.angle) * xRatio * (this.speed + this.n)
    this.v -= Math.cos(this.angle) * yRatio * (this.speed + this.n)

    this.size =
      this.baseSize +
      2 * this.baseSize * Math.abs(this.n) +
      5 -
      (this.generation / 30) * this.baseSize
    this.x = this.u * width
    this.y = this.v * height
  }

  draw() {
    if (this.mature) return
    // draw shape

    this.context.fillStyle = this.color + '0a'

    this.context.beginPath()
    this.context.arc(this.x, this.y, this.size, 0, Math.PI * 2)

    this.context.fill()
  }
}
