class Img {
  constructor(params) {
    let { src } = params
    this.img = new Image()
    this.img.src = src // '../images/mountains-06.png'
    this.img.onload = () => {
      this.data = this.img
    }
  }

  // get color at coords
  // get pixels ordered by shit
  // get stats
}

class Mouse {
  constructor(params) {
    let events = {
      auxclick:
        'a pointing device button has been pressed (non-primary) - pressed AND released',
      click:
        'a pointing device button (code for primary) - pressed AND released',
      contextmenu: 'The Right Button has been clicked',
      dblclick: 'A Pointing device button is clicked twice on an element',
      mousedown: 'pointing device is pressed',
      mouseenter:
        'A pointing device is moved onto the element that has the listner attached... (shapes?)',
      mouseleave: 'mouse moved off element',
      mouseover: 'moved onto the element or child',
      mouseout: 'pointing device has mvoed off element or child',
      mouseup: 'point device is released over an element',
      select: 'some text is being selected',
      wheel: 'wheel is being rotated.',
    }
    console.log(params)
    this.x = 0
    this.y = 0
    this.pressed = false
    this.context = params.context
    this.onMouseMove = e => {
      if (e.target.localName === 'canvas') {
        this.x = e.clientX - e.toElement.offsetLeft
        this.y = e.clientY - e.toElement.offsetTop
        this.u = this.x / e.srcElement.clientWidth
        this.v = this.y / e.srcElement.clientHeight
      }
      // TODO: Game controls
      // potentially wrong - tests?
      // this.x = clientX - width / 2
      // this.y = clientY - height / 2
      // console.log('Mouse Info:', e)
    }
    this.onMouseDown = event => {
      if (event.button === 0) this.pressed = true

      console.log('Mouse Info:', event)
    }
    window.addEventListener('mousedown', this.onMouseDown)
    window.addEventListener('mousemove', this.onMouseMove)
  }
  isPressed() {
    if (this.pressed) {
      this.pressed = false
      return true
    }
  }
}

class Keyboard {
  constructor(params) {
    this.handleKeyDown = event => {
      // TODO: game controls
      // console.log(event)
      // let time = dt
      // bpm.tap({time})
      // zeroOffset = dt % 1
    }
    window.addEventListener('keydown', this.handleKeyDown)
  }
}

class Controls extends Keyboard {
  constructor() {
    super()
    this.pressed = {}
    this.handleKeyDown = event => {
      if (event.repeat) return
      this.pressed[event.key] = true
    }
    this.handleKeyUp = event => {
      if (event.repeat) return
      this.pressed[event.key] = false
    }
    window.addEventListener('keydown', this.handleKeyDown)
    window.addEventListener('keyup', this.handleKeyUp)
  }
}

export { Img, Mouse, Keyboard, Controls }
