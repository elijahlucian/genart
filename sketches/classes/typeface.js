class Caption {
  constructor(params) {
    let { font, size, caption, color } = params
    this.font = font
    this.size = size // in viewPort height ratio
    this.color = color
    this.caption = caption || 'ELI7VH'
    this.dt = 0
    this.top = params.top || 0
    this.left = params.left || 0
    this.range = params.range || 0.3
  }

  update({time, musicalTime, width, height, top, left, music}) {
    this.time = time
    this.x = width * 0.05 + this.left, //13
    this.y = Math.sin(time) * height * 0.01 + this.top
    this.scaleSize = this.size * 10 + this.range * 1 * Math.sin(-this.time) + 1 + (music || 0.5) * 20
  }

  draw({context}) { 
    context.font = `${this.scaleSize}px "${this.font}"`
    context.save()
    context.filter = `blur(${4 * Math.sin(-this.time) + 5}px)`
    context.fillStyle = '#fff5'
    context.fillText(this.caption, this.x + 5 - Math.sin(-this.time) * 50, this.y + 3 * Math.sin(-this.time) + 6)
    context.restore()
    context.fillStyle = this.color
    context.fillText(this.caption, this.x - Math.sin(-this.time) * 50, this.y)
  }
}

export { Caption }