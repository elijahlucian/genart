class BPM {
  constructor(params) {
    this.timeslices = []
    this.bpm = params.bpm || 60
    this.lastTime = -10
    this.time = 0
    this.dt = 0
    this.tapTempoStarted = false
    this.musicalTime = 0
    this.tap = this.tap.bind(this)
    
    window.addEventListener('keydown', this.tap)
  }

  tap(event) {
    if(this.tapTempoStarted == false) {
      console.log("starting BPM Detection")
      this.tapTempoStarted = true
      this.dt =0
      return
    } else if (this.tapTempoStarted == true) {
      if(this.dt > 2) {
        this.tapTempoStarted = false
        this.timeslices = []
        console.log("Resetting...")
        return
      }
      console.log("TAP:", event.key, this.dt)
      this.timeslices.push(this.dt)
      if(this.timeslices.length > 4) {
        let avg = this.timeslices.reduce((t,v) => t+v) / this.timeslices.length
        this.bpm = this.calculateBPMFromDelta(avg)
        console.log("BPM...", this.bpm, 'AVG...', avg)
        this.musicalTime = 0
      }
    }
    
    this.dt = 0
    return
  }

  update({time}) {
    this.dt += time - this.time
    
    this.time = time
    if(this.musicalTime >= 0) this.musicalTime = this.dt * (this.bpm / 60)
  }

  getMusicalTime() {
    return this.musicalTime
  }

  calculateBPMFromDelta(n) {
    return 60 / n
  }

  reset() {
    this.dt = 0
  }
}

class Rhythm {
  constructor(params) {
    let {u,v} = params

  }

  update({time}) {}
  draw({}) {}

}

class Tempo {
  constructor({bpm}) {
    this.musicalTime = 0
    this.bpm = 120
  }
  
  zero() {
    // set time to 0
  }

  reset() {
    // reset bpm?
  }

  update({time}) {
    this.musicalTime = time * (this.bpm / 120)
  }

  beat() {
    return this.musicalTime % 1
  }

  quarter() {
    return this.musicalTime * 2 % 1
  }

  eighth() {
    return this.musicalTime * 4 % 1
  }

  sixteenth() {
    return this.musicalTime * 8 % 1
  }
}


const musicalTime = ({zeroOffset, time, bpm}) => {
  return zeroOffset + time * (bpm / 120)
}

const tapTempo = (time, delta) => {
  // get keypress time
  let offset = time - delta
  let bpmDelta = offset 
}

export { BPM, Tempo, musicalTime }