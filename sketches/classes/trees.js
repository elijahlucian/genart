const { PI, TAU, vectors } = require('../modules/maf')
const { range } = require('../modules/utils')
const random = require('canvas-sketch-util/random')

// a nifty tree library

class NewTree {
  constructor(params) {
    let { i, u, v, size, color, maxHeight, life } = params
    Object.assign(this, { i, u, v, size, color, maxHeight, life })
    // draw a box over time
    // check for collisions with other boxes
    if (this instanceof NewTree) console.log(this)
  }

  grow() {
    let { u, v, size, color, maxHeight, i } = this
    this.trunk = new Trunk({ u, v, size: size, color, maxHeight, i })
  }

  checkCollisions(shapes) {
    for (let branch of this.trunk.branches) {
      // make sure you are not passing another branch
      // if(collide) branch.kill()
    }
  }

  update({ time, width, height }) {
    if (this.life > 0) {
      this.life -= 0.01
    } else {
      this.kill()
    }

    this.size = this.size
    this.trunk.update({ time, width, height })
    // for (let branch of this.trunk.branches) {
    //   branch.update() // optimize later if needed
    // }
  }

  draw({ context }) {
    this.trunk.draw({ context })
  }

  kill() {
    this.dead = true
  }
}

class Leaf {
  constructor(params) {}
}

class Branch {
  constructor(params) {
    this.thickness = params.thickness
    this.leaves = []
    this.twig = false
    this.points = []
  }
  update({ time, width, height }) {
    for (let i = -1; i <= 1.01; i += 0.04) {
      let u = i * 100
      let v = i
      this.points.push([])
    }
    // console.log('branch!')
    // draw points
  }
  draw({ context }) {}
}

class Limb {}

class Trunk extends NewTree {
  constructor(params) {
    super(params)

    this.branches = []
    this.x = 0
    this.y = 0
    this.points = []
    this.treeHeight = 0
    this.treeWidth = 0.8
    this.nextBranch = 5
    this.angle = params.angle || 0
  }

  branch(params) {
    this.branches.push(new Branch(params))
  }

  update({ time, width, height }) {
    this.branches.forEach(branch => {
      branch.update({ time, width, height })
    })
    this.x = this.u * width
    this.y = this.v * height
    if (this.treeHeight > this.maxHeight) {
      // this.treeWidth += 0.01
    } else {
      this.nextBranch -= 0.1
      if (this.nextBranch < 0) {
        this.branch({
          u: this.u,
          v: this.v,
          angle: random.boolean() ? 270 : 90,
          thickness: this.treeWidth,
        })
        this.nextBranch = 5
      }
      this.treeHeight += 0.05
    }

    this.points = []
    for (let i = -1; i <= 1.01; i += 0.04) {
      let u = (i * this.treeHeight * this.treeWidth) / this.maxHeight
      let v = (Math.abs(i) - 1) * this.treeHeight

      let n = random.noise3D(this.i, u, v)

      let x = u * this.size + n * this.size * 0.12
      let y = v * this.size // - n * this.size * 0.1

      this.points.push([x, y])
    }
  }

  draw({ context }) {
    context.strokeStyle = '#fff'
    context.lineWidth = 14
    context.fillStyle = this.color
    context.save()
    context.translate(this.x, this.y)
    context.rotate((this.angle / 360) * TAU)
    context.beginPath()
    this.points.forEach(point => {
      let [x, y] = point
      context.lineTo(x, y)
    })
    // context.fillRect(0, 0, this.size, this.size)
    context.closePath()
    context.fill()
    context.stroke()
    context.restore()
  }
}

export { NewTree }
