export class Node {
  constructor({ i, u, v, color, size, z, hsl }) {
    Object.assign(this, { i, u, v, color, size, z, hsl })
    this.x = 0
    this.y = 0
  }

  update({ time, width, height, lastShape, noise }) {
    this.lastShape = lastShape
    this.x = this.u * width // + Math.sin(this.i + time + noise) * (50 + 50 * noise)
    this.y = this.v * height // + Math.cos(this.i + time + noise) * (50 + 50 * noise)
  }

  draw({ context, noise }) {
    const { h, s, l } = this.hsl
    // this.color = `hsla(${h}, ${s}%, ${l}%, ${noise + 0.8})`

    context.strokeStyle = this.color || '#fff'
    context.beginPath()
    context.lineWidth = 4 // noise + 1.5
    context.arc(this.x, this.y, this.size * 2, 0, Math.PI * 2)
    if (this.lastShape) {
      context.moveTo(this.x, this.y)
      context.lineTo(this.lastShape.x, this.lastShape.y)
    }
    context.stroke()
  }
}

export class Vertex {}
