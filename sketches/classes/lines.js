const random = require('canvas-sketch-util/random')

class Mountain {
  constructor(params) {
    let {u,v,i,z} = params
    this.bg = params.bg
    this.u = u
    this.v = v
    this.i = i
    this.z = z
    this.x = 0
    this.y = 0
    this.n = 0
    this.segments = []
    this.segment_count = params.segment_count || 50
    this.noise_height = params.noise_height || 1
    this.color = params.color
  }
  
  update({time,width,height}) {
    let t = time * 0.5
    this.segments = []
    
    this.y = this.z * height
    for (let i = 1; i <= this.segment_count; i++) {
      let u = i / this.segment_count
      
      this.n = random.noise3D(u * 10 + time, this.z * 100 - time, t)
      this.n *= Math.abs(this.z - 1)

      this.segments.push({
        x: u * width, 
        y: this.z * height + this.n * this.noise_height
      })
    }
  }

  draw({context,width,height}) {
    let margin = height / 10
    context.save()
    context.strokeStyle = this.bg
    context.fillStyle = this.color
    context.beginPath()

    context.moveTo(-margin,height + margin)
    context.lineTo(-margin,this.y)

    this.segments.forEach(p => {
      context.lineTo(p.x, p.y)
      // add noise in between segments
    })

    context.lineTo(width + margin, this.y)
    context.lineTo(width + margin, height + margin)
    context.closePath()
    context.fill()
    context.stroke()
    context.restore()
  }
}

export { Mountain }