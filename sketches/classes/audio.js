var createAnalyser = require('web-audio-analyser')
var average = require('analyser-frequency-average')

// TODO: Audio Class that handles all the analyzer shit, so it can be called on raw audio source or loaded file.

class AudioLoader {
  constructor(params) {
    let { src, looping, band_count } = params

    this.audioContext = new AudioContext()
    this.peaks = []
    this.band_ranges = []
    this.bands = []
    this.loaded = false
    this.playing = false
    this.looping = false
    this.sensitivity = params.sensitivity || 0.1
    this.band_count = band_count
    this.bands_min = Math.log10(params.bandwidthMin || 20)
    this.bands_max = Math.log10(params.bandwidthMax || 20000)
    this.log_size = (this.bands_max - this.bands_min) / this.band_count
    this.gain = params.gain || 1.0
    
    fetch('../audio/' + src)
    .then(response => {
      return response.arrayBuffer()
    })
    .then(data => {
      this.rawAudioBuffer = data
      this.audioContext.decodeAudioData(data, audioBuffer => {
        // TODO: get left and right channels.
        this.data = audioBuffer.getChannelData(0)
        this.audioBuffer = audioBuffer
      })
    })

    for(let i = 1; i <= this.band_count; i++ ) {
      let band_low = 10 ** (this.bands_min + (i-1) * this.log_size)
      let band_hi = 10 ** (this.bands_min + i * this.log_size)
      this.band_ranges.push([band_low,band_hi])
    }
    // console.log(this.band_ranges)

    // this.tri_bands = {
    //   low: average(this.audioUtil.analyser, this.audioUtil.frequencies(), 50, 350),
    //   mid: average(this.audioUtil.analyser, this.audioUtil.frequencies(), 400, 2000),
    //   high: average(this.audioUtil.analyser, this.audioUtil.frequencies(), 2500, 15000),
    // }

  }

  play() {
    if(this.playing) {
      this.source.disconnect(this.audioContext.destination)
      this.source.stop()
      this.playing = false
    } else {
      this.playing = true
      this.source = this.audioContext.createBufferSource();
      this.source.buffer = this.audioBuffer;
      this.gainNode = this.audioContext.createGain()
      this.gainNode.gain.value = this.gain
      this.source.connect(this.gainNode)
      this.gainNode.connect(this.audioContext.destination)
      this.audioUtil = createAnalyser(this.source, this.audioContext, {mute: true}) 
      this.audioUtil.analyser.fftSize = 512
      this.audioUtil.analyser.smoothingTimeConstant = this.sensitivity // 0 = rage, 0.7 = fun, 2.2 = chill
      console.log("SOURCE", this.source)
      console.log("UTIL", this.audioUtil)

      
      // this.source.loopStart = 1
      // this.source.loopEnd = 2
      this.source.loop = this.looping
      // this.source.playbackRate.value = 1.2
      this.source.connect(this.audioContext.destination);
      this.source.start();
    }
  }

  update(params = {}) {
    let newBands = []

    if(this.audioUtil) {
      this.band_ranges.forEach(band => {
        let [l,h] = band
        newBands.push(average(this.audioUtil.analyser, this.audioUtil.frequencies(), l, h))
      })
      
      this.bands = newBands
    } else {
      this.bands = newBands
    }
  }

  getSample() {
    return this.data[this.index]
  }

  getBand(i) {
    if(i > this.bands.length) {
      return 1
    }
    return this.bands[i]
  }

  draw({context,width,height}) {
    
    context.beginPath()
    this.bands.forEach((band, i) => {
      let u = i / this.bands.length * height
      let v = height / 2
    })
    context.stroke()
    // get all peaks as uv coordinates.
  }
}

class AudioAnalyser {
  constructor() {
    navigator.mediaDevices.getUserMedia( {audio:true})
    .then( stream => {
      this.audioContext = new AudioContext()

      this.mediaStreamSource = this.audioContext.createMediaStreamSource(stream)
      this.mediaStreamSource.connect(this.audioContext.destination)

      this.audioUtil = createAnalyser(this.mediaStreamSource, this.audioContext, {mute: true}) 

      this.audioUtil.analyser.fftSize = 256
      this.audioUtil.analyser.smoothingTimeConstant = 0.7 // 0 = rage, 0.7 = fun, 2.2 = chill
      // this.data = this.audioUtil.analyser
      this.audio = {}
    })
  }
  update(params) {
    this.audio = {

      low: average(this.audioUtil.analyser, this.audioUtil.frequencies(), 50, 350),
      mid: average(this.audioUtil.analyser, this.audioUtil.frequencies(), 400, 2000),
      high: average(this.audioUtil.analyser, this.audioUtil.frequencies(), 2500, 15000),
    }
  }
}

export { 
  AudioAnalyser,
  AudioLoader,
}