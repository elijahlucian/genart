const { TAU, vectors } = require('../modules/maf')
const { range } = require('../modules/utils')
const random = require('canvas-sketch-util/random')

class Circle {
  constructor({
    size,
    i,
    z,
    palette,
    stepMultiplier,
    u = 0.2,
    v = 0.8,
    context,
  }) {
    Object.assign(this, { size, i, z, u, v, context })
    this.vectorIndex = random.pick(range(7))
    // let {u,v} = random.pick(vectors)
    this.color = random.pick(palette) + '66'
    this.stepMultiplier = stepMultiplier || 1
    this.dead = false
    this.randomValues = []
    this.nextChild = 10
    this.child = false
  }

  update({ time, width, height }) {
    if (this.dead) return

    if (this.nextChild > 0) {
      this.nextChild -= 0.1
    } else {
      this.child = true
      this.nextChild = 10
    }

    this.x = this.u * width
    this.y = this.v * height
    let r = random.value() < 0.5
    this.randomValues.push(r)
    this.vectorIndex += r ? 1 : -1

    let { u, v } = vectors[Math.abs(this.vectorIndex) % 7]
    // let { u, v } = random.pick(vectors)
    this.u += (u / 100) * this.stepMultiplier
    this.v += (v / 100) * this.stepMultiplier

    if (this.u <= 0.1 || this.u >= 0.9 || this.v <= 0.1 || this.v >= 0.9) {
      // let trues = this.randomValues.filter(v => v === true)
      // let falses = this.randomValues.filter(v => v === false)
      console.log(`Circle ${this.i} is EL MUERTE`)
      this.dead = true
    }
  }

  draw() {
    if (this.dead) return
    if (this.child) this.child = false
    let n = random.noise2D(this.x, this.y)
    this.context.beginPath()
    this.context.fillStyle = this.color
    this.context.arc(this.x, this.y, this.size * Math.abs(n) * 0.5, 0, TAU)
  }

  fill() {
    this.context.fill()
  }

  stroke() {
    this.context.stroke()
  }
}

export { Circle }
