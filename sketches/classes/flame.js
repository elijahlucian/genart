class Flame {
  constructor({u,v,n,color,width}) {
    this.u = u
    this.v = v
    this.x = u * width
    this.y = v * width
    this.n = n
    this.r = color.r
    this.g = color.g
    this.b = color.b

    this.state = {}
  
    this.move = (t) => {
      this.u = simplex.noise2D(this.u, t)
      this.v = simplex.noise2D(this.v, t)
    }
    this.draw = (context) => {
      context.save()
      context.translate()
      context.lineWidth = 10
      context.strokeStyle = `rgba(
        ${this.r},
        ${this.g},
        ${this.b},
        1,
      )`
      context.moveTo(0,0)
      context.lineTo(
        this.u * 10,
        this.v * 10,
      )
      context.stroke()
      context.restore()
    }
  }
}

export { Flame }