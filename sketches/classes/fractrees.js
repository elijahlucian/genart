export class FracTree2 {
  constructor({
    context,
    u,
    v,
    color,
    transparency,
    angle,
    baseSize,
    palette,
    life,
    limit,
    rand,
    speed = 8,
    generation = 0,
  }) {
    Object.assign(this, {
      context,
      z: u,
      u,
      v,
      color,
      palette,
      transparency,
      angle,
      baseSize,
      life,
      limit,
      rand,
      speed,
      generation,
    })
    this.startingLife = this.life
    this.branches = []
    this.size = baseSize
    this.dead = false
    this.mature = false
    this.iterations = 0
  }

  branch() {
    let {
      context,
      u,
      v,
      color,
      palette,
      transparency,
      baseSize,
      rand,
      limit,
    } = this
    for (let i = 0; i < 3; i++) {
      if (this.rand.chance(0.2 + this.generation / 75)) return

      let n = this.rand.noise3D(u, v, this.generation)
      let outlier = this.rand.chance(0.01) ? this.rand.range(0.2, 0.2) : 0
      let spread = this.generation * 0.2 * (this.rand.boolean() ? 1 : -1)
      // if (this.angle + n * 0.1 + outlier + spread > 1.6) continue
      this.branches.push(
        new FracTree2({
          angle: (this.angle += n * 0.1 + outlier + spread),
          context,
          u,
          v,
          color: this.color, //this.rand.pick(palette),
          transparency,
          baseSize,
          palette,
          life:
            this.startingLife +
            (n * this.startingLife) / 2 -
            this.generation / 10,
          rand,
          limit,
          generation: this.generation + 1,
        })
      )
    }
  }

  grow() {
    this.branches.forEach(branch => {
      branch.update()
      branch.draw()
    })
  }

  bloom() {
    // draw some kind of flower thing?

    this.context.fillStyle = this.rand.pick(this.palette) + 'a0'

    for (let i = 0; i < 10; i++) {
      const berrySize = this.rand.value() * this.baseSize * 10

      const n = this.rand.noise3D(
        this.u * (this.generation + 10),
        this.v * (this.generation + 10),
        i / 10
      )

      this.context.beginPath()
      this.context.arc(
        this.x + Math.sin(n) * berrySize * 3,
        this.y + Math.cos(n) * berrySize * 3,
        berrySize,
        0,
        6.18
      )
      this.context.fill()
    }
  }

  checkNormalBounds() {}

  update() {
    if (this.dead) return
    // grow the current branch in general direction
    // if reaches EOL, branch out if total agent count is not done.

    if (this.v < 0) {
      this.dead = true //this.v = 0
    }

    if (this.mature) {
      this.grow()
      return
    }

    // if (this.rand.chance((0.001 * this.generation) / 40)) this.bloom()

    let { width, height } = this.context.canvas
    this.life -= 0.09

    if (this.life < 0) {
      this.mature = true
      this.branch()
    }

    let xRatio = 1 / width
    let yRatio = 1 / height

    this.n = this.rand.noise3D(
      this.u * 10,
      this.v * 10,
      ((this.startingLife - this.life) * this.generation) / 2
    )
    this.u += Math.sin(this.angle) * xRatio * (this.speed + this.n)
    this.v -= Math.cos(this.angle) * yRatio * (this.speed + this.n)

    this.size =
      this.baseSize +
      4 * this.baseSize * Math.abs(this.n) -
      (this.generation / 20) * this.baseSize

    // this.size = 10

    this.x = this.u * width
    this.y = this.v * height
  }

  draw() {
    if (this.mature) return
    // draw shape

    this.context.fillStyle = this.color + '82'

    // this.context.beginPath()
    this.context.save()
    this.context.translate(this.x, this.y)
    this.context.rotate(this.n)
    this.context.fillRect(0, 0, this.size, this.size)
    this.context.restore()
    // this.context.arc(this.x, this.y, this.size, 0, Math.PI * 2)

    this.context.fill()
  }
}

export class LineTree {
  constructor(params) {
    Object.assign(this, params)
    this.branches = []
    this.generation = this.generation || 1
    this.speed = this.speed || 1
    if (this.generation < this.limit) {
      // add branches
      const nextGen = { ...this }
      nextGen.generation += 1
      this.branches.push(new LineTree(nextGen))
      console.log(this.generation)
    }
  }

  drawBranches() {
    this.branches.forEach(branch => branch.draw())
  }
  updateBranches() {
    this.branches.forEach(branch => branch.update({ parent: this }))
  }

  update({ parent }) {
    const { width, height } = this.context.canvas

    const xSpeed = (1 / width) * this.speed
    const ySpeed = (1 / height) * this.speed

    this.x = this.u * width
    this.y = this.v * height

    // this.angle = time
    this.updateBranches()

    this.lineLength = this.baseSize
    this.size = this.baseSize
  }

  draw() {
    // const { width, height } = this.context.canvas

    // this.context.fillStyle = this.color
    this.context.strokeStyle = this.color //+ '11'
    this.context.save()
    this.context.translate(this.x, this.y)

    this.context.beginPath()
    this.context.moveTo(0, 0)
    this.context.lineTo(
      Math.sin(this.angle) * this.lineLength,
      -Math.cos(this.angle) * this.lineLength
    )
    this.context.stroke()
    this.drawBranches()
    this.context.restore()
  }
}
