export class Blade {
  constructor({ u, v, h, s, color, child, speed }) {
    this.u = u
    this.v = v
    this.h = h
    this.s = s || 1
    this.rate = 1
    this.cut = false
    this.cutAt = h
    this.spawn = false
    this.child = child
    this.color = color || '#fff'
    this.speed = speed
    this.nextGrow = 0
    this.r = 0
  }

  grow(time) {
    if (this.cut) {
      // console.log(time, this.nextGrow)
      if (time > this.nextGrow) {
        this.cut = false
        this.h = this.cutAt
      }
      return
    }

    this.h += 0.0001 + Math.abs(1 + this.speed) * 0.0005
    this.nextGrow = time
  }

  fall() {
    this.v += 0.001 + this.h * 0.01
    this.r += 0.0001 + this.speed * 0.01
  }

  check(other) {
    if (this.cut || this.child) return

    if (Math.abs(this.u - other.u) < 0.003) {
      if (Math.abs(1 - this.h - other.v < 0.003)) {
        // console.log(this.u, other.u, this.h, other.v)
        this.nextGrow += 5 * (1 + this.speed)
        this.cut = true
        this.cutAt = Math.abs(1 - other.v)
        this.spawn = true
        // this.h = Math.abs(1 - other.v)
      }
    }
  }
}
