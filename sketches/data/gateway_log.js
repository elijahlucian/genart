module.exports = {
  text: `2018-11-16 22:23:54 INFO root Loaded configuration from /etc/gateway/gateway.conf.
  2018-11-16 22:23:54 INFO root Loading cable grain levels from /var/lib/gateway/cable_grain_level.dat.
  2018-11-16 22:23:54 INFO root No cable grain levels available.
  2018-11-16 22:23:54 INFO root Loading gateway state from /var/lib/gateway/gateway.dat.
  2018-11-16 22:23:55 INFO PersistentState Recovering gateway state from format v2.
  2018-11-16 22:23:55 INFO root Saving persistence file to ensure it is the most up-to-date format.
  2018-11-16 22:23:58 INFO RadioSearchReactor Radio local addr is 0x618029.
  2018-11-16 22:23:58 INFO RadioSearchReactor Starting
  2018-11-16 22:23:58 DEBUG snaplib.snaplib.PacketSerialProtocol RETRY #21: 1
  2018-11-16 22:23:58 DEBUG snaplib.snaplib.PacketSerialProtocol RETRY #21: 2
  2018-11-16 22:23:59 DEBUG snaplib.snaplib.PacketSerialProtocol RETRY #21: 3
  2018-11-16 22:23:59 DEBUG snaplib.snaplib.PacketSerialProtocol RETRY #21: 4
  2018-11-16 22:23:59 DEBUG snaplib.snaplib.PacketSerialProtocol RETRY #21: 5
  2018-11-16 22:24:00 DEBUG snaplib.snaplib.PacketSerialProtocol RETRY #21: 6
  2018-11-16 22:24:00 INFO RadioSearchReactor Radio bridge addr is 0x058bcb.
  2018-11-16 22:24:00 INFO RadioSearchReactor Radio bridge network ID is 0x1c2c.
  2018-11-16 22:24:00 INFO RadioSearchReactor Radio bridge radio channel is 13.
  2018-11-16 22:24:00 INFO RadioParamQueryReactor Starting
  2018-11-16 22:24:00 DEBUG RadioParamQueryReactor Issuing request for vmstat(4, 4).
  2018-11-16 22:24:01 INFO root FN/FNR firmware and CN script update enabled, loading update configuration.
  2018-11-16 22:24:44 INFO CmdPipeIf Creating command FIFO at /var/run/gateway/cmd_pipe.
  2018-11-16 22:24:44 INFO root Connecting to bridge radio on /dev/ttyS1 at 115200.
  2018-11-16 22:24:44 INFO GatewayReactor [GW 618029] Starting.
  2018-11-16 22:24:44 INFO GatewayReactor [GW 618029] Gateway firmware version 3.18.
  2018-11-16 22:24:44 INFO GwChannelMonitor [GW 618029] Starting channel monitor for radio 058bcb.
  2018-11-16 22:24:44 INFO GwRadioOutIf [GW 618029] Sending request for channel id to 058bcb
  2018-11-16 22:24:44 INFO GatewayReactor [GW 618029] Loading saved CNv1 paired reactor 0545f8
  2018-11-16 22:24:44 INFO GatewayReactor [GW 618029] Loading saved CNv2 paired reactor 0x08a56d
  2018-11-16 22:24:44 INFO GwCnv1Reactor [GW 618029] [CN 0545f8] Starting.
  2018-11-16 22:24:45 INFO GwCnv1Reactor [GW 618029] [CN 0545f8] Started, channel read will be issued at first opportunity.
  2018-11-16 22:24:45 INFO HubMaster [GW 618029] [CN 08a56d] Starting.
  2018-11-16 22:24:45 INFO HubMaster [GW 618029] [CN 08a56d] HubMaster does not support fans or sensors.
  2018-11-16 22:24:45 INFO GwHubReactor [GW 618029] [CN hff0002] Starting.
  2018-11-16 22:24:45 INFO GwCnv2FanReactor [GW 618029] [CN hff0002] [DO 0] Starting reactor; fan is off.
  2018-11-16 22:24:45 INFO MqttOpQueue [GW 618029] [CN hff0002] [DO 0] Skipped persistent message publish qid=1.
  2018-11-16 22:24:45 DEBUG GwCloudOutIf [GW 618029] [CN hff0002] [DO 0] publish fan/update, qid=1 31 bytes 0x0a0e0a0636313830323910ad87bddf0510001a076866663030303220012800 FanUpdate{header {source: "618029", timestamp: 1542407085}, cmdId: 0, nodeId: "hff0002", digitalOutputId: 1, on: false}
  2018-11-16 22:24:45 INFO GwCnv2FanReactor [GW 618029] [CN hff0002] [DO 1] Starting reactor; fan is off.
  2018-11-16 22:24:45 INFO MqttOpQueue [GW 618029] [CN hff0002] [DO 1] Skipped persistent message publish qid=1.
  2018-11-16 22:24:45 DEBUG GwCloudOutIf [GW 618029] [CN hff0002] [DO 1] publish fan/update, qid=2 31 bytes 0x0a0e0a0636313830323910ad87bddf0510011a076866663030303220022800 FanUpdate{header {source: "618029", timestamp: 1542407085}, cmdId: 1, nodeId: "hff0002", digitalOutputId: 2, on: false}
  2018-11-16 22:24:45 INFO GwCnv2SamplerReactor [GW 618029] [CN hff0002] Starting.
  2018-11-16 22:24:45 INFO MqttOpQueue [GW 618029] [CN hff0002] Skipped persistent message publish qid=1.
  2018-11-16 22:24:46 DEBUG GwCloudOutIf [GW 618029] [CN hff0002] publish cables/update, qid=3 67 bytes 0x0a0e0a0636313830323910ad87bddf051207686666303030321a0908fd011003180120001a0808021002180a20011a08080c1001180920011a0908fb01100218012001 CablesUpdate{header {source: "618029", timestamp: 1542407085}, nodeId: "hff0002", cables {cableId: 253, cableType: PP0T, sensorCount: 1, channelNumber: 0}, cables {cableId: 2, cableType: MOISTURE, sensorCount: 10, channelNumber: 1}, cables {cableId: 12, cableType: TEMPERATURE, sensorCount: 9, channelNumber: 1}, cables {cableId: 251, cableType: MOISTURE, sensorCount: 1, channelNumber: 1}}
  2018-11-16 22:24:46 INFO GwCnv2SamplerReactor [GW 618029] [CN hff0002] Deferring channel scan until radio connection.
  2018-11-16 22:24:46 INFO GwCnv2UpdaterReactor [GW 618029] [CN hff0002] Starting.
  2018-11-16 22:24:46 INFO SnapSelCon [GW 618029] [CN hff0002] Starting connector to 128.8.165.109:23513.
  2018-11-16 22:24:46 INFO SnapSelCon [GW 618029] [CN hff0002] Connecting to 128.8.165.109:23513.
  2018-11-16 22:24:46 INFO MqttOpQueue [GW 618029] [CN 08a56d] Skipped persistent message publish qid=1.
  2018-11-16 22:24:46 DEBUG GwCloudOutIf [GW 618029] [CN 08a56d] publish hubs/update, qid=4 41 bytes 0x0a0e0a0636313830323910ae87bddf0512063038613536641a0f0a076866663030303212044e6f6e65 HubsUpdate{header {source: "618029", timestamp: 1542407086}, nodeId: "08a56d", hubs {hubId: "hff0002", firmwareVersion: "None"}}
  2018-11-16 22:24:46 INFO GwCnv2UpdaterReactor [GW 618029] [CN 08a56d] Starting.
  2018-11-16 22:24:46 INFO SnapSelCon [GW 618029] [CN 08a56d] Starting connector to 128.8.165.109:2050.
  2018-11-16 22:24:46 INFO SnapSelCon [GW 618029] [CN 08a56d] Connecting to 128.8.165.109:2050.
  2018-11-16 22:24:46 INFO GwBinRegistryReactor [GW 618029] Restoring saved sensor BmSensorKey(HubAddr(0xff0002), channel=0, cable=253, sensor=1, <BmSensorType.pp0t: 3>).
  2018-11-16 22:24:46 INFO GwBinRegistryReactor [GW 618029] Restoring saved sensor BmSensorKey(HubAddr(0xff0002), channel=1, cable=2, sensor=1, <BmSensorType.moisture: 2>).
  2018-11-16 22:24:46 INFO GwBinRegistryReactor [GW 618029] Restoring saved sensor BmSensorKey(HubAddr(0xff0002), channel=1, cable=2, sensor=2, <BmSensorType.moisture: 2>).
  2018-11-16 22:24:46 INFO GwBinRegistryReactor [GW 618029] Restoring saved sensor BmSensorKey(HubAddr(0xff0002), channel=1, cable=2, sensor=3, <BmSensorType.moisture: 2>).
  2018-11-16 22:24:46 INFO GwBinRegistryReactor [GW 618029] Restoring saved sensor BmSensorKey(HubAddr(0xff0002), channel=1, cable=2, sensor=4, <BmSensorType.moisture: 2>).
  2018-11-16 22:24:46 INFO GwBinRegistryReactor [GW 618029] Restoring saved sensor BmSensorKey(HubAddr(0xff0002), channel=1, cable=2, sensor=5, <BmSensorType.moisture: 2>).
  2018-11-16 22:24:47 INFO GwBinRegistryReactor [GW 618029] Restoring saved sensor BmSensorKey(HubAddr(0xff0002), channel=1, cable=2, sensor=6, <BmSensorType.moisture: 2>).
  2018-11-16 22:24:47 INFO GwBinRegistryReactor [GW 618029] Restoring saved sensor BmSensorKey(HubAddr(0xff0002), channel=1, cable=2, sensor=7, <BmSensorType.moisture: 2>).
  2018-11-16 22:24:47 INFO GwBinRegistryReactor [GW 618029] Restoring saved sensor BmSensorKey(HubAddr(0xff0002), channel=1, cable=2, sensor=8, <BmSensorType.moisture: 2>).
  2018-11-16 22:24:47 INFO GwBinRegistryReactor [GW 618029] Restoring saved sensor BmSensorKey(HubAddr(0xff0002), channel=1, cable=2, sensor=9, <BmSensorType.moisture: 2>).
  2018-11-16 22:24:47 INFO GwBinRegistryReactor [GW 618029] Restoring saved sensor BmSensorKey(HubAddr(0xff0002), channel=1, cable=2, sensor=10, <BmSensorType.moisture: 2>).
  2018-11-16 22:24:47 INFO GwBinRegistryReactor [GW 618029] Restoring saved sensor BmSensorKey(HubAddr(0xff0002), channel=1, cable=12, sensor=1, <BmSensorType.temperature: 1>).
  2018-11-16 22:24:47 INFO GwBinRegistryReactor [GW 618029] Restoring saved sensor BmSensorKey(HubAddr(0xff0002), channel=1, cable=12, sensor=2, <BmSensorType.temperature: 1>).
  2018-11-16 22:24:47 INFO GwBinRegistryReactor [GW 618029] Restoring saved sensor BmSensorKey(HubAddr(0xff0002), channel=1, cable=12, sensor=3, <BmSensorType.temperature: 1>).
  2018-11-16 22:24:47 INFO GwBinRegistryReactor [GW 618029] Restoring saved sensor BmSensorKey(HubAddr(0xff0002), channel=1, cable=12, sensor=4, <BmSensorType.temperature: 1>).
  2018-11-16 22:24:47 INFO GwBinRegistryReactor [GW 618029] Restoring saved sensor BmSensorKey(HubAddr(0xff0002), channel=1, cable=12, sensor=5, <BmSensorType.temperature: 1>).
  2018-11-16 22:24:47 INFO GwBinRegistryReactor [GW 618029] Restoring saved sensor BmSensorKey(HubAddr(0xff0002), channel=1, cable=12, sensor=6, <BmSensorType.temperature: 1>).
  2018-11-16 22:24:47 INFO GwBinRegistryReactor [GW 618029] Restoring saved sensor BmSensorKey(HubAddr(0xff0002), channel=1, cable=12, sensor=7, <BmSensorType.temperature: 1>).
  2018-11-16 22:24:47 INFO GwBinRegistryReactor [GW 618029] Restoring saved sensor BmSensorKey(HubAddr(0xff0002), channel=1, cable=12, sensor=8, <BmSensorType.temperature: 1>).
  2018-11-16 22:24:47 INFO GwBinRegistryReactor [GW 618029] Restoring saved sensor BmSensorKey(HubAddr(0xff0002), channel=1, cable=12, sensor=9, <BmSensorType.temperature: 1>).
  2018-11-16 22:24:47 INFO GwBinRegistryReactor [GW 618029] Restoring saved sensor BmSensorKey(HubAddr(0xff0002), channel=1, cable=251, sensor=1, <BmSensorType.orht: 4>).
  2018-11-16 22:24:47 INFO GwBinRegistryReactor [GW 618029] Restoring saved sensor BmSensorKey(SnapAddr(0x0545f8), channel=1, cable=1, sensor=1, <BmSensorType.temperature: 1>).
  2018-11-16 22:24:47 INFO GwBinRegistryReactor [GW 618029] Restoring saved sensor BmSensorKey(SnapAddr(0x0545f8), channel=1, cable=1, sensor=2, <BmSensorType.temperature: 1>).
  2018-11-16 22:24:47 INFO GwBinRegistryReactor [GW 618029] Restoring saved sensor BmSensorKey(SnapAddr(0x0545f8), channel=1, cable=1, sensor=3, <BmSensorType.temperature: 1>).
  2018-11-16 22:24:47 INFO GwBinRegistryReactor [GW 618029] Restoring saved sensor BmSensorKey(SnapAddr(0x0545f8), channel=1, cable=1, sensor=4, <BmSensorType.temperature: 1>).
  2018-11-16 22:24:47 INFO GwBinRegistryReactor [GW 618029] Restoring saved sensor BmSensorKey(SnapAddr(0x0545f8), channel=1, cable=1, sensor=5, <BmSensorType.temperature: 1>).
  2018-11-16 22:24:47 INFO GwBinRegistryReactor [GW 618029] Restoring saved sensor BmSensorKey(SnapAddr(0x0545f8), channel=1, cable=1, sensor=6, <BmSensorType.temperature: 1>).
  2018-11-16 22:24:47 INFO GwBinRegistryReactor [GW 618029] Restoring saved sensor BmSensorKey(SnapAddr(0x0545f8), channel=1, cable=1, sensor=7, <BmSensorType.temperature: 1>).
  2018-11-16 22:24:47 INFO GwBinRegistryReactor [GW 618029] Restoring saved sensor BmSensorKey(SnapAddr(0x0545f8), channel=1, cable=1, sensor=8, <BmSensorType.temperature: 1>).
  2018-11-16 22:24:47 INFO GwBinRegistryReactor [GW 618029] Restoring saved sensor BmSensorKey(SnapAddr(0x0545f8), channel=1, cable=1, sensor=9, <BmSensorType.temperature: 1>).
  2018-11-16 22:24:47 INFO GwBinRegistryReactor [GW 618029] Restoring saved sensor BmSensorKey(SnapAddr(0x0545f8), channel=1, cable=1, sensor=10, <BmSensorType.temperature: 1>).
  2018-11-16 22:24:47 INFO GwBinRegistryReactor [GW 618029] Restoring saved sensor BmSensorKey(SnapAddr(0x0545f8), channel=6, cable=1, sensor=1, <BmSensorType.moisture: 2>).
  2018-11-16 22:24:47 INFO GwBinRegistryReactor [GW 618029] Restoring saved sensor BmSensorKey(SnapAddr(0x0545f8), channel=6, cable=1, sensor=2, <BmSensorType.moisture: 2>).
  2018-11-16 22:24:47 INFO GwBinRegistryReactor [GW 618029] Restoring saved sensor BmSensorKey(SnapAddr(0x0545f8), channel=6, cable=1, sensor=3, <BmSensorType.moisture: 2>).
  2018-11-16 22:24:47 INFO GwBinRegistryReactor [GW 618029] Restoring saved sensor BmSensorKey(SnapAddr(0x0545f8), channel=6, cable=1, sensor=4, <BmSensorType.moisture: 2>).
  2018-11-16 22:24:47 INFO GwBinRegistryReactor [GW 618029] Restoring saved digital output BmFanKey(HubAddr(0xff0002), dout_id=0).
  2018-11-16 22:24:47 INFO GwBinRegistryReactor [GW 618029] Restoring saved digital output BmFanKey(HubAddr(0xff0002), dout_id=1).
  2018-11-16 22:24:48 INFO MqttSelConn Starting.
  2018-11-16 22:24:48 INFO MqttSelConn Last-will and testament will be published to $SYS/broker/connection/gateway/618029/state after 1800s of inactivity.
  2018-11-16 22:24:48 INFO MqttSelConn Connecting to ra.managegrain.com:8883 with keepalive=1200s.
  2018-11-16 22:24:48 INFO mqtt Starting.
  2018-11-16 22:24:48 INFO mqtt Looking up host ra.managegrain.com:8883.
  2018-11-16 22:24:48 INFO tun Bringing up tun device snap0
  2018-11-16 22:24:48 INFO listener Starting.
  2018-11-16 22:24:48 INFO listener Binding to 128.97.128.41:2050.
  2018-11-16 22:24:48 INFO mqtt Found family=inet sock=sock_stream proto=tcp addr=52.36.95.245:8883 (chosen)
  2018-11-16 22:24:48 INFO mqtt Connecting.
  2018-11-16 22:24:48 INFO mqtt Connected.
  2018-11-16 22:24:49 INFO GwRadioMsgRouter Recieved vmstat from 058bcb, status_code: 4, hi_byte: 4, net_id: 13
  2018-11-16 22:24:49 INFO mqtt Launching message MqttConnect(client_id='gateway.618029', clean_session=False, keep_alive=600s, username=***, password=***, will=MqttWill(topic=$SYS/broker/connection/gateway/618029/state, payload=0x31, retain=False, qos=1)).
  2018-11-16 22:24:49 DEBUG mqtt send 94 bytes 0x105c00044d51545404cc0258000e676174657761792e363138303239002b245359532f62726f6b65722f636f6e6e656374696f6e2f676174657761792f3631383032392f7374617465000131000767617465776179000767617465776179.
  2018-11-16 22:24:49 DEBUG mqtt recv 4 bytes 0x20020100
  2018-11-16 22:24:49 INFO mqtt Received MqttConnack(session_present=True, return_code=<ConnackResult.accepted: 0>).
  2018-11-16 22:24:50 INFO MqttSelConn Connected.
  2018-11-16 22:24:50 DEBUG MqttSelConn Subscribing to gateway/618029/#.
  2018-11-16 22:24:50 INFO MqttOpQueue Skipped ephemeral publish topic=$SYS/broker/connection/gateway/618029/state.
  2018-11-16 22:24:50 INFO MqttOpQueue Skipped ephemeral publish topic=gateway/applicationStatus.
  2018-11-16 22:24:50 INFO MqttOpQueue Skipped ephemeral publish topic=gateway/update.
  2018-11-16 22:24:50 INFO mqtt Launching message MqttSubscribe(packet_id=0, topics=[Topic('gateway/618029/#', max_qos=1)]).
  2018-11-16 22:24:50 DEBUG mqtt send 23 bytes 0x821500000010676174657761792f3631383032392f2301.
  2018-11-16 22:24:50 DEBUG mqtt recv 5 bytes 0x9003000001
  2018-11-16 22:24:50 INFO mqtt Received MqttSuback(packet_id=0, results=[<SubscribeResult.qos1: 1>]).
  2018-11-16 22:24:56 INFO GwRadioMsgRouter [GW 618029] [CN 08a7fb] Received CNv2 beacon 0x0081c308a7fb0101000300010000 from node_addr=0x08a7fb->0x08a7fb.
  2018-11-16 22:24:56 DEBUG GatewayReactor [GW 618029] [CN 08a7fb] Received CNv2 beacon MsgCnBeacon<msg_id=33219, beacon_status=BeaconStatus<node_id=08a7fb, node_type=1, hardware_rev=1, release_ver=3, pre_release_ver=1>> from unpaired node.
  2018-11-16 22:25:11 INFO GwRadioMsgRouter [GW 618029] [CN 0545f8] Received message 0x0205050c from node_addr=0545f8.
  2018-11-16 22:25:11 DEBUG GwCnv1Reactor [GW 618029] [CN 0545f8] Received node checkin message while channel scan from node is overdue.  Commanding channel scan.
  2018-11-16 22:25:11 INFO GwCnv1Reactor [GW 618029] [CN 0545f8] Requesting reading of channel 1.
  2018-11-16 22:25:11 DEBUG GwRadioOutIf [GW 618029] [CN 0545f8] Sending 0x0310000215 to 0x0545f8.
  2018-11-16 22:25:11 INFO GwRadioMsgRouter [GW 618029] [CN 0545f8] Received message 0x010607 from node_addr=0545f8.
  2018-11-16 22:25:11 INFO GwRadioMsgRouter [GW 618029] [CN 0545f8] GATEWAY_WAKEUP_PKT_TYPE received but ignored.
  2018-11-16 22:25:13 INFO GwRadioMsgRouter [GW 618029] [CN 08a6de] Received CNv2 beacon 0x00e97f08a6de0101000100050000 from node_addr=0x08a6de->0x08a6de.
  2018-11-16 22:25:13 DEBUG GatewayReactor [GW 618029] [CN 08a6de] Received CNv2 beacon MsgCnBeacon<msg_id=59775, beacon_status=BeaconStatus<node_id=08a6de, node_type=1, hardware_rev=1, release_ver=1, pre_release_ver=5>> from unpaired node.
  2018-11-16 22:25:13 INFO GwRadioMsgRouter [GW 618029] [CN 0545f8] Received message 0x3510010a01010808d601010608d001010108d601010408d601010a08dc01010208dc01010508d001010708d601010308d601010908d041 from node_addr=0545f8.
  2018-11-16 22:25:13 INFO GwCnv1Reactor [GW 618029] [CN 0545f8] Received sensor readings for channel 1.
  2018-11-16 22:25:13 INFO GwCnv1Reactor [GW 618029] [CN 0545f8] Channel 1 sensor TemperatureSensor(cable_number=1, sensor_number=8, temp=2262).
  2018-11-16 22:25:13 INFO GwCnv1Reactor [GW 618029] [CN 0545f8] Channel 1 sensor TemperatureSensor(cable_number=1, sensor_number=6, temp=2256).
  2018-11-16 22:25:13 INFO GwCnv1Reactor [GW 618029] [CN 0545f8] Channel 1 sensor TemperatureSensor(cable_number=1, sensor_number=1, temp=2262).
  2018-11-16 22:25:14 INFO GwCnv1Reactor [GW 618029] [CN 0545f8] Channel 1 sensor TemperatureSensor(cable_number=1, sensor_number=4, temp=2262).
  2018-11-16 22:25:14 INFO GwCnv1Reactor [GW 618029] [CN 0545f8] Channel 1 sensor TemperatureSensor(cable_number=1, sensor_number=10, temp=2268).
  2018-11-16 22:25:14 INFO GwCnv1Reactor [GW 618029] [CN 0545f8] Channel 1 sensor TemperatureSensor(cable_number=1, sensor_number=2, temp=2268).
  2018-11-16 22:25:14 INFO GwCnv1Reactor [GW 618029] [CN 0545f8] Channel 1 sensor TemperatureSensor(cable_number=1, sensor_number=5, temp=2256).
  2018-11-16 22:25:14 INFO GwCnv1Reactor [GW 618029] [CN 0545f8] Channel 1 sensor TemperatureSensor(cable_number=1, sensor_number=7, temp=2262).
  2018-11-16 22:25:14 INFO GwCnv1Reactor [GW 618029] [CN 0545f8] Channel 1 sensor TemperatureSensor(cable_number=1, sensor_number=3, temp=2262).
  2018-11-16 22:25:14 INFO GwCnv1Reactor [GW 618029] [CN 0545f8] Channel 1 sensor TemperatureSensor(cable_number=1, sensor_number=9, temp=2256).
  2018-11-16 22:25:14 INFO GwRadioMsgRouter [GW 618029] [CN 0545f8] Received message 0x0210ff11 from node_addr=0545f8.
  2018-11-16 22:25:14 INFO GwCnv1Reactor [GW 618029] [CN 0545f8] Received end-of-read sensor bundle.
  2018-11-16 22:25:14 INFO GwCnv1Reactor [GW 618029] [CN 0545f8] Requesting reading of channel 6.
  2018-11-16 22:25:14 DEBUG GwRadioOutIf [GW 618029] [CN 0545f8] Sending 0x0310004053 to 0x0545f8.
  2018-11-16 22:25:14 INFO GwRadioMsgRouter [GW 618029] [CN 0545f8] Received message 0x0611f00df2f2150d from node_addr=0545f8.
  2018-11-16 22:25:14 INFO GwCnv1Reactor [GW 618029] [CN 0545f8] Received hardware status update: battery at 3.570000V, rssi -21dBm.
  2018-11-16 22:25:14 INFO MqttOpQueue [GW 618029] [CN 0545f8] Skipped persistent message publish qid=1.
  2018-11-16 22:25:14 DEBUG GwCloudOutIf [GW 618029] [CN 0545f8] publish node/update, qid=5 45 bytes 0x0a0e0a0636313830323910ca87bddf0512063035343566381a0530302e3233220530302e30342de17a64403015 NodeUpdate{header {source: "618029", timestamp: 1542407114}, nodeId: "0545f8", firmwareVersion: "00.23", cneFirmwareVersion: "00.04", batteryVoltage: 3.57, signalStrength: 21}
  2018-11-16 22:25:14 INFO GwRadioMsgRouter [GW 618029] [CN 0545f8] Received message 0x010607 from node_addr=0545f8.
  2018-11-16 22:25:14 INFO GwRadioMsgRouter [GW 618029] [CN 0545f8] GATEWAY_WAKEUP_PKT_TYPE received but ignored.
  2018-11-16 22:25:22 INFO GwRadioMsgRouter [GW 618029] [CN 0545f8] Received message 0x1f10060402010408e9020602010108f5021802010208e301f202010308e9021040 from node_addr=0545f8.
  2018-11-16 22:25:22 INFO GwCnv1Reactor [GW 618029] [CN 0545f8] Received sensor readings for channel 6.
  2018-11-16 22:25:22 INFO GwCnv1Reactor [GW 618029] [CN 0545f8] Channel 6 sensor MoistureSensor(cable_number=1, sensor_number=4, temp=2281, rh=1638).
  2018-11-16 22:25:22 INFO GwCnv1Reactor [GW 618029] [CN 0545f8] Channel 6 sensor MoistureSensor(cable_number=1, sensor_number=1, temp=2293, rh=1700).
  2018-11-16 22:25:22 INFO GwCnv1Reactor [GW 618029] [CN 0545f8] Channel 6 sensor MoistureSensor(cable_number=1, sensor_number=2, temp=2275, rh=1568).
  2018-11-16 22:25:22 INFO GwCnv1Reactor [GW 618029] [CN 0545f8] Channel 6 sensor MoistureSensor(cable_number=1, sensor_number=3, temp=2281, rh=1672).
  2018-11-16 22:25:22 INFO GwRadioMsgRouter [GW 618029] [CN 0545f8] Received message 0x0210ff11 from node_addr=0545f8.
  2018-11-16 22:25:22 INFO GwCnv1Reactor [GW 618029] [CN 0545f8] Received end-of-read sensor bundle.
  2018-11-16 22:25:22 INFO GwCnv1Reactor [GW 618029] [CN 0545f8] Received all expected sensor samples from node.  Relaying to cloud.
  2018-11-16 22:25:23 INFO GwCnv1Reactor [GW 618029] [CN 0545f8] Sending data from channel scan to cloud.
  2018-11-16 22:25:23 INFO MqttOpQueue [GW 618029] [CN 0545f8] Skipped persistent message publish qid=1.
  2018-11-16 22:25:23 DEBUG GwCloudOutIf [GW 618029] [CN 0545f8] publish temperatureCable/update, qid=6 98 bytes 0x0a0e0a0636313830323910d287bddf051206303534356638180120012a05080110ac232a05080210b8232a05080310ac232a05080410ac232a05080510a0232a05080610a0232a05080710ac232a05080810ac232a05080910a0232a05080a10b823 TemperatureCableUpdate{header {source: "618029", timestamp: 1542407122}, nodeId: "0545f8", cableId: 1, channelNumber: 1, sensors {sensorIndex: 1, temperature: 2262}, sensors {sensorIndex: 2, temperature: 2268}, sensors {sensorIndex: 3, temperature: 2262}, sensors {sensorIndex: 4, temperature: 2262}, sensors {sensorIndex: 5, temperature: 2256}, sensors {sensorIndex: 6, temperature: 2256}, sensors {sensorIndex: 7, temperature: 2262}, sensors {sensorIndex: 8, temperature: 2262}, sensors {sensorIndex: 9, temperature: 2256}, sensors {sensorIndex: 10, temperature: 2268}}
  2018-11-16 22:25:23 INFO MqttOpQueue [GW 618029] [CN 0545f8] Skipped persistent message publish qid=1.
  2018-11-16 22:25:23 DEBUG GwCloudOutIf [GW 618029] [CN 0545f8] publish moistureCable/update, qid=7 68 bytes 0x0a0e0a0636313830323910d287bddf051206303534356638180120062a08080110ea2318a40d2a08080210c62318a00c2a08080310d22318880d2a08080410d22318e60c MoistureCableUpdate{header {source: "618029", timestamp: 1542407122}, nodeId: "0545f8", cableId: 1, channelNumber: 6, sensors {sensorIndex: 1, temperature: 2293, relativeHumidity: 1700}, sensors {sensorIndex: 2, temperature: 2275, relativeHumidity: 1568}, sensors {sensorIndex: 3, temperature: 2281, relativeHumidity: 1672}, sensors {sensorIndex: 4, temperature: 2281, relativeHumidity: 1638}}
  2018-11-16 22:25:23 INFO GwCnv1Reactor [GW 618029] [CN 0545f8] Received data for all sensor(s).
  2018-11-16 22:25:23 INFO GwRadioMsgRouter [GW 618029] [CN 0545f8] Received message 0x0611f00deef21509 from node_addr=0545f8.
  2018-11-16 22:25:23 INFO GwCnv1Reactor [GW 618029] [CN 0545f8] Received hardware status update: battery at 3.566000V, rssi -21dBm.
  2018-11-16 22:25:24 INFO MqttOpQueue [GW 618029] [CN 0545f8] Skipped persistent message publish qid=1.
  2018-11-16 22:25:24 DEBUG GwCloudOutIf [GW 618029] [CN 0545f8] publish node/update, qid=8 45 bytes 0x0a0e0a0636313830323910d487bddf0512063035343566381a0530302e3233220530302e30342d583964403015 NodeUpdate{header {source: "618029", timestamp: 1542407124}, nodeId: "0545f8", firmwareVersion: "00.23", cneFirmwareVersion: "00.04", batteryVoltage: 3.566, signalStrength: 21}
  2018-11-16 22:25:43 INFO GwRadioMsgRouter [GW 618029] [CN 059d6b] Received message 0x0301533990 from node_addr=059d6b.
  2018-11-16 22:25:43 DEBUG GwUnknownCnv1Reactor [GW 618029] [CN 059d6b] Ignoring discover from unpaired node.
  2018-11-16 22:25:48 INFO GwRadioOutIf Sending request for channel id to 058bcb
  2018-11-16 22:25:48 INFO GwRadioMsgRouter Recieved vmstat from 058bcb, status_code: 4, hi_byte: 4, net_id: 13
  2018-11-16 22:25:56 INFO GwRadioMsgRouter [GW 618029] [CN 08a7fb] Received CNv2 beacon 0x0081c408a7fb0101000300010000 from node_addr=0x08a7fb->0x08a7fb.
  2018-11-16 22:25:56 DEBUG GatewayReactor [GW 618029] [CN 08a7fb] Received CNv2 beacon MsgCnBeacon<msg_id=33220, beacon_status=BeaconStatus<node_id=08a7fb, node_type=1, hardware_rev=1, release_ver=3, pre_release_ver=1>> from unpaired node.
  2018-11-16 22:26:13 INFO GwRadioMsgRouter [GW 618029] [CN 08a6de] Received CNv2 beacon 0x00e98008a6de0101000100050000 from node_addr=0x08a6de->0x08a6de.
  2018-11-16 22:26:13 DEBUG GatewayReactor [GW 618029] [CN 08a6de] Received CNv2 beacon MsgCnBeacon<msg_id=59776, beacon_status=BeaconStatus<node_id=08a6de, node_type=1, hardware_rev=1, release_ver=1, pre_release_ver=5>> from unpaired node.
  2018-11-16 22:26:22 INFO GwRadioMsgRouter [GW 618029] [CN 0545f8] Received message 0x0205050c from node_addr=0545f8.
  2018-11-16 22:26:22 DEBUG GwCnv1Reactor [GW 618029] [CN 0545f8] Received node checkin message.  Commanding node to sleep.
  2018-11-16 22:26:22 DEBUG GwRadioOutIf [GW 618029] [CN 0545f8] Sending 0x0827000000000000002f to 0x0545f8.
  2018-11-16 22:26:43 INFO GwRadioMsgRouter [GW 618029] [CN 059d6b] Received message 0x0301533990 from node_addr=059d6b.
  2018-11-16 22:26:44 DEBUG GwUnknownCnv1Reactor [GW 618029] [CN 059d6b] Ignoring discover from unpaired node.
  2018-11-16 22:26:48 INFO GwRadioOutIf Sending request for channel id to 058bcb
  2018-11-16 22:26:48 INFO GwRadioMsgRouter Recieved vmstat from 058bcb, status_code: 4, hi_byte: 4, net_id: 13
  2018-11-16 22:26:53 DEBUG SnapSelCon Write-ready.
  2018-11-16 22:26:53 INFO SnapSelCon Connection failure.
  2018-11-16 22:26:53 INFO GwHubReactor [GW 618029] [CN hff0002] Connection timed out.
  2018-11-16 22:26:53 INFO SnapSelCon [GW 618029] [CN hff0002] Starting connector to 128.8.165.109:23513.
  2018-11-16 22:26:53 INFO SnapSelCon [GW 618029] [CN hff0002] Connecting to 128.8.165.109:23513.
  2018-11-16 22:26:54 DEBUG SnapSelCon Write-ready.
  2018-11-16 22:26:54 INFO SnapSelCon Connection failure.
  2018-11-16 22:26:54 INFO HubMaster [GW 618029] [CN 08a56d] Connection timed out (errno=110).
  2018-11-16 22:26:54 INFO SnapSelCon [GW 618029] [CN 08a56d] Starting connector to 128.8.165.109:2050.
  2018-11-16 22:26:54 INFO SnapSelCon [GW 618029] [CN 08a56d] Connecting to 128.8.165.109:2050.
  2018-11-16 22:26:54 DEBUG SnapSelCon Write-ready.
  2018-11-16 22:26:54 INFO SnapSelCon Connected.
  2018-11-16 22:26:54 INFO GwHubReactor [GW 618029] [CN hff0002] Radio connected.
  2018-11-16 22:26:54 DEBUG GwHubRadioOutMq [GW 618029] [CN hff0002] Sending MsgCnHi<msg_id=0> (3-bytes 0x010000).
  2018-11-16 22:26:54 DEBUG SnapSelCon Write-ready.
  2018-11-16 22:26:54 DEBUG SnapSelCon Sent 5 bytes 0x0003010000.
  2018-11-16 22:26:54 DEBUG SnapSelCon Write-ready.
  2018-11-16 22:26:55 INFO SnapSelCon Connected.
  2018-11-16 22:26:55 INFO HubMaster [GW 618029] [CN 08a56d] Radio connected.
  2018-11-16 22:26:55 DEBUG GwCnv2RadioOutMq [GW 618029] [CN 08a56d] Sending MsgCnHi<msg_id=0> (3-bytes 0x010000).
  2018-11-16 22:26:55 DEBUG SnapSelCon Write-ready.
  2018-11-16 22:26:55 DEBUG SnapSelCon Sent 5 bytes 0x0003010000.
  2018-11-16 22:26:55 DEBUG SnapSelCon Read-ready.
  2018-11-16 22:26:55 DEBUG SnapSelCon Received 23 bytes 0x001502229b00000008a56d0101000300010000deadbeef.
  2018-11-16 22:26:55 INFO SnapSelCon Received 21 byte message 0x02229b00000008a56d0101000300010000deadbeef.
  2018-11-16 22:26:55 INFO HubMaster [GW 618029] [CN 08a56d] Received MsgCnHiOkay<msg_id=8859, request_msg_id=0, err_code=0, beacon_status=BeaconStatus<node_id=08a56d, node_type=1, hardware_rev=1, release_ver=3, pre_release_ver=1>>, bootloader_ver=3735928559.
  2018-11-16 22:26:55 DEBUG GwCnv2RadioOutMq [GW 618029] [CN 08a56d] Sending MsgCnPair<msg_id=1> (9-bytes 0x04000161802908a56d).
  2018-11-16 22:26:55 DEBUG SnapSelCon Write-ready.
  2018-11-16 22:26:55 DEBUG SnapSelCon Sent 11 bytes 0x000904000161802908a56d.
  2018-11-16 22:26:56 DEBUG SnapSelCon Read-ready.
  2018-11-16 22:26:56 DEBUG SnapSelCon Received 23 bytes 0x001502260b000000ff00020201000300020100deadbeef.
  2018-11-16 22:26:56 INFO SnapSelCon Received 21 byte message 0x02260b000000ff00020201000300020100deadbeef.
  2018-11-16 22:26:56 INFO GwHubReactor [GW 618029] [CN hff0002] Received MsgCnHiOkay<msg_id=9739, request_msg_id=0, err_code=0, beacon_status=BeaconStatus<node_id=ff0002, node_type=2, hardware_rev=1, release_ver=3, pre_release_ver=2>>, bootloader_ver=3735928559.
  2018-11-16 22:26:56 DEBUG GwHubRadioOutMq [GW 618029] [CN hff0002] Sending MsgCnPair<msg_id=1> (9-bytes 0x040001618029ff0002).
  2018-11-16 22:26:56 DEBUG SnapSelCon Write-ready.
  2018-11-16 22:26:56 DEBUG SnapSelCon Sent 11 bytes 0x0009040001618029ff0002.
  2018-11-16 22:26:56 DEBUG SnapSelCon Read-ready.
  2018-11-16 22:26:56 DEBUG SnapSelCon Received 8 bytes 0x000606229c0001a1.
  2018-11-16 22:26:56 INFO SnapSelCon Received 6 byte message 0x06229c0001a1.
  2018-11-16 22:26:56 INFO HubMaster [GW 618029] [CN 08a56d] Received MsgCnPairFail<msg_id=8860, req_msg_id=1, err_code=<MsgError.comm_error_already_paired: 161>>; pair okay, sending discover command.
  2018-11-16 22:26:56 DEBUG GwCnv2RadioOutMq [GW 618029] [CN 08a56d] Sending MsgCnDiscover<msg_id=2> (3-bytes 0x100002).
  2018-11-16 22:26:56 DEBUG SnapSelCon Write-ready.
  2018-11-16 22:26:56 DEBUG SnapSelCon Sent 5 bytes 0x0003100002.
  2018-11-16 22:26:56 INFO GwRadioMsgRouter [GW 618029] [CN 08a7fb] Received CNv2 beacon 0x0081c508a7fb0101000300010000 from node_addr=0x08a7fb->0x08a7fb.
  2018-11-16 22:26:57 DEBUG GatewayReactor [GW 618029] [CN 08a7fb] Received CNv2 beacon MsgCnBeacon<msg_id=33221, beacon_status=BeaconStatus<node_id=08a7fb, node_type=1, hardware_rev=1, release_ver=3, pre_release_ver=1>> from unpaired node.
  2018-11-16 22:26:57 DEBUG SnapSelCon Read-ready.
  2018-11-16 22:26:57 DEBUG SnapSelCon Received 8 bytes 0x000605260c000100.
  2018-11-16 22:26:57 INFO SnapSelCon Received 6 byte message 0x05260c000100.
  2018-11-16 22:26:57 INFO GwHubReactor [GW 618029] [CN hff0002] Received MsgCnPairOkay<msg_id=9740, req_msg_id=1, err_code=<MsgError.err_ok: 0>>.
  2018-11-16 22:26:57 INFO GwHubReactor [GW 618029] [CN hff0002] Sending node update
  2018-11-16 22:26:57 INFO MqttOpQueue [GW 618029] [CN hff0002] Skipped persistent message publish qid=1.
  2018-11-16 22:26:57 DEBUG GwCloudOutIf [GW 618029] [CN hff0002] publish node/update, qid=9 65 bytes 0x0a0e0a0636313830323910b188bddf051207686666303030321a1d464e2d332e323b20426f6f742d333733353932383535393b2048572d3122002d000000003000 NodeUpdate{header {source: "618029", timestamp: 1542407217}, nodeId: "hff0002", firmwareVersion: "FN-3.2; Boot-3735928559; HW-1", cneFirmwareVersion: "", batteryVoltage: 0.0, signalStrength: 0}
  2018-11-16 22:26:57 DEBUG GwHubRadioOutMq [GW 618029] [CN hff0002] Sending MsgCnDiscover<msg_id=2> (3-bytes 0x100002).
  2018-11-16 22:26:57 DEBUG SnapSelCon Write-ready.
  2018-11-16 22:26:57 DEBUG SnapSelCon Sent 5 bytes 0x0003100002.
  2018-11-16 22:26:59 DEBUG SnapSelCon Read-ready.
  2018-11-16 22:26:59 DEBUG SnapSelCon Received 21 bytes 0x001311229d0002000000015bd9ff0002001e6a2700.
  2018-11-16 22:26:59 INFO SnapSelCon Received 19 byte message 0x11229d0002000000015bd9ff0002001e6a2700.
  2018-11-16 22:26:59 INFO HubMaster [GW 618029] [CN 08a56d] Received MsgCnDiscoverOkay<msg_id=8861, req_msg_id=2, err_code=0, chan_list=[], dout_ids=[], nat_table=NatTable([NatEntry(23513, HubAddr(0xff0002))]), uptime_s=1993255, reset_src=MsgNodeResetSrc.Low_Voltage_Detect>.
  2018-11-16 22:27:00 INFO MqttOpQueue [GW 618029] [CN 08a56d] Skipped persistent message publish qid=1.
  2018-11-16 22:27:00 DEBUG GwCloudOutIf [GW 618029] [CN 08a56d] publish digitalOutputs/update, qid=10 26 bytes 0x0a0e0a0636313830323910b388bddf0510021a06303861353664 DigitalOutputsUpdate{header {source: "618029", timestamp: 1542407219}, cmdId: 2, nodeId: "08a56d"}
  2018-11-16 22:27:00 DEBUG GwCnv2UpdaterReactor [GW 618029] [CN 08a56d] Radio connected.
  2018-11-16 22:27:06 DEBUG SnapSelCon Read-ready.
  2018-11-16 22:27:06 DEBUG SnapSelCon Received 57 bytes 0x013811260d00020002020001fd010342ec80480000000000000001140c08012840e5ae040000000000000c0401283010af040000000000000c.
  2018-11-16 22:27:06 DEBUG SnapSelCon Read-ready.
  2018-11-16 22:27:06 DEBUG SnapSelCon Received 57 bytes 0x050128a88c6f040000000000000c090128386e6f040000000000000c0301283873ae040000000000000c070128fe8c6f040000000000000c02.
  2018-11-16 22:27:07 DEBUG SnapSelCon Read-ready.
  2018-11-16 22:27:07 DEBUG SnapSelCon Received 57 bytes 0x0128f9686f040000000000000c01012803c9ae040000000000000c06012877846804000000000000020a0242a0000e00000000000000020802.
  2018-11-16 22:27:08 DEBUG SnapSelCon Read-ready.
  2018-11-16 22:27:08 DEBUG SnapSelCon Received 57 bytes 0x42d0dd0d000000000000000201024208040e0000000000000002070242a8b40e0000000000000002060242a8cd0e0000000000000002040242.
  2018-11-16 22:27:08 DEBUG SnapSelCon Read-ready.
  2018-11-16 22:27:08 DEBUG SnapSelCon Received 57 bytes 0x2cea0e000000000000000203024283f30e0000000000000002020242a31f0e000000000000000205024253d60e00000000000000fb010242e7.
  2018-11-16 22:27:09 DEBUG SnapSelCon Read-ready.
  2018-11-16 22:27:09 DEBUG SnapSelCon Received 29 bytes 0x7b480000000000000002090242dfbc0e0000000000000000001e69fb00.
  2018-11-16 22:27:09 INFO SnapSelCon Received 312 byte message 0x11260d00020002020001fd010342ec80480000000000000001140c08012840e5ae040000000000000c0401283010af040000000000000c050128a88c6f040000000000000c090128386e6f040000000000000c0301283873ae040000000000000c070128fe8c6f040000000000000c020128f9686f040000000000000c01012803c9ae040000000000000c06012877846804000000000000020a0242a0000e0000000000000002080242d0dd0d000000000000000201024208040e0000000000000002070242a8b40e0000000000000002060242a8cd0e00000000000000020402422cea0e000000000000000203024283f30e0000000000000002020242a31f0e000000000000000205024253d60e00000000000000fb010242e77b480000000000000002090242dfbc0e0000000000000000001e69fb00.
  2018-11-16 22:27:09 INFO GwHubReactor [GW 618029] [CN hff0002] Received MsgCnDiscoverOkay<msg_id=9741, req_msg_id=2, err_code=0, chan_list=[ActiveChannel<channel_id=0, sensors=[Sensor<cable_number=253, sensor_number=1, sensor_type=SensorType.pressure, rom_id_family_code=66, rom_id_serial_number=ec8048000000, measurement=SensorMeasurement<temperature=0, sub_reading=0>>]>, ActiveChannel<channel_id=1, sensors=[Sensor<cable_number=12, sensor_number=8, sensor_type=SensorType.temperature, rom_id_family_code=40, rom_id_serial_number=40e5ae040000, measurement=SensorMeasurement<temperature=0, sub_reading=0>>, Sensor<cable_number=12, sensor_number=4, sensor_type=SensorType.temperature, rom_id_family_code=40, rom_id_serial_number=3010af040000, measurement=SensorMeasurement<temperature=0, sub_reading=0>>, Sensor<cable_number=12, sensor_number=5, sensor_type=SensorType.temperature, rom_id_family_code=40, rom_id_serial_number=a88c6f040000, measurement=SensorMeasurement<temperature=0, sub_reading=0>>, Sensor<cable_number=12, sensor_number=9, sensor_type=SensorType.temperature, rom_id_family_code=40, rom_id_serial_number=386e6f040000, measurement=SensorMeasurement<temperature=0, sub_reading=0>>, Sensor<cable_number=12, sensor_number=3, sensor_type=SensorType.temperature, rom_id_family_code=40, rom_id_serial_number=3873ae040000, measurement=SensorMeasurement<temperature=0, sub_reading=0>>, Sensor<cable_number=12, sensor_number=7, sensor_type=SensorType.temperature, rom_id_family_code=40, rom_id_serial_number=fe8c6f040000, measurement=SensorMeasurement<temperature=0, sub_reading=0>>, Sensor<cable_number=12, sensor_number=2, sensor_type=SensorType.temperature, rom_id_family_code=40, rom_id_serial_number=f9686f040000, measurement=SensorMeasurement<temperature=0, sub_reading=0>>, Sensor<cable_number=12, sensor_number=1, sensor_type=SensorType.temperature, rom_id_family_code=40, rom_id_serial_number=03c9ae040000, measurement=SensorMeasurement<temperature=0, sub_reading=0>>, Sensor<cable_number=12, sensor_number=6, sensor_type=SensorType.temperature, rom_id_family_code=40, rom_id_serial_number=778468040000, measurement=SensorMeasurement<temperature=0, sub_reading=0>>, Sensor<cable_number=2, sensor_number=10, sensor_type=SensorType.humidity, rom_id_family_code=66, rom_id_serial_number=a0000e000000, measurement=SensorMeasurement<temperature=0, sub_reading=0>>, Sensor<cable_number=2, sensor_number=8, sensor_type=SensorType.humidity, rom_id_family_code=66, rom_id_serial_number=d0dd0d000000, measurement=SensorMeasurement<temperature=0, sub_reading=0>>, Sensor<cable_number=2, sensor_number=1, sensor_type=SensorType.humidity, rom_id_family_code=66, rom_id_serial_number=08040e000000, measurement=SensorMeasurement<temperature=0, sub_reading=0>>, Sensor<cable_number=2, sensor_number=7, sensor_type=SensorType.humidity, rom_id_family_code=66, rom_id_serial_number=a8b40e000000, measurement=SensorMeasurement<temperature=0, sub_reading=0>>, Sensor<cable_number=2, sensor_number=6, sensor_type=SensorType.humidity, rom_id_family_code=66, rom_id_serial_number=a8cd0e000000, measurement=SensorMeasurement<temperature=0, sub_reading=0>>, Sensor<cable_number=2, sensor_number=4, sensor_type=SensorType.humidity, rom_id_family_code=66, rom_id_serial_number=2cea0e000000, measurement=SensorMeasurement<temperature=0, sub_reading=0>>, Sensor<cable_number=2, sensor_number=3, sensor_type=SensorType.humidity, rom_id_family_code=66, rom_id_serial_number=83f30e000000, measurement=SensorMeasurement<temperature=0, sub_reading=0>>, Sensor<cable_number=2, sensor_number=2, sensor_type=SensorType.humidity, rom_id_family_code=66, rom_id_serial_number=a31f0e000000, measurement=SensorMeasurement<temperature=0, sub_reading=0>>, Sensor<cable_number=2, sensor_number=5, sensor_type=SensorType.humidity, rom_id_family_code=66, rom_id_serial_number=53d60e000000, measurement=SensorMeasurement<temperature=0, sub_reading=0>>, Sensor<cable_number=251, sensor_number=1, sensor_type=SensorType.humidity, rom_id_family_code=66, rom_id_serial_number=e77b48000000, measurement=SensorMeasurement<temperature=0, sub_reading=0>>, Sensor<cable_number=2, sensor_number=9, sensor_type=SensorType.humidity, rom_id_family_code=66, rom_id_serial_number=dfbc0e000000, measurement=SensorMeasurement<temperature=0, sub_reading=0>>]>], dout_ids=[0, 1], nat_table=NatTable([]), uptime_s=1993211, reset_src=MsgNodeResetSrc.Low_Voltage_Detect>.
  2018-11-16 22:27:10 INFO MqttOpQueue [GW 618029] [CN hff0002] Skipped persistent message publish qid=1.
  2018-11-16 22:27:10 DEBUG GwCloudOutIf [GW 618029] [CN hff0002] publish digitalOutputs/update, qid=11 31 bytes 0x0a0e0a0636313830323910be88bddf0510031a076866663030303220012002 DigitalOutputsUpdate{header {source: "618029", timestamp: 1542407230}, cmdId: 3, nodeId: "hff0002", digitalOutputIds: 1, digitalOutputIds: 2}
  2018-11-16 22:27:10 INFO GwCnv2SamplerReactor [GW 618029] [CN hff0002] Unchanged SensorKey(channel=0, cable=253, sensor=1, SensorType.pressure).
  2018-11-16 22:27:10 INFO GwCnv2SamplerReactor [GW 618029] [CN hff0002] Unchanged SensorKey(channel=1, cable=2, sensor=1, SensorType.humidity).
  2018-11-16 22:27:10 INFO GwCnv2SamplerReactor [GW 618029] [CN hff0002] Unchanged SensorKey(channel=1, cable=2, sensor=2, SensorType.humidity).
  2018-11-16 22:27:10 INFO GwCnv2SamplerReactor [GW 618029] [CN hff0002] Unchanged SensorKey(channel=1, cable=2, sensor=3, SensorType.humidity).
  2018-11-16 22:27:11 INFO GwCnv2SamplerReactor [GW 618029] [CN hff0002] Unchanged SensorKey(channel=1, cable=2, sensor=4, SensorType.humidity).
  2018-11-16 22:27:11 INFO GwCnv2SamplerReactor [GW 618029] [CN hff0002] Unchanged SensorKey(channel=1, cable=2, sensor=5, SensorType.humidity).
  2018-11-16 22:27:11 INFO GwCnv2SamplerReactor [GW 618029] [CN hff0002] Unchanged SensorKey(channel=1, cable=2, sensor=6, SensorType.humidity).
  2018-11-16 22:27:11 INFO GwCnv2SamplerReactor [GW 618029] [CN hff0002] Unchanged SensorKey(channel=1, cable=2, sensor=7, SensorType.humidity).
  2018-11-16 22:27:11 INFO GwCnv2SamplerReactor [GW 618029] [CN hff0002] Unchanged SensorKey(channel=1, cable=2, sensor=8, SensorType.humidity).
  2018-11-16 22:27:11 INFO GwCnv2SamplerReactor [GW 618029] [CN hff0002] Unchanged SensorKey(channel=1, cable=2, sensor=9, SensorType.humidity).
  2018-11-16 22:27:11 INFO GwCnv2SamplerReactor [GW 618029] [CN hff0002] Unchanged SensorKey(channel=1, cable=2, sensor=10, SensorType.humidity).
  2018-11-16 22:27:11 INFO GwCnv2SamplerReactor [GW 618029] [CN hff0002] Unchanged SensorKey(channel=1, cable=12, sensor=1, SensorType.temperature).
  2018-11-16 22:27:11 INFO GwCnv2SamplerReactor [GW 618029] [CN hff0002] Unchanged SensorKey(channel=1, cable=12, sensor=2, SensorType.temperature).
  2018-11-16 22:27:11 INFO GwCnv2SamplerReactor [GW 618029] [CN hff0002] Unchanged SensorKey(channel=1, cable=12, sensor=3, SensorType.temperature).
  2018-11-16 22:27:11 INFO GwCnv2SamplerReactor [GW 618029] [CN hff0002] Unchanged SensorKey(channel=1, cable=12, sensor=4, SensorType.temperature).
  2018-11-16 22:27:11 INFO GwCnv2SamplerReactor [GW 618029] [CN hff0002] Unchanged SensorKey(channel=1, cable=12, sensor=5, SensorType.temperature).
  2018-11-16 22:27:11 INFO GwCnv2SamplerReactor [GW 618029] [CN hff0002] Unchanged SensorKey(channel=1, cable=12, sensor=6, SensorType.temperature).
  2018-11-16 22:27:11 INFO GwCnv2SamplerReactor [GW 618029] [CN hff0002] Unchanged SensorKey(channel=1, cable=12, sensor=7, SensorType.temperature).
  2018-11-16 22:27:11 INFO GwCnv2SamplerReactor [GW 618029] [CN hff0002] Unchanged SensorKey(channel=1, cable=12, sensor=8, SensorType.temperature).
  2018-11-16 22:27:11 INFO GwCnv2SamplerReactor [GW 618029] [CN hff0002] Unchanged SensorKey(channel=1, cable=12, sensor=9, SensorType.temperature).
  2018-11-16 22:27:11 INFO GwCnv2SamplerReactor [GW 618029] [CN hff0002] Unchanged SensorKey(channel=1, cable=251, sensor=1, SensorType.humidity).
  2018-11-16 22:27:11 DEBUG GwCnv2SamplerReactor [GW 618029] [CN hff0002] Radio connected.
  2018-11-16 22:27:11 INFO GwCnv2SamplerReactor [GW 618029] [CN hff0002] Channel scan active; issuing scan command.
  2018-11-16 22:27:11 DEBUG GwCnv2SamplerRadioOutMq [GW 618029] [CN hff0002] Sending MsgCnReadChannels(msg_id=3, chan_id=0) (4-bytes 0x13000300).
  2018-11-16 22:27:11 DEBUG GwHubRadioOutMq [GW 618029] [CN hff0002] Sending MsgCnReadChannels(msg_id=3, chan_id=0) (4-bytes 0x13000300).
  2018-11-16 22:27:11 DEBUG GwCnv2UpdaterReactor [GW 618029] [CN hff0002] Radio connected.
  2018-11-16 22:27:11 DEBUG GwCnv2FanReactor [GW 618029] [CN hff0002] [DO 0] Radio connected.
  2018-11-16 22:27:11 INFO GwCnv2FanReactor [GW 618029] [CN hff0002] [DO 0] Radio connected; Sending FanOff to node.
  2018-11-16 22:27:11 INFO GwCnv2FanReactor [GW 618029] [CN hff0002] [DO 0] Sending dout_off command.
  2018-11-16 22:27:11 DEBUG FanCnRadioOutMq [GW 618029] [CN hff0002] [DO 0] Sending MsgCnDigitalOutputOff<msg_id=4, dout_id=0> (4-bytes 0x0d000400).
  2018-11-16 22:27:11 DEBUG GwHubRadioOutMq [GW 618029] [CN hff0002] [DO 0] Queuing MsgCnDigitalOutputOff<msg_id=4, dout_id=0> (4-bytes 0x0d000400).
  2018-11-16 22:27:11 DEBUG GwCnv2FanReactor [GW 618029] [CN hff0002] [DO 1] Radio connected.
  2018-11-16 22:27:11 INFO GwCnv2FanReactor [GW 618029] [CN hff0002] [DO 1] Radio connected; Sending FanOff to node.
  2018-11-16 22:27:11 INFO GwCnv2FanReactor [GW 618029] [CN hff0002] [DO 1] Sending dout_off command.
  2018-11-16 22:27:11 DEBUG FanCnRadioOutMq [GW 618029] [CN hff0002] [DO 1] Sending MsgCnDigitalOutputOff<msg_id=5, dout_id=1> (4-bytes 0x0d000501).
  2018-11-16 22:27:11 DEBUG GwHubRadioOutMq [GW 618029] [CN hff0002] [DO 1] Queuing MsgCnDigitalOutputOff<msg_id=5, dout_id=1> (4-bytes 0x0d000501).
  2018-11-16 22:27:12 DEBUG SnapSelCon Write-ready.
  2018-11-16 22:27:12 DEBUG SnapSelCon Sent 6 bytes 0x000413000300.
  2018-11-16 22:27:13 INFO GwRadioMsgRouter [GW 618029] [CN 08a6de] Received CNv2 beacon 0x00e98108a6de0101000100050000 from node_addr=0x08a6de->0x08a6de.
  2018-11-16 22:27:13 DEBUG GatewayReactor [GW 618029] [CN 08a6de] Received CNv2 beacon MsgCnBeacon<msg_id=59777, beacon_status=BeaconStatus<node_id=08a6de, node_type=1, hardware_rev=1, release_ver=1, pre_release_ver=5>> from unpaired node.
  2018-11-16 22:27:17 DEBUG SnapSelCon Read-ready.
  2018-11-16 22:27:17 DEBUG SnapSelCon Received 25 bytes 0x001714260e000300010001fd010342ec804800000008dcfffd.
  2018-11-16 22:27:17 INFO SnapSelCon Received 23 byte message 0x14260e000300010001fd010342ec804800000008dcfffd.
  2018-11-16 22:27:17 INFO GwCnv2SamplerReactor [GW 618029] [CN hff0002] Node acknowledged MsgCnReadChannels(msg_id=3, chan_id=0) with MsgCnReadChannelsOkay<msg_id=9742, request_msg_id=3, err_code=0>, get_chan_list=[ActiveChannel<channel_id=0, sensors=[Sensor<cable_number=253, sensor_number=1, sensor_type=SensorType.pressure, rom_id_family_code=66, rom_id_serial_number=ec8048000000, measurement=SensorMeasurement<temperature=2268, sub_reading=-3>>]>], get_bridge_list=[]>.
  2018-11-16 22:27:17 DEBUG GwCnv2SamplerReactor [GW 618029] [CN hff0002] Channel 0 cable 253 PP0T sensor 1 t=22.68C, p=-7.47Pa.
  2018-11-16 22:27:18 INFO MqttOpQueue [GW 618029] [CN hff0002] Skipped persistent message publish qid=1.
  2018-11-16 22:27:18 DEBUG GwCloudOutIf [GW 618029] [CN hff0002] publish pp0tCable/update, qid=12 48 bytes 0x0a0e0a0636313830323910c588bddf0512076866663030303218fd0120002a10080110b82318fdffffffffffffffff01 Pp0tCableUpdate{header {source: "618029", timestamp: 1542407237}, nodeId: "hff0002", cableId: 253, channelNumber: 0, sensors {sensorIndex: 1, temperature: 2268, pressure: -3}}
  2018-11-16 22:27:18 DEBUG GwHubRadioOutMq [GW 618029] [CN hff0002] Sending MsgCnDigitalOutputOff<msg_id=4, dout_id=0> (4-bytes 0x0d000400).
  2018-11-16 22:27:18 INFO GwCnv2SamplerReactor [GW 618029] [CN hff0002] Scanning next channel 1.
  2018-11-16 22:27:18 DEBUG GwCnv2SamplerRadioOutMq [GW 618029] [CN hff0002] Sending MsgCnReadChannels(msg_id=6, chan_id=1) (4-bytes 0x13000601).
  2018-11-16 22:27:18 DEBUG GwHubRadioOutMq [GW 618029] [CN hff0002] Queuing MsgCnReadChannels(msg_id=6, chan_id=1) (4-bytes 0x13000601).
  2018-11-16 22:27:18 DEBUG SnapSelCon Write-ready.
  2018-11-16 22:27:18 DEBUG SnapSelCon Sent 6 bytes 0x00040d000400.
  2018-11-16 22:27:19 DEBUG SnapSelCon Read-ready.
  2018-11-16 22:27:19 DEBUG SnapSelCon Received 9 bytes 0x00070e260f00040000.
  2018-11-16 22:27:19 INFO SnapSelCon Received 7 byte message 0x0e260f00040000.
  2018-11-16 22:27:19 DEBUG GwCnv2FanReactor [GW 618029] [CN hff0002] [DO 0] Node acknowledged MsgCnDigitalOutputOff<msg_id=4, dout_id=0>.
  2018-11-16 22:27:19 DEBUG GwHubRadioOutMq [GW 618029] [CN hff0002] [DO 0] Sending MsgCnDigitalOutputOff<msg_id=5, dout_id=1> (4-bytes 0x0d000501).
  2018-11-16 22:27:19 DEBUG SnapSelCon Write-ready.
  2018-11-16 22:27:19 DEBUG SnapSelCon Sent 6 bytes 0x00040d000501.
  2018-11-16 22:27:20 DEBUG SnapSelCon Read-ready.
  2018-11-16 22:27:20 DEBUG SnapSelCon Received 9 bytes 0x00070e261000050001.
  2018-11-16 22:27:20 INFO SnapSelCon Received 7 byte message 0x0e261000050001.
  2018-11-16 22:27:20 DEBUG GwCnv2FanReactor [GW 618029] [CN hff0002] [DO 1] Node acknowledged MsgCnDigitalOutputOff<msg_id=5, dout_id=1>.
  2018-11-16 22:27:20 DEBUG GwHubRadioOutMq [GW 618029] [CN hff0002] [DO 1] Sending MsgCnReadChannels(msg_id=6, chan_id=1) (4-bytes 0x13000601).
  2018-11-16 22:27:20 DEBUG SnapSelCon Write-ready.
  2018-11-16 22:27:20 DEBUG SnapSelCon Sent 6 bytes 0x000413000601.
  2018-11-16 22:27:24 INFO GwRadioMsgRouter [GW 618029] [CN 0545f8] Received message 0x0205050c from node_addr=0545f8.
  2018-11-16 22:27:24 DEBUG GwCnv1Reactor [GW 618029] [CN 0545f8] Received node checkin message.  Commanding node to sleep.
  2018-11-16 22:27:24 DEBUG GwRadioOutIf [GW 618029] [CN 0545f8] Sending 0x0827000000000000002f to 0x0545f8.
  2018-11-16 22:27:44 INFO GwRadioMsgRouter [GW 618029] [CN 059d6b] Received message 0x0301533990 from node_addr=059d6b.
  2018-11-16 22:27:44 DEBUG GwUnknownCnv1Reactor [GW 618029] [CN 059d6b] Ignoring discover from unpaired node.
  2018-11-16 22:27:48 INFO GwRadioOutIf Sending request for channel id to 058bcb
  2018-11-16 22:27:48 INFO GwRadioMsgRouter Recieved vmstat from 058bcb, status_code: 4, hi_byte: 4, net_id: 13
  2018-11-16 22:27:56 INFO GwRadioMsgRouter [GW 618029] [CN 08a7fb] Received CNv2 beacon 0x0081c608a7fb0101000300010000 from node_addr=0x08a7fb->0x08a7fb.
  2018-11-16 22:27:56 DEBUG GatewayReactor [GW 618029] [CN 08a7fb] Received CNv2 beacon MsgCnBeacon<msg_id=33222, beacon_status=BeaconStatus<node_id=08a7fb, node_type=1, hardware_rev=1, release_ver=3, pre_release_ver=1>> from unpaired node.
  2018-11-16 22:28:01 DEBUG SnapSelCon Read-ready.
  2018-11-16 22:28:01 DEBUG SnapSelCon Received 57 bytes 0x01211426110006000101140c08012840e5ae04000008d600000c0401283010af04000008d600000c050128a88c6f04000008d000000c090128.
  2018-11-16 22:28:01 DEBUG SnapSelCon Read-ready.
  2018-11-16 22:28:01 DEBUG SnapSelCon Received 57 bytes 0x386e6f04000008d000000c0301283873ae04000008d600000c070128fe8c6f04000008d000000c020128f9686f04000008ca00000c01012803.
  2018-11-16 22:28:02 DEBUG SnapSelCon Read-ready.
  2018-11-16 22:28:02 DEBUG SnapSelCon Received 57 bytes 0xc9ae04000008d600000c06012877846804000008d00000020a0242a0000e00000008e5064702080242d0dd0d00000008ec065c020102420804.
  2018-11-16 22:28:02 DEBUG SnapSelCon Read-ready.
  2018-11-16 22:28:02 DEBUG SnapSelCon Received 57 bytes 0x0e00000008f5070402070242a8b40e00000008f2071f02060242a8cd0e00000008ce0688020402422cea0e00000008e006690203024283f30e.
  2018-11-16 22:28:03 DEBUG SnapSelCon Read-ready.
  2018-11-16 22:28:03 DEBUG SnapSelCon Received 57 bytes 0x00000008e206bd02020242a31f0e00000008d706810205024253d60e00000008d30669fb010242e77b4800000008d3066c02090242dfbc0e00.
  2018-11-16 22:28:04 DEBUG SnapSelCon Read-ready.
  2018-11-16 22:28:04 DEBUG SnapSelCon Received 6 bytes 0x000008ee06b0.
  2018-11-16 22:28:04 INFO SnapSelCon Received 289 byte message 0x1426110006000101140c08012840e5ae04000008d600000c0401283010af04000008d600000c050128a88c6f04000008d000000c090128386e6f04000008d000000c0301283873ae04000008d600000c070128fe8c6f04000008d000000c020128f9686f04000008ca00000c01012803c9ae04000008d600000c06012877846804000008d00000020a0242a0000e00000008e5064702080242d0dd0d00000008ec065c0201024208040e00000008f5070402070242a8b40e00000008f2071f02060242a8cd0e00000008ce0688020402422cea0e00000008e006690203024283f30e00000008e206bd02020242a31f0e00000008d706810205024253d60e00000008d30669fb010242e77b4800000008d3066c02090242dfbc0e00000008ee06b0.
  2018-11-16 22:28:04 INFO GwCnv2SamplerReactor [GW 618029] [CN hff0002] Node acknowledged MsgCnReadChannels(msg_id=6, chan_id=1) with MsgCnReadChannelsOkay<msg_id=9745, request_msg_id=6, err_code=0>, get_chan_list=[ActiveChannel<channel_id=1, sensors=[Sensor<cable_number=12, sensor_number=8, sensor_type=SensorType.temperature, rom_id_family_code=40, rom_id_serial_number=40e5ae040000, measurement=SensorMeasurement<temperature=2262, sub_reading=0>>, Sensor<cable_number=12, sensor_number=4, sensor_type=SensorType.temperature, rom_id_family_code=40, rom_id_serial_number=3010af040000, measurement=SensorMeasurement<temperature=2262, sub_reading=0>>, Sensor<cable_number=12, sensor_number=5, sensor_type=SensorType.temperature, rom_id_family_code=40, rom_id_serial_number=a88c6f040000, measurement=SensorMeasurement<temperature=2256, sub_reading=0>>, Sensor<cable_number=12, sensor_number=9, sensor_type=SensorType.temperature, rom_id_family_code=40, rom_id_serial_number=386e6f040000, measurement=SensorMeasurement<temperature=2256, sub_reading=0>>, Sensor<cable_number=12, sensor_number=3, sensor_type=SensorType.temperature, rom_id_family_code=40, rom_id_serial_number=3873ae040000, measurement=SensorMeasurement<temperature=2262, sub_reading=0>>, Sensor<cable_number=12, sensor_number=7, sensor_type=SensorType.temperature, rom_id_family_code=40, rom_id_serial_number=fe8c6f040000, measurement=SensorMeasurement<temperature=2256, sub_reading=0>>, Sensor<cable_number=12, sensor_number=2, sensor_type=SensorType.temperature, rom_id_family_code=40, rom_id_serial_number=f9686f040000, measurement=SensorMeasurement<temperature=2250, sub_reading=0>>, Sensor<cable_number=12, sensor_number=1, sensor_type=SensorType.temperature, rom_id_family_code=40, rom_id_serial_number=03c9ae040000, measurement=SensorMeasurement<temperature=2262, sub_reading=0>>, Sensor<cable_number=12, sensor_number=6, sensor_type=SensorType.temperature, rom_id_family_code=40, rom_id_serial_number=778468040000, measurement=SensorMeasurement<temperature=2256, sub_reading=0>>, Sensor<cable_number=2, sensor_number=10, sensor_type=SensorType.humidity, rom_id_family_code=66, rom_id_serial_number=a0000e000000, measurement=SensorMeasurement<temperature=2277, sub_reading=1607>>, Sensor<cable_number=2, sensor_number=8, sensor_type=SensorType.humidity, rom_id_family_code=66, rom_id_serial_number=d0dd0d000000, measurement=SensorMeasurement<temperature=2284, sub_reading=1628>>, Sensor<cable_number=2, sensor_number=1, sensor_type=SensorType.humidity, rom_id_family_code=66, rom_id_serial_number=08040e000000, measurement=SensorMeasurement<temperature=2293, sub_reading=1796>>, Sensor<cable_number=2, sensor_number=7, sensor_type=SensorType.humidity, rom_id_family_code=66, rom_id_serial_number=a8b40e000000, measurement=SensorMeasurement<temperature=2290, sub_reading=1823>>, Sensor<cable_number=2, sensor_number=6, sensor_type=SensorType.humidity, rom_id_family_code=66, rom_id_serial_number=a8cd0e000000, measurement=SensorMeasurement<temperature=2254, sub_reading=1672>>, Sensor<cable_number=2, sensor_number=4, sensor_type=SensorType.humidity, rom_id_family_code=66, rom_id_serial_number=2cea0e000000, measurement=SensorMeasurement<temperature=2272, sub_reading=1641>>, Sensor<cable_number=2, sensor_number=3, sensor_type=SensorType.humidity, rom_id_family_code=66, rom_id_serial_number=83f30e000000, measurement=SensorMeasurement<temperature=2274, sub_reading=1725>>, Sensor<cable_number=2, sensor_number=2, sensor_type=SensorType.humidity, rom_id_family_code=66, rom_id_serial_number=a31f0e000000, measurement=SensorMeasurement<temperature=2263, sub_reading=1665>>, Sensor<cable_number=2, sensor_number=5, sensor_type=SensorType.humidity, rom_id_family_code=66, rom_id_serial_number=53d60e000000, measurement=SensorMeasurement<temperature=2259, sub_reading=1641>>, Sensor<cable_number=251, sensor_number=1, sensor_type=SensorType.humidity, rom_id_family_code=66, rom_id_serial_number=e77b48000000, measurement=SensorMeasurement<temperature=2259, sub_reading=1644>>, Sensor<cable_number=2, sensor_number=9, sensor_type=SensorType.humidity, rom_id_family_code=66, rom_id_serial_number=dfbc0e000000, measurement=SensorMeasurement<temperature=2286, sub_reading=1712>>]>], get_bridge_list=[]>.
  2018-11-16 22:28:04 DEBUG GwCnv2SamplerReactor [GW 618029] [CN hff0002] Channel 1 cable 2 moisture sensor 1 t=22.93C, h=0.1796.
  2018-11-16 22:28:04 DEBUG GwCnv2SamplerReactor [GW 618029] [CN hff0002] Channel 1 cable 2 moisture sensor 2 t=22.63C, h=0.1665.
  2018-11-16 22:28:04 DEBUG GwCnv2SamplerReactor [GW 618029] [CN hff0002] Channel 1 cable 2 moisture sensor 3 t=22.74C, h=0.1725.
  2018-11-16 22:28:04 DEBUG GwCnv2SamplerReactor [GW 618029] [CN hff0002] Channel 1 cable 2 moisture sensor 4 t=22.72C, h=0.1641.
  2018-11-16 22:28:04 DEBUG GwCnv2SamplerReactor [GW 618029] [CN hff0002] Channel 1 cable 2 moisture sensor 5 t=22.59C, h=0.1641.
  2018-11-16 22:28:04 DEBUG GwCnv2SamplerReactor [GW 618029] [CN hff0002] Channel 1 cable 2 moisture sensor 6 t=22.54C, h=0.1672.
  2018-11-16 22:28:04 DEBUG GwCnv2SamplerReactor [GW 618029] [CN hff0002] Channel 1 cable 2 moisture sensor 7 t=22.90C, h=0.1823.
  2018-11-16 22:28:04 DEBUG GwCnv2SamplerReactor [GW 618029] [CN hff0002] Channel 1 cable 2 moisture sensor 8 t=22.84C, h=0.1628.
  2018-11-16 22:28:04 DEBUG GwCnv2SamplerReactor [GW 618029] [CN hff0002] Channel 1 cable 2 moisture sensor 9 t=22.86C, h=0.1712.
  2018-11-16 22:28:04 DEBUG GwCnv2SamplerReactor [GW 618029] [CN hff0002] Channel 1 cable 2 moisture sensor 10 t=22.77C, h=0.1607.
  2018-11-16 22:28:04 DEBUG GwCnv2SamplerReactor [GW 618029] [CN hff0002] Channel 1 cable 12 temperature sensor 1 t=22.62C.
  2018-11-16 22:28:04 DEBUG GwCnv2SamplerReactor [GW 618029] [CN hff0002] Channel 1 cable 12 temperature sensor 2 t=22.50C.
  2018-11-16 22:28:04 DEBUG GwCnv2SamplerReactor [GW 618029] [CN hff0002] Channel 1 cable 12 temperature sensor 3 t=22.62C.
  2018-11-16 22:28:04 DEBUG GwCnv2SamplerReactor [GW 618029] [CN hff0002] Channel 1 cable 12 temperature sensor 4 t=22.62C.
  2018-11-16 22:28:04 DEBUG GwCnv2SamplerReactor [GW 618029] [CN hff0002] Channel 1 cable 12 temperature sensor 5 t=22.56C.
  2018-11-16 22:28:05 DEBUG GwCnv2SamplerReactor [GW 618029] [CN hff0002] Channel 1 cable 12 temperature sensor 6 t=22.56C.
  2018-11-16 22:28:05 DEBUG GwCnv2SamplerReactor [GW 618029] [CN hff0002] Channel 1 cable 12 temperature sensor 7 t=22.56C.
  2018-11-16 22:28:05 DEBUG GwCnv2SamplerReactor [GW 618029] [CN hff0002] Channel 1 cable 12 temperature sensor 8 t=22.62C.
  2018-11-16 22:28:05 DEBUG GwCnv2SamplerReactor [GW 618029] [CN hff0002] Channel 1 cable 12 temperature sensor 9 t=22.56C.
  2018-11-16 22:28:05 DEBUG GwCnv2SamplerReactor [GW 618029] [CN hff0002] Channel 1 cable 251 ORHT sensor 1 t=22.59C, h=0.1644.
  2018-11-16 22:28:05 INFO MqttOpQueue [GW 618029] [CN hff0002] Skipped persistent message publish qid=1.
  2018-11-16 22:28:05 DEBUG GwCloudOutIf [GW 618029] [CN hff0002] publish temperatureCable/update, qid=13 92 bytes 0x0a0e0a0636313830323910f488bddf05120768666630303032180c20012a05080110ac232a0508021094232a05080310ac232a05080410ac232a05080510a0232a05080610a0232a05080710a0232a05080810ac232a05080910a023 TemperatureCableUpdate{header {source: "618029", timestamp: 1542407284}, nodeId: "hff0002", cableId: 12, channelNumber: 1, sensors {sensorIndex: 1, temperature: 2262}, sensors {sensorIndex: 2, temperature: 2250}, sensors {sensorIndex: 3, temperature: 2262}, sensors {sensorIndex: 4, temperature: 2262}, sensors {sensorIndex: 5, temperature: 2256}, sensors {sensorIndex: 6, temperature: 2256}, sensors {sensorIndex: 7, temperature: 2256}, sensors {sensorIndex: 8, temperature: 2262}, sensors {sensorIndex: 9, temperature: 2256}}
  2018-11-16 22:28:06 INFO MqttOpQueue [GW 618029] [CN hff0002] Skipped persistent message publish qid=1.
  2018-11-16 22:28:06 DEBUG GwCloudOutIf [GW 618029] [CN hff0002] publish moistureCable/update, qid=14 39 bytes 0x0a0e0a0636313830323910f488bddf05120768666630303032180220012a08080910dc2318b00d MoistureCableUpdate{header {source: "618029", timestamp: 1542407284}, nodeId: "hff0002", cableId: 2, channelNumber: 1, sensors {sensorIndex: 9, temperature: 2286, relativeHumidity: 1712}}
  2018-11-16 22:28:06 INFO MqttOpQueue [GW 618029] [CN hff0002] Skipped persistent message publish qid=1.
  2018-11-16 22:28:06 DEBUG GwCloudOutIf [GW 618029] [CN hff0002] publish moistureCable/update, qid=15 40 bytes 0x0a0e0a0636313830323910f488bddf0512076866663030303218fb0120012a08080110a62318ec0c MoistureCableUpdate{header {source: "618029", timestamp: 1542407284}, nodeId: "hff0002", cableId: 251, channelNumber: 1, sensors {sensorIndex: 1, temperature: 2259, relativeHumidity: 1644}}
  2018-11-16 22:28:06 INFO GwCnv2SamplerReactor [GW 618029] [CN hff0002] Channel scan complete.
  2018-11-16 22:28:13 INFO GwRadioMsgRouter [GW 618029] [CN 08a6de] Received CNv2 beacon 0x00e98208a6de0101000100050000 from node_addr=0x08a6de->0x08a6de.
  2018-11-16 22:28:13 DEBUG GatewayReactor [GW 618029] [CN 08a6de] Received CNv2 beacon MsgCnBeacon<msg_id=59778, beacon_status=BeaconStatus<node_id=08a6de, node_type=1, hardware_rev=1, release_ver=1, pre_release_ver=5>> from unpaired node.
  2018-11-16 22:28:26 INFO GwRadioMsgRouter [GW 618029] [CN 0545f8] Received message 0x0205050c from node_addr=0545f8.
  2018-11-16 22:28:26 DEBUG GwCnv1Reactor [GW 618029] [CN 0545f8] Received node checkin message.  Commanding node to sleep.
  2018-11-16 22:28:26 DEBUG GwRadioOutIf [GW 618029] [CN 0545f8] Sending 0x0827000000000000002f to 0x0545f8.
  2018-11-16 22:28:45 INFO GwRadioMsgRouter [GW 618029] [CN 059d6b] Received message 0x0301533990 from node_addr=059d6b.
  2018-11-16 22:28:45 DEBUG GwUnknownCnv1Reactor [GW 618029] [CN 059d6b] Ignoring discover from unpaired node.`
}