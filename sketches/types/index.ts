type SketchParams = {
  time: number
  context: CanvasRenderingContext2D
  width: number
  height: number
}

module.exports = { SketchParams }
