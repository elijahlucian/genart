global.THREE = require('three');

const physical = (params) => new THREE.MeshPhysicalMaterial({
  // let { color } = params
  color: params.color,
  roughness: 0.75,
  flatShading: true,
  // wireframe: true,
})

const toon = new THREE.MeshToonMaterial({
  color: '#ffffff'
})

const basic = (params) => new THREE.MeshBasicMaterial(params)

export { physical, toon, basic }