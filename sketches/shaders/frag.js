const glslify = require('glslify')

const fragmentShader = glslify(/* glsl */`
#pragma glslify: hsl2rgb = require('glsl-hsl2rgb');
varying vec2 vUv;
uniform float playhead;
void main () {
	// number of horizontal bands
	float bands = 12.0;
	// offset texture by loop time
	float offset = playhead;
	// get a 0..1 value from this
	float y = mod(offset + vUv.y, 1.0);
	// get N discrete steps of hue
	float hue = floor(y * bands) / bands;
	// now get a color
	float sat = 0.55;
	float light = 0.6;
	vec3 color = hsl2rgb(hue, sat, light);
	gl_FragColor = vec4(color, 1.0);
}
`);

module.exports = { fragmentShader }