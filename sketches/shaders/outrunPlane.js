const glslify = require('glslify')

const vert = glslify(/* glsl */`
  uniform float n;
  uniform float i;
  uniform float f;
  uniform float time;

  varying vec2 vUv;
  varying float index;
  varying float vDepth;
  varying vec2 b;

  #pragma glslify: noise = require('glsl-noise/simplex/4d')
  void main() {
    vDepth = 1.0;
    float speed = 1.;
    float t = time * speed;
    
    // b = barycentric;

    vUv = uv;
    mat4 p = projectionMatrix;
    mat4 m = modelViewMatrix;

    vec3 pos = position.xyz;
    pos += normal * sin(vUv.y + time); // trippy

    gl_Position = p * m * vec4(pos, 1.);

    // vDepth = step(mod((gl_Position.z - 22.) + time, 1.), 0.2);
  }
`)

const frag = glslify(/* glsl */`
uniform float time;
  uniform float n;
  uniform float i;
  
  varying vec2 vUv;
  varying float index;
  varying float vDepth;
  varying vec2 b;

  #pragma glslify: noise = require('glsl-noise/simplex/3d')
  #pragma glslify: hsl2rgb = require('glsl-hsl2rgb');

  #define PI 3.1415926535897932384626433832795
  #define TAU (2.*PI)

  float scanlines(float time, float uv, float frequency) {
    return step(mod(uv * frequency + time * 0.5, 1.), 0.4) * 0.2;
  }

  void main() {
    float pi = 3.14159265359;
    float tau = 2. * pi;
    float speed = 0.1;
    float t = time * speed;
    vec3 color = vec3(
      1.,
      1.,
      1.
    );
    
    color = hsl2rgb(1., 1., 1.);

    float ground = step(mod(10. * vUv.x + time, 1.), 0.08);
    // if(ground > 1.) { 
    ground +=  smoothstep(0.125,0.1,mod(40. * vUv.y, 1.));

    if(vUv.x > 0.985) 
      {
        ground = 1.0;
      }
    if(vUv.x < 0.007){
        ground = 1.0;
      }

    // }
    
    color *= ground;
    
    color.z *= 0.9;
    color.x *= 0.2;
    
    color -= scanlines(time * 15., vUv.x, 120.) * 0.2;
    
    vec3 rgb = vec3(0.5);
    gl_FragColor = vec4(color, 1.);

  }
`)

export { vert, frag }