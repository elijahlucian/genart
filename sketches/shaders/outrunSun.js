const glslify = require('glslify')
const { scanlines } = require('../shaders/functions')
const { pitau } = require('../shaders/definitions')

const vert = glslify(/* glsl */`
  #define PI 3.1415926535897932384626433832795
  #define TAU (2.*PI)

  uniform float time;

  varying vec2 vUv;

  #pragma glslify: noise = require('glsl-noise/classic/4d')

  void main() {
    vUv = uv;
    mat4 p = projectionMatrix;
    mat4 m = modelViewMatrix;

    vec3 pos = position.xyz;

    float heatwaves = sin(position.y * PI * 16. + time * TAU) * 0.005 * abs(position.y - 1.);

    pos.x > 0.5 ? pos.x += heatwaves : pos.x -= heatwaves;

    gl_Position = p * m * vec4(pos, 1.);
  }
`)

const frag = glslify(/* glsl */`
  #define PI 3.1415926535897932384626433832795
  #define TAU (2.*PI)

  uniform float time;
  varying vec2 vUv;

  #pragma glslify: noise = require('glsl-noise/simplex/3d')
  #pragma glslify: hsl2rgb = require('glsl-hsl2rgb');

  float scanlines(float time, vec2 uv, float frequency) {
    return step(mod(uv.y * frequency + time * 0.5, 1.), 0.4) * 0.2;
  }

  void main() {
    vec3 color = vec3(1.0);
    float t = time * 0.1;

    float sun = step(mod(vUv.y * 18. + time, 1.), -0.3 + 0.35 * vUv.y * 3.7);
    float sunstreaks = scanlines(time * PI, vUv, 130.);
    color.x *= sun;
    color.y *= sun * (0.6 + vUv.y - 0.7 );
    color += sunstreaks * color;
    color.z = 0.;
    color -= smoothstep(0.3, 0.8, abs(vUv.x * PI * mod(t, 1.) - 1.) * 2.) * 0.15;

    gl_FragColor = vec4(color, 1.);
  }
`)

export { vert, frag }