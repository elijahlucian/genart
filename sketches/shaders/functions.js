const scanlines = `float scanlines(float time, vec2 uv, float frequency) {
  return step(mod(uv.y * frequency + time * 0.5, 1.), 0.4) * 0.2;
}`

export { scanlines }