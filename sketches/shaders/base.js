const glslify = require('glslify')

const vert = glslify(/* glsl */`
  #define PI 3.1415926535897932384626433832795
  #define TAU (2.*PI)

  uniform float time;

  varying vec2 vUv;

  #pragma glslify: noise = require('glsl-noise/simplex/4d')

  void main() {
    vUv = uv;
    mat4 p = projectionMatrix;
    mat4 m = modelViewMatrix;

    vec3 pos = position.xyz;

    gl_Position = p * m * vec4(pos, 1.);
  }
`)

const frag = glslify(/* glsl */`
  #define PI 3.1415926535897932384626433832795
  #define TAU (2.*PI)

  uniform float time;
  varying vUv;

  #pragma glslify: noise = require('glsl-noise/simplex/3d')
  #pragma glslify: hsl2rgb = require('glsl-hsl2rgb');

  void main() {

    gl_FragColor = vec4(color, 1.);
  }
`)

export { vert, frag }