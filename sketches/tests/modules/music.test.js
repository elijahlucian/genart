const { calculateBPMFromDelta } = require('../../modules/music')

const musicTests = () => {
  inputOutputs = [
    {i: 0.125,o: 480},
    {i: 0.25,o: 240},
    {i: 0.5, o: 120},
    {i: 1, o: 60},
    {i: 2, o: 30},
    {i: 4, o: 15},
    {i: 8, o: 7.5},
    {i: 16, o: 3.75},
  ]

  inputOutputs.forEach(({i,o}) => {
    test(`calculateBPMFromDelta - input: ${i} should produce ${o}`, () => {
      expect(calculateBPMFromDelta(i)).toBe(o)
    })
  })
}

musicTests()