const myPalletes = {
  tambo: [
    '#75cfbd',
    '#ffaf87',
    '#ff8e72',
    '#ed6a5e',
    '#377771',
  ],
  tambo2: [
    '000000',
    '6ec1b1',
    '75cfbd',
    '7ddbc8',
    'ffffff',
  ],
  rain: [
    '#75cfbd',
    '#595959',
    '#7f7f7f',
    '#cccccc',
    '#f2f2f2',
  ],
  chill: [
    '#75cfbd',
    '#b0a1ba',
    '#abc8c7',
    '#b8e2c8',
    '#bff0d4',
  ]
}

module.exports = myPalletes