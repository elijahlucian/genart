const dopeSeeds = {
  camo: 703161,
  cowabunga: 877532,
  blood: 320116,
  dijon: 227398,
  candy: 358275,
  sun: 595838,
  vineyard: 639653,
}

module.exports = dopeSeeds