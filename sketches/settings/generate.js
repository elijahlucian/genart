// general.js

// TODO: make various confis
	
	// animated
	// static

	// instagram
	// logo
	// threejs
	// shaders


const general = {
	suffix: `__seed_${random.getSeed()}`,
}

// ANIMATION

const animated = {
	animate: true,	
}

const looping = {
	fps: 30,
}

// DIMENSIONS

const print = {
	dimensions: 'a4',
	dimensions: [8.5,11],
	dimensions: [11,17],
	units: 'in',
  pixelsPerInch: 300,
}

const instagram = {
  // dimensions: [ 1024, 1024 ], // instagram
  dimensions: [ 2048, 2048 ], // instagram
  // dimensions: [ 4096, 4096 ], // instagram
  // dimensions: [ 8192, 8192 ], // instagram
}

const desktop = {
	  dimensions: [1920,1080], // desktop
}