import canvasSketch from 'canvas-sketch'
import random from 'canvas-sketch-util/random'

import BezierEasing from 'bezier-easing'
import { palettes } from '../modules/nice-color-palettes'

import { getLongEdgeFromArray } from '../modules/utils'
import { BPM } from '../classes/music'
import { rgbToHSL } from '../modules/color'

import { Node, Vertex } from '../classes/graph'
import { Mouse, Keyboard, Controls } from '../classes/utils'
import { Caption } from '../classes/typeface'
// GLOBALS
var zeroOffset = 0
var dt = 0

const bpm = new BPM({
  bpm: 105,
})

const bez = new BezierEasing(0.22, 0.92, 0.73, 0.63)

let seed = {
  palette: 1571354515027,
  // determined: 'dank',
  random: Date.now(), //random.getRandomSeed()
}

random.setSeed(seed.palette || seed.defined || seed.random)
console.log('Palette Seed =>', random.getSeed())

const palette = random.pick(palettes)
const bg = palette.shift()
const mainColor = random.pick(palette)

random.setSeed(seed.determined || seed.random)
console.log('Algo Seed =>', random.getSeed())

document.body.style.backgroundColor = bg
document.title = 'JS Sketches'

// const canvasSize = 2048
const settings = {
  suffix: `__seed_${random.getSeed()}`,
  // dimensions: [canvasSize,canvasSize],
  // dimensions: dimensions.square.insta,
  // orientation: 'landscape',
  // units: 'in'
  // pixelsPerInch: 300,
  animate: true,
}

// TODO: WEBCAM
// TODO: Image Loader

const sketch = params => {
  console.log('SKETCH PARAMS =>', params)
  let { context, width, height } = params

  context.lineCap = 'square' // || "round" || "square" || "butt"
  context.lineJoin = 'square' // || "bevel" || "miter" || "square"

  const longEdge = getLongEdgeFromArray([width, height])

  const config = {
    speed: 0.7,
    agent_count: 10,
    agent_limit: 100,
    trails: true,
    margin: longEdge / 20,
    lineWidth: longEdge / 150,
    shapeSize: longEdge / 80,
    bandwidthMin: 120,
    bandwidthMax: 10000,
  }

  var mouse = new Mouse({ width, height })
  var controls = new Controls()
  // var analyser = new AudioAnalyser() // live audio from mic source
  var caption = new Caption({
    font: 'Inconsolata',
    size: 6,
    top: height * 0.95,
    left: width * 0.01,
    // caption: 'just some drums and keys',
    color: random.pick(palette),
  })

  // instantiate classes here
  // plan agent count for band count.

  let hsl = rgbToHSL(mainColor)

  console.log(hsl)
  var nodes = []
  console.log('MAINCOLOR', mainColor)

  for (let i = 0; i < config.agent_count; i++) {
    const z = i / config.agent_count - 1
    const { h, s, l } = hsl
    const noise = (random.noise1D(z) + 1.5) / 3
    const color = `hsla(${h},${s}%,${l}%,${noise})`
    const size = noise * config.shapeSize

    nodes.push(
      new Node({
        i,
        u: random.value(),
        v: random.value(),
        hsl,
        color,
        size,
        z,
      })
    )
  }

  nodes.forEach((node, i) => {
    const { u, v } = node
    // compare all other nodes distance
  })

  context.fillStyle = bg // + (config.trails ? '33' : 'ff')
  context.fillRect(0, 0, width, height)

  caption.update({ time: 0, width, height })
  caption.draw({ context })

  return ({ time, context, width, height }) => {
    bpm.update({ time })

    context.fillStyle = bg // + (config.trails ? '33' : 'ff')
    context.fillRect(0, 0, width, height)

    caption.update({ time, width, height })

    dt = time

    nodes.forEach((shape, i) => {
      let lastShape = nodes[i - 1]
      const noise = random.noise2D(time, shape.z)
      shape.update({ time, width, height, lastShape, noise })
      shape.draw({ context, noise })
    })
    caption.draw({ context })
  }
}

canvasSketch(sketch, settings)
