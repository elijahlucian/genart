global.THREE = require('three')

const clock = new THREE.Clock({ autostart: true })

const canvasSketch = require('canvas-sketch')
const utils = require('../modules/utils')
const Simplex = require('simplex-noise')
const simplex = new Simplex()
const glslify = require('glslify')
const Bezier = require('bezier-easing')
const palettes = require('nice-color-palettes')
const random = require('canvas-sketch-util/random')
let palette = random.pick(palettes)

const materials = require('../three-basics/materials')

backgroundColor = palette.shift()

const sun = require('../shaders/outrunSun')
const outrunPlane = require('../shaders/outrunPlane')

const bez = Bezier(0.11, 0.73, 0.32, 0.92)

document.body.style.backgroundColor = 'black'

// require('three/examples/js/controls/OrbitControls')

const settings = {
  animate: true,
  dimensions: [2048, 2048],
  context: 'webgl',
  attributes: { antialias: true },
}

const sketch = ({ context }) => {
  const renderer = new THREE.WebGLRenderer({
    context,
  })

  renderer.setClearColor(backgroundColor, 1)
  // renderer.setClearColor('#000', 1)
  const camera = new THREE.PerspectiveCamera(45, 1, 0.01, 1000)
  camera.position.set(-0, 18, -50)
  camera.lookAt(new THREE.Vector3())

  // // const controls = new THREE.OrbitControls(camera)
  const scene = new THREE.Scene()

  const sphereSize = 1

  const sphereGeo = new THREE.SphereGeometry(sphereSize, 2, 2)
  const sphereGeoTemp = new THREE.SphereGeometry(sphereSize, 2, 2)

  let spheres = []

  scene.add(new THREE.AmbientLight('hsla(20,80%,50%,1)'))
  const light = new THREE.PointLight('#45caf7', 1, 15.5)
  light.position.set(2, -0.5, -8).multiplyScalar(1.2)
  scene.add(light)

  rotationIndex = 0
  count = 0

  return {
    resize({ pixelRatio, viewportWidth, viewportHeight }) {
      renderer.setPixelRatio(pixelRatio)
      renderer.setSize(viewportWidth, viewportHeight)
      camera.aspect = viewportWidth / viewportHeight
      camera.updateProjectionMatrix()
    },

    render({ time }) {
      // controls.update()
      renderer.render(scene, camera)
    },

    unload() {
      // controls.dispose()
      renderer.dispose()
    },
  }
}

canvasSketch(sketch, settings)
