const canvasSketch = require('canvas-sketch')
const glslify = require('glslify')

const { fragmentShader } = require('../../shaders/frag')
const { vertexShader } = require('../../shaders/vert')

// Ensure ThreeJS is in global scope for the 'examples/'
global.THREE = require('three')

// Include any additional ThreeJS examples below
// require('three/examples/js/controls/OrbitControls');

const settings = {
  // Make the loop animated
  dimensions: [800, 800],
  animate: true,
  fps: 24,
  duration: 6,
  // Get a WebGL canvas rather than 2D
  context: 'webgl',
  // playbackRate: 'throttle',
  // Turn on MSAA
  attributes: { antialias: true },
}

const sketch = ({ context }) => {
  // Create a renderer
  const renderer = new THREE.WebGLRenderer({
    context,
  })

  // WebGL background color
  renderer.setClearColor('#000', 1)

  // Setup a camera
  const camera = new THREE.PerspectiveCamera(45, 1, 0.01, 100)
  camera.position.set(0, 0, -6)
  camera.lookAt(new THREE.Vector3())

  // Setup camera controller
  // // const controls = new THREE.OrbitControls(camera);

  // Setup your scene
  const scene = new THREE.Scene()

  const meshCount = 1
  const meshOffset = meshCount / 2

  const meshes = []

  for (var i = 0; i <= meshCount - 1; i++) {
    // for (var i = meshCount - meshOffset; i < meshOffset; i++) {

    const mesh = new THREE.Mesh(
      new THREE.BoxGeometry(1, 1, 1),
      new THREE.ShaderMaterial({
        fragmentShader,
        vertexShader,
        uniforms: {
          playhead: { value: i },
        },
      }),
    )

    absI = Math.abs(i)

    mesh.i = i
    mesh.offset = i / 50
    mesh.absI = absI

    mesh.vector = {
      x: (0.5 - Math.random()) / 100,
      y: (0.5 - Math.random()) / 100,
    }

    mesh.rotation.x = i / 10
    scene.add(mesh)
    meshes.push(mesh)
  }

  console.log(meshes)

  // Specify an ambient/unlit colour
  scene.add(new THREE.AmbientLight('#59314f'))

  // Add some light
  const light = new THREE.PointLight('#45caf7', 1, 15.5)
  light.position.set(2, 2, -4).multiplyScalar(1.5)

  scene.add(light)

  // draw each frame
  return {
    // Handle resize events here
    resize({ pixelRatio, viewportWidth, viewportHeight }) {
      renderer.setPixelRatio(pixelRatio)
      renderer.setSize(viewportWidth, viewportHeight)
      camera.aspect = viewportWidth / viewportHeight
      camera.updateProjectionMatrix()
    },
    // Update & render your scene here
    render({ playhead }) {
      meshes.forEach((mesh) => {
        const sin = Math.sin(mesh.i + (playhead * Math.PI) / 3)
        mesh.rotation.y = 5 * playhead
        mesh.rotation.x = 3 * playhead
        mesh.position.x = 500 * mesh.vector.x * sin
        mesh.position.y = 500 * mesh.vector.y * sin

        mesh.material.uniforms.playhead.value = playhead + mesh.i
      })

      light.position.set(
        Math.sin(playhead * Math.PI) * 2,
        Math.cos(playhead * Math.PI) * 3,
        Math.sin(playhead * Math.PI) * 2,
      )

      // controls.update();
      renderer.render(scene, camera)
    },
    // Dispose of events & renderer for cleaner hot-reloading
    unload() {
      // controls.dispose();
      renderer.dispose()
    },
  }
}

canvasSketch(sketch, settings)
