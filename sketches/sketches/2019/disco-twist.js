const canvasSketch = require('canvas-sketch');
const BezierEasing = require('bezier-easing');
const Simplex = require('simplex-noise')
const { PI, TAU } = require('../../modules/maf')

const bez = new BezierEasing(.22,.92,.73,.63)

const settings = {
  dimensions: [ 12000, 12000 ],
  // dimensions: [ 1024, 1024 ],
  // animate: true,
  fps: 30,
};

document.body.style.backgroundColor = '#000000'  

const sketch = ({width, height}) => {
  const canvas = document.createElement('canvas')
  
  return ({ time, context, width, height }) => {
    // context.fillStyle = '#000000';
    // context.fillRect(0, 0, width, height);
    
    const margin = 200
    const w = (width) / 2 - margin
    const h = (height) / 2
    const speed = 0.5

    time = PI

    const t = time * speed
    
    context.lineCap = "round" // || "round" || "square" || "butt"
    context.lineJoin = "round" // || "bevel" || "miter" || "square"
    
    context.strokeStyle = '#fff'
    context.lineWidth = width / 40
    let dt = t % 1
    
    let lineResolution = 0.001
    let start = false
    let lastX
    let lastY
    for (let u = 0; u <= 1.01; u+=lineResolution) {
      
      context.beginPath()
      let x = Math.sin(time * 0.5 + u * TAU) * width / 3.5 + width / 2 + Math.sin(time + u * TAU * 20) * (width / 8) * Math.sin(t * 0.1 * PI)
      let y = Math.cos(t + u * TAU) * height / 3.5 + height / 2 + Math.cos(time * 2 + u * TAU * 10) * (width / 8) * Math.cos(t * 0.1 * PI)
      if(start) {
        context.moveTo(x,y)
        lastX = x
        lastY = y
        start = false
      } else {
        context.strokeStyle = `hsl(${(-time * TAU * 50 + u * 360) % 360}, 100%, 50%)`
        context.moveTo(lastX,lastY)
        context.lineTo(x,y)
        lastX = x 
        lastY = y
        context.stroke()
      }
    }
    context.closePath()
  };  
};

canvasSketch(sketch, settings);