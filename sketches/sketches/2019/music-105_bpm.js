const canvasSketch = require('canvas-sketch');
const random = require('canvas-sketch-util/random')

const BezierEasing = require('bezier-easing');
const { palettes }  = require('../../modules/nice-color-palettes')
const { paletteSeeds } = require('../../seeds/palettes')

const utils = require('../../modules/utils')
const maf = require('../../modules/maf')
const { BPM, Tempo, musicalTime } = require('../../classes/music')
const { dimensions } = require('../../modules/dimensions')

const { BaseShape } = require('../../classes/shapes')
const { Img, Mouse, Keyboard, Controls } = require('../../classes/utils')
const { AudioAnalyser, AudioLoader } = require('../../classes/audio')
const { Caption } = require('../../classes/typeface')
// GLOBALS
var zeroOffset = 0
var dt = 0

const bpm = new BPM(105)
const bez = new BezierEasing(.22,.92,.73,.63)

let seed = {
  // palette: '314962',
  // determined: 'dank',
  random: random.getRandomSeed()
}

let img = new Img({src: '../images/summer-nights.jpg'})

// paletteSeed
random.setSeed(seed.palette || seed.defined || seed.random)
console.log("Palette Seed =>", random.getSeed())

const palette = random.pick(palettes)
const bg = palette.shift()

// algoSeed
random.setSeed(seed.determined || seed.random)
console.log("Algo Seed =>", random.getSeed())

document.body.style.backgroundColor = bg
document.title = "JS Sketches"

// const canvasSize = 2048
const settings = {
  suffix: `__seed_${random.getSeed()}`,
  // dimensions: [canvasSize,canvasSize],
  dimensions: dimensions.desktop.hd,
  // orientation: 'landscape',
  // units: 'in'
  // pixelsPerInch: 300,
  animate: true,
};

// TODO: WEBCAM
// TODO: Image Loader

const sketch = ({width, height}) => {
  const longEdge = utils.getLongEdgeFromArray([width,height])

  const config = {
    speed: 0.7,
    agent_count: 15,
    agent_limit: 100,
    trails: true,
    margin: longEdge / 20,
    lineWidth: longEdge / 150,
    shapeSize: longEdge / 200,
    bandwidthMin: 360,
    bandwidthMax: 10000,
    audio: {
      gain: -1
    }
  }

  var mouse = new Mouse({width,height})
  var controls = new Controls()
  // var analyser = new AudioAnalyser()

  var title = new Caption({
    font: 'Sheppaloe', 
    size: 16,
    top: 100,
    left: 200,
    caption: 'Summer Nights',
    color: random.pick(palette),
  })
  
  var artistName = new Caption({
    font: 'Cocogoose', 
    size: 5.3,
    top: 500,
    left: 2000
    caption: 'ELIJAH LUCIAN & DAVID OCEAN',
    color: random.pick(palette),
  })
  
  var music = new AudioLoader({
    src: 'summer nights mix.wav',
    looping: false,
    band_count: config.agent_count,
    sensitivity: 0.2,
    bandwidthMin: config.bandwidthMin,
    bandwidthMax: config.bandwidthMax,
    gain: config.audio.gain,
  })
  
  // var analyser = new AudioAnalyser()

  // instantiate classes here 
  // plan agent count for band count.

  shapes = []

  for (let i = 0; i < config.agent_count; i++) {
    let z = i / config.agent_count - 1
    shapes.push(
      new BaseShape({
        i,
        z,
        color: random.pick(palette),
        baseSize: config.shapeSize,
        stroke: false,
        palette,
      })
    )
  }

  return ({ time, context, width, height }) => {
    // if(!analyser.audioUtil) return
    if(!music.data) return
    if(!img.data) return

    // make this more atomic and shit    
    let mt
    if(bpm) {
     mt = musicalTime({
        zeroOffset,
        time,
        bpm: bpm.bpm
      })
    } else { mt = time }
    let b = mt % 1
    let q = mt * 2 % 1

    let beats = {b,q}

    music.update()
    // analyser.update()
    if(mouse.isPressed()) music.play()
    

    dt = time
    // context.fillStyle = bg + (config.trails ? '13' : 'ff')
    // context.fillRect(0, 0, width, height);
    
    const t = time * config.speed
    
    context.lineCap = "square" // || "round" || "square" || "butt"
    context.lineJoin = "square" // || "bevel" || "miter" || "square"
    context.strokeStyle = '#fff2'
    context.lineWidth = config.lineWidth

    title.update({time, width, height, music})
    artistName.update({time,width,height, music})

    shapes.forEach((shape, i) => {
      shape.update({
        time: mt, 
        width, 
        height, 
        music: music.getBand(i), 
        beats
      })
      // shape.draw({context})
    });
    title.draw({context})
    artistName.draw({context})
  };  
};

canvasSketch(sketch, settings);