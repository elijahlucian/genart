const canvasSketch = require('canvas-sketch');
const random = require('canvas-sketch-util/random')

const BezierEasing = require('bezier-easing');
const { palettes }  = require('../../modules/nice-color-palettes')
const { paletteSeeds } = require('../../seeds/palettes')

const utils = require('../../modules/utils')
const { rgbToHSL } = require('../../modules/color')
const maf = require('../../modules/maf')
const { BPM, Tempo, musicalTime } = require('../../classes/music')
const { dimensions } = require('../../modules/dimensions')

const { Vine } = require('../../classes/shapes')
const { Img, Mouse, Keyboard, Controls } = require('../../classes/utils')
const { AudioAnalyser, AudioLoader } = require('../../classes/audio')
const { Caption } = require('../../classes/typeface')
// GLOBALS
var zeroOffset = 0
var dt = 0

const bpm = new BPM({
  bpm: 105,
})

const bez = new BezierEasing(.22,.92,.73,.63)

let seed = {
  // palette: '314962',
  // determined: 'dank',
  random: Date.now(), //random.getRandomSeed()
}

// let img = new Img({src: '../images/mountains-06.png'})

// paletteSeed
random.setSeed(seed.palette || seed.defined || seed.random)
console.log("Palette Seed =>", random.getSeed())

const palette = random.pick(palettes)
const bg = palette.shift()
const mainColor = random.pick(palette)

// algoSeed
random.setSeed(seed.determined || seed.random)
console.log("Algo Seed =>", random.getSeed())

document.body.style.backgroundColor = bg
document.title = "JS Sketches"

// const canvasSize = 2048
const settings = {
  suffix: `__seed_${random.getSeed()}`,
  // dimensions: [canvasSize,canvasSize],
  dimensions: dimensions.square.insta,
  // orientation: 'landscape',
  // units: 'in'
  // pixelsPerInch: 300,
  animate: true,
};

// TODO: WEBCAM
// TODO: Image Loader


const sketch = (params) => {
  console.log("SKETCH PARAMS =>",params)

  let { context, width, height } = params

  context.lineCap = "square" // || "round" || "square" || "butt"
  context.lineJoin = "square" // || "bevel" || "miter" || "square"

  const longEdge = utils.getLongEdgeFromArray([width,height])
  const shortEdge = utils.getShortEdgeFromArray([width,height])

  const config = {
    speed: 0.7,
    agent_count: 5,
    agent_limit: 1000,
    trails: true,
    margin: shortEdge / 20,
    lineWidth: shortEdge / 100,
    shapeSize: shortEdge / 80,
    bandwidthMin: 120,
    bandwidthMax: 10000,
    audio: {
      gain: -0.8
    }
  }

  var mouse = new Mouse({width,height})
  var controls = new Controls()
  // var analyser = new AudioAnalyser() // live audio from mic source
  var caption = new Caption({
    font: 'Inconsolata', 
    size: 6,
    // caption: 'just some drums and keys',
    color: random.pick(palette),
  })

  let hsl = rgbToHSL(mainColor)
  
  console.log(hsl)
  var shapes = []
  console.log("MAINCOLOR", mainColor)

  for (let i = 0; i < config.agent_count; i++) {
    let z = i / config.agent_count - 1
    shapes.push(
      new Vine({
        i,
        z,
        u: 0.618 + (random.noise1D(z) * 0.5),
        v: 1,
        angle: 0,
        color: random.pick(palette),
        baseSize: config.shapeSize,
        lineWidth: config.lineWidth,
        segments: 20,
        life: 30,
      })
    )
  }
  
  caption.update({time: 0, width, height})
  caption.draw({context})
  
  return ({ time, context, width, height }) => {
    context.fillStyle = bg + '44' //(config.trails ? '33' : 'ff')
    context.fillRect(0, 0, width, height);
    bpm.update({time})

    let {b,q} = bpm.getMusicalTime()
    let beats = {b,q}

    caption.update({time, width, height})

    dt = time
    
    const t = time * config.speed
    
    shapes.forEach((shape, i) => {
      if(shape.v < 0) return
      if(shape.dead) return
      shape.update({
        time: time - i * 0.2,
        width, 
        height, 
        beats
      })
      shape.draw({context})
      if(shape.spawn) {
        let { u, v, angle, baseSize, i } = shape
        let angle_delta = 12.5
        if(shapes.length > config.agent_limit) { 
          shape.kill()
          return
        }
        for (let index = -1; index <= 1; index+=2) {
          if(shapes.length > 25) { if(random.chance(0.2)) continue }
          else if(shapes.length > 30) { if(random.chance(0.25)) continue }
          else if(shapes.length > 55) { if(random.chance(0.55)) continue }
          else if(shapes.length > 80) { if(random.chance(0.85)) continue }
          else if(shapes.length > 120) { if(random.chance(0.99)) continue }
          if(index===1) {
            if(random.boolean()) continue
          }
          let hardAngle = 0 // (random.chance(0.15) ? 45 : 0)
          shapes.push(new FracTree({
            u, 
            v, 
            angle: (hardAngle + angle + angle_delta) * index,
            color: mainColor, //random.pick(palette),
            baseSize: baseSize * 0.75,
            index: shapes.length,
            i: i + 1,
            life: shape.og_hp * random.range(0.85,0.95),
          }))  
        }
        shape.kill()
      }
    });
    // caption.draw({context})
  };  
};

canvasSketch(sketch, settings);