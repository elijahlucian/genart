
const canvasSketch = require('canvas-sketch');
const random = require('canvas-sketch-util/random')
const eases = require('eases')
const palettes = require('nice-color-palettes')
const BezierEasing = require('bezier-easing')

const generate = require('../../modules/generate')
// let seed = "dank25"

random.setSeed(random.getRandomSeed())
// random.setSeed(seed)

console.log("Random Seed =>",random.getSeed())

palette = random.pick(palettes)
foreground = palette.slice(0,3)
background = palette.slice(3,5)

console.log()

// Ensure ThreeJS is in global scope for the 'examples/'
global.THREE = require('three');

// Include any additional ThreeJS examples below
require('three/examples/js/controls/OrbitControls');

const settings = {
  // Make the loop animated
  animate: true,
  dimensions: [ 2048, 2048 ], 
  // fps: 24,
  // duration: 6,
  // Get a WebGL canvas rather than 2D
  context: 'webgl',
  // Turn on MSAA
  attributes: { antialias: true }
};

const sketch = ({ context, width, height }) => {
  // Create a renderer
  const renderer = new THREE.WebGLRenderer({
    context
  });

  // WebGL background color
  renderer.setClearColor(random.pick(background), 1);
  // renderer.setClearColor(random.pick(palette), 0); // transparent

  // Setup a camera
  const camera = new THREE.OrthographicCamera();

  const aspect = width / height;
  
  // Ortho zoom
  const zoom = 1.5;
  
  // Bounds
  camera.left = -zoom * aspect;
  camera.right = zoom * aspect;
  camera.top = zoom;
  camera.bottom = -zoom;
  
  // Near/Far
  camera.near = -100;
  camera.far = 100;
  
  // Set position & look at world center
  camera.position.set(zoom, zoom, zoom);
  camera.lookAt(new THREE.Vector3());
  
  // Update the camera
  camera.updateProjectionMatrix();

  // Setup camera controller
  // const controls = new THREE.OrbitControls(camera);

  // Setup your scene
  const scene = new THREE.Scene();

  let boxes = []

  const fragmentShader = `
    varying vec2 vUv;  
    varying vec4 p;
    uniform vec3 color;

    void main() {
      vec3 c = vec3(color * step(0.5,mod(vUv.x * 10., 1.)));
      c *= step(mod(vUv.y * 10., 1.), .5);
      gl_FragColor = vec4(c, 1.0);
    }
  `
  const vertexShader = `
    varying vec2 vUv;
    varying vec4 p;
    uniform float time;
    uniform vec3 color;

    void main() {
      vUv = uv;
      vec3 pos = position.xyz * sin(time);
      gl_Position = projectionMatrix * modelViewMatrix * vec4(pos, 1.0);
    }
  `

  const box = new THREE.BoxGeometry(1, 1, 1)
  const meshes = []
  const boxNumber = 40

  for (let i = 0; i < boxNumber; i++) {
    const mesh = new THREE.Mesh(
      box,
      new THREE.ShaderMaterial({
        fragmentShader,
        vertexShader,
        uniforms: {
          time: { value: 0 },
          color: { value: new THREE.Color(random.pick(foreground))}
        },
        // color: random.pick(foreground),
      })
    )
    mesh.spinDirection = random.value() > 0.5 ? 1 : -1
    mesh.timeOffset = random.value() * 100
    mesh.position.set(
      random.gaussian()/4, 
      random.gaussian()/2, 
      random.gaussian()/4
    )
    // let distanceFromCenter = Math.sqrt(mesh.position.x ^ 2 + mesh.position.z ^ 2)
    // console.log(distanceFromCenter)
    mesh.scale.set(
      random.gaussian()/1, 
      random.gaussian()/5, 
      random.gaussian()/1
    )

    mesh.scale.multiplyScalar(random.range(0.2,0.5))
    scene.add(mesh);
    meshes.push(mesh)
  }

  scene.add(new THREE.AmbientLight(random.pick(background)))

  const light = new THREE.DirectionalLight('#fff', 1)
  light.position.set(1,3,2)
  scene.add(light)

  // draw each frame
  return {
    
    // Handle resize events here
    resize ({ pixelRatio, viewportWidth, viewportHeight }) {
      renderer.setPixelRatio(pixelRatio);
      renderer.setSize(viewportWidth, viewportHeight);
      camera.aspect = viewportWidth / viewportHeight;
      camera.updateProjectionMatrix();
    },
    
    // Update & render your scene here
    render ({ time, playhead }) {
      // http://cubic-bezier.com/#.29,.96,.78,.15
      // controls.update();
      let t = Math.sin(time / 10 * Math.PI * 2)
      let easeFn = BezierEasing(.28, .85, .43, 1)
      // scene.rotation.y = easeFn(t)
      
      meshes.forEach(mesh => {
        mesh.material.uniforms.time.value = (time + mesh.timeOffset) * mesh.spinDirection // * mesh.timeOffset
        // mesh.rotation.y = Math.sin(time + mesh.timeOffset) * mesh.spinDirection
      });

      // camera.lookAt(new THREE.Vector3());
      renderer.render(scene, camera);
    },
    // Dispose of events & renderer for cleaner hot-reloading
    unload () {
      // controls.dispose();
      renderer.dispose();
    }
  };
};

canvasSketch(sketch, settings);
