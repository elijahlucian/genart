const canvasSketch = require('canvas-sketch');
const math = require('canvas-sketch-util/math')
const random = require('canvas-sketch-util/random')
// const niceColorPalletes = require('nice-color-palettes')
const {palettes} = require('../../modules/palettes')
// const gatewayLog = require('./gateway_log')

random.setSeed(random.getRandomSeed())
// random.setSeed('125852')

const settings = {
  suffix: `__seed_${random.getSeed()}`,
  // dimensions: 'a4', // 
  // dimensions: [ 9, 16 ],
  // // dimensions: [ 512, 512 ], // instagram
  // dimensions: [ 1024, 1024 ], // instagram
  dimensions: [ 2048, 2048 ], // instagram
  // dimensions: [ 4096, 4096 ], // instagram
  // orientation: 'landscape',
  // animate: true,
  fps: 24,
  // units: 'in',
  // pixelsPerInch: 300,
};

// code 0047

const sketch = () => {
  const size = 20
  const randomRadius = true 
  const radius = 25


  console.log("Palette Seed =>",random.getSeed())

  palette = random.pick(Object.values(palettes))
  foreground = palette.slice(0,3)
  background = palette.slice(3,5)

  const algoSeeds = {
    thic: '64',
    datbutt: '66',
    jlo: '76',
    sideboob: '504043',
    curvy: '340363',
    dancer: '318179',
  }
  let algoseed

  // algoseed = algoSeeds.curvy
  // algoseed = '340363'

  random.setSeed(algoseed ||  random.getRandomSeed())
  
  console.log("Algo Seed =>",random.getSeed())

  const gradient = (context, start, end, width, height, left_x=0, top_y=0) => { 
    fill = context.createLinearGradient(left_x, top_y, width, height)
    fill.addColorStop(0, start)
    fill.addColorStop(1, end)
    return fill
  }

  const lerp = (min, max, n) => { 
    return (max - min) * n + min
  }

  const createGrid = (args) => {
    const points = []
    const size = args.size
    
    for (let y = 0; y < size; y++) {
      for (let x = 0; x < size; x++) {
    
        const u = x / (size - 1)
        const v = y / (size - 1)

        vectorScalar = 5
        let vectorNoise = 2 * Math.PI * random.noise2D(u * vectorScalar,v * vectorScalar)

        const vector = {
          i: Math.sin(vectorNoise),
          j: Math.cos(vectorNoise),
          n: vectorNoise,
        }

        const i = points.length
        
        points.push({
          // color: random.pick(foreground),
          color: foreground[i % 3],
          rotation: 0,
          vector,
          x,
          y,
          i,
          position: {u, v},
          rotation: random.noise2D(u,v)
        })
      }
    }
    return points
  }
  
  points = createGrid({ size, randomRadius, radius })//.filter(() => random.value() > 0.8)
  const foregroundColor = random.pick(foreground)
  const backgroundColor = random.pick(background)
  
  return ({ context, width, height, time }) => {
    // bgColor = gradient(context, background[0], background[1], width/3, height)
    // let t = time * 0.1
    let t = random.range(0,1000)

    context.fillStyle = backgroundColor // 'white'
    context.fillRect(0,0,width,height)
    
    let state = {
      rotation: 0
    }

    console.log()

    const stepWidth = width / 10
    const stepHeight = height / 6
    const margin = width / 8
    const lineWidth = 4
    
    let count = 0
    
    context.strokeStyle = foregroundColor
    
    points.forEach(point => {
      
      var lineLength = width / 40
      
      let {u,v} = point.position
      const {i,j,n} = point.vector
      
      u = random.noise2D(
        (u + random.value() * 0.0043) * Math.sin(t),
        v * Math.sin(t)
      )/2 + 0.5

      context.save()
      
      context.translate(
        (margin / 2 + ((u) * (width - margin))), 
        margin / 2 + ((v) * (height - margin))
      )

      // context.rotate(point.rotation += n / 100 )

      context.lineCap = 'square'
      context.lineWidth = Math.sin(n + u + t) * 20 + 20
      context.strokeStyle = point.color

      lineLength *= Math.sin(t + u)

      let lineX = i*lineLength / 2
      let lineY = j*lineLength / 2

      context.beginPath()
      context.moveTo(-lineX,-lineY)
      context.lineTo(lineX,lineY)
      context.stroke()
      
      context.restore()

    });

    console.log("> rendered!")

  };
};

canvasSketch(sketch, settings);
