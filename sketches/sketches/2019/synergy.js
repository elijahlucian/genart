const canvasSketch = require('canvas-sketch');
import BezierEasing from 'bezier-easing'
import simplexNoise from 'simplex-noise'

import { lerp } from '../../modules/maf'

const simplex = new simplexNoise()
  
var mouse = {
  x: 0,
  y: 0,
}

const img = new Image()
img.src = '../images/moon.png'

const settings = {
  dimensions: [ 2048, 2048 ],
  // dimensions: [ 1024, 1024 ],
  // dimensions: [ 1920, 1080 ],
  animate: true,
  canvas: document.createElement('canvas')
};

const sketch = ({context, width, height}) => {
  let step = 0.5
  let steps = 10
  let blockSize = width / steps
  let margin = 500

  document.body.style.backgroundColor = '#000'
  document.body.children[2]
  let imageData = []

  const onMouseMove = (event) => {
    mouse.x = event.clientX - width / 2
    mouse.y = event.clientY - height / 2
  }

  window.addEventListener('mousemove', onMouseMove)

  img.onload = () => {
    console.log(img.naturalWidth)
    context.drawImage(img, 0,0,width,height)
    context.fillStyle = '#000000';
    
    for(let u = 0; u < 1; u+=step) {
      for(let v = 0; v < 1; v+=step) {
        let x = u * width
        let y = v * height
        let n = simplex.noise2D(u, v)
        
        imageData.push({
          i: imageData.length,
          n,
          u,
          v,
          data: context.getImageData(x,y,1,1).data
        })
      }
    }
    imageData = [imageData[3]] // shuffle(imageData)
    context.fillRect(0,0,width,height)
  }

  return ({ time, context, width, height, frame }) => {
    let t = time * 0.5

    context.fillStyle = '#0000000f';
    context.fillRect(0,0,width,height)

    if(imageData.length < 1) return
    
    imageData.forEach(pixel => {
      let {i,u,v,n,data} = pixel
      let x = lerp({min: margin, max: width, t: u})
      let y = lerp({min: margin, max: height, t: v})
      
      let n3 = Math.abs(simplex.noise3D(u * 5,v * 5,t))
      
      if(n3 < 0) return

      let easing = BezierEasing(1,.29,.85,.65)

      context.save()
      // context.translate(
      //   x + -mouse.x * n * 0.4, 
      //   y + -mouse.y * n * 0.4
      // )

      context.translate(
        x,y
      )

      // context.rotate(n3 * Math.PI * 2 + time + i)
      context.rotate(n * easing(v))

      let [r,g,b,a] = data
      context.fillStyle = `rgba(
        ${r + (n3)},
        ${g + (n3)},
        ${b + (n3)},
        ${n3 / 0.7}
      )`
    
      context.strokeStyle = `rgba(
        ${r + 50},
        ${g + 50},
        ${b + 50},
        1
      )`
      context.lineWidth = 10

      let shapeSize = n3 * 200 // step * width * 0.8 // / 4 + (n3 / 0.7) * 50
      
      // context.beginPath()
      // context.rect(-shapeSize / 2,-shapeSize / 2,shapeSize,shapeSize)
      // context.stroke()

      // context.beginPath()
      // context.arc(0,0,shapeSize, 0, Math.PI * 4 * (n3))
      // context.clip()
      // // context.drawImage(img, -u * img.naturalWidth / 2, -v * img.naturalHeight / 2)
      // // context.fill()
      // context.stroke()

      context.beginPath()
      context.moveTo(0,0)
      context.save()
      let segments = 2
      let size = 150
      context.rotate((t) % 1)
      for(let i = 0; i < segments; i++) {
        
        let nn = simplex.noise3D(n, i, t) * 0.01
        
        context.rotate(Math.PI * 2 / segments)
        
        let side = Math.cos(t) * 2 
        let top = Math.sin(t) * 5
        let bottom = Math.sin(t) * 2
        context.lineTo(side*size,bottom*size)
        context.lineTo(0*size,top*size)
        context.lineTo(-side*size,bottom*size)
        context.lineTo(0*size,0*size)
        
      }
      context.restore()

      context.stroke()

      context.restore()
    })
  }
};

canvasSketch(sketch, settings);
