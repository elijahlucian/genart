const canvasSketch = require('canvas-sketch');

// Ensure ThreeJS is in global scope for the 'examples/'
global.THREE = require('three');

// Include any additional ThreeJS examples below
require('three/examples/js/controls/OrbitControls');

const settings = {
  // Make the loop animated
  animate: true,
  // Get a WebGL canvas rather than 2D
  context: 'webgl',
  // Turn on MSAA
  attributes: { antialias: true }
};

const sketch = ({ context }) => {
  // Create a renderer
  const renderer = new THREE.WebGLRenderer({
    context
  });

  // WebGL background color
  renderer.setClearColor('#000', 1);

  // Setup a camera
  const camera = new THREE.PerspectiveCamera(45, 1, 0.01, 100);
  camera.position.set(3, 3, -10);
  camera.lookAt(new THREE.Vector3());

  // Setup camera controller
  const controls = new THREE.OrbitControls(camera);

  // Setup your scene
  const scene = new THREE.Scene();

  var shape = new THREE.Shape()

  shape.moveTo( 0, 0 )
  shape.bezierCurveTo(0,0,1,-5,2.5,0)
  shape.lineTo(2.5,0)
  shape.lineTo(3,3)
  shape.lineTo(2,1.2)
  shape.lineTo(1.25,3)
  shape.lineTo(0.75,1.2)
  shape.lineTo(0,3)
  shape.lineTo(-0.75,1.2)
  shape.lineTo(-1.25,3)
  shape.lineTo(-2,1.2)
  shape.lineTo(-3,3)
  shape.lineTo(-2.5,0)
  shape.bezierCurveTo(-2.5,0,-1,-5,0,0)

  var shape2 = new THREE.Shape()

  var start = true
  
  let r = Math.random()

  for (let u = 0; u <= 1; u+= 0.02) {
      let x = Math.sin(Math.sin(u * 20) * Math.PI ) * 2
      let y = Math.cos(Math.cos(u * 20) * Math.PI ) * 2
      if(start){
        shape.moveTo(x,y)
        start = false
      } else {
        shape2.lineTo(x,y)
      }
  }

  // shape.bezierCurveTo()

  let geometry = new THREE.ExtrudeGeometry(
    shape2,
    {
      depth: 2,
      bevelEnabled: true,
      bevelSegments: true,
      steps: 2,
      bevelSize: 0,
      bevelThickness: 1,
    }
  );

  const mesh = new THREE.Mesh(
    geometry,
    new THREE.MeshPhysicalMaterial({
      color: 'white',
      roughness: 0.75,
      flatShading: true,
      // wireframe: true,
    })
  )


  // const mesh = new THREE.Mesh(
  //   new THREE.BoxGeometry(1, 1, 1),
    new THREE.MeshPhysicalMaterial({
      color: 'white',
      roughness: 0.75,
      flatShading: true
    })
  // );
  scene.add(mesh);

  // Specify an ambient/unlit colour
  scene.add(new THREE.AmbientLight('#59314f'));

  // Add some light
  const light = new THREE.PointLight('#ffffff', 1, 45.5);
  light.position.set(4, 4, -12).multiplyScalar(1.5);
  light.lookAt(mesh)
  scene.add(light);

  // draw each frame
  return {
    // Handle resize events here
    resize ({ pixelRatio, viewportWidth, viewportHeight }) {
      renderer.setPixelRatio(pixelRatio);
      renderer.setSize(viewportWidth, viewportHeight);
      camera.aspect = viewportWidth / viewportHeight;
      camera.updateProjectionMatrix();
    },
    // Update & render your scene here
    render ({ time }) {
      // mesh.rotation.x = time * (10 * Math.PI / 180);
      // mesh.rotation.y = time * (10 * Math.PI / 180);
      // mesh.rotation.z = time * (40 * Math.PI / 180);
      controls.update();
      renderer.render(scene, camera);
    },
    // Dispose of events & renderer for cleaner hot-reloading
    unload () {
      controls.dispose();
      renderer.dispose();
    }
  };
};

canvasSketch(sketch, settings);
