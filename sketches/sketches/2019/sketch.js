const canvasSketch = require('canvas-sketch');

const settings = {
  // dimensions: 'a4', // 
  // dimensions: [ 9, 16 ],
  dimensions: [ 2048, 2048 ], // instagram
  // orientation: 'landscape',
  // units: 'in',
  pixelsPerInch: 600,
};

const sketch = () => {
  const getPoints = () => {
    let points = []
    let count = 20
    for (let x = 0; x < count; x++) {
      for (let y = 0; x < count; y++) {
      }
    }
  }

  return ({ context, width, height }) => {
    context.fillStyle = 'hsl(160, 50%, 40%)';
    
    fill = context.createLinearGradient(0, 0, width, height);
    fill.addColorStop(0, 'hsl(44, 95%, 66%)')
    fill.addColorStop(1, 'hsl(344, 79%, 51%)')
    
    context.fillStyle = fill
    context.fillRect(0, 0, width, height);
    
    context.fillStyle = 'hsla(160, 20%, 100%, 15%)'
    context.strokeStyle = 'hsla(0, 100%, 100%, 20%)'
    context.lineWidth = 1

    for (let i = 0; i < width+100; i += width/20) {
      for (let k = 0; k < height+100; k += height/20) {
        console.log(i,k)
        context.beginPath()
        if (Math.random() < 0.5) {
          context.arc(i,k,Math.random() * 100,0,Math.PI*2,false)
          context.fill()
          // context.stroke()
        }
        
      }
      
    }

  };
};

canvasSketch(sketch, settings);
