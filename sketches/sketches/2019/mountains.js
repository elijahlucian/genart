const canvasSketch = require('canvas-sketch');
const random = require('canvas-sketch-util/random')
const niceColorPalletes = require('nice-color-palettes')
// const gatewayLog = require('./gateway_log')


// window.fetch('http://www.colourlovers.com/api/palettes/top?format=json&numResults=100').then(data => console.log(data))

random.setSeed(random.getRandomSeed())

const settings = {
  suffix: `__seed_${random.getSeed()}`,
  dimensions: [ 2048, 2048 ], // instagram
};

const sketch = () => {
  const size = 10
  const randomRadius = true 
  const radius = 25

  console.log("Random Seed =>",random.getSeed())

  palette = random.pick(niceColorPalletes)
  reducedPalette = palette.slice(0,3)
  background = palette.slice(3,5)

  // window.addEventListener("keydown", utils.newSeed, false);
  
  const gradient = (context, start, end, width, height, left_x=0, top_y=0) => { 
    fill = context.createLinearGradient(left_x, top_y, width, height)
    fill.addColorStop(0, start)
    fill.addColorStop(1, end)
    return fill
  }

  const lerp = (min, max, t) => { 
    return (max - min) * t + min
  }

  const createGrid = (args) => {
    const points = []
    const size = args.size
    
    for (let y = 0; y < size; y++) {
      for (let x = 0; x < size; x++) {
    
        const u = x / (size - 1)
        const v = y / (size - 1)
        const radius = Math.max(0, random.noise2D(u,v) + 1) * 0.01
        
        points.push({
          color: random.pick(reducedPalette),
          position: {u, v},
          radius,
          rotation: random.noise2D(u,v)
        })
      }
    }
    return points
  }

  points = createGrid({ size, randomRadius, radius })//.filter(() => random.value() > 0.8)
  
  return ({ context, width, height }) => {

    let state = {
      rotation: 0
    }

    const stepWidth = width / 10
    const stepHeight = height / 6
    const margin = width / 8
    const lineWidth = width/100

    let count = 0
    bgColor = gradient(context, background[0], background[1], width/3, height)
    context.fillStyle = bgColor
    // context.fillStyle = random.pick(background) // 'white'
    context.fillRect(0,0,width,height)
    
    context.fillStyle = 'rgba(255,255,255,0.3)'

    // points.forEach(point => {
  for (let u = stepHeight*2; u < height - stepHeight; u+= stepHeight) {
    var y = u//utils.lerp(margin, width - margin, u)
    context.moveTo(0+lineWidth/2,height)
    context.lineTo(0+lineWidth/2,y)

    for (let v = 0; v < width; v+= stepWidth) {
      var x = v//utils.lerp(margin, height - margin, v)
      
      let fontSize = radius * width * 10
      let length = 200

      context.font = `${fontSize}px "Inconsolata"` // `${margin/10}px`
      
      // context.save()
      // context.translate(x,y)
      context.globalCompositeOperation = 'source-atop'
      // context.fillStyle='white'
      context.fillStyle = `rgba(${count*10},255,255,0.2)`
      context.globalCompositeOperation = 'destination-out';
      // context.fillRect(x,y,40, 40)
      context.lineWidth = lineWidth
      context.strokeStyle = bgColor
      context.lineTo(x,y + random.range(-80,80))
      
      // context.restore()
      
      count += 1
    }
    context.fill()
    
    context.lineTo(width-lineWidth/2,0)
    context.lineTo(width-lineWidth/2,height)
    
  }
  context.moveTo(0+lineWidth/2,0)
  context.lineTo(0+lineWidth/2,height-lineWidth/2)

  context.lineTo(0,height-lineWidth/2)
  context.lineTo(width,height-lineWidth/2)
  
  context.stroke()
    // context.restore()
    // generate.DarkNoise(context, width, height, background[1])

    console.log("> rendered!")

  };
};

canvasSketch(sketch, settings);
