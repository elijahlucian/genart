const canvasSketch = require('canvas-sketch');
var createAnalyser = require('web-audio-analyser')
var audioContext, analyser, audioUtil
var average = require('analyser-frequency-average')

global.THREE = require('three');
var OBJLoader = require('three-obj-loader');
OBJLoader(THREE);

const settings = {
  animate: true,
  fps: 1,
  dimensions: [ 1920, 1080 ], 
  context: 'webgl',
  attributes: { antialias: true }
};

const sketch = ({ context }) => {
  const renderer = new THREE.WebGLRenderer({
    context
  });

  renderer.setClearColor("#2e2633", 1);
  const origin = new THREE.Vector3()

  let camera = new THREE.PerspectiveCamera(
    45, 45, 1, 1000
  )
  camera.position.set(0, 5, 5);
  camera.lookAt(origin);

  const scene = new THREE.Scene();

  const mesh = new THREE.Mesh(
    new THREE.BoxGeometry(7, 1, 1),
    new THREE.MeshPhysicalMaterial({
      color: "#e5625c",
      roughness: 0.75,
      flatShading: true
    })
  )
  
  scene.add(mesh)

  mesh.position.set(0, -3.5, 0)
   
  var vapsquadTitle, vapsquadTitleBoundingBox
  var objLoader  = new THREE.OBJLoader();
  objLoader.load('objects/vapsquad-name-rotated.obj', event => {
    console.log(event)
    let scale = 0.5
    vapsquadTitle = event.children[0]
    vapsquadTitle.scale.set(scale,scale,scale)
    vapsquadTitle.position.set(-3,-2.7,0)
    
    scene.add(vapsquadTitle)
  })
  
  scene.add(new THREE.AmbientLight("#ffffff"));

  const light = new THREE.PointLight("#ffffff", 1, 45.5, 2);
  light.position.set(3, 3, 5).multiplyScalar(1.5);
  scene.add(light);

  navigator.mediaDevices.getUserMedia( {audio:true})
  .then( stream => {
    audioContext = new AudioContext()
  
    var mediaStreamSource = audioContext.createMediaStreamSource(stream)
    mediaStreamSource.connect(audioContext.destination)

    audioUtil = createAnalyser(mediaStreamSource, audioContext, {mute: true}) 

    analyser = audioUtil.analyser
    analyser.fftSize = 256
    analyser.smoothingTimeConstant = 0.7
      // 0 = rage
      // 0.7 = fun
      // 2.2 = chill
    console.log(analyser)
  })

  return {
    resize ({ pixelRatio, viewportWidth, viewportHeight }) {
      renderer.setPixelRatio(pixelRatio);
      renderer.setSize(viewportWidth, viewportHeight);
      camera.aspect = viewportWidth / viewportHeight;
      camera.updateProjectionMatrix();
    },

    render ({ time }) {
      if(!analyser || !vapsquadTitle) return
      let low = average(analyser, audioUtil.frequencies(), 50, 350)
      let mid = average(analyser, audioUtil.frequencies(), 400, 2000)
      let high = average(analyser, audioUtil.frequencies(), 2500, 15000)
      // console.log(avg)
      
      mesh.rotation.x = Math.PI * 0.25
      mesh.scale.y = 3 * mid

      vapsquadTitle.position.z = mid * 1.6 + 0.6
      vapsquadTitle.position.y = mid - 3.2
      
      vapsquadTitle.rotation.z = Math.sin(time) * 0.1 
      vapsquadTitle.position.y = -1 + high
      // vapsquadTitle.scale.y = 4 * avg
  
      // let bufferLength = analyser.frequencyBinCount
      // let dataArray = new Uint8Array(bufferLength)
      // analyser.getByteTimeDomainData(dataArray)
      // console.log(dataArray)

      renderer.render(scene, camera);
    },

    unload () {
      renderer.dispose();
    }
  };
};

canvasSketch(sketch, settings);
