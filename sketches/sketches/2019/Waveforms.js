const canvasSketch = require('canvas-sketch');
const random = require('canvas-sketch-util/random')

const BezierEasing = require('bezier-easing');
const { palettes }  = require('../../modules/nice-color-palettes')
const { paletteSeeds } = require('../../seeds/palettes')

var createAnalyser = require('web-audio-analyser')
var average = require('analyser-frequency-average')

const utils = require('../../modules/utils')
const maf = require('../../modules/maf')
const { BPM, Tempo, musicalTime } = require('../../modules/music')
const { dimensions } = require('../../modules/dimensions')

const { BaseShape } = require('../../classes/shapes')
const { Img, Mouse, Keyboard, Controls, AudioAnalyser, AudioLoader } = require('../../classes/utils')

var audio

// GLOBALS
var zeroOffset = 0
var dt = 0

const bpm = new BPM()
const bez = new BezierEasing(.22,.92,.73,.63)

let seed = {
  // palette: paletteSeeds.sunny,
  // determined: 'dank',
  random: random.getRandomSeed()
}

let img = new Img({src: '../images/mountains-06.png'})
// console.log(img)

// paletteSeed
random.setSeed(seed.palette || seed.defined || seed.random)
console.log("Palette Seed =>", random.getSeed())

const palette = random.pick(palettes)
const bg = palette.shift()

// algoSeed
random.setSeed(seed.determined || seed.random)
console.log("Algo Seed =>", random.getSeed())

document.body.style.backgroundColor = bg
document.title = "JS Sketches"

// const canvasSize = 2048
const settings = {
  suffix: `__seed_${random.getSeed()}`,
  // dimensions: [canvasSize,canvasSize],
  dimensions: dimensions.square.insta,
  // orientation: 'landscape',
  // units: 'in'
  // pixelsPerInch: 300,
  animate: true,
};

const longEdge = utils.getLongEdgeFromArray(settings.dimensions || [500,500])

const config = {
  speed: 0.5,
  trails: true,
  margin: longEdge / 20,
  lineWidth: longEdge / 150,
  shapeSize: longEdge / 200,
}
// TODO: WEBCAM
// TODO: Image Loader

const initialize = () => {
}

const sketch = ({width, height}) => {
  var mouse = new Mouse({width,height})
  var controls = new Controls()
  var music = new AudioLoader({src: 'Elijah-SpyWare3.mp3'})
  var analyser = new AudioAnalyser()
  initialize({width,height})

  // instantiate classes here 
  agent_count = 0
  agent_limit = 100

  let shapes = []
  for (let i = 0; i < agent_count; i++) {
    let z = i / agent_count - 1
    shapes.push(
      new BaseShape({
        i,
        z,
        color: random.pick(palette),
        baseSize: config.shapeSize,
        stroke: false,
        palette,
      })
    )
  }

  return ({ time, context, width, height }) => {
    
    if(!analyser.audioUtil) return
    if(!music.data) return
    if(!img.data) return

    music.update()
    analyser.update()

    dt = time
    // context.fillStyle = bg + (config.trails ? '00' : 'ff')
    context.fillStyle = '#000'
    context.fillRect(0, 0, width, height);
    
    const t = time * config.speed
    
    context.lineCap = "square" // || "round" || "square" || "butt"
    context.lineJoin = "square" // || "bevel" || "miter" || "square"
    context.strokeStyle = '#fff2'
    context.lineWidth = config.lineWidth

    // make this more atomic and shit    
    let mt
    if(bpm) {
     mt = musicalTime({
        zeroOffset,
        time,
        bpm: bpm.bpm
      })
    } else { mt = time }
    let b = mt % 1
    let q = mt * 2 % 1

    let beats = {b,q}
    
    for (let i = 0; i < music.data.length / 4; i+= 10000) {
      const sample = Math.abs(music.data[i]);
      let u = i/music.data.length * 4
      context.beginPath()
      context.strokeStyle = `hsla(${(u * 360) + Math.floor(dt + b) * 100 % 360}, 100%, 50%, 1)`  
      context.moveTo(u*width, height)
      context.lineTo(u*width, height - 500 - Math.abs(sample) * height * Math.sin(t + i))
      context.stroke()  
    }

    shapes.forEach((shape, i) => {
      shape.update({
        time, 
        width, 
        height, 
        // audio: analyser.audio, 
        // music, 
        beats
      })
      shape.draw({context})
    });
  };  
};

canvasSketch(sketch, settings);