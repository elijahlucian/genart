const canvasSketch = require('canvas-sketch')
const random = require('canvas-sketch-util/random')

const BezierEasing = require('bezier-easing')
const { palettes } = require('../../modules/nice-color-palettes')
const { paletteSeeds } = require('../../seeds/palettes')

const utils = require('../../modules/utils')
const maf = require('../../modules/maf')
const { BPM, Tempo, musicalTime } = require('../../classes/music')
const { dimensions } = require('../../modules/dimensions')

const { NewTree } = require('../../classes/trees')
const { Img, Mouse, Keyboard, Controls } = require('../../classes/utils')
const { AudioAnalyser, AudioLoader } = require('../../classes/audio')
const { Caption } = require('../../classes/typeface')
// GLOBALS
var zeroOffset = 0
var dt = 0
var shapes = []
var treeparams = {}

const bpm = new BPM({
  bpm: 105,
})

const bez = new BezierEasing(0.22, 0.92, 0.73, 0.63)

let seed = {
  palette: 1567900321849,
  // determined: 'dank',
  random: Date.now(), //random.getRandomSeed()
}

// let img = new Img({src: '../images/mountains-06.png'})

// paletteSeed
random.setSeed(seed.palette || seed.defined || seed.random)
console.log('Palette Seed =>', random.getSeed())

const palette = random.pick(palettes)
const bg = palette.shift()
const mainColor = palette[2]

// algoSeed
random.setSeed(seed.determined || seed.random)
console.log('Algo Seed =>', random.getSeed())

document.body.style.backgroundColor = bg
document.title = 'JS Sketches'

// const canvasSize = 2048
const settings = {
  suffix: `__seed_${random.getSeed()}`,
  // dimensions: [canvasSize,canvasSize],
  dimensions: dimensions.square.insta,
  // dimensions: dimensions.print.displate,
  // orientation: 'landscape',
  // units: 'in'
  // pixelsPerInch: 300,
  fps: 24,
  animate: true,
}

// TODO: WEBCAM
// TODO: Image Loader

const drawTree = params => {
  let tree = new NewTree(treeparams)
  tree.grow()
  shapes.push(tree)
}

const sketch = params => {
  console.log('SKETCH PARAMS =>', params)
  let { context, width, height } = params

  context.lineCap = 'square' // || "round" || "square" || "butt"
  context.lineJoin = 'square' // || "bevel" || "miter" || "square"

  const longEdge = utils.getLongEdgeFromArray([width, height])

  const config = {
    speed: 0.7,
    agent_count: 1,
    agent_limit: 200,
    trails: true,
    margin: longEdge / 20,
    lineWidth: longEdge / 150,
    shapeSize: longEdge / 20,
    bandwidthMin: 120,
    bandwidthMax: 10000,
    audio: {
      gain: -0.8,
    },
  }

  var mouse = new Mouse({ width, height })
  var controls = new Controls()
  // var analyser = new AudioAnalyser() // live audio from mic source
  var caption = new Caption({
    font: 'Inconsolata',
    size: 6,
    caption: 'just some drums and keys',
    color: random.pick(palette),
  })
  var music = new AudioLoader({
    src: '105 bpm.mp3',
    looping: false,
    band_count: config.agent_count,
    sensitivity: 0.2,
    bandwidthMin: config.bandwidthMin,
    bandwidthMax: config.bandwidthMax,
    gain: config.audio.gain,
  })
  // var analyser = new AudioAnalyser()

  // instantiate ../classes here
  // plan agent count for band count.

  console.log('MAINCOLOR', mainColor)

  for (let i = 0; i < config.agent_count; i++) {
    let z = i / config.agent_count - 1
    treeparams = {
      i,
      z,
      u: 0.5,
      v: 1,
      angle: 0,
      color: mainColor,
      size: config.shapeSize,
      life: 50,
      maxHeight: 18,
    }
    let tree = new NewTree(treeparams)
    tree.grow()
    shapes.push(tree)
  }

  context.fillStyle = bg // + (config.trails ? '33' : 'ff')
  context.fillRect(0, 0, width, height)

  caption.update({ time: 0, width, height })
  // caption.draw({context})

  return ({ time, context, width, height }) => {
    const t = time * config.speed

    context.fillStyle = bg // + (config.trails ? '33' : 'ff')
    context.fillRect(0, 0, width, height)

    shapes.forEach((tree, i) => {
      tree.update({ time, width, height })
      tree.draw({ context })
    })

    if (mouse.isPressed()) {
      // shapes = []
      treeparams.color = random.pick(palette)
      treeparams.u = random.value()
      treeparams.i = shapes.length
      drawTree(treeparams)
    }
  }
}

canvasSketch(sketch, settings)
