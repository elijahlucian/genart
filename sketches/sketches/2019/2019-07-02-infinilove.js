const canvasSketch = require('canvas-sketch')
const BezierEasing = require('bezier-easing')
const glslify = require('glslify')
const Simplex = require('simplex-noise')
const random = require('canvas-sketch-util/random')
const simplex = new Simplex()
const utils = require('../../modules/utils')
const { PI, TAU } = require('../../modules/maf')

const bez = new BezierEasing(0.22, 0.92, 0.73, 0.63)

const settings = {
  // dimensions: [ 6500, 6500 ],
  // dimensions: [1024, 1024],
  animate: true,
  fps: 30,
}

document.body.style.backgroundColor = '#000000'

const sketch = ({ width, height }) => {
  const canvas = document.createElement('canvas')

  return ({ time, context, width, height }) => {
    context.fillStyle = '#000000'
    context.fillRect(0, 0, width, height)

    const margin = 200
    const w = width / 2 - margin
    const h = height / 2
    const speed = 0.5
    const t = time // TAU + TAU //time * speed

    context.lineCap = 'round' // || "round" || "square" || "butt"
    context.lineJoin = 'round' // || "bevel" || "miter" || "square"

    context.strokeStyle = '#fff'
    context.lineWidth = width / 40

    let lineResolution = 0.001
    let start = false
    let lastX
    let lastY
    for (let u = 0; u <= 1.01; u += lineResolution) {
      context.beginPath()
      let x =
        (Math.sin(t * 0.5 + u * TAU) * width) / 3.5 +
        width / 2 +
        Math.sin(t + u * TAU * 20) * (width / 8) * Math.sin(t * 0.1 * PI)
      let y =
        (Math.cos(t + u * TAU) * height) / 3.5 +
        height / 2 +
        Math.cos(t * 2 + u * TAU * 10) * (width / 8) * Math.cos(t * 0.1 * PI)
      if (start) {
        context.moveTo(x, y)
        lastX = x
        lastY = y
        start = false
      } else {
        context.strokeStyle = `hsl(${
          (-t * TAU * 50 + u * 360) % 360
        }, 100%, 50%)`
        context.moveTo(lastX, lastY)
        context.lineTo(x, y)
        lastX = x
        lastY = y
        context.stroke()
      }
    }
    context.closePath()
  }
}

canvasSketch(sketch, settings)
