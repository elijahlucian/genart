const canvasSketch = require('canvas-sketch');
var createAnalyser = require('web-audio-analyser')
var audioContext, analyser, audioUtil
var average = require('analyser-frequency-average')

global.THREE = require('three');

const settings = {
  animate: true,
  fps: 1,
  dimensions: [ 1024, 1024 ], 
  context: 'webgl',
  attributes: { antialias: true }
};

const sketch = ({ context }) => {
  var meshes = []
  const renderer = new THREE.WebGLRenderer({
    context
  });

  renderer.setClearColor("#2e2633", 1);
  const origin = new THREE.Vector3()

  let camera = new THREE.PerspectiveCamera(
    45, 45, 1, 1000
  )
  camera.position.set(15, 4, 5);
  camera.lookAt(origin);

  const scene = new THREE.Scene();

  scene.add(new THREE.AmbientLight("#ffffff"));

  const light = new THREE.PointLight("#ffffff", 1, 15.5, 2);
  light.position.set(3, 3, 5).multiplyScalar(1.5);
  scene.add(light);

  navigator.mediaDevices.getUserMedia( {audio:true})
  .then( stream => {
    audioContext = new AudioContext()
  
    var mediaStreamSource = audioContext.createMediaStreamSource(stream)
    mediaStreamSource.connect(audioContext.destination)

    audioUtil = createAnalyser(mediaStreamSource, audioContext, {mute: true}) 

    analyser = audioUtil.analyser
    analyser.fftSize = 256
    console.log(analyser)

    var offset = analyser.frequencyBinCount / 2

    for (let i = 0; i < analyser.frequencyBinCount; i++) {
      const mesh = new THREE.Mesh(
        new THREE.BoxGeometry(0.5, 0.5, 0.5),
        new THREE.MeshPhysicalMaterial({
          color: "#e5625c",
          roughness: 0.75,
          flatShading: true
        })
      )
      mesh.i = i
      mesh.position.set(i - offset, 0, 0)
      scene.add(mesh)
      meshes.push(mesh)
    }

  })

  return {
    resize ({ pixelRatio, viewportWidth, viewportHeight }) {
      renderer.setPixelRatio(pixelRatio);
      renderer.setSize(viewportWidth, viewportHeight);
      camera.aspect = viewportWidth / viewportHeight;
      camera.updateProjectionMatrix();
    },

    render ({ time }) {
      if(!analyser) return
      let avg = average(analyser, audioUtil.frequencies(), 500, 3000)
      // console.log(avg)
  
      meshes.forEach(mesh => {
        mesh.rotation.y += 0.001 * mesh.i
      })
      // let bufferLength = analyser.frequencyBinCount
      // let dataArray = new Uint8Array(bufferLength)
      // analyser.getByteTimeDomainData(dataArray)
      // console.log(dataArray)

      renderer.render(scene, camera);
    },

    unload () {
      renderer.dispose();
    }
  };
};

canvasSketch(sketch, settings);
