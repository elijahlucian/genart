const canvasSketch = require('canvas-sketch')
const random = require('canvas-sketch-util/random')

const palettes = require('nice-color-palettes')

import {dimensions,  doubleSize } from '../../modules/dimensions'
import paletteSeeds from '../../seeds/palettes'

import { Circle } from '../../classes/growth'

console.log('PALETTES', paletteSeeds)

let seed = {
  // palette: paletteSeeds.tree2,
  // determined: 'dank',
  random: random.getRandomSeed(),
}

random.setSeed(seed.palette || seed.determined || seed.random)
console.log('Random Seed =>', random.getSeed())
const palette = random.pick(palettes)
const backgroundColor = palette.shift()

const settings = {
  dimensions: [6500,6500],
  animate: true,
}

document.body.style.backgroundColor = '#000000'
document.body.style.backgroundColor = backgroundColor

const sketch = ({ width, height, context }) => {
  // instantiate classes here
  const circle_size = width / 50
  const agent_count = 1
  const agent_limit = 100
  const stepMultiplier = 0.3

  let circles = []
  for (let i = 0; i < agent_count; i++) {
    let z = i / agent_count - 1
    let size = Math.abs(random.noise1D(z)) * circle_size

    circles.push(
      new Circle({
        size,
        i,
        z,
        palette,
        stepMultiplier,
        context,
      })
    )
  }
  context.fillStyle = '#000000'
  context.fillStyle = backgroundColor

  context.fillRect(0, 0, width, height)

  return ({ time, context, width, height }) => {
    context.lineCap = 'square' // || "round" || "square" || "butt"
    context.lineJoin = 'square' // || "bevel" || "miter" || "square"
    context.strokeStyle = '#fff1'
    context.lineWidth = 2

    context.globalCompositeOperation = 'lighten'

    if (circles.length > agent_limit) return

    circles.forEach(circle => {
      circle.update({ time, width, height })
      if (circle.child) {
        let i = circles.length
        circles.push(
          new Circle({
            size: Math.abs(random.noise1D(i)) * circle_size + width * 0.01,
            i,
            palette,
            u: circle.u,
            v: circle.v,
            stepMultiplier,
            context,
          })
        )
      }
      circle.draw({ context })
      circle.fill()
      circle.stroke()
    })

    // context.restore()
  }
}

canvasSketch(sketch, settings)
