global.THREE = require('three');

const clock = new THREE.Clock({autostart: true})

const canvasSketch = require('canvas-sketch');
const utils = require('../../modules/utils')
const Simplex = require('simplex-noise')
const simplex = new Simplex()
const glslify = require('glslify')
const Bezier = require('bezier-easing')
const palettes = require('nice-color-palettes')
const random = require('canvas-sketch-util/random')
let palette = random.pick(palettes)


const { PI, TAU, sin, cos } = require('../../modules/maf')

const materials =  require('../../three-basics/materials')

backgroundColor = palette.shift()

const sun = require('../../shaders/outrunSun')
const outrunPlane = require('../../shaders/outrunPlane')

const bez = Bezier(.11,.73,.32,.92)

document.body.style.backgroundColor = 'black'

require('three/examples/js/controls/OrbitControls');


const settings = {
  animate: true,
  dimensions: [2048,2048],
  // dimensions: [3840,2160],
  context: 'webgl',
  attributes: { antialias: true },
};

const sketch = ({ context }) => {
  const renderer = new THREE.WebGLRenderer({
    context
  });

  // renderer.setClearColor(backgroundColor, 1);
  renderer.setClearColor('#000', 1);
  // renderer.setClearColor('#fff', 1);
  const camera = new THREE.PerspectiveCamera(45, 1, 0.01, 1000);
  camera.position.set(-0, 18, -50);
  camera.lookAt(new THREE.Vector3());

  const controls = new THREE.OrbitControls(camera);
  const scene = new THREE.Scene();

  const sphereSize = 1

  const sphereGeo = new THREE.SphereGeometry(sphereSize,2,2)
  const sphereGeoTemp = new THREE.SphereGeometry(sphereSize,2,2)

  let spheres = []
  
  let step = 0.2
  let size = 8
  let scale = 5
  for (let u = -size; u < size; u++) {
    for (let v = -size; v < size; v++) {

      let x = u * scale
      let z = v * scale

      const sphereMesh = new THREE.Mesh(
        sphereGeo,
        materials.toon
      )

      sphereMesh.u = u
      sphereMesh.v = v
      sphereMesh.n2 = simplex.noise2D(u,v)
      sphereMesh.index = 1
      sphereMesh.ogVertices = sphereMesh.geometry.vertices
      sphereMesh.position.x = x
      sphereMesh.position.z = z + 10

      scene.add( sphereMesh )    
      spheres.push(sphereMesh)
    }
  }



  scene.add(new THREE.AmbientLight('hsla(20,80%,50%,1)'));
  const light = new THREE.PointLight('#45caf7', 1, 15.5);
  light.position.set(2, -0.5, -8).multiplyScalar(1.2);
  scene.add(light);

  rotationIndex = 0
  count = 0

  return {
    // Handle resize events here
    resize ({ pixelRatio, viewportWidth, viewportHeight }) {
      renderer.setPixelRatio(pixelRatio);
      renderer.setSize(viewportWidth, viewportHeight);
      camera.aspect = viewportWidth / viewportHeight;
      camera.updateProjectionMatrix();
    },

    // Update & render your scene here
    
    render ({ time }) {
      count += 1
      let deltaTime = clock.getDelta()
      let speed = 0.24
      let t = time * speed
      
      spheres.forEach(sphereMesh => {
        let verticesLength = sphereMesh.geometry.vertices.length
        for (let i = 0; i < verticesLength; i++) {
          const vertex = sphereMesh.geometry.vertices[i];
          const ogVertex = sphereGeoTemp.vertices[i]
          let n = random.noise4D(
            sphereMesh.position.x * 0.3,
            sphereMesh.position.y * 0.3,
            sphereMesh.position.z * 0.3,
            t + i
          )
          
          // sphereMesh.geometry.vertices[i].copy(
            //   sphereGeoTemp.vertices[i].multiplyScalar(1 + 0.3 * n)
            // )
          sphereMesh.geometry.vertices[i].normalize().multiplyScalar(sphereSize + Math.abs(n * 2.7))
        }
        let s = Math.sin(time + sphereMesh.v)
        let ease = bez((4 * sphereMesh.index + time) % 1) * 0.05
        sphereMesh.rotation.y = time + sphereMesh.v * 0.1
        sphereMesh.position.y = s * 3

        let hue = 180 * s + 180
        sphereMesh.material.color.set(`hsl(${hue},100%,50%)`) 
        sphereMesh.geometry.computeVertexNormals()
        sphereMesh.geometry.verticesNeedUpdate = true
        sphereMesh.geometry.normalsNeedUpdate = true
      });
      controls.update();
      renderer.render(scene, camera);
    },
  
    unload () {
      controls.dispose();
      renderer.dispose();
    }
  };
};

canvasSketch(sketch, settings);

