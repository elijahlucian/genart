const canvasSketch = require('canvas-sketch');
const random = require('canvas-sketch-util/random')
const niceColorPalletes = require('nice-color-palettes')
// const gatewayLog = require('./gateway_log')


// window.fetch('http://www.colourlovers.com/api/palettes/top?format=json&numResults=100').then(data => console.log(data))

random.setSeed(random.getRandomSeed())
// random.setSeed(seed)

const settings = {
  suffix: `__seed_${random.getSeed()}`,
  // dimensions: 'a4', // 
  // dimensions: [ 9, 16 ],
  // // dimensions: [ 512, 512 ], // instagram
  // dimensions: [ 1024, 1024 ], // instagram
  // dimensions: [ 2048, 2048 ], // instagram
  dimensions: [ 4096, 4096 ], // instagram
  // orientation: 'landscape',
  // animate: true,
  // units: 'in',
  // pixelsPerInch: 300,
};

// code 0047

const sketch = () => {
  const size = 40
  const randomRadius = true 
  const radius = 25

  console.log("Random Seed =>",random.getSeed())

  palette = random.pick(niceColorPalletes)
  foreground = palette.slice(0,3)
  background = palette.slice(3,5)

  // window.addEventListener("keydown", utils.newSeed, false);
  
  const gradient = (context, start, end, width, height, left_x=0, top_y=0) => { 
    fill = context.createLinearGradient(left_x, top_y, width, height)
    fill.addColorStop(0, start)
    fill.addColorStop(1, end)
    return fill
  }

  const lerp = (min, max, t) => { 
    return (max - min) * t + min
  }

  const createGrid = (args) => {
    const points = []
    const size = args.size
    
    for (let y = 0; y < size; y++) {
      for (let x = 0; x < size; x++) {
    
        const u = x / (size - 1)
        const v = y / (size - 1)

        vectorScalar = 5
        let vectorNoise = 2 * Math.PI * random.noise2D(u * vectorScalar,v * vectorScalar)

        const vector = {
          i: Math.sin(vectorNoise),
          j: Math.cos(vectorNoise),
          n: vectorNoise,
        }

        const i = points.length
        
        points.push({
          // color: random.pick(foreground),
          color: foreground[i % 3],
          rotation: 0,
          vector,
          x,
          y,
          i,
          position: {u, v},
          rotation: random.noise2D(u,v)
        })
      }
    }
    return points
  }
  // context.fillStyle = bgColor
  points = createGrid({ size, randomRadius, radius })//.filter(() => random.value() > 0.8)
  
  return ({ context, width, height, time }) => {
    // bgColor = gradient(context, background[0], background[1], width/3, height)
    context.fillStyle = background[0] // 'white'
    context.fillRect(0,0,width,height)
    
    let state = {
      rotation: 0
    }

    const stepWidth = width / 10
    const stepHeight = height / 6
    const margin = width / 8
    const lineWidth = width / 300
    const lineLength = width / 10

    let count = 0

    let foregroundColor = random.pick(foreground)
    context.strokeStyle = foregroundColor



    points.forEach(point => {

      
      const {u,v} = point.position
      const {i,j,n} = point.vector
      
      const tnoise = random.noise3D(i,j,time) / 100
      const pnoise = random.noise3D(u,v,time * n) / 100
      
      const xnoise = random.noise2D(i,time / 5) / 400
      const ynoise = random.noise2D(j,time / 5) / 400

      // if(i < 0.95) return
      
      context.save()
      
      context.translate((u + xnoise * n) * width, (v + ynoise * n) * height)
      context.rotate(point.rotation += Math.abs(pnoise) * Math.PI * 4)

      context.lineCap = 'round'
      context.lineWidth = lineWidth * n // point.vector.n * lineWidth
      context.strokeStyle = point.color

      let lineX = i*lineLength / 2
      let lineY = j*lineLength / 2

      context.beginPath()
      context.moveTo(-lineX,-lineY)
      context.lineTo(lineX,lineY)
      context.stroke()
      
      context.restore()

    });

    // console.log("> rendered!")

  };
};

canvasSketch(sketch, settings);
