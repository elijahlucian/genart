const canvasSketch = require('canvas-sketch');
const random = require('canvas-sketch-util/random')
const niceColorPalletes = require('nice-color-palettes')
// const gatewayLog = require('./gateway_log')

const generate = require('../../modules/generate')


let seed = "562582"

random.setSeed(random.getRandomSeed())
// random.setSeed(seed)

let seeds = {
  free_as_a_bird: "562582"
}

const settings = {
  suffix: `__seed_${random.getSeed()}`,
  // dimensions: 'a4', // 
  dimensions: [ 9, 16 ],
  // dimensions: [ 2048, 2048 ], // instagram
  // dimensions: [ 4096, 4096 ], // instagram
  orientation: 'landscape',
  units: 'in',
  pixelsPerInch: 300,
};

const sketch = () => {
  const size = 10
  const randomRadius = true 
  const radius = 25

  console.log("Random Seed =>",random.getSeed())

  palette = random.pick(niceColorPalletes)
  reducedPalette = palette.slice(0,3)
  background = palette.slice(0,5)
  
  const newSeed = (e) => {
    console.log(e)
    seed += e.key
    random.setSeed(seed)
    points = generate.grid({
      size,
      randomRadius,
      radius,
    })
    window.requestAnimationFrame(() => {
      // console.log("a callback? wtf", seed)
    })
    e.preventDefault()
  }
  window.addEventListener("keydown", newSeed, false);
  
  const gradient = (context, start, end, width, height, left_x=0, top_y=0) => { 
    fill = context.createLinearGradient(left_x, top_y, width, height)
    fill.addColorStop(0, start)
    fill.addColorStop(1, end)
    return fill
  }


  points = generate.grid({ size, randomRadius, radius }).filter(() => random.value() > 0.8)
  
  return ({ context, width, height }) => {

    let state = {
      rotation: 0
    }

    const margin = width / 8
    let count = 0
    backFill = gradient(context, background[0], background[1], width/3, height)
    context.fillStyle = backFill
    // context.fillStyle = random.pick(background) // 'white'
    context.fillRect(0,0,width,height)
    
    // pick two points at random
    
    // const buildings = (context, pairs) => {}
    
    // TODO - 2019-01-24 11:54 - EL - Animate these things to music somehow
    //   some kind of height / velocity - left to right mapping on pitch.

    generate.buildings(context, margin, width, height, 4)
    // generate.DarkNoise(context, width, height, background[1])

    console.log("> rendered!")

  };
};

canvasSketch(sketch, settings);
