const canvasSketch = require('canvas-sketch');
import BezierEasing from 'bezier-easing'
import simplexNoise from 'simplex-noise'

import {shuffle, lerp} from '../../utils/utils'
import { Flame } from '../../classes/flame'

const simplex = new simplexNoise()
  
var mouse = {
  x: 0,
  y: 0,
}

const img = new Image()
img.src = '../images/moon.png'

const settings = {
  dimensions: [ 2048, 2048 ],
  // dimensions: [ 1024, 1024 ],
  // dimensions: [ 1920, 1080 ],
  animate: true,
  canvas: document.createElement('canvas')
};

const sketch = ({context, width, height}) => {
  let step = 0.2
  let steps = 10
  let blockSize = width / steps
  let margin = 500

  document.body.style.backgroundColor = '#000'
  document.body.children[2]
  let imageData = []

  var flames = []

  img.onload = () => {
    console.log(img.naturalWidth)
    context.drawImage(img, 0,0,width,height)
    context.fillStyle = '#000000';
    
    for(let u = 0; u < 1; u+=step) {
      for(let v = 0; v < 1; v+=step) {
        let x = u * width
        let y = v * height
        let n = simplex.noise4D(u, v,x,y)

        let data = context.getImageData(x,y,1,1).data
        let color = {
          r: data[0],
          g: data[1],
          b: data[2],
          a: data[3],
        }
        let flame = new Flame({u,v,n,color,width})

        // imageData.push({
        //   i: imageData.length,
        //   n,
        //   u,
        //   v,
        //   x,
        //   y,
        //   data
        // })

        flames.push(flame)

      }
    }
    imageData = shuffle(imageData)
    context.fillRect(0,0,width,height)
  }

  return ({ time, context, width, height, frame }) => {
    let t = time * 0.1

    context.fillStyle = '#00000006';
    context.fillRect(0,0,width,height)

    if(imageData.length < 1) return
    
    flames.forEach(flame => {
      let {u,v,n,data} = flame
      let {move, draw} = flame
      move(t)
      draw(context)
    })

    imageData.forEach(pixel => {
      let {i,u,v,n,data} = pixel
      let vu = simplex.noise4D(u,n,i,t)
      let vv = simplex.noise4D(v,n,i,t)
      let x = width * Math.sin(vu / 2 + 0.5)
      let y = height * Math.cos(vv + 0.5)

      context.save()
      context.translate(
        Math.round(x),
        Math.round(y), 
      )

      let [r,g,b,a] = data
      context.strokeStyle = `rgba(
        ${r + 50},
        ${g + 50},
        ${b + 50},
        1
      )`
      context.beginPath()
      context.moveTo(0,0
      )

      context.lineTo(
        u * 10,
        v 
      )
      
      // pixel.u = simplex.noise2D(pixel.u,t)
      // pixel.v = simplex.noise2D(pixel.v,t)
      context.stroke()
      context.restore()
    })
  }
};

canvasSketch(sketch, settings);
