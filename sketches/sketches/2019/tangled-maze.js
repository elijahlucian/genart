const canvasSketch = require('canvas-sketch');
const random = require('canvas-sketch-util/random')
const {palettes} = require('../../modules/palettes')
// const gatewayLog = require('../../modules/gateway_log')

let seed 
// seed = "786270"

random.setSeed(seed || random.getRandomSeed())


const settings = {
  suffix: `__seed_${random.getSeed()}`,
  // dimensions: 'a4', // 
  // dimensions: [ 9, 16 ],
  // dimensions: [512, 512],
  // dimensions: [ 2048, 2048 ], // instagram
  // dimensions: [ 4096, 4096 ], // instagram
  // dimensions: [ 6500, 6500 ], // instagram
  dimensions: [ 7500, 10000 ], // instagram
  // orientation: 'landscape',
  // units: 'in',
  // pixelsPerInch: 300,
};

const sketch = () => {
  console.log("Random Seed =>",random.getSeed())
  
  const palette = random.pick(Object.values(palettes))
  const background = palette.splice(random.rangeFloor(0,palette.length),1)
  console.log("palette", palette)
  
  return ({ context, width, height }) => {
    console.log("background", background)
    context.fillStyle = background[0] 
    context.fillRect(0,0,width,height)


    context.lineCap = 'square';
    context.strokeStyle = random.pick(palette)
    
    const gridSize = 60 // 60

    const portrait = height > width
    const stepX = width / gridSize * (portrait ? 1 : height / width)
    const stepY = height / gridSize * (portrait ? width / height : 1)
    
    context.lineWidth = width / (stepX * 1.3)
    console.log("stuff",width, gridSize, stepX, stepY)

    for (let x = 0; x < width; x+= stepX) {
      for (let y = 0; y < height; y+= stepY) {

        if(random.value() > 0.5){
          context.moveTo(x, y)
          context.lineTo(x + stepX, y + stepY)
          
        } else {
          context.moveTo(x, y + stepY)
          context.lineTo(x + stepX, y)
        }
      }
      context.stroke()

    }

    
    console.log("> rendered!")

  };
};

canvasSketch(sketch, settings);
