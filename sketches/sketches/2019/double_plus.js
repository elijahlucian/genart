const canvasSketch = require('canvas-sketch');
const math = require('canvas-sketch-util/math')
const random = require('canvas-sketch-util/random')
const niceColorPalletes = require('nice-color-palettes')
const gatewayLog = require('../../data/gateway_log')

const generate = require('../../modules/generate')

console.log(gatewayLog.text.slice(0,5))

const settings = {
  // dimensions: 'a4', // 
  // dimensions: [ 9, 16 ],
  dimensions: [ 2048, 2048 ], // instagram
  // dimensions: [ 4096, 4096 ], // instagram
  // orientation: 'landscape',
  // units: 'in',
  // pixelsPerInch: 300,
};

const sketch = () => {

  let seed = "dank"
  const size = 20
  const randomRadius = true 
  const radius = 25

  palette = random.pick(niceColorPalletes)
  random.setSeed(seed)
  reducedPalette = palette.slice(0,3)
  background = palette.slice(3,5)
  
  
  const newSeed = (e) => {
    console.log(e)
    seed += e.key
    random.setSeed(seed)
    points = createGrid({
      size,
      randomRadius,
      radius,
    })
    window.requestAnimationFrame(() => {
      // console.log("a callback? wtf", seed)
    })
    e.preventDefault()
  }
  window.addEventListener("keydown", newSeed, false);
  
  const gradient = (context, start, end, width, height, left_x=0, top_y=0) => { 
    fill = context.createLinearGradient(left_x, top_y, width, height)
    fill.addColorStop(0, start)
    fill.addColorStop(1, end)
    return fill
  }

  const lerp = (min, max, t) => { 
    return (max - min) * t + min
  }

  const createGrid = (args) => {
    const points = []
    const size = args.size
    
    for (let y = 0; y < size; y++) {
      for (let x = 0; x < size; x++) {
    
        const u = x / (size - 1)
        const v = y / (size - 1)
        const radius = Math.max(0, random.noise2D(u,v) + 1) * 0.01
        
        points.push({
          color: random.pick(reducedPalette),
          position: {u, v},
          radius,
        })
      }
    }
    return points
  }

  points = createGrid({ size, randomRadius, radius })//.filter(() => random.value() > 0.5)
  
  return ({ context, width, height }) => {

    let state = {
      rotation: 0
    }

    const margin = width / 12
    let count = 0
    // context.fillStyle = gradient(context, '#f4225a', '#f4b800', width/3, height)
    context.fillStyle = random.pick(background) // 'white'
    context.fillRect(0,0,width,height)
    
    context.save()
    
    points.forEach(point => {
      
      let x = math.lerp(margin, width - margin, point.position.u)
      let y = math.lerp(margin, height - margin, point.position.v)
      let fontSize = point.radius * width * 10

      // TODO - 2019-01-23 16:09 - EL - Idea!
      // DIYarc(context, x, y, 0, Math.PI *2, true, 'white') 
      context.fillStyle = point.color
      context.font = `${fontSize}px "Inconsolata"` // `${margin/10}px`
      
      state.rotation += Math.PI/500
      context.rotate(state.rotation)
      context.rotate(state.rotation)
      
      context.fillText("+", x-fontSize/6, y + fontSize/4)
      count += 1
    });

    context.restore()
    
    generate.darkNoise({document, mainContext: context, width, height})

    console.log("DONESKI!")

  };
};

canvasSketch(sketch, settings);
