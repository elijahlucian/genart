const canvasSketch = require('canvas-sketch');
import BezierEasing from 'bezier-easing'
import simplexNoise from 'simplex-noise'

// import { Flame } from '../../classes/flame'
import {shuffle, lerp} from '../../modules/utils'

const simplex = new simplexNoise()

var mouse = {
  x: 0,
  y: 0,
}

const img = new Image()
img.src = '../images/moon.png'

const settings = {
  dimensions: [ 2048, 2048 ],
  // dimensions: [ 1024, 1024 ],
  // dimensions: [ 1920, 1080 ],
  animate: true,
  canvas: document.createElement('canvas')
};

const sketch = ({context, width, height}) => {
  let step = 0.1
  let steps = 10
  let blockSize = width / steps
  let margin = 500

  document.body.style.backgroundColor = '#000'
  document.body.children[2]
  let imageData = []

  const onMouseMove = (event) => {
    mouse.x = event.clientX - width / 2
    mouse.y = event.clientY - height / 2
  }

  window.addEventListener('mousemove', onMouseMove)

  class Flame {
    constructor(u,v,n,color,i) {
      this.i = i
      this.u = u
      this.v = v
      this.x = u * width
      this.y = v * width
      this.n = n
      this.color = color
      this.style = `rgba(
        ${color.r},
        ${color.g},
        ${color.b},
        1
      )`
      this.state = {}
    
      this.move = (t) => {
        this.x = (simplex.noise3D(this.u, i, t) + 1) / 2 * width
        this.y = (simplex.noise3D(this.v, i, t) + 1) / 2 * height
      }

      this.draw = (context) => {
        let {r,g,b} = this.color
        let {u,v,x,y} = this
        
        context.save()
        context.translate(x,y)
        context.lineWidth = 1
        // context.strokeStyle = `rgba(${r},${g},${b},1)`
        context.moveTo(0,0)
        context.lineTo(
          u * 100,
          v * 100,
        )
        context.stroke()
        context.restore()
      }
    }
  }

  var flames = []

  img.onload = () => {
    console.log(img.naturalWidth)
    context.drawImage(img, 0,0,width,height)
    context.fillStyle = '#000000';
    
    for(let u = 0; u < 1; u+=step) {
      for(let v = 0; v < 1; v+=step) {
        let i = flames.length
        let x = u * width
        let y = v * height
        let n = simplex.noise4D(u,v,x,y)

        let data = context.getImageData(x,y,1,1).data
        let color = {
          r: data[0],
          g: data[1],
          b: data[2],
          a: data[3],
        }
        let flame = new Flame(u,v,n,color,i)
        flames.push(flame)
      }
    }
    console.log(flames)
    imageData = shuffle(imageData)
    context.fillRect(0,0,width,height)
  }

  let lastLine = {
    x: width/2,
    y: height/2,
  }
  
  return ({ time, context, width, height, frame }) => {
    let t = time * 0.05

    context.fillStyle = '#0000003f';
    context.fillRect(0,0,width,height)

    if(flames.length < 1) return

    flames.forEach(flame => {
      let {x,y} = flame 
      flame.move(t)
      context.strokeStyle = flame.style
      context.beginPath()
      context.moveTo(x,y)
      context.lineWidth = 10
      // context.lineTo(lastLine.x, lastLine.y)
      context.quadraticCurveTo(x,y,lastLine.x,lastLine.y)
      context.stroke()
      lastLine = {
        x,
        y,
      }
    })
  }
};

canvasSketch(sketch, settings);
