const canvasSketch = require('canvas-sketch')
const random = require('canvas-sketch-util/random')

const BezierEasing = require('bezier-easing')
const { palettes } = require('../../modules/nice-color-palettes')
const { paletteSeeds } = require('../../seeds/palettes')

const utils = require('../../modules/utils')
const { hsl, rgbToHSL } = require('../../modules/color')
const { PI, TAU } = require('../../modules/maf')
const { BPM, Tempo, musicalTime } = require('../../classes/music')
const { dimensions } = require('../../modules/dimensions')

const { Vine } = require('../../classes/shapes')
const { Img, Mouse, Keyboard, Controls } = require('../../classes/utils')
const { AudioAnalyser, AudioLoader } = require('../../classes/audio')
const { Caption } = require('../../classes/typeface')
// GLOBALS
var zeroOffset = 0
var dt = 0

const bpm = new BPM({
  bpm: 105,
})

const bez = new BezierEasing(0.22, 0.92, 0.73, 0.63)

let seed = {
  // palette: '314962',
  // determined: 'dank',
  random: Date.now(), //random.getRandomSeed()
}

// let img = new Img({src: '../images/mountains-06.png'})

// paletteSeed
random.setSeed(seed.palette || seed.defined || seed.random)
console.log('Palette Seed =>', random.getSeed())

const palette = random.pick(palettes)
const bg = palette.shift()
const mainColor = random.pick(palette)

// algoSeed
random.setSeed(seed.determined || seed.random)
console.log('Algo Seed =>', random.getSeed())

document.body.style.backgroundColor = '#000' // bg
document.title = 'JS Sketches'

// const canvasSize = 2048
const settings = {
  suffix: `__seed_${random.getSeed()}`,
  // dimensions: [canvasSize,canvasSize],
  dimensions: dimensions.square.insta,
  // orientation: 'landscape',
  fps: 23,
  // units: 'in'
  // pixelsPerInch: 300,
  animate: true,
}

// TODO: WEBCAM
// TODO: Image Loader

const sketch = params => {
  console.log('SKETCH PARAMS =>', params)
  let { context, width, height } = params

  context.lineCap = 'round' // || "round" || "square" || "butt"
  // context.lineJoin = 'round' // || "bevel" || "miter" || "square"

  const longEdge = utils.getLongEdgeFromArray([width, height])

  const config = {
    speed: 0.7,
    agent_count: 1,
    agent_limit: 1000,
    trails: true,
    margin: longEdge / 20,
    lineWidth: longEdge / 150,
    shapeSize: longEdge / 80,
    bandwidthMin: 120,
    bandwidthMax: 10000,
    audio: {
      gain: -0.8,
    },
  }

  var mouse = new Mouse({ width, height })
  var controls = new Controls()

  var caption = new Caption({
    font: 'Inconsolata',
    size: 6,
    top: height * 0.95,
    left: width * 0.01,
    // caption: 'just some drums and keys',
    color: random.pick(palette),
  })

  var shapes = []
  console.log('MAINCOLOR', mainColor)

  for (let i = 0; i < config.agent_count; i++) {
    let z = i / config.agent_count - 1
    shapes.push(
      new Vine({
        i,
        z,
        u: 0.33,
        v: 1,
        angle: 0,
        color: mainColor, // random.pick(palette),
        baseSize: config.shapeSize,
        life: 30,
      })
    )
  }

  let foreground = random.pick(palette)

  caption.update({ time: 0, width, height })
  caption.draw({ context })

  return ({ time, context, width, height }) => {
    // context.strokeStyle = foreground
    context.fillStyle = '#000000' // + (config.trails ? '33' : 'ff')
    context.fillRect(0, 0, width, height)

    let last = { x: 0, y: width / 2 }

    for (let u = -0.1; u <= 1.1; u += 0.005) {
      let s = Math.sin(1 * time + u * TAU * 4 + random.noise1D(u * time * 0.1))
      let x = (u * height * s) / 4 + height / 2
      let y =
        (Math.sin(2 * time + u * TAU) * 200 + u * width) * 0.6 + width * 0.2
      if (u < 0) {
        last = { x, y }
        continue
      }

      // context.lineWidth = 100 * u * u
      context.fillStyle = hsl(
        random.noise2D(u, time) * s * 0.1 * 200 + 10,
        100,
        50
      )
      let size = (width / 20) * u * random.noise1D(u + time * 0.3)
      context.save()
      context.translate(x, y)
      context.rotate(random.noise2D(x / 1000, y / 1000))
      context.fillRect(-size, -size, size, size)
      context.restore()
      // context.beginPath()
      // context.moveTo(last.y - 10, last.x)
      // context.lineTo(y, x)
      context.stroke()

      last = { x, y }
    }

    bpm.update({ time })

    let { b, q } = bpm.getMusicalTime()
    let beats = { b, q }

    caption.update({ time, width, height })

    dt = time

    const t = time * config.speed

    shapes.forEach((shape, i) => {
      shape.update({ time, width, height })
      shape.draw({ context })
    })
    // caption.draw({ context });
  }
}

canvasSketch(sketch, settings)
