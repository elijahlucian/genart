const canvasSketch = require('canvas-sketch')
const math = require('canvas-sketch-util/math')
const random = require('canvas-sketch-util/random')
const niceColorPalletes = require('nice-color-palettes')
import { palettes } from '../../modules/palettes'

const seeds = {
  deep: '218696',
  pink: '562582',
  architect: '815285',
  darkArchitect: '945348',
}

// random.setSeed(random.getRandomSeed())
random.setSeed(seeds.darkArchitect)

const settings = {
  suffix: `__seed_${random.getSeed()}`,
  // dimensions: 'a4', //
  // dimensions: [ 9, 16 ],
  dimensions: [2048, 2048], // instagram
  // dimensions: [ 4096, 4096 ], // instagram
  // dimensions: [ 6500, 6500 ], // instagram
  // dimensions: [ 11000, 11000 ], // instagram
  // orientation: 'landscape',
  // units: 'in',
  // pixelsPerInch: 300,
}

const sketch = ({ width, height }) => {
  const size = 80
  const randomRadius = true
  const radius = 25

  console.log('Random Seed =>', random.getSeed())

  const palette = random.pick(Object.values(palettes))
  // const palette = random.pick(niceColorPalletes)
  const randomIndex = random.rangeFloor(0, palette.length)
  console.log('randomIndex', randomIndex)
  console.log('palette', palette)

  const background = palette.splice(randomIndex, 1)

  const createGrid = (args) => {
    const points = []
    const size = args.size
    console.log(width)
    const stepX = 2048 / width
    const stepY = 2048 / height

    for (let y = 0; y < size; y += stepY) {
      for (let x = 0; x < size; x += stepX) {
        const u = x / (size - 1)
        const v = y / (size - 1)
        const radius = Math.max(0, random.noise2D(u, v) + 1) * 0.01

        points.push({
          color: random.pick(palette),
          position: { u, v },
          radius,
          rotation: random.noise2D(u, v),
        })
      }
    }
    return points
  }

  const points = createGrid({ size, randomRadius, radius }).filter(
    () => random.value() > 0.8,
  )

  return ({ context, width, height }) => {
    let state = {
      rotation: 0,
    }

    const margin = 49 // width / 8
    // context.fillStyle = gradient(context, background[0], background[1], width/3, height)
    context.fillStyle = background // 'white'
    context.fillRect(0, 0, width, height)

    for (const point of points) {
      let x = math.lerp(margin, width - margin, point.position.u)
      let y = math.lerp(margin, height - margin, point.position.v)

      let fontSize = point.radius * width * 10
      let length = 202

      // TODO - 2019-01-23 16:09 - EL - Idea!
      // DIYarc(context, x, y, 0, Math.PI *2, true, 'white')
      context.fillStyle = point.color
      context.font = `${fontSize}px "Inconsolata"` // `${margin/10}px`

      context.save()
      context.translate(Math.round(x), Math.round(y))

      state.rotation += Math.PI / 3.14

      // context.rotate(point.rotation)
      random.value() > 0.5
        ? context.fillRect(-length / 2 - 1, 0, length + 4, 2)
        : context.fillRect(0, -length / 2 - 1, 2, length + 4)

      context.restore()
    }

    // context.restore()

    // generate.DarkNoise(context, width, height, background[1])

    console.log('> rendered!')
  }
}

canvasSketch(sketch, settings)
