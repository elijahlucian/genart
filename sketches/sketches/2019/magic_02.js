
const canvasSketch = require('canvas-sketch');
const random = require('canvas-sketch-util/random')
const palettes = require('nice-color-palettes')
palette = random.pick(palettes)
const glslify = require('glslify')

global.THREE = require('three');


require('three/examples/js/controls/OrbitControls');

const frag = glslify(/* glsl */`

varying vec2 vUv;
varying vec3 v_normal;
uniform float color;
uniform float time;

void main () {


  gl_FragColor = vec4(vec3(sin(time)), 0.5);
}
`);

const vert = glslify(/* glsl */`
#pragma glslify: snoise3 = require(glsl-noise/simplex/3d);
varying vec2 vUv;
varying vec3 v_normal;
uniform float time;
uniform float noise;

uniform float displacement;

void main () {
  v_normal = normal;

  vec3 newPosition = position + normal * vec3(displacement);

  gl_Position = projectionMatrix * modelViewMatrix * vec4(vec3(newPosition.xyz), 1.0);
}
`);

const settings = {
  // Make the loop animated
  animate: true,
  dimensions: [ 512, 512 ], 
  // Get a WebGL canvas rather than 2D
  context: 'webgl',
  // Turn on MSAA
  attributes: { antialias: true }
};

const makeShaderBox = (size, index, x, y) => {
  const loader = new THREE.TextureLoader();
  const mesh = new THREE.Mesh(
    new THREE.BoxGeometry(size,size,size),
    new THREE.ShaderMaterial({
      fragmentShader: frag,
      vertexShader: vert,
      uniforms: {
        time: { value: index }
      }
    })
  )
  return mesh
}

const getCamera = (kind="perspective") => {
  switch (key) {
    case 'orthagonal':
      let orth = new THREE.OrthographicCamera(
        l,r,t,b, 1, 1000
      )
      orth.position.set(0, 0, 5);
      return orth
    case 'perspective':
      let pers = new THREE.PerspectiveCamera(
        45, 1, 1, 1000
      )
      pers.position.set(5, 5, 15);
      return pers
    default:
    return null
  }
}

const makeShaderSphere = (size, index, x, y) => {
  const loader = new THREE.TextureLoader();
  var uniforms = {
    displacement: { type: 'f', value: [] },
    time: { value: index },
  }

  var sphere = new THREE.Mesh(
    new THREE.SphereGeometry( size, 32, 32 ),
    new THREE.ShaderMaterial({
      fragmentShader: frag,
      vertexShader: vert,
      uniforms,
    })
   );
   return sphere
}

const makeBox = (size) => {
  const mesh = new THREE.Mesh(
    new THREE.BoxGeometry(size, size, size),
    new THREE.MeshPhysicalMaterial({
      color: 'white',
      roughness: 0.75,
      flatShading: true
    })
  );
  return mesh;
}

const makeHeartShape = () => {
  var heartShape = new THREE.Shape();

  heartShape.moveTo( 2.5, 2.5 );
  heartShape.bezierCurveTo( 2.5, 2.5, 2.0, 0, 0, 0 );
  heartShape.bezierCurveTo( 3.0, 0, 3.0, 3.5,3.0,3.5 );
  heartShape.bezierCurveTo( 3.0, 5.5, 1.0, 7.7, 2.5, 0.95 );
  heartShape.bezierCurveTo( 6.0, 7.7, 8.0, 5.5, 8.0, 3.5 );
  heartShape.bezierCurveTo( 8.0, 3.5, 8.0, 0, 5.0, 0 );
  heartShape.bezierCurveTo( 3.5, 0, 2.5, 2.5, 2.5, 2.5 );

  var extrudeSettings = { depth: 0.8, bevelEnabled: true, bevelSegments: 2, steps: 2, bevelSize: 1, bevelThickness: 1 };

  var geometry = new THREE.ExtrudeGeometry( heartShape, extrudeSettings );

  var mesh = new THREE.Mesh( geometry, new THREE.MeshPhongMaterial() );
  return mesh
}

const sketch = ({ context }) => {
  // Create a renderer
  const renderer = new THREE.WebGLRenderer({
    context
  });
  
  // WebGL background color
  backgroundColor = random.pick(palette)
  // renderer.setClearColor(random.pick(palette), 0); // transparent
  renderer.setClearColor(backgroundColor, 1);
  renderer.setClearColor('black',1);

  // Setup a camera
  // const camera = new THREE.PerspectiveCamera(45, 1, 0.01, 100);
  
  // const l,r,t,b = [-5,5,5,-5]

  let l = -5
  let r = 5
  let t = 5
  let b = -5
  const origin = new THREE.Vector3()

  // Audio Listener

  var listener = new THREE.AudiooListener()

  // Camera Settings

  const camera = getCamera('perspective');
  
  camera.lookAt(origin);
  camera.add(listener)
  
  var sound = new THREE.Audio( listener )

  var audioLoader = new THREE.audioLoader()
  audioLoader.load( 'bazanzansooka.mp3', ( buffer ) => {
    sound.setBuffer( buffer );
    sound.setLoop( true );
    sound.setVolume( 0.5 );
    sound.play();
  })

  // Setup camera controller
  const controls = new THREE.OrbitControls(camera);

  // Setup your scene
  const scene = new THREE.Scene();

  const backdrop = new THREE.Mesh(
    new THREE.BoxGeometry(10,10,1),
    new THREE.MeshBasicMaterial({
      color: backgroundColor,
      reflectivity: 0,
      // roughness: 1,
      // flatShading: false,
    })
  )
  backdrop.position.set(0,0,-5)
  // scene.add(backdrop)

  const shaderBoxes = []
  const meshes = []

  let gridSize = 3

  for (let x = -gridSize; x <= gridSize; x++) {
    for (let y = -gridSize; y <= gridSize; y++) {
      let index = shaderBoxes.length
      // let mesh = makeShaderSphere(0.5, index, x, y)
      let mesh = makeShaderBox(0.5, index, x ,y)
      // let mesh = makeBox(0.75)

      mesh.i = index
      mesh.x = x
      mesh.y = y
      mesh.noise = random.noise2D(x,y)
      mesh.rand = random.gaussian()
      mesh.position.set(x,y,0)

      mesh.rotation.x = Math.PI / 7 + random.noise1D(x) + random.noise1D(y)
      mesh.rotation.y = mesh.noise * 0.1
      
      if(random.value() > 0.5) {
        meshes.push(mesh)
        scene.add(mesh)
      }

    }
  }
  
  const heart = makeHeartShape()
  heart.rotation.z = Math.PI
  heart.position.z = 3
  heart.scale.set(0.25,0.25,0.25)
  // scene.add(heart)

  // Specify an ambient/unlit colour
  scene.add(new THREE.AmbientLight(random.pick(palette)));

  // Add some light
  // const light = new THREE.PointLight('#45caf7', 1, 15.5);
  const light = new THREE.PointLight(random.pick(palette), 1, 15.5, 2);
  light.position.set(3, 3, 5).multiplyScalar(1.5);
  scene.add(light);

  // draw each frame
  return {
    // Handle resize events here
    resize ({ pixelRatio, viewportWidth, viewportHeight }) {
      renderer.setPixelRatio(pixelRatio);
      renderer.setSize(viewportWidth, viewportHeight);
      camera.aspect = viewportWidth / viewportHeight;
      camera.updateProjectionMatrix();
    },
    // Update & render your scene here
    render ({ time }) {
      console.log(sound)
      // light.power = Math.max(Math.sin(time * Math.PI / 2) * 4 * Math.PI, 0)

      backdrop.position.z = Math.sin(time) * 0.2 - 0.4

      shaderBoxes.forEach(box => {

      })

      meshes.forEach(mesh => {
        // mesh.scale.z = Math.abs(Math.cos(mesh.i + time + mesh.noise) * 1)
        // mesh.scale.y = random.noise2D(mesh.noise, time * 0.1)
        mesh.material.uniforms.time.value = (mesh.i + time)
        if (mesh.i === 0) console.log(mesh)

        mesh.position.z = random.noise2D(mesh.noise, time * 0.1) * 0.4
        mesh.rotation.y += Math.abs(random.noise2D(mesh.noise + mesh.i, time * 0.05) * 0.05) // * Math.PI
        mesh.rotation.x += Math.sin(mesh.i + time * 0.3 + mesh.rand) * 0.01
      })

      controls.update();
      renderer.render(scene, camera);
    },
    // Dispose of events & renderer for cleaner hot-reloading
    unload () {
      controls.dispose();
      renderer.dispose();
    }
  };
};

canvasSketch(sketch, settings);
