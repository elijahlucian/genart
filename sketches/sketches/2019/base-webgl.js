
const canvasSketch = require('canvas-sketch');
const random = require('canvas-sketch-util/random')

const palettes = require('nice-color-palettes')

palette = random.pick(palettes)

// Ensure ThreeJS is in global scope for the 'examples/'
global.THREE = require('three');

// Include any additional ThreeJS examples below
require('three/examples/js/controls/OrbitControls');

const settings = {
  // Make the loop animated
  animate: true,
  dimensions: [ 6500, 6500 ], 
  // Get a WebGL canvas rather than 2D
  context: 'webgl',
  // Turn on MSAA
  attributes: { antialias: true }
};

const makeBox = (size) => {
  const mesh = new THREE.Mesh(
    new THREE.BoxGeometry(size, size, size),
    new THREE.MeshPhysicalMaterial({
      color: 'white',
      roughness: 0.75,
      flatShading: true
    })
  );
  return mesh;
}

const sketch = ({ context }) => {

  
  // Create a renderer
  const renderer = new THREE.WebGLRenderer({
    context
  });
  
  // WebGL background color
  renderer.setClearColor(random.pick(palette), 1);
  // renderer.setClearColor(random.pick(palette), 0); // transparent

  // Setup a camera
  // const camera = new THREE.PerspectiveCamera(45, 1, 0.01, 100);
  
  // const l,r,t,b = [-5,5,5,-5]

  let l = -5
  let r = 5
  let t = 5
  let b = -5
  const origin = new THREE.Vector3()

  // const camera = new THREE.PerspectiveCamera(
  //   45, 1, 1, 1000
  // )
  // camera.position.set(3, 3, 10);
  
  const camera = new THREE.OrthographicCamera(
    l,r,t,b, 1, 1000
  )
  camera.position.set(0, 0, 5);

  camera.lookAt(origin);

  // Setup camera controller
  const controls = new THREE.OrbitControls(camera);

  // Setup your scene
  const scene = new THREE.Scene();

  const backdrop = new THREE.Mesh(
    new THREE.BoxGeometry(10,10,1),
    new THREE.MeshBasicMaterial({
      color: random.pick(palette),
      reflectivity: 0,
      // roughness: 1,
      // flatShading: false,
    })
  )
  backdrop.position.set(0,0,-5)
  scene.add(backdrop)

  const meshes = []
  for (let x = -3; x <= 3; x++) {
    for (let y = -3; y <= 3; y++) {
      let mesh = makeBox(0.75)
      mesh.noise = random.noise2D(x,y)
      mesh.rand = random.gaussian()
      mesh.position.set(x,y,0)
      mesh.rotation.x -= Math.PI / 7 + random.noise1D(x) + random.noise1D(y)
      meshes.push(mesh)
      scene.add(mesh)   
    }
  }
  

  // Specify an ambient/unlit colour
  scene.add(new THREE.AmbientLight(random.pick(palette)));

  // Add some light
  // const light = new THREE.PointLight('#45caf7', 1, 15.5);
  const light = new THREE.PointLight(random.pick(palette), 1, 15.5, 2);
  light.position.set(3, 3, 5).multiplyScalar(1.5);
  scene.add(light);

  // draw each frame
  return {
    // Handle resize events here
    resize ({ pixelRatio, viewportWidth, viewportHeight }) {
      renderer.setPixelRatio(pixelRatio);
      renderer.setSize(viewportWidth, viewportHeight);
      camera.aspect = viewportWidth / viewportHeight;
      camera.updateProjectionMatrix();
    },
    // Update & render your scene here
    render ({ time }) {
      time = 0.5
      light.power = Math.max(Math.sin(time * Math.PI / 2) * 4 * Math.PI, 0)

      backdrop.position.z = Math.sin(time) * 0.4 - 0.3
      meshes.forEach(mesh => {
        mesh.scale.z = Math.cos(time + mesh.noise) * 1
        mesh.position.z = Math.sin(random.noise1D(mesh.noise) + time) * 0.4
        mesh.rotation.y = Math.sin(time) * (10 * Math.PI / 180)
      })

      controls.update();
      renderer.render(scene, camera);
    },
    // Dispose of events & renderer for cleaner hot-reloading
    unload () {
      controls.dispose();
      renderer.dispose();
    }
  };
};

canvasSketch(sketch, settings);
