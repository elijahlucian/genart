const canvasSketch = require('canvas-sketch');
const palettes = require('nice-color-palettes')
const random = require('canvas-sketch-util/random')

const palette = random.pick(palettes)

console.log(palette)

// Ensure ThreeJS is in global scope for the 'examples/'
global.THREE = require('three');

// Include any additional ThreeJS examples below
require('three/examples/js/controls/OrbitControls');

const settings = {
  // Make the loop animated
  animate: true,
  // Get a WebGL canvas rather than 2D
  context: 'webgl',
  // Turn on MSAA
  attributes: { antialias: true }
};

const params = {
  width: 10,
  height: 10,
  depth: 10,
  get offsetX() { return this.width / 2 },
  get offsetY() { return this.height / 2 },
  get offsetZ() { return this.depth / 2 },
  spacingX: 0.5,
}

const simpleBox = (vec3) => {
  let mesh = new THREE.Mesh(
    new THREE.BoxGeometry(.3, .3, .3),
    new THREE.MeshPhysicalMaterial({
      color: 'white',
      roughness: 0.75,
      flatShading: true
    })
  )
  mesh.position.copy(vec3)
  return mesh
}

const drawSphere = () => {
  var geometry = new THREE.SphereGeometry( 0.25, 32, 32 );
  var material = new THREE.MeshBasicMaterial( {color: '#ffffff'} );
  var sphere = new THREE.Mesh( geometry, material );
  return sphere
}

const drawBox = (params, i,j,k) => {
  
  let mesh = new THREE.Mesh(
    new THREE.BoxGeometry(.5, .5, .5),
    new THREE.MeshPhysicalMaterial({
      color: 'white',
      roughness: 0.75,
      flatShading: true
    })
  );

  // let variance = rando/m.noise3D(i,j,k) * 0.2

  mesh.i = i
  mesh.j = j
  mesh.k = k
  mesh.v3 = THREE.Vector3(i,j,k)
  mesh.position.set(
    i - params.offsetX, // * (i - params.offsetX),
    j - params.offsetY,
    k - params.offsetZ
  )
  mesh.rotation.x = random.noise1D(i/7) / 2
  mesh.rotation.y = random.noise1D(j/7) / 2
  // mesh.rotation.z = random.noise1D(k/7) / 2
  return mesh
}

const sketch = ({ context }) => {
  // Create a renderer
  const renderer = new THREE.WebGLRenderer({
    context
  });

  // WebGL background color
  renderer.setClearColor(random.pick(palette), 1);

  // Setup a camera
  const camera = new THREE.PerspectiveCamera(45, 1, 0.01, 100);
  camera.position.set(14, 14, 15);
  camera.lookAt(new THREE.Vector3());

  // Setup camera controller
  const controls = new THREE.OrbitControls(camera);

  // Setup your scene
  const scene = new THREE.Scene();
  const meshes = []

  for (let i = 0; i <= params.width; i++) {
    for (let j = 0; j <= params.height; j++) {
      for (let k = 0; k <= params.depth; k++) {
        
        mesh = drawBox(params,i,j,k)
        meshes.push(mesh)
        scene.add(mesh);
      }
    }
  }

  // Specify an ambient/unlit colour
  scene.add(new THREE.AmbientLight(random.pick(palette), 0.9));
  scene.add(new THREE.DirectionalLight(random.pick(palette), 0.5));

  // Add some light
  const lightBack = new THREE.PointLight(random.pick(palette), 1, 35.5);
  lightBack.position.set(6, -6, 0).multiplyScalar(1.5);
  // scene.add(lightBack);
  
  lightpos = new THREE.Vector3(-4,6,-5)
  // scene.add(simpleBox(lightpos))

  const floatyLight = new THREE.PointLight(random.pick(palette), 1, 9);
  floatyLight.position.copy(lightpos).multiplyScalar(1.5);
  scene.add(floatyLight);

  let sphere = drawSphere()
  scene.add(sphere)

  // draw each frame
  return {
    // Handle resize events here
    resize ({ pixelRatio, viewportWidth, viewportHeight }) {
      renderer.setPixelRatio(pixelRatio);
      renderer.setSize(viewportWidth, viewportHeight);
      camera.aspect = viewportWidth / viewportHeight;
      camera.updateProjectionMatrix();
    },
    // Update & render your scene here
    render ({ time }) {

      meshes.forEach(mesh => {
        // mesh.rotation.y += 0.01 //* (10 * Math.PI / 180);
        // mesh.rotation.x += 0.01
        // mesh.rotation.z += 0.01
        // if(mesh.i === 1) {
        // }
        let lightDiff = mesh.position.distanceTo(floatyLight.position)
        let scale = lightDiff > 2 ? (lightDiff - 2) * 0.2 : 0
        mesh.scale.x = scale // Math.abs(lightDiff - 10)
        mesh.scale.y = scale // Math.abs(lightDiff - 10)
        mesh.scale.z = scale // Math.abs(lightDiff - 10)
        // mesh.rotation.y = scale
      });

      floatyLight.position.y = Math.sin(time) * 5
      floatyLight.position.x = Math.sin(time / 3) * 6
      
      let v3 = new THREE.Vector3(
        Math.sin(time * 0.3) * 6,
        Math.sin(time * 0.5) * 6,
        Math.sin(time) * 6
      )

      floatyLight.position.copy(v3)
      sphere.position.copy(v3)

      // lightWhite.position.x = Math.sin(time * 0.3) * 6
      // lightWhite.position.y = Math.sin(time * 0.5) * 5
      // lightWhite.position.z = Math.sin(time) * -5

      controls.update();
      renderer.render(scene, camera);
    },
    // Dispose of events & renderer for cleaner hot-reloading
    unload () {
      controls.dispose();
      renderer.dispose();
    }
  };
};

canvasSketch(sketch, settings);
