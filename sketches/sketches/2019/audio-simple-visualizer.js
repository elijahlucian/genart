const canvasSketch = require('canvas-sketch');
var createAnalyser = require('web-audio-analyser')
var random = require('canvas-sketch-util/random')
var palettes = require('nice-color-palettes')
var audioContext, analyser, audioUtil
var average = require('analyser-frequency-average')
global.THREE = require('three')
var OBJLoader = require('three-obj-loader')(THREE)
  , EffectComposer = require('three-effectcomposer')(THREE)

var threeAddons = require('three-addons')

const palette = random.pick(palettes)

console.log("PALETTE > ", palette)
const settings = {
  animate: true,
  fps: 1,
  dimensions: [ 1920, 1080 ], 
  context: 'webgl',
  attributes: { antialias: true }
};

var lastFrame = {
  lastLow: 0.2,
  lastMid: 0.2,
  lastHigh: 0.2,
}

const sketch = ({ context }) => {


  const renderer = new THREE.WebGLRenderer({
    context
  });

  // renderer.setClearColor(random.pick(palette), 1);
  renderer.setClearColor('#000000', 1);
  const origin = new THREE.Vector3()

  let camera = new THREE.PerspectiveCamera(
    45, 45, 1, 1000
  )
  camera.position.set(0, 5, 5);
  camera.lookAt(origin);

  const scene = new THREE.Scene();

  // const mesh = new THREE.Mesh(
  //   new THREE.BoxGeometry(7, 1, 1),
  //   new THREE.MeshPhysicalMaterial({
  //     color: random.pick(palette),
  //     flatShading: true
  //   })
  // )
  // scene.add(mesh)

  var planet = new THREE.Mesh(
    new THREE.SphereGeometry( 1, 6, 6 ),
    new THREE.MeshPhongMaterial({     
      color: random.pick(palette),
      flatShading: true
    } )
  )
  planet.position.z = -5
  planet.position.y = -2
  scene.add(planet)

  // mesh.position.set(0, -3.5, 0)
   
  var vapsquadTitle, vapsquadSpaceship
  var objLoader  = new THREE.OBJLoader();

  objLoader.load('objects/voice_acting_power_squad_extruded.obj', event => {
    let scale = 0.5
    vapsquadTitle = event.children[0]
    vapsquadTitle.material.color.r = 0.1
    vapsquadTitle.scale.set(scale,scale,scale)
    vapsquadTitle.position.set(-3,-2.7,0)
    vapsquadTitle.phyiscs = {}

    scene.add(vapsquadTitle)
  })

  objLoader.load('objects/vapsquad-spaceship.obj', event => {
    console.log(event)
    let scale = 0.1
    
    vapsquadSpaceship = event.children[0]
    vapsquadSpaceship.scale.set(scale,scale,scale)
    // vapsquadSpaceship.position.z = -5
    // vapsquadSpaceship.position.y = -2
    vapsquadSpaceship.rotation.y = Math.PI * 0.1
    vapsquadSpaceship.scale.set(scale,scale,scale)
    vapsquadSpaceship.phyiscs = {
      state: 0,
      height: 0,
    }
    
    scene.add(vapsquadSpaceship)
  })
  
  scene.add(new THREE.AmbientLight("#ffffff", 0.1));

  const light = new THREE.PointLight("#ffffff", 2);
  light.position.set(15, 20, -5)
  light.lookAt(origin)
  scene.add(light);

  navigator.mediaDevices.getUserMedia( {audio:true})
  .then( stream => {
    audioContext = new AudioContext()
  
    var mediaStreamSource = audioContext.createMediaStreamSource(stream)
    mediaStreamSource.connect(audioContext.destination)

    audioUtil = createAnalyser(mediaStreamSource, audioContext, {mute: true}) 

    analyser = audioUtil.analyser
    analyser.fftSize = 256
    analyser.smoothingTimeConstant = 0.7
      // 0 = rage
      // 0.7 = fun
      // 2.2 = chill
    console.log(analyser)
  })

  return {
    resize ({ pixelRatio, viewportWidth, viewportHeight }) {
      renderer.setPixelRatio(pixelRatio);
      renderer.setSize(viewportWidth, viewportHeight);
      camera.aspect = viewportWidth / viewportHeight;
      camera.updateProjectionMatrix();
    },

    render ({ time }) {
      if(!analyser || !vapsquadTitle) return
      // wrap this in a class

      let sin = Math.sin(time)

      let low = average(analyser, audioUtil.frequencies(), 50, 350)
      let mid = average(analyser, audioUtil.frequencies(), 400, 2000)
      let high = average(analyser, audioUtil.frequencies(), 2500, 15000)
      
      let breath = (sin + 1) * 0.1

      low = (low > 0.2 ? (low + lastFrame.low) * 0.5 : 0.2) 
      mid = (mid > 0.2 ? (mid + lastFrame.mid) * 0.5 : 0.2) 
      high = (high > 0.2 ? (high + lastFrame.high) * 0.5 : 0.2) 

      lastFrame.low = low
      lastFrame.mid = mid
      lastFrame.high = high

      // mesh.rotation.x = Math.PI * 0.25
      // mesh.scale.y = 3 * mid
      // vapsquadTitle.position.z = mid * 1.6 + 0.6
      // vapsquadTitle.position.y = mid
      // vapsquadTitle.rotation.z = Math.sin(time)  

      vapsquadTitle.scale.y = high * 10
      vapsquadTitle.position.y = -5 + high
      vapsquadTitle.position.z = 0
      vapsquadTitle.position.x = -6.5
      vapsquadTitle.material.color.r = low
      vapsquadTitle.material.color.g = mid
      vapsquadTitle.material.color.b = high
      vapsquadTitle.rotation.x = Math.PI * 0.25

      if(low > vapsquadSpaceship.phyiscs.height) vapsquadSpaceship.phyiscs.height = low
      let heightOffset = vapsquadSpaceship.phyiscs.height / 2
      
      // TODO: somehow add physics to objects.
      let topX = 6.5
      let topY = -0.5
      planet.rotation.y += 0.02 //+ mid * 0.05 + (low - 0.2) * 0.02
      planet.position.x = topX
      planet.position.y = topY
      planet.scale.set(2,2,2).multiplyScalar(low * 0.2)

      vapsquadSpaceship.position.x = sin * (vapsquadSpaceship.phyiscs.height + heightOffset) + topX
      vapsquadSpaceship.position.z = Math.cos(time) * (vapsquadSpaceship.phyiscs.height + heightOffset) - 5 //+ topY
      vapsquadSpaceship.position.y = topY
      vapsquadSpaceship.phyiscs.state += (high - 0.2) + sin * 0.01
      vapsquadSpaceship.rotation.z = Math.sin(vapsquadSpaceship.phyiscs.state) * Math.PI * 0.2
      vapsquadSpaceship.rotation.y += 0.007
      vapsquadSpaceship.phyiscs.height -= 0.001

      renderer.render(scene, camera);
    },

    unload () {
      renderer.dispose();
    }
  };
};

canvasSketch(sketch, settings);
