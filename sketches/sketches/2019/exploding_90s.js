
const canvasSketch = require('canvas-sketch');
const random = require('canvas-sketch-util/random')
const eases = require('eases')
const palettes = require('nice-color-palettes')
const BezierEasing = require('bezier-easing')

const generate = require('../../modules/generate')
const utils = require ('../../modules/utils')
const glslify = require('glslify')
// let seed = "dank25"

random.setSeed(random.getRandomSeed())
// random.setSeed(seed)

console.log("Random Seed =>",random.getSeed())

palette = random.pick(palettes)
foreground = palette.slice(0,3)
background = palette.slice(3,5)

console.log()

// Ensure ThreeJS is in global scope for the 'examples/'
global.THREE = require('three');

// Include any additional ThreeJS examples below
require('three/examples/js/controls/OrbitControls');

const settings = {
  // Make the loop animated
  animate: true,
  dimensions: [ 2048, 2048 ], 
  // fps: 24,
  // duration: 6,
  // Get a WebGL canvas rather than 2D
  context: 'webgl',
  // Turn on MSAA
  attributes: { antialias: true }
};

const sketch = ({ context, width, height }) => {
  // Create a renderer
  const renderer = new THREE.WebGLRenderer({
    context
  });

  // WebGL background color
  renderer.setClearColor(random.pick(background), 1);
  // renderer.setClearColor(random.pick(palette), 0); // transparent

  // Setup a camera
  const camera = new THREE.OrthographicCamera();

  const aspect = width / height;
  
  // Ortho zoom
  // const zoom = 0.4; // near
  const zoom = 2.7; // far
  
  // Bounds
  camera.left = -zoom * aspect;
  camera.right = zoom * aspect;
  camera.top = zoom;
  camera.bottom = -zoom;
  
  // Near/Far
  camera.near = -100;
  camera.far = 100;
  
  // Set position & look at world center
  camera.position.set(0, zoom, zoom);
  camera.lookAt(new THREE.Vector3());
  
  // Update the camera
  camera.updateProjectionMatrix();

  // Setup camera controller
  const controls = new THREE.OrbitControls(camera);

  // Setup your scene
  const scene = new THREE.Scene();

  let boxes = []

  "SHADERS!!"
  const fragmentShader = glslify(/*glsl*/`
    varying vec2 vUv;  
    uniform vec3 color;
    uniform float time;

    #pragma glslify: noise = require('glsl-noise/simplex/3d')

    void main() {
      vec3 color = vec3(color);
      
      float offset = 0.05 * noise(vec3(vUv.xy * 9.0, time * 0.4));

      gl_FragColor = vec4(color * vUv.y + offset, 0.9); // flat
    }
  `)

  const vertexShader = glslify(/*glsl*/`
    varying vec2 vUv;
    uniform float time;
    uniform vec3 color;

    #pragma glslify: noise = require('glsl-noise/simplex/4d')

    void main() {
      vUv = uv;
      vec3 pos = position.xyz;// * sin(time);

      pos += normal * noise(vec4(position.xyz * 0.5, time * 0.5)) * 0.3;
      pos += normal * noise(vec4(position.xyz * 11.0, time * 0.5)) * 0.85;
      
      gl_Position = projectionMatrix * modelViewMatrix * vec4(pos, 1.0);
    }

  `)

  const box = new THREE.SphereGeometry(1, 32, 32)
  const meshes = []
  const boxNumber = 100

  for (let i = 0; i < boxNumber; i++) {
    const mesh = new THREE.Mesh(
      box,
      new THREE.ShaderMaterial({
        fragmentShader,
        vertexShader,
        uniforms: {
          time: { value: 0 },
          color: { value: new THREE.Color(random.pick(foreground))}
        },
        // color: random.pick(foreground),
      })
    )
    mesh.spinDirection = random.value() > 0.5 ? 1 : -1
    mesh.timeOffset = random.value() * 100
    
    mesh.radius = random.gaussian(0,0.3)
    mesh.startingAngle = random.range(0,360)
    mesh.angle = mesh.startingAngle

    
    mesh.position.set(
      utils.get3dPosition(mesh.angle, mesh.radius, 'x'), // random.gaussian(0,0.3), 
      random.range(-3,3), 
      utils.get3dPosition(mesh.angle, mesh.radius, 'z')
    )
    
    // console.log("WTF => ", mesh.radius, "POS => ", mesh.position)
    // mesh.scale.set(
    //   random.gaussian(), 
    //   random.gaussian(), 
    //   random.gaussian()
    // )
    mesh.scaleOffset = random.range(0.03,0.1)
    mesh.scale.multiplyScalar(mesh.scaleOffset)
    scene.add(mesh);
    meshes.push(mesh)
  }

  scene.add(new THREE.AmbientLight('white', 1))

  const light = new THREE.DirectionalLight('#fff', 1)
  light.position.set(1,3,2)
  scene.add(light)

  // draw each frame
  return {
    
    // Handle resize events here
    resize ({ pixelRatio, viewportWidth, viewportHeight }) {
      renderer.setPixelRatio(pixelRatio);
      renderer.setSize(viewportWidth, viewportHeight);
      camera.aspect = viewportWidth / viewportHeight;
      camera.updateProjectionMatrix();
    },
    
    // Update & render your scene here
    render ({ time, playhead }) {
      // http://cubic-bezier.com/#.29,.96,.78,.15
      controls.update();
      let t = Math.sin(time / 10 * Math.PI * 2)
      let easeFn = BezierEasing(.28, .85, .43, 1)
      // scene.rotation.y = time / 10
      
      meshes.forEach(mesh => {
        mesh.material.uniforms.time.value = (time + mesh.timeOffset)// * mesh.timeOffset / 100 + mesh.timeOffset) * mesh.spinDirection // * mesh.timeOffset
        mesh.scale.multiplyScalar(1.01)
        if (mesh.position.y > 5) {
          mesh.position.y = -3
          mesh.position.x = utils.get3dPosition(mesh.angle, 0.01, 'x')
          // mesh.position.z = utils.get3dPosition(mesh.angle, 0.01, 'z')
        }else {
          mesh.position.y += 0.02  * mesh.timeOffset / 100 * random.gaussian(3,0.2) / 10
          yOffset = (mesh.position.y + 3) / 6
          mesh.position.x = utils.get3dPosition(mesh.angle, mesh.radius * 2 * yOffset, 'x')
          // mesh.position.z = utils.get3dPosition(mesh.angle, mesh.radius * 2 * yOffset, 'z')
        }
        // mesh.rotation.y = Math.sin(time + mesh.timeOffset) * mesh.spinDirection
      });

      camera.lookAt(new THREE.Vector3());
      renderer.render(scene, camera);
      
      // return() => {
      //   return [
      //     { data: meshes[0], extension: '.obj' },
      //     { data: "hello world", extension: '.txt' }
      //   ]
      // }

    },
    // Dispose of events & renderer for cleaner hot-reloading
    unload () {
      // controls.dispose();
      
      renderer.dispose();
    }
  };
};

canvasSketch(sketch, settings);
