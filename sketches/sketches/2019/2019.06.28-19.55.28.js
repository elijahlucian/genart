const canvasSketch = require('canvas-sketch');
const BezierEasing = require('bezier-easing');
const glslify = require('glslify');
const Simplex = require('simplex-noise')
const simplex = new Simplex()
const utils = require('../../utils/utils')

const bez = new BezierEasing(.73,.28,.82,.44)

const settings = {
  dimensions: [ 2048, 2048 ],
  animate: true,
};

document.body.style.backgroundColor = '#000000'  

const vert = `
  void main() {
    gl_Position = vec4(0.0,0.0,0.0,1.0);
    gl_PointSize = 64.0;
  }
`

const frag = `
  void main() {
    gl_FragColor = vec4(1.0,1.0,1.0,1.0);
  }
`

const createShader =  (gl,source,type) => {
  let shader = gl.createShader(type)
  gl.shaderSource(shader, source)
  gl.compileShader(shader)

  if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
    var info = gl.getShaderInfoLog(shader);
    throw "could not compile web gl shader. \n\n" + info;
  }
  return shader;
}

const sketch = ({width, height}) => {
  const canvas = document.createElement('canvas')
  console.log(canvas)
  
  const gl = canvas.getContext('webgl')
  gl.width = width
  gl.height = height
  const program = gl.createProgram()
  console.log(gl)

  const vertShader = createShader(gl, vert, gl.VERTEX_SHADER)
  const fragShader = createShader(gl, frag, gl.FRAGMENT_SHADER)

  gl.attachShader(program, vertShader)
  gl.attachShader(program, fragShader)
  gl.linkProgram(program)
  // gl.detachShader(program, vertShader)
  // gl.detachShader(program, fragShader)
  // gl.deleteShader(vertShader)
  // gl.deleteShader(fragShader)

  gl.enableVertexAttribArray(0)
  const buffer = gl.createBuffer()
  gl.bindBuffer(gl.ARRAY_BUFFER, buffer)
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array([0.0, 0.0]), gl.STATIC_DRAW);
  gl.vertexAttribPointer(0, 1, gl.FLOAT, false, 0, 0)

  gl.useProgram(program)
  gl.drawArrays(gl.POINTS, 0, 1)

  const TAU = Math.PI * 2
  const PI = Math.PI
  
  return ({ time, context, width, height }) => {
    const margin = 200
    const w = (width) / 2 - margin
    const h = (height) / 2
    var size = 20
    var step = 0.02
    const speed = 3.
    const t = time * speed
    
    context.fillStyle = '#000000';
    context.fillRect(0, 0, width, height);
    // context.strokeStyle = '#ffffff'
    
    context.lineCap = "round" || "butt" || "square"
    context.lineJoin = "round" // || "bevel" || "miter"
    
    var start = true
    
    // let r = (255 * Math.sin(t))
    // let g = (255)
    // let b = (255)
    // context.strokeStyle = `rgb(${r},${g},${b})`
    
  const chillTime = time * 0.1
  const barCount = 20
  let bars = utils.range(barCount)
  // bars.forEach(bar => {
  //   return
  //   let u = bar / barCount
  //   let v = bar / barCount - barCount / 2.0
  //   let n =  simplex.noise2D(u,chillTime)
  //   context.lineWidth = 50 * n + 50
  //   context.strokeStyle = `hsla(${180 * n + 180},${100}%,${30}%,0.5)`
  //   // context.strokeStyle = '#ffffff22'
  //   context.beginPath()
  //   context.moveTo(width / 2 + v * n * 100 / 2,0)
  //   context.lineTo(u * width + w / 2 * Math.sin(chillTime) + n * 300,height)
  //   context.stroke()
  //   context.closePath()
  // });

  let segmentCount = 30
  const segments = utils.range(segmentCount)

  bars.forEach(bar => {
    context.beginPath()
    let u = bar / barCount
    let h = bar / barCount - barCount / 2
    let lastV = 0
    context.lineWidth = 10
    context.strokeStyle = `hsla(${180},${100}%,${30}%,0.3)`
    context.moveTo(u*width,-100)
    segments.forEach(segment => {
      let v = segment / segmentCount
      let j = segment / segmentCount - segmentCount / 2
      let n = simplex.noise3D(u * 20,v * 13,time)
      context.lineTo(u * width + n * 10, v * height)
      lastV = v
    })
    context.stroke()
    context.closePath()
  })

  layers = [-1,1,0]
  layers.forEach(n => {
    context.beginPath()
    switch (n) {
        case -1:
          context.lineWidth = 10
          context.strokeStyle = 'hsla(50,100%,60%,0.8)'
          break;
        case 0:
          context.lineWidth = 10
          context.strokeStyle = 'hsla(30,100%,100%,0.9)'
          break;
        case 1:
          context.lineWidth = 10
          context.strokeStyle = 'hsla(10,100%,60%,0.8)'
          break;
      }
      for (let i = -size; i <= size; i+=step) {
        let u = Math.sin(bez(i) * TAU + t) 
        let v = Math.cos(bez(i) * TAU + t) 
        
        u *= i / PI
        v += i / PI

        // u *= Math.sin((u + v) * 5)
        // v *= Math.cos((u + v) * 5)
        
        let x,y;

        if(i > 0.5) {
          x = w / 3 * Math.sin(u * 0.15) * 15 + margin + w + simplex.noise3D(u,n,time) * 5
          y = h / 4 * v + margin + h + margin + simplex.noise3D(v,n,time) * 5
        } else {
          x = w / 3 * u / 5 + margin + w + n * (u) * 1 //bez(u * 10)
          y = h / 3 * v + margin + h + n * v * v + simplex.noise3D(n,u,time) * 2
        }

        if (start) {
          context.moveTo(x,y)
          start = false
          continue
        }
        context.lineTo(
          x,
          y
        )
      }
      context.stroke()
    })
    context.closePath();
  
  };
};



canvasSketch(sketch, settings);