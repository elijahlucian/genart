const canvasSketch = require('canvas-sketch');
const BezierEasing = require('bezier-easing');
const glslify = require('glslify');
const Simplex = require('simplex-noise')
const random = require('canvas-sketch-util/random')
const palettes = require('nice-color-palettes')
const simplex = new Simplex()
const utils = require('../../modules/utils')
const { PI, TAU } = require('../../modules/maf')

const bez = new BezierEasing(.22,.92,.73,.63)


const settings = {
  dimensions: [ 12000, 12000 ],
  // dimensions: [ 1024, 1024 ],
  // duration: 6,
  animate: true,
};

// let seed = random.getRandomSeed()
let seed = "314718"
// random.setSeed(seed)
console.log("Random Seed:", seed)

const palette = random.pick(palettes)
const bg = random.pick(palette)
const c1 = random.pick(palette)
const c2 = random.pick(palette)
console.log(bg)
document.body.style.backgroundColor = '#000000'  
document.body.style.backgroundColor = '#ffffff'  


const sketch = ({context, width, height}) => {
  const canvas = document.createElement('canvas')
  
  return ({ time, context, width, height, frame }) => {
    // console.log(frame)

    var grd = context.createLinearGradient(0, 0, 0, height);
    grd.addColorStop(0, c1);
    grd.addColorStop(1, c2);

    // context.fillStyle = bg;
    context.fillStyle = grd
    context.fillRect(0, 0, width, height);
    
    const margin = 200
    const w = (width) / 2 - margin
    const h = (height) / 2
    const speed = 0.5
    const t = 0.5 // time * speed
    
    context.lineCap = "butt" // || "round" || "square" || "butt"
    context.lineJoin = "square" // || "bevel" || "miter" || "square"
    
    context.strokeStyle = '#fff'
    context.lineWidth = width / 200
    
    let lineResolution = 0.2
    let start = false

    let lineWidth = width * 0.002
    let length = width * 0.0035
    let o = width * 0.0037
    
    context.save()
    
    for (let j = 0; j <= length; j++) {
      let offset = (j%2==0) ? 0 : width / (length - 1) / 2
      for (let i = 0; i <= length; i++) {
        
        let u = i / (length - 1)
        let v = j / (length - 1)
        
        for (let j = 0; j < 2; j++) {
          context.beginPath()
          let n = random.noise4D(u,v,j,t) * 0.15
          let h = j * 50 * n * 10

          context.lineWidth = lineWidth

          context.rotate(PI * n * 0.2)
          context.moveTo(h, o * 2)
          context.lineTo(h + o, 0)
          context.lineTo(h + 0, -o * 2)
          context.lineTo(h + -o, 0)
          context.closePath()
          context.stroke()


          context.fill()
          context.restore()
          context.save()
          context.translate(u *  width + offset, (v) * height)  
        }
        
      }
    }
    
  };  
};

canvasSketch(sketch, settings);