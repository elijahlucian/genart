const canvasSketch = require('canvas-sketch');
const utils = require('../../utils/utils')
const Simplex = require('simplex-noise')
const simplex = new Simplex()
const glslify = require('glslify')
// const hsl2rgb = require('glsl-hsl2rgb')
// const noise = require('glsl-noise')
// Ensure ThreeJS is in global scope for the 'examples/'
global.THREE = require('three');

// Include any additional ThreeJS examples below
require('three/examples/js/controls/OrbitControls');

const PI = Math.PI
const TAU = PI * 2
const sin = Math.sin
const cos = Math.cos

const settings = {
  // Make the loop animated
  animate: true,
  dimensions: [2048,2048],
  // Get a WebGL canvas rather than 2D
  context: 'webgl',
  // Turn on MSAA
  attributes: { antialias: true }
};

const vertexShader = glslify(/* glsl */`
  varying vec2 vUv;
  uniform float n;
  uniform float i;
  uniform float time;
  varying float index;

  #pragma glslify: noise = require('glsl-noise/simplex/4d')

  void main() {
    vUv = uv;
    mat4 p = projectionMatrix;
    mat4 m = modelViewMatrix;

    vec3 pos = position.xyz;
    
    pos += normal * 0.5 * position.xyz * sin(position.x + (position.y + time)) * 3.;
    
    pos.y += sin(vUv.x * 6.28 + n * 5. * time * .1 + index) * 0.5 * sin(.1 * time + n);
    
    gl_Position = p * m * vec4(pos, 1.0);
  }
`)

const fragmentShader = glslify(/* glsl */`
  varying vec2 vUv;
  uniform float time;
  uniform float n;
  uniform float i;

  #pragma glslify: noise = require('glsl-noise/simplex/3d')
  #pragma glslify: hsl2rgb = require('glsl-hsl2rgb');

  void main() {
    vec3 color = vec3(
      1.,
      1.,
      1.
    );
    
    color = hsl2rgb(1., 1., 1.);
    
    vec3 rgb = vec3(0.5);

    rgb.x *= sin(vUv.x * 15. + n + time * 15. * n) + 1.;
    rgb.y *= cos((time * 0.2 + vUv.x) * 50. + vUv.y * 20. + n + time * 10. * n) + 1.;
    rgb.z *= rgb.y;
    rgb.x = step(rgb.x, 0.2);
    rgb.y = step(rgb.y, 0.05);
    rgb.z = step(rgb.z, 0.05);
    rgb.x *= rgb.z;
    rgb.x += step(mod(10. * n * time + vUv.x * 40., 2.), 0.1);
    rgb.z += rgb.x * 1.;
    gl_FragColor = vec4(color * rgb, 1.0);
  }
`)

const sketch = ({ context }) => {
  const renderer = new THREE.WebGLRenderer({
    context
  });

  renderer.setClearColor('#000', 1);
  const camera = new THREE.PerspectiveCamera(45, 1, 0.01, 100);
  camera.position.set(-10, 12, -18);
  camera.lookAt(new THREE.Vector3());

  const controls = new THREE.OrbitControls(camera);
  const scene = new THREE.Scene();

  schoolSize =5
  school = utils.range(schoolSize)

  fishes = []

  school.forEach(index => {
    let i = index / schoolSize
    let n = simplex.noise2D(index,0)
    var shaderMaterial = new THREE.ShaderMaterial({
      fragmentShader,
      vertexShader,
      // side: THREE.BackSide,
      uniforms: {
        index: { value: index },
        time: { value: 0 },
        n: { value: n }
      }
    })

    pointsCount = 12
    pointArray = utils.range(pointsCount)
    let points = pointArray.map((item,i) => {
      let u = i / pointsCount
      let n = simplex.noise2D(i,0)
      
      return new THREE.Vector3(
        15 * u - 7.5,
        0, //n * 0.2,
        0,//n * 0.1 + 5,
        )
    })
    var curve = new THREE.CatmullRomCurve3(points)
    var geometry = new THREE.TubeGeometry( curve, pointsCount, 0.3, 25, false )

    const mesh = new THREE.Mesh(
      geometry,
      shaderMaterial
    );

    mesh.position.x = simplex.noise2D(index, 0) * 10
    mesh.position.y = simplex.noise2D(mesh.position.x, 0) * 10
    mesh.position.z = simplex.noise2D(mesh.position.y, 0) * 10
    mesh.geometry.center()
    scene.add(mesh);  
    fishes.push(mesh);
  })

  scene.add(new THREE.AmbientLight('hsla(20,80%,50%,1)'));
  const light = new THREE.PointLight('#45caf7', 1, 15.5);
  light.position.set(2, 2, -4).multiplyScalar(.5);
  scene.add(light);

  return {
    // Handle resize events here
    resize ({ pixelRatio, viewportWidth, viewportHeight }) {
      renderer.setPixelRatio(pixelRatio);
      renderer.setSize(viewportWidth, viewportHeight);
      camera.aspect = viewportWidth / viewportHeight;
      camera.updateProjectionMatrix();
    },

    // Update & render your scene here
    render ({ time }) {
      let speed = 0.01
      let t = time * speed
      
      fishes.forEach((mesh, index) => {
        mesh.material.uniforms.time.value = time
        mesh.rotation.x = index * 0.1

        let vertices = mesh.geometry.vertices.length
        
        for (let i = 0; i < vertices; i++) {
          let u = i / vertices
          let vertex = mesh.geometry.vertices[i]
          // mesh.geometry.vertices[i].x = u * 20 - 10
          // mesh.geometry.vertices[i].y += (Math.sin(u * TAU + time + index)) / 100
          if(i==0) {
          } else {
          }
        }


        mesh.geometry.verticesNeedUpdate = true
      })
      
      controls.update();
      renderer.render(scene, camera);
    },
    // Dispose of events & renderer for cleaner hot-reloading
    unload () {
      controls.dispose();
      renderer.dispose();
    }
  };
};

canvasSketch(sketch, settings);
