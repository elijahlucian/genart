global.THREE = require('three');

const canvasSketch = require('canvas-sketch');
const utils = require('../../utils/utils')
const Simplex = require('simplex-noise')
const simplex = new Simplex()
const glslify = require('glslify')
const Bezier = require('bezier-easing')
const palettes = require('nice-color-palettes')
const random = require('canvas-sketch-util/random')
let palette = random.pick(palettes)

const materials =  require('../../three-basics/materials')

backgroundColor = palette.shift()

const bez = Bezier(.17,.67,.83,.67)

// const hsl2rgb = require('glsl-hsl2rgb')
// const noise = require('glsl-noise')
// Ensure ThreeJS is in global scope for the 'examples/'

document.body.style.backgroundColor = 'black'

// Include any additional ThreeJS examples below
require('three/examples/js/controls/OrbitControls');

const PI = Math.PI
const TAU = PI * 2
const sin = Math.sin
const cos = Math.cos

const settings = {
  animate: true,
  // dimensions: [2048,2048],
  context: 'webgl',
  attributes: { antialias: true },
};

const vertexShader = glslify(/* glsl */`
  uniform float n;
  uniform float i;
  uniform float f;
  uniform float time;

  varying vec2 vUv;
  varying float index;
  varying float vDepth;

  #pragma glslify: noise = require('glsl-noise/simplex/4d')

  void main() {

    float speed = 1.;
    float t = time * speed;

    vUv = uv;
    mat4 p = projectionMatrix;
    mat4 m = modelViewMatrix;

    vec3 pos = position.xyz;
    
    pos += normal * .5 * position.xyz * sin(f * position.x + 40. + t) * .1;
    
    gl_Position = p * m * vec4(pos, 1.);

    // vDepth = step(mod((gl_Position.z - 22.) + time, 1.), 0.2);
  }
`)

const fragmentShader = glslify(/* glsl */`
  uniform float time;
  uniform float n;
  uniform float i;
  
  varying vec2 vUv;
  varying float index;
  varying float vDepth;

  #pragma glslify: noise = require('glsl-noise/simplex/3d')
  #pragma glslify: hsl2rgb = require('glsl-hsl2rgb');

  #define PI 3.1415926535897932384626433832795
  #define TAU (2.*PI)

  void main() {
    float pi = 3.14159265359;
    float tau = 2. * pi;
    float speed = 0.1;
    float t = time * speed;
    vec3 color = vec3(
      1.,
      1.,
      1.
    );
    
    color = hsl2rgb(1., 1., 1.);
    
    vec3 rgb = vec3(0.5);

    float glowySpots = step(sin(vUv.x * 100. - time + sin(vUv.y * 100. + time * 10.)), -.5);
    // glowySpots *= 

    rgb.x *= sin(vUv.x * 15. + n + time * 15. * n) + 1.;
    rgb.y *= cos((time * 11.2 + vUv.x * TAU * 1.1) + vUv.y * TAU * 4. + n + time * 10. * n) + 1.;
    rgb.z *= rgb.y;
    rgb.x = step(rgb.x, 0.2);
    rgb.y = step(rgb.y, 0.05);
    rgb.z = step(rgb.z, 0.05);
    rgb.x *= rgb.z;
    rgb.x += step(mod(-3. * time + vUv.x * 40., 2.), 0.1);
    rgb.z += rgb.x * 1.;
    rgb += 0.06;
    // rgb += glowySpots;

    gl_FragColor = vec4(color * rgb, vDepth);

  }
`)

const sketch = ({ context }) => {
  const renderer = new THREE.WebGLRenderer({
    context
  });

  // renderer.setClearColor(backgroundColor, 1);
  renderer.setClearColor('#000', 1);
  // renderer.setClearColor('#fff', 1);
  const camera = new THREE.PerspectiveCamera(45, 1, 0.01, 100);
  camera.position.set(-10, 12, -18);
  camera.lookAt(new THREE.Vector3());

  const controls = new THREE.OrbitControls(camera);
  const scene = new THREE.Scene();

  schoolSize = 4
  school = utils.range(schoolSize)

  fishes = []

  school.forEach(index => {
    let f = index / (schoolSize - 1)
    let n = simplex.noise2D(index,0)

    var shaderMaterial = new THREE.ShaderMaterial({
      fragmentShader,
      vertexShader,
      transparent: true,
      // depthWrite: false,
      // depthTest: false,
      side: THREE.DoubleSide,
      blending: THREE.NormalBlending,
      uniforms: {
        index: { value: index },
        time: { value: 0 },
        n: { value: n },
        f: { value: f },
      }
    })

    pointsCount = 40
    pointArray = utils.range(pointsCount)
    
    let points = pointArray.map((item,i) => {
      let u = i / (pointsCount - 1)
      let n = simplex.noise2D(i,0)
      
      return new THREE.Vector3(
        Math.sin(u * TAU) * 4 / (index + 1),// * f, //15 * u - 7.5,
        Math.cos(u * TAU) * 4 / (index + 1),// * f, //n * 0.2,
        0,//n * 0.1 + 5,
        )
    })
  
    var curve = new THREE.CatmullRomCurve3(points)
    var geometry = new THREE.TubeGeometry( 
      curve, 
      pointsCount, 
      0.3,
      25, 
      true 
    )

    const mesh = new THREE.Mesh(
      geometry,
      shaderMaterial,
      // materials.physical,
    );

    if(index > 0) mesh.rotation.y = (PI / 2)
    // mesh.position.x = simplex.noise2D(index, 0) * 10
    // mesh.position.y = simplex.noise2D(mesh.position.x, 0) * 10
    // mesh.position.z = simplex.noise2D(mesh.position.y, 0) * 10
    mesh.geometry.center()
    scene.add(mesh);  
  fishes.push(mesh);
  })

  scene.add(new THREE.AmbientLight('hsla(20,80%,50%,1)'));
  const light = new THREE.PointLight('#45caf7', 1, 15.5);
  light.position.set(2, 2, -4).multiplyScalar(.5);
  scene.add(light);

  rotationIndex = 0

  return {
    // Handle resize events here
    resize ({ pixelRatio, viewportWidth, viewportHeight }) {
      renderer.setPixelRatio(pixelRatio);
      renderer.setSize(viewportWidth, viewportHeight);
      camera.aspect = viewportWidth / viewportHeight;
      camera.updateProjectionMatrix();
    },

    // Update & render your scene here
    render ({ time }) {
      let speed = 0.24
      let t = time * speed

      fishes.forEach((mesh, index) => {
        let n = index / (fishes.length - 1)
        let s = Math.sin(time + n * 5)
        let o = index / 2 * TAU
        let b = ((s + 1) / 2)
        
        // mesh.position.z = s * 2
        
        if(index > 0) {

          mesh.position.x = Math.sin(t * PI + o) * 4
          mesh.position.y = Math.cos(t * PI + o) * 4

          mesh.rotation.x = PI / 2
          mesh.rotation.y -= PI * 0.004
          // mesh.rotation.y = -mesh.position.y * PI * 0.1
        }

        // mesh.rotation.y = b * TAU
        // mesh.rotation.y = bez(n) // bez((time / PI) % 1) * TAU
      
        // mesh.material.uniforms.time.value = time
        mesh.rotation.z += 0.01

        let vertices = mesh.geometry.vertices.length

        for (let i = 0; i < vertices; i++) {
          let u = i / vertices
          let vertex = mesh.geometry.vertices[i]
          // mesh.geometry.vertices[i].x = u * 20 - 10
          // mesh.geometry.vertices[i].y += (Math.sin(u * TAU + time + index)) / 100
          if(i==0) {
          } else {
          }
        }

        mesh.geometry.verticesNeedUpdate = true
      })
      
      controls.update();
      renderer.render(scene, camera);
    },
    // Dispose of events & renderer for cleaner hot-reloading
    unload () {
      controls.dispose();
      renderer.dispose();
    }
  };
};

canvasSketch(sketch, settings);
