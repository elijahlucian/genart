const canvasSketch = require('canvas-sketch')
const math = require('canvas-sketch-util/math')
const random = require('canvas-sketch-util/random')
const niceColorPalletes = require('nice-color-palettes')
const myPalletes = require('../../modules/palettes')
// const gatewayLog = require('./gateway_log')

const generate = require('../../modules/generate')

const canvasSize = 1024
const settings = {
  dimensions: [canvasSize, canvasSize], // instagram
}

const sketch = () => {
  let seed = '562582'
  const size = 10
  const randomRadius = true
  const radius = 25

  random.setSeed(random.getRandomSeed())
  // random.setSeed(seed)

  console.log('Random Seed =>', random.getSeed())

  palette = random.pick(niceColorPalletes)
  reducedPalette = palette.slice(0, 3)
  background = palette.slice(3, 5)

  const newSeed = (e) => {
    console.log(e)
    seed += e.key
    random.setSeed(seed)
    points = createGrid({
      size,
      randomRadius,
      radius,
    })
    window.requestAnimationFrame(() => {
      // console.log("a callback? wtf", seed)
    })
    // e.preventDefault()
  }
  window.addEventListener('keydown', newSeed, false)

  const gradient = (
    context,
    start,
    end,
    width,
    height,
    left_x = 0,
    top_y = 0,
  ) => {
    fill = context.createLinearGradient(left_x, top_y, width, height)
    fill.addColorStop(0, start)
    fill.addColorStop(1, end)
    return fill
  }

  const lerp = (min, max, t) => {
    return (max - min) * t + min
  }

  const createGrid = (args) => {
    const points = []
    const size = args.size

    for (let y = 0; y < size; y++) {
      for (let x = 0; x < size; x++) {
        const u = x / (size - 1)
        const v = y / (size - 1)
        const radius = Math.max(0, random.noise2D(u, v) + 1) * 0.01

        points.push({
          color: random.pick(reducedPalette),
          position: { u, v },
          radius,
          rotation: random.noise2D(u, v),
        })
      }
    }
    return points
  }

  points = createGrid({ size, randomRadius, radius }).filter(
    () => random.value() > 0.8,
  )

  return ({ context, width, height }) => {
    let state = {
      rotation: 0,
    }

    const margin = width / 8
    let count = 0
    // context.fillStyle = gradient(context, background[0], background[1], width/3, height)
    context.fillStyle = random.pick(background) // 'white'
    context.fillRect(0, 0, width, height)

    points.forEach((point) => {
      let x = math.lerp(margin, width - margin, point.position.u)
      let y = math.lerp(margin, height - margin, point.position.v)
      let fontSize = point.radius * width * 10
      let length = 8000

      // TODO - 2019-01-23 16:09 - EL - Idea!
      // DIYarc(context, x, y, 0, Math.PI *2, true, 'white')
      context.fillStyle = point.color
      context.font = `${fontSize}px "Inconsolata"` // `${margin/10}px`

      context.save()
      context.translate(x, y)
      state.rotation += Math.PI / 3.14
      // context.rotate(state.rotation)
      context.rotate(point.rotation)
      context.fillRect(-canvasSize * 1.5, 0, canvasSize * 10, canvasSize / 52.5)
      // context.fillText("☠", 0-fontSize/2, 0+ fontSize/3)
      context.restore()

      count += 1
    })

    // context.restore()

    // generate.DarkNoise(context, width, height, background[1])

    console.log('> rendered!')
  }
}

canvasSketch(sketch, settings)
