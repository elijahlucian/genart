const canvasSketch = require('canvas-sketch');
import Unsplash from 'unsplash-js'
import simplexNoise from 'simplex-noise'
const simplex = new simplexNoise()
const unsplash = new Unsplash({
  applicationId: "c39e003c8d008e3fd82b87ffce2330e600998c35f0c188f90a48b60e26ad9ab2",
  secret: "4fd595ad0acc6a5b6374b98dd0366370b450e72b8e435c9bc8d2088aa5b96146",
  // callbackUrl: "urn:ietf:wg:oauth:2.0:oob",
  // bearerToken: "{USER_BEARER_TOKEN}"
});

unsplash.photos.listPhotos(2,15,"latest")
  .then(res => res.json())
  .then(data => {
    // console.log(data)
  })
  
const img = new Image()
img.src = '../images/coin.jpg' // "https://unsplash.com/photos/Aca6sQjoddw/download"

const settings = {
  dimensions: [ 2048, 2048 ],
  animate: true,
};

const sketch = () => {

  let image

  img.onload = () => {
    console.log(img.naturalWidth)
    image = img
  }


  return ({ time, context, width, height }) => {

    context.fillStyle = 'white';
    context.fillStyle = `hsl(${time} % 360, 50%, 50%)`
    context.fillRect(0, 0, width, height);
    
    if(image) {
    
      context.drawImage(
        image,
        0,
        0,
        width,
        height
      )

      // let step = 0.125
      let step = width * 0.00002
      let imgW = width * step
      let imgH = height * step
      let margin = width * 0.01
      let t = time * 0.1
    
      for(let u = 0; u < 1; u+=step) {
        for(let v = 0; v < 1; v+=step) {
          let i = u + v
          let x = width * u
          let y = height * v
          let n = simplex.noise2D(u,v)
          let n3 = simplex.noise3D(u,v,t)

          context.save()
          context.translate(x,y)
          // context.rotate(Math.sin(time + i) * 0.3)
          context.drawImage(
            image,
            n3 * 2 + image.naturalWidth * u / 10,
            n3 * 2 + image.naturalHeight * v / 10,
            step * image.naturalWidth,
            step * image.naturalHeight,
            0,
            0,
            imgW - margin * (n3),
            imgH - margin * (n3),
          )
          context.restore()
        }
      }
    }
  };
};

canvasSketch(sketch, settings);
