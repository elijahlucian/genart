const canvasSketch = require('canvas-sketch');
const random = require('canvas-sketch-util/random')

const glslify = require('glslify');
const Simplex = require('simplex-noise')
const { palettes } = require('../../modules/nice-color-palettes')
const { paletteSeeds } = require('../../seeds/palettes')

const utils = require('../../modules/utils')
const { PI, TAU, vectors } = require('../../modules/maf')
const { Tempo, musicalTime } = require('../../modules/music')
const { BPM } = require('../../modules/music')

const { Mountain } = require('../../classes/lines')

let seed = {
  palette: paletteSeeds.sunny,
  // determined: 'dank',
  random: random.getRandomSeed()
}

// paletteSeed
random.setSeed("753186" || seed.palette || seed.defined || seed.random)
console.log("Palette Seed =>", random.getSeed())

const palette = random.pick(palettes)
const bg = palette.shift()

// mainSeed

random.setSeed(seed.determined || seed.random)
console.log("Algo Seed =>", random.getSeed())

document.body.style.backgroundColor = bg

const settings = {
  dimensions: [ 11180, 11180 ],
  // dimensions: [ 8192, 8192 ],
  // dimensions: [ 4096, 4096 ],
  // dimensions: [ 2048, 2048 ],
  // dimensions: [ 1024, 1024 ],
  fps: 30,
  // animate: true,
};

/////////

const sketch = ({width, height}) => {

  // instantiate classes here 
  agent_count = 7
  agent_limit = 100

  let shapes = []

  for (let i = 0; i < agent_count; i++) {
    shapes.push(new Mountain({
      i,
      z: i / (agent_count - 1),
      width,
      height,
      color: random.pick(palette),
      segment_count: 10,
      noise_height: height/agent_count * 0.6,
      bg
    }))
  }

  return ({ time, context, width, height }) => {
    dt = time
    // context.fillStyle = '#00000066';
    context.fillStyle = bg
    context.fillRect(0, 0, width, height);
    const margin = 200
    const speed = 0.5
    const t = time * speed
    
    context.lineCap = "square" // || "round" || "square" || "butt"
    context.lineJoin = "square" // || "bevel" || "miter" || "square"
    context.lineWidth = height / 100

    shapes.forEach((shape, i) => {
      if(shape.z < 0.1 || shape.z > 0.9) return
      shape.update({time,width,height})
      shape.draw({context,width,height})
    });
  };  
};

canvasSketch(sketch, settings);