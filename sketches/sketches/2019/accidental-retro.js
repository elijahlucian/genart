const canvasSketch = require('canvas-sketch');
const math = require('canvas-sketch-util/math')
const random = require('canvas-sketch-util/random')
const niceColorPalletes = require('nice-color-palettes')

const settings = {
  // dimensions: 'a4', // 
  // dimensions: [ 9, 16 ],
  // dimensions: [ 2048, 2048 ], // instagram
  dimensions: [ 6500, 6500 ], // instagram
  // orientation: 'landscape',
  // units: 'in',
  pixelsPerInch: 600,
};

const sketch = () => {
  
  palette = random.pick(niceColorPalletes)
  reducedPalette = palette.slice(0,3)
  background = palette.slice(3,5)

  console.log("PALETTE => ", palette)
  console.log("FOREGROUND PALETTE => ", reducedPalette)
  console.log("BACKGROUND PALETTE => ", background)

  const gradient = (context, start, end, width, height) => { 
    fill = context.createLinearGradient(0, 0, width, height)
    fill.addColorStop(0, start)
    fill.addColorStop(1, end)
    return fill
  }
  
  const lerp = (min, max, t) => { 
    return (max - min) * t + min
  }

  const createGrid = (args) => {
    const points = []
    const size = args.size
    random.setSeed(args.seed)
    
    for (let x = 0; x < size; x++) {
      for (let y = 0; y < size; y++) {
    
        const u = x / (size - 1)
        const v = y / (size - 1)
        const radius = random.noise2D(u,v)
        
        points.push({
          color: random.pick(reducedPalette),
          position: {u, v},
          radius,
        })
      }
    }
    return points
  }

  points = createGrid({
    size: 40,
    randomRadius: true,
    radius: 25,
    seed: 'dank',
  }).filter(() => random.value() > 0.5)

  
  return ({ context, width, height }) => {
    const margin = width / 10

    // context.fillStyle = gradient(context, '#f4225a', '#f4b800', width/3, height)
    context.fillStyle = random.pick(background) // 'white'
    context.fillRect(0,0,width,height)

    points.forEach(point => {
      let x = lerp(margin, width - margin, point.position.u)
      let y = lerp(margin, height - margin, point.position.v)
      
      // TODO - 2019-01-23 16:09 - EL - Idea!
      // DIYarc(context, x, y, 0, Math.PI *2, true, 'white') 
      
      context.beginPath()
      context.lineWidth = 10
      context.fillStyle = point.color
      context.arc(x, y, point.radius * width, 0,Math.PI * 2, false)
      context.fill()
      
      context.beginPath()
      context.fillStyle = 'hsla(180, 0%, 0%, 10%)'
      context.arc(x, y, point.radius * width, offs = 5.2, offs + Math.PI, false)
      context.fill()
    });
      
  };
};

canvasSketch(sketch, settings);
