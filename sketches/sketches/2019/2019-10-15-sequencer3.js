const THREE = require('three')
const {
  ChromaticAberrationEffect,
  NoiseEffect,
  BloomEffect,
  EffectPass,
  RenderPass,
  EffectComposer,
  BlendFunction,
  BlendMode,
} = require('postprocessing')

const { noteToString } = require('midi-translate')

const clock = new THREE.Clock({ autostart: true })
const canvasSketch = require('canvas-sketch')
const palettes = require('nice-color-palettes')
const random = require('canvas-sketch-util/random')
const utils = require('../../modules/utils')

const { BPM } = require('../../classes/music')
const { Mouse } = require('../../classes/utils')

const bezierEasing = require('bezier-easing')
const bez = bezierEasing(0.11, 0.73, 0.32, 0.92)

const { Square } = require('../../classes/three')

const Tone = require('tone')
const bpm = new BPM({ bpm: 240 })

var synth, filter, delay

Tone.Master.volume.value = 20
Tone.Transport.bpm.value = bpm.bpm

require('three/examples/js/controls/OrbitControls')

const config = {
  speed: 0.24,
  shape: {
    size: 0.6,
    edges: 30,
  },
  grid: {
    size: 1,
    steps: 7,
    scale: 15,
    margin: 1,
  },
  seed: {
    palette: '870427',
    // algorithm: '',
    random: random.getRandomSeed(),
  },
  shader: { enabled: true },
  // dimensions: dimensions.square.insta,
}

config.camera = {
  orthographic: {
    fov: config.grid.scale + config.grid.margin * 8,
    depth: 500,
  },
  perspective: {},
}

const settings = {
  animate: true,
  dimensions: config.dimensions,
  // dimensions: [2048,2048],
  context: 'webgl',
  attributes: { antialias: true },
}

random.setSeed(config.seed.palette || config.seed.random)
console.log('Palette Seed:', random.getSeed())

let palette = random.pick(palettes)
const backgroundColor = '#111' || palette.shift()
document.body.style.backgroundColor = backgroundColor || '#000'

random.setSeed(config.seed.algorithm || config.seed.random)
console.log('Algo Seed:', random.getSeed())

async function setupSynth() {
  synth = new Tone.PolySynth(2, Tone.Synth)
  filter = new Tone.Filter(200, 'lowpass', -48)
  delay = new Tone.FeedbackDelay(0.25, 0.6)
  delay.wet.value = 0.1

  filter.Q.value = 0

  synth.volume.value = -30
  synth.chain(filter, delay, Tone.Master)
}

function sketch(params) {
  setupSynth()
  const notes = ['A3', 'C4', 'D4', 'E4', 'G4']

  console.log('3D CANVAS PARAMS =>', params)
  const { context, width, height } = params
  const mouse = new Mouse({ width, height })

  const scene = new THREE.Scene()
  const renderer = new THREE.WebGLRenderer({
    context,
    alpha: true,
  })

  scene.background = new THREE.Color(backgroundColor)

  let { fov, depth } = config.camera.orthographic

  const camera = new THREE.OrthographicCamera(
    -fov - 4,
    fov,
    fov,
    -fov,
    -depth,
    depth
  )
  camera.position.set(0, 120, 0)
  camera.lookAt(new THREE.Vector3())

  const controls = new THREE.OrbitControls(camera)

  // create uv grid
  let shapes = []

  // TODO: After every 4 bars, randomize!
  // TODO: Position notes based on pitch

  const agents = 8
  utils.sequence(agents).forEach(n => {
    let ui = n / (agents - 1)
    let r = random.rangeFloor(0, 5)
    let randomNote = noteToString(Math.floor(random.noise1D(ui) * 12 + 60))

    if (random.chance(0.2)) return

    console.log(randomNote)
    let shape = new Square({
      index: n,
      ui,
      u: (n - 4) / 2,
      v: 0.5,
      color: palette[r],
      note: notes[r],
    })
    // shape.mesh.position.x = random.range(-18, 18)
    // shape.mesh.position.z = random.range(-12, 22)
    shapes.push(shape)
    scene.add(shape.mesh)
  })

  scene.add(new THREE.AmbientLight('#fff', 0.2))
  let directionalLight = new THREE.DirectionalLight('#fff', 2.9)
  directionalLight.position.set(0, 0, 0)
  scene.add(directionalLight)

  const composer = new EffectComposer(renderer)

  composer.setSize(width, height)

  const bloomEffect = new BloomEffect({
    resolutionScale: 0.85,
    distinction: 5.2,
  })
  bloomEffect.blendMode.opacity.value = 0.1
  const ca = new ChromaticAberrationEffect({
    offset: new THREE.Vector2(0.001, 0),
  })
  ca.blendMode.opacity.value = 1

  const film = new NoiseEffect()

  film.blendMode = new BlendMode(BlendFunction.SOFT_LIGHT)
  film.blendMode.opacity.value = 0.2

  const effectPass = new EffectPass(camera, ca, bloomEffect, film)
  const renderPass = new RenderPass(scene, camera)

  effectPass.renderToScreen = true

  composer.addPass(renderPass)
  composer.addPass(effectPass)
  // const shaderPass = new ShaderPass(new THREE.ShaderMaterial())

  let nextPhrase = 32

  return {
    // Handle resize events here
    resize({ pixelRatio, viewportWidth, viewportHeight }) {
      renderer.setPixelRatio(pixelRatio)
      renderer.setSize(viewportWidth, viewportHeight)
      camera.aspect = viewportWidth / viewportHeight
      camera.updateProjectionMatrix()
    },

    // Update & render your scene here

    

    render({ time }) {
      bpm.update({ time })

      

      if (mouse.isPressed()) {
        console.log(camera.position)
        // console.log(bpm.reseit())
      }
      let t = bpm.getMusicalTime() || time
      const s = Math.sin(t / 4)

      

      if (t > nextPhrase) {  
       console.log("4 bars!")
       nextPhrase += 32
      }
      filter.frequency.value = s * 1250 + 1650

      shapes.forEach(shape => {
        shape.update({ time: t, beats: bpm.getMusicalTime() })

        if (shape.bing === true) {
          synth.triggerAttackRelease(shape.note, '16n')
          shape.mesh.position.z = -s * 10
          // console.log('playing note', shape.note)
          shape.bing = false
        }
      })

      controls.update()
      // camera.position.x
      renderer.render(scene, camera)
      if (config.shader.enabled) composer.render(clock.getDelta())
    },

    unload() {
      controls.dispose()
      renderer.dispose()
    },
  }
}

canvasSketch(sketch, settings)
