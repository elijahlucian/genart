global.THREE = require('three');
const { ChromaticAberrationEffect, NoiseEffect, BloomEffect, EffectPass, RenderPass, EffectComposer, ShaderPass, BlendFunction, BlendMode } = require('postprocessing')

const clock = new THREE.Clock({autostart: true})
const canvasSketch = require('canvas-sketch');
const utils = require('../../modules/utils')
const glslify = require('glslify')
const palettes = require('nice-color-palettes')
const random = require('canvas-sketch-util/random')

const maf = require('../../modules/maf')
const {dimensions} = require('../../modules/dimensions')
// const config = require('../../modules/config')

const bezierEasing = require('bezier-easing')
const bezier = bezierEasing(.11,.73,.32,.92)

const materials =  require('../../three-basics/materials')
const { Sphere, Arc, Triangle } = require('../../classes/three')
const { Mouse, Keyboard, Controls } = require('../../classes/utils')

require('three/examples/js/controls/OrbitControls');

console.log(dimensions.default)

const config = {
  speed: 0.24,
  shape: {
    size: 0.6,
    edges: 30,
  },
  grid: {
    size: 1,
    steps: 7,
    scale: 9,
    margin: 1,
  },
  seed: {
    // palette: '418861',
    // algorithm: '',
    random: random.getRandomSeed()
  },
  shader: { enabled: true },
  dimensions: dimensions.square.insta
}

config.camera = {
  orthographic: {
    fov: config.grid.scale + config.grid.margin * 2,
    depth: 100,
  },
  perspective: {
  }
}

const settings = {
  // animate: true,
  // dimensions: config.dimensions,
  dimensions: [2048,2048],
  context: 'webgl',
  attributes: { antialias: true },
};

random.setSeed("292779" || config.seed.palette || config.seed.random)
console.log("Palette Seed:", random.getSeed())

let palette = random.pick(palettes)
const backgroundColor = palette.shift()
document.body.style.backgroundColor = backgroundColor || '#000'

random.setSeed(config.seed.algorithm || config.seed.random)
console.log("Algo Seed:", random.getSeed())


const sketch = (params) => {
  console.log("3D CANVAS PARAMS =>", params)
  const { context, width, height } = params
  var mouse = new Mouse({width,height})
  var keyboard = new Keyboard()

  const scene = new THREE.Scene();
  const renderer = new THREE.WebGLRenderer({
    context,
  });

  scene.background = new THREE.Color(backgroundColor)
  // TODO: Add MusicalTime

  // TODO: Encapsultae Camera
  let {fov,depth} = config.camera.orthographic
  // let { fov, aspect, near, far } = config.camera.perspective

  // const camera = new THREE.PerspectiveCamera(45, 1, 0.01, 1000);
  const camera = new THREE.OrthographicCamera(-fov,fov,fov,-fov,-depth,depth)
  camera.position.set(0, 0, 30);
  camera.lookAt(new THREE.Vector3());

  const controls = new THREE.OrbitControls(camera);
  

  // create uv grid
  
  let shapes = []
  const { steps, scale } = config.grid
  let stepSize = 1 / steps
  let grid = utils.create3dUV({step: stepSize})

  grid.forEach(({u,v,row,col}, index) => {
      let { size, edges } = config.shape
      let shape = new Triangle({
        u,
        v,
        index,
        size,
        scale,
        edges,
        row,
        col,
        stepSize,
        bezier,
        color: random.value() > 0.8 ? random.pick(palette) : '#fff',
        normalized_index: index / (grid.length - 1)
      })

      scene.add( shape.mesh )
      shapes.push(shape)

  }) 
  scene.add(new THREE.AmbientLight('#fff', .8));
  let directionalLight= new THREE.DirectionalLight('#fff', 0.8);
  directionalLight.position.set(0,10,10)
  scene.add(directionalLight)

  const composer = new EffectComposer(renderer)
  
  composer.setSize(width,height)
  
  const bloomEffect = new BloomEffect({
    resolutionScale: 0.85,
    distinction: 5.2,
  })
  bloomEffect.blendMode.opacity.value = 0.5
  const ca = new ChromaticAberrationEffect({
    offset: new THREE.Vector2(0.002, 0),
  })
  ca.blendMode.opacity.value = 0.5
  
  const film = new NoiseEffect()

  film.blendMode = new BlendMode(BlendFunction.SOFT_LIGHT)
  film.blendMode.opacity.value = 0.2

  const effectPass = new EffectPass(camera, ca, bloomEffect, film)
  const renderPass = new RenderPass(scene, camera)
  
  // const shaderPass = new ShaderPass( new THREE.ShaderMaterial())

  effectPass.renderToScreen = true 
  
  composer.addPass(renderPass)
  composer.addPass(effectPass)

  return {
    // Handle resize events here
    resize ({ pixelRatio, viewportWidth, viewportHeight }) {
      renderer.setPixelRatio(pixelRatio);
      renderer.setSize(viewportWidth, viewportHeight);
      camera.aspect = viewportWidth / viewportHeight;
      camera.updateProjectionMatrix();
    },

    // Update & render your scene here
    
    render ({ time }) {
      
      let t = time * config.speed
      
      shapes.forEach(shape => {
        shape.update({time})
      });
      controls.update();
      renderer.render(scene, camera);
      if(config.shader.enabled) composer.render(clock.getDelta())
    },
  
    unload () {
      controls.dispose();
      renderer.dispose();
    }
  };
};

canvasSketch(sketch, settings);

