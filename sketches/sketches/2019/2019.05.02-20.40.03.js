const canvasSketch = require('canvas-sketch');
const palettes = require('nice-color-palettes')
const random = require('canvas-sketch-util/random')

const palette = random.pick(palettes)



// http.get('https://blockchain.info/unconfirmed-transactions?format=json').then(res => {
//   console.log(res)
// })

// Ensure ThreeJS is in global scope for the 'examples/'
global.THREE = require('three');

// Include any additional ThreeJS examples below
require('three/examples/js/controls/OrbitControls');

const settings = {
  // Make the loop animated
  animate: true,
  // Get a WebGL canvas rather than 2D
  context: 'webgl',
  // Turn on MSAA
  attributes: { antialias: true }
};

const params = {
  width: 1,
  height: 1,
  depth: 1,
  get offsetX() { return this.width / 2 },
  get offsetY() { return this.height / 2 },
  spacingX: 0.5,
}

const simpleBox = (vec3) => {
  let mesh = new THREE.Mesh(
    new THREE.BoxGeometry(.3, .3, .3),
    new THREE.MeshPhysicalMaterial({
      color: 'white',
      roughness: 0.75,
      flatShading: true
    })
  )
  mesh.position.copy(vec3)
  return mesh
}

const drawBox = (params, i,j,k) => {
  
  let geometry = new THREE.Geometry()

  geometry.vertices.push( new THREE.Vector3(0,0,0) );
  geometry.vertices.push( new THREE.Vector3(4,0,0) );
  geometry.vertices.push( new THREE.Vector3(2,2,0) );
  
  geometry.faces.push( new THREE.Face3(0,1,2) )

  geometry.center()

  let mesh = new THREE.Mesh(
    geometry,
    new THREE.MeshPhysicalMaterial({
      color: 'white',
      roughness: 0.75,
      flatShading: true
    })
  );

  mesh.i = i
  mesh.j = j
  mesh.v3 = THREE.Vector3(i,j,k)
  mesh.position.set(
    params.spacingX * (i - params.offsetX),
    j - params.offsetY,
    random.noise2D(i,j) * 2
  )
  mesh.rotation.y = random.noise2D(i,j) / 2
  return mesh
}

const sketch = ({ context }) => {
  // Create a renderer
  const renderer = new THREE.WebGLRenderer({
    context
  });

  // WebGL background color
  renderer.setClearColor('#000', 1);

  // Setup a camera
  const camera = new THREE.PerspectiveCamera(45, 1, 0.01, 100);
  camera.position.set(0, 0, 20);
  camera.lookAt(new THREE.Vector3());

  // Setup camera controller
  const controls = new THREE.OrbitControls(camera);

  // Setup your scene
  const scene = new THREE.Scene();
  const meshes = []

  for (let i = 0; i < params.width; i++) {
    for (let j = 0; j < params.height; j++) {
      mesh = drawBox(params,i,j,0)
      meshes.push(mesh)
      scene.add(mesh);
    }
  }

  // Specify an ambient/unlit colour
  scene.add(new THREE.AmbientLight(random.pick(palette)));

  // Add some light
  const lightBack = new THREE.PointLight(random.pick(palette), 1, 35.5);
  lightBack.position.set(6, -6, 0).multiplyScalar(1.5);
  scene.add(lightBack);
  
  lightpos = new THREE.Vector3(-4,6,-5)
  // scene.add(simpleBox(lightpos))

  const lightWhite = new THREE.PointLight(random.pick(palette), 1, 9);
  lightWhite.position.copy(lightpos).multiplyScalar(1.5);
  scene.add(lightWhite);
  
  // draw each frame
  return {
    // Handle resize events here
    resize ({ pixelRatio, viewportWidth, viewportHeight }) {
      renderer.setPixelRatio(pixelRatio);
      renderer.setSize(viewportWidth, viewportHeight);
      camera.aspect = viewportWidth / viewportHeight;
      camera.updateProjectionMatrix();
    },
    // Update & render your scene here
    render ({ time }) {

      let t = time * .1

      meshes.forEach(mesh => {
        // mesh.rotation.y += 0.01 //* (10 * Math.PI / 180);
        // mesh.rotation.x += random.noise2D(mesh.i, mesh.j) * 0.1
        // if(mesh.i === 1) {
        // }
        let lightDiff = mesh.position.distanceTo(lightWhite.position)
        // mesh.scale.x = Math.abs(lightDiff - 10)
        mesh.geometry.vertices[2] = new THREE.Vector3(
          Math.sin(t),
          2,
          0
        )

        mesh.geometry.verticesNeedUpdate = true;
        // mesh.scale.y = Math.sin(Math.sin(t * 15) * 5) * 2
        // mesh.rotation.y = Math.sin(t * 10) * Math.PI / 3
      
      });

      lightBack.position.y = Math.sin(t) * 5
      lightBack.position.x = Math.sin(t) * 6
      
      lightWhite.position.x = Math.sin(t) * 6
      lightWhite.position.y = Math.sin(t) * 5
      lightWhite.position.z = Math.sin(t) * -10

      controls.update();
      renderer.render(scene, camera);
    },
    // Dispose of events & renderer for cleaner hot-reloading
    unload () {
      controls.dispose();
      renderer.dispose();
    }
  };
};

canvasSketch(sketch, settings);
