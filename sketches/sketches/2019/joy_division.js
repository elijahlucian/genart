const canvasSketch = require('canvas-sketch');
const random = require('canvas-sketch-util/random')
const niceColorPalletes = require('nice-color-palettes')
// const gatewayLog = require('./gateway_log')




random.setSeed(random.getRandomSeed())
// random.setSeed(seed)

const size = 4096
const step = size / 12

const randomPalette = random.pick(niceColorPalletes)
const lineColor = random.pick(niceColorPalletes)

const settings = {
  suffix: `__seed_${random.getSeed()}`,
  // dimensions: 'a4', // 
  // dimensions: [ 9, 16 ],
  dimensions: [ size, size ],
  // dimensions: [ 2048, 2048 ], // instagram
  // dimensions: [ 4096, 4096 ], // instagram
  // orientation: 'landscape',
  // units: 'in',
  // pixelsPerInch: 300,
  // animate:true,
};

const sketch = () => {

  console.log("Random Seed =>",random.getSeed())
  const background = random.pick(randomPalette)

  var lines = []

  for (let i = 0; i <= size; i += step) {
    var line = []
    for (let j = 0; j <= size; j += step ) {
      let center = size / 2
      let offset = Math.abs(j - center) / (size / 10)
      let randomY = random.value() * size / 20
      let point = {x: j, y: i + randomY * offset * -1}
      line.push(point)
    }
    lines.push(line)
  }


  return ({ context, width, height, time }) => {
    context.fillStyle = 'white';
    context.fillStyle = background;
    context.fillRect(0, 0, width, height);
    context.lineWidth = width/50
    
  
    context.strokeStyle = random.pick(randomPalette);

    // lines.forEach(line => {
    for (let i = 6; i < lines.length-3; i++) {
      
    // }
      context.beginPath()
      
      // context.lineTo(line[0].x, line[0].y + size/16)
      let line = lines[i]
      context.moveTo(line[0].x, line[0].y)

      let a,b,c,d

      for (var segment = 0; segment < line.length -1 ; segment++) {
        a = line[segment].x // + Math.sin(time + segment) * 50
        b = line[segment].y // + Math.cos(time + segment) * 100
        c = (line[segment].x + line[segment + 1].x) / 2 // + Math.sin(time + segment) * 50
        d = (line[segment].y + line[segment + 1].y) / 2 // + Math.sin(time + segment) * 100
        context.quadraticCurveTo(a, b, c, d)
        // context.lineTo(a,b)
      }
      
      context.quadraticCurveTo(line[segment-1].x, line[segment-1].y, line[segment].x, line[segment].y)

      // context.quadraticCurveTo()
      // line.forEach(segment => {
      //   context.lineTo(segment.x, segment.y)
      // })
      
      context.lineTo(line[lines.length-1].x, line[segment].y)

      context.save()
      context.globalCompositeOperation = 'darken'
      // context.fill()
      context.restore()
      context.stroke()
    }
    
    // context.fillStyle = 'white';
    context.save()
    context.globalCompositeOperation = 'multiply'
    context.fillStyle = background;
    context.fillRect(0, 0, width, height);
    context.lineWidth = width/200
    context.restore()


  };
};

canvasSketch(sketch, settings);
