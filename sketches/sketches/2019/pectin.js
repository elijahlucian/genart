const canvasSketch = require('canvas-sketch');

const settings = {
  // dimensions: 'a4', // 
//   dimensions: [ 9, 16 ],
  // dimensions: [ 2048, 2048 ], // instagram
  // dimensions: [ 7500, 11000 ], // instagram
  // dimensions: [ 11000, 7500 ], // instagram
  // dimensions: [ 5500, 11000 ], // instagram
  dimensions: [ 11000, 5500 ], // instagram
  // dimensions: [ 6500, 6500 ], // instagram
  // dimensions: [ 2400, 4500 ], // instagram
  // orientation: 'landscape',
  // units: 'in',
  // pixelsPerInch: 300,
};


const sketch = () => {



  return ({ context, width, height }) => {
    context.fillStyle = 'hsl(160, 50%, 40%)';
    
    const fill = context.createLinearGradient(0, 0, 0, height);
    fill.addColorStop(0, 'hsl(44, 95%, 66%)')
    fill.addColorStop(1, 'hsl(344, 79%, 55%)')
    
    context.fillStyle = fill
    context.fillRect(0, 0, width, height);
    
    context.fillStyle = 'hsla(160, 20%, 100%, 10%)'
    context.strokeStyle = 'hsla(0, 100%, 100%, 80%)'
    context.beginPath()
    
    context.translate(width/2,height/2)
    context.rotate(Math.PI * 0.5)
    for (let i = 0; i < 10; i++) {
      context.arc(0, 0, (width > height ? height : width)*0.324 *(width == height ? 0.8 : 1), i*120, Math.PI*(i/10)*2, false)
      // context.arc(width/2, height/3.24*2, width*0.324, 0, Math.PI*(i/10)*2, false)
      context.fill()
    }
    
  };
};

canvasSketch(sketch, settings);
