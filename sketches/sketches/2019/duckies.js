const canvasSketch = require('canvas-sketch');
const random = require('canvas-sketch-util/random')
const niceColorPalletes = require('nice-color-palettes')
// const gatewayLog = require('./gateway_log')

// let seed = "808001"

const dopeSeeds = {
  camo: 703161,
  cowabunga: 877532,
  tahiti: 808001,
}

random.setSeed(random.getRandomSeed())
// random.setSeed(seed)

const settings = {
  suffix: `__seed_${random.getSeed()}`,
  dimensions: [ 12000,12000 ], // instagram
  // animate: true,
};

const sketch = ({width}) => {
  const size = width / 120
  const randomRadius = true 
  const radius = 15

  console.log("Random Seed =>",random.getSeed())

  let palette = random.pick(niceColorPalletes)
  const background = random.pick(palette)
  // TAHITI TREAT PALETTE
  // const background = '#CE0F5C'

  // palette = [
  //   '#CE0F5C',
  //   '#2BA4A3',
  //   '#EFE371',
  //   '#EB1440',
  //   '#F07347'
  // ]
    // palette[0],palette[2],palette[3],palette[4]]

  // random.setSeed(random.getRandomSeed())


  // window.addEventListener("keydown", utils.newSeed, false);
  
  const lerp = (min, max, t) => { 
    return (max - min) * t + min
  }

  const createGrid = (args) => {
    const points = []
    const size = args.size
    
    const step = 1

    for (let y = 0; y < size; y+=step) {
      for (let x = 0; x < size; x+=step) {
    
        const u = x / (size - 1)
        const v = y / (size - 1)
        const radius = Math.max(0, random.noise2D(u,v) + 1) * 0.01
        const noise = random.noise2D(u,v)

        points.push({
          color: random.pick(palette),
          position: {u, v},
          radius,
          rotation: noise,
          noise: noise,

        })
      }
    }
    return points
  }

  const points = createGrid({ size, randomRadius, radius }).filter(() => random.value() > 0.8)
  
  return ({ context, width, height, time }) => {

    // const margin = width / 8
    const margin = -(width / 10)
    let count = 0
    // // context.fillStyle = gradient(context, background[0], background[1], width/3, height)
    context.fillStyle = background
    context.fillRect(0,0,width,height)
    

    points.forEach(point => {
      
      let x = lerp(margin, width - margin, point.position.u)
      let y = lerp(margin, height - margin, point.position.v)

      // TODO - 2019-01-23 16:09 - EL - Idea!

      context.fillStyle = point.color
      
      context.save()
      context.translate(x,y)
      context.rotate(Math.PI * point.noise * 0.1)
      context.fillRect(
        Math.sin(time * 0.2) * width / 10 * point.noise,
        random.noise2D(time * 0.2,point.position.u) * height/40,
        width / 15,
        height / 15

      )
      
      context.restore()
      
      count += 1
    });

    // context.restore()

    console.log("> rendered!")

  };
};

canvasSketch(sketch, settings);
