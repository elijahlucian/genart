global.THREE = require('three')
const {
  ChromaticAberrationEffect,
  NoiseEffect,
  BloomEffect,
  EffectPass,
  RenderPass,
  EffectComposer,
} = require('postprocessing')
const clock = new THREE.Clock()

const canvasSketch = require('canvas-sketch')
const utils = require('../../utils/utils')
const Simplex = require('simplex-noise')
const simplex = new Simplex()
const glslify = require('glslify')
const Bezier = require('bezier-easing')
const palettes = require('nice-color-palettes')
const random = require('canvas-sketch-util/random')
let palette = random.pick(palettes)

const materials = require('../../three-basics/materials')

backgroundColor = palette.shift()

const sun = require('../../shaders/outrunSun')
const outrunPlane = require('../../shaders/outrunPlane')

const bez = Bezier(0.17, 0.67, 0.83, 0.67)

document.body.style.backgroundColor = 'black'

// require('three/examples/js/controls/OrbitControls')

const PI = Math.PI
const TAU = PI * 2
const sin = Math.sin
const cos = Math.cos

const settings = {
  animate: true,
  // dimensions: [2048, 2048],
  dimensions: [11000, 11000],
  // dimensions: [3840,2160],
  context: 'webgl',
  animate: false,
  attributes: { antialias: true },
}

const sketch = ({ context, width, height }) => {
  const renderer = new THREE.WebGLRenderer({
    context,
  })

  // renderer.setClearColor(backgroundColor, 1);
  renderer.setClearColor('#000', 1)
  // renderer.setClearColor('#fff', 1);
  const camera = new THREE.PerspectiveCamera(125, 1, 0.01, 100)
  camera.position.set(-0, 12, -18)
  camera.lookAt(new THREE.Vector3())

  // // const controls = new THREE.OrbitControls(camera)
  const scene = new THREE.Scene()

  var sunShader = new THREE.ShaderMaterial({
    vertexShader: sun.vert,
    fragmentShader: sun.frag,
    uniforms: {
      time: { value: 0 },
    },
  })
  var sunGeometry = new THREE.SphereGeometry(1, 32, 32)
  var sunMesh = new THREE.Mesh(sunGeometry, sunShader)

  sunMesh.position.z = 30
  sunMesh.position.y = -15
  sunMesh.scale.x = 40
  sunMesh.scale.y = 40
  sunMesh.scale.z = 0.1

  scene.add(sunMesh)

  var groundShader = new THREE.ShaderMaterial({
    fragmentShader: outrunPlane.frag,
    vertexShader: outrunPlane.vert,
    transparent: true,
    // depthWrite: false,
    // depthTest: false,
    side: THREE.DoubleSide,
    blending: THREE.NormalBlending,
    uniforms: {
      time: { value: 0 },
    },
  })

  var planeGeo = new THREE.PlaneGeometry(40, 350, 20, 20)
  var planeMat = new THREE.MeshBasicMaterial({
    color: random.pick(palette),
    side: THREE.DoubleSide,
    wireframe: true,
  })
  var planeMesh = new THREE.Mesh(planeGeo, groundShader)

  planeMesh.geometry.center()
  planeMesh.rotation.x = PI / 2
  planeMesh.rotation.z = PI / 2
  planeMesh.position.y = -5
  scene.add(planeMesh)

  scene.add(new THREE.AmbientLight('hsla(20,80%,50%,1)'))
  const light = new THREE.PointLight('#45caf7', 1, 15.5)
  light.position.set(2, 2, -4).multiplyScalar(0.5)
  scene.add(light)

  rotationIndex = 0

  const composer = new EffectComposer(renderer)
  composer.setSize(width, height)

  const bloomEffect = new BloomEffect({
    resolutionScale: 0.25,
    distinction: 1,
  })

  const ca = new ChromaticAberrationEffect({
    offset: new THREE.Vector2(0.008, 0),
  })

  const bloom = new EffectPass(camera, bloomEffect, ca)
  bloom.renderToScreen = true
  ca.renderToScreen = true
  const renderPass = new RenderPass(scene, camera)

  // noiseEffect.blendMode.opacity.value = 0.75;

  composer.addPass(renderPass)
  composer.addPass(ca)
  composer.addPass(bloom)

  console.log(ca.uniforms)

  return {
    // Handle resize events here
    resize({ pixelRatio, viewportWidth, viewportHeight }) {
      renderer.setPixelRatio(pixelRatio)
      renderer.setSize(viewportWidth, viewportHeight)
      camera.aspect = viewportWidth / viewportHeight
      camera.updateProjectionMatrix()
    },

    // Update & render your scene here
    render({ time }) {
      if (!planeMesh.geometry.vertices) return
      let speed = 0.24
      let t = time * speed

      let vertices = planeMesh.geometry.vertices.length

      for (let i = 0; i < vertices; i++) {
        let vertex = planeMesh.geometry.vertices[i]
        let u = i / vertices
        let x = vertex.x * 0.1
        let y = vertex.y * 0.1
        let n = simplex.noise3D(x, y, t)

        planeMesh.geometry.vertices[i].z = n * 0.7
        planeMesh.geometry.vertices[i].z += Math.sin(x + time) + 0.5
        // planeMesh.geometry.vertices[i].y += (Math.sin(u * TAU + time + index)) / 100

        if (i == 0) {
        } else {
        }
      }

      bloomEffect.blendMode.opacity.value = 0.5 + 1 * random.noise1D(t)

      sunMesh.material.uniforms.time.value = time
      planeMesh.material.uniforms.time.value = time
      planeMesh.geometry.verticesNeedUpdate = true

      // controls.update()
      renderer.render(scene, camera)
      composer.render(clock.getDelta())
    },

    unload() {
      // controls.dispose()
      renderer.dispose()
    },
  }
}

canvasSketch(sketch, settings)
