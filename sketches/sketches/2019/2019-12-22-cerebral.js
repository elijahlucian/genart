const canvasSketch = require('canvas-sketch')
const random = require('canvas-sketch-util/random')

import { palettes } from '../../modules/nice-color-palettes'
import utils from '../../modules/utils'
import { dimensions } from '../../modules/dimensions'

const { TheNet } = require('../../classes/cerebral')
const { Mouse } = require('../../classes/utils')

let seed = {
  // palette: '314962',
  // determined: 'dank',
  random: Date.now(), //random.getRandomSeed()
}

// paletteSeed
random.setSeed(seed.palette || seed.defined || seed.random)
console.log('Palette Seed =>', random.getSeed())

const palette = palettes[2]

const rIndex = Math.floor(Math.random() * palette.length)
const bg = palette[rIndex]
delete palette[rIndex]
// const bg = palette.shift()
const mainColor = random.pick(palette)

let tree

// algoSeed
random.setSeed(seed.determined || seed.random)
console.log('Algo Seed =>', random.getSeed())

document.body.style.backgroundColor = bg
document.title = 'JS Sketches'

// const canvasSize = 2048
const settings = {
  suffix: `__seed_${random.getSeed()}`,
  dimensions: [6500, 6500],
  // dimensions: dimensions.square.insta,
  // dimensions: dimensions.print.displate,
  // orientation: 'landscape',
  // units: 'in'
  // pixelsPerInch: 300,
  animate: true,
}

// TODO: WEBCAM
// TODO: Image Loader

let lines = []

const sketch = (params) => {
  console.log('SKETCH PARAMS =>', params)
  let { context, width, height } = params

  context.lineCap = 'square' // || "round" || "square" || "butt"
  context.lineJoin = 'square' // || "bevel" || "miter" || "square"

  const longEdge = utils.getLongEdgeFromArray([width, height])

  const config = {
    speed: 115,
    agent_count: 100,
    agent_limit: 2,
    trails: true,
    margin: longEdge / 20,
    lineWidth: longEdge / 150,
    shapeSize: longEdge / 80,
    bandwidthMin: 120,
    bandwidthMax: 10000,
    audio: {
      gain: -0.8,
    },
  }

  var mouse = new Mouse({ width, height })

  console.log('MAINCOLOR', mainColor)

  for (let i = 0; i < config.agent_count; i++) {
    lines.push(
      new TheNet({
        i,
        u: Math.random(),
        v: 0,
        angle: 0,
        // color: mainColor,
        color: random.pick(palette),
        transparency: random.value(),
        baseSize: config.shapeSize * 0.5,
        life: 3000,
        context,
        rand: random,
        limit: 40,
      }),
    )
  }

  // tree = new TheNet({
  //   u: 0.5,
  //   v: 0,
  //   angle: 0,
  //   color: mainColor,
  //   baseSize: config.shapeSize * 0.3,
  //   life: 30,
  //   context,
  //   rand: random,
  // })

  context.fillStyle = bg // + (config.trails ? '33' : 'ff')
  context.fillRect(0, 0, width, height)

  return ({ time, width, height }) => {
    lines.forEach((tree) => {
      if (tree.dead) return
      tree.update({ time: time * 5, width, height, speed: 5 })
      tree.draw({ fill: true, stroke: false })
    })
  }
}

canvasSketch(sketch, settings)
