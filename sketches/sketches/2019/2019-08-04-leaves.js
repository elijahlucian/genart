const canvasSketch = require('canvas-sketch');
const random = require('canvas-sketch-util/random')

const BezierEasing = require('bezier-easing');
const { palettes }  = require('../../modules/nice-color-palettes')
const { paletteSeeds } = require('../../seeds/palettes')

const utils = require('../../modules/utils')
const maf = require('../../modules/maf')
const { BPM, Tempo, musicalTime } = require('../../classes/music')
const { dimensions } = require('../../modules/dimensions')

const { Leaf } = require('../../classes/shapes')
const { Img, Mouse, Keyboard, Controls } = require('../../classes/utils')
const { Caption } = require('../../classes/typeface')
// GLOBALS
var zeroOffset = 0
var dt = 0

const bpm = new BPM({
  bpm: 105,
})

const bez = new BezierEasing(.22,.92,.73,.63)

let seed = {
  palette: 1564940172523,
  // determined: 'dank',
  random: Date.now(), //random.getRandomSeed()
}

// let img = new Img({src: '../images/mountains-06.png'})

// paletteSeed
random.setSeed(seed.palette || seed.defined || seed.random)
console.log("Palette Seed =>", random.getSeed())

const palette = random.pick(palettes)
const bg = palette.shift()
const mainColor = random.pick(palette)

// algoSeed
random.setSeed(seed.determined || seed.random)
console.log("Algo Seed =>", random.getSeed())

document.body.style.backgroundColor = bg
document.title = "JS Sketches"

// const canvasSize = 2048
const settings = {
  suffix: `__seed_${random.getSeed()}`,
  // dimensions: [canvasSize,canvasSize],
  dimensions: dimensions.square.insta,
  // orientation: 'landscape',
  // units: 'in'
  // pixelsPerInch: 300,
  animate: true,
};

// TODO: WEBCAM
// TODO: Image Loader

const sketch = (params) => {
  console.log("SKETCH PARAMS =>",params)
  let { context, width, height } = params

  context.lineCap = "square" // || "round" || "square" || "butt"
  context.lineJoin = "square" // || "bevel" || "miter" || "square"

  const longEdge = utils.getLongEdgeFromArray([width,height])

  const config = {
    speed: 0.7,
    agent_count: 1,
    agent_limit: 1000,
    trails: true,
    margin: longEdge / 20,
    lineWidth: longEdge / 150,
    shapeSize: longEdge / 80,
    bandwidthMin: 120,
    bandwidthMax: 10000,
    audio: {
      gain: -0.8
    }
  }

  var mouse = new Mouse({width,height})
  var controls = new Controls()
  // var analyser = new AudioAnalyser() // live audio from mic source
  var caption = new Caption({
    font: 'Inconsolata', 
    size: 6,
    top: height * 0.95,
    left: width * 0.01,
    // caption: 'just some drums and keys',
    color: random.pick(palette),
  })

  // instantiate classes here 
  // plan agent count for band count.

  var shapes = []
  console.log("MAINCOLOR", mainColor)

  for (let i = 0; i < config.agent_count; i++) {
    let z = i / config.agent_count - 1
    shapes.push(

      new Leaf({
        u: i
      })
    )
  }
  context.fillStyle = bg // + (config.trails ? '33' : 'ff')
  context.fillRect(0, 0, width, height);
  
  caption.update({time: 0, width, height})
  caption.draw({context})
  
  return ({ time, context, width, height }) => {
    bpm.update({time})

    // context.fillStyle = bg // + (config.trails ? '33' : 'ff')
    // context.fillRect(0, 0, width, height);

    // if(!analyser.audioUtil) return 

    let {b,q} = bpm.getMusicalTime()
    let beats = {b,q}

    // music.update()
    // if(mouse.isPressed()) music.play()
    
    caption.update({time, width, height})

    dt = time
    
    const t = time * config.speed
    
    shapes.forEach((shape, i) => {
      shape.update({time,width,height})
      shape.draw({context})
    });
    // caption.draw({context})
  };  
};

canvasSketch(sketch, settings);