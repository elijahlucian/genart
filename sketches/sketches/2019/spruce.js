const canvasSketch = require('canvas-sketch');
const math = require('canvas-sketch-util/math')
const random = require('canvas-sketch-util/random')
const niceColorPalletes = require('nice-color-palettes')
const {palettes} = require('../../modules/palettes')
// const gatewayLog = require('../../modules/gateway_log')

const generate = require('../../modules/generate')

const settings = {
  // dimensions: 'a4', // 
  // dimensions: [ 9, 16 ],
  // dimensions: [ 2048, 2048 ], // instagram
  dimensions: [ 11000, 11000 ], // instagram
  // orientation: 'landscape',
  antiAliasing: true,
  // units: 'in',
  // pixelsPerInch: 300,
};

const sketch = ({width,height}) => {

  let seed
  // seed = "668044"

  random.setSeed(seed || random.getRandomSeed())

  console.log("Palette Seed =>",random.getSeed())

  const palette = random.pick(Object.values(palettes))
  const background = palette.splice(random.rangeFloor(0,palette.length), 1)

  let algoSeed

  random.setSeed(algoSeed || random.getRandomSeed())
  console.log("Algo Seed =>",random.getSeed())

  const points = []
  const gridSize = 60
  
  const portrait = height > width
  const stepX = width / gridSize * (portrait ? 1 : height / width)
  const stepY = height / gridSize * (portrait ? width / height : 1)
  

  for (let y = -100; y <= height + 100; y+=stepX) {
    for (let x = -100; x <= width + 100; x+=stepY) {
  
      const u = x / (width - 1)
      const v = y / (height - 1)
      
      const uScale = gridSize / 30

      const n = random.noise2D(u * uScale,v * uScale)

      const radius = Math.max(0, n + 1) * 0.01 * (30/gridSize)
        
      points.push({
        color: random.pick(palette),
        position: {u, v},
        radius,
        rotation: n * 9
      })
    }
  }

  return ({ context, width, height }) => {
    const margin = 0 // width / 8


    context.fillStyle = random.pick(background) // 'white'
    context.fillRect(0,0,width,height)

    for (const point of points) {
      let x = math.lerp(margin, width - margin, point.position.u)
      let y = math.lerp(margin, height - margin, point.position.v)
      let length = width / 16 * (30 / gridSize)

      // TODO - 2019-01-23 16:09 - EL - Idea!
      // DIYarc(context, x, y, 0, Math.PI *2, true, 'white') 
      context.fillStyle = point.color
      
      context.save()
      context.translate(x,y)

      context.rotate(point.rotation)
      context.fillRect(-length/2,0,length,point.radius * width / 4 + width / 500)
      context.restore()
    }

    // context.restore()
    
    // generate.DarkNoise(context, width, height, background[1])

    console.log("> rendered!", points.length)

  };
};

canvasSketch(sketch, settings);
