
const canvasSketch = require('canvas-sketch');
const random = require('canvas-sketch-util/random')
const palettes = require('nice-color-palettes')
palette = random.pick(palettes)
const glslify = require('glslify')
const utils = require('../../utils/utils')
var audioContext = new window.AudioContext

global.THREE = require('three');

require('three/examples/js/controls/OrbitControls');

const frag = glslify(/* glsl */`

varying vec2 vUv;
varying vec3 v_normal;
// uniform float color;
uniform float time;
uniform sampler2D tAudioData;
uniform float nData;

void main () {
  // vec3 backgroundColor = vec3( 0.5, 0.5, 0.5 );
  // vec3 color = vec3( 1.0, 1.0, 0.0 );
  // float f = texture2D( tAudioData, vec2( vUv.x, 0.0 ) ).r;
  // float i = step( vUv.y, f ) * step( f - 0.0125, vUv.y );
  
  gl_FragColor = vec4(vec3(nData), 1.0 );
}
`);

const vert = glslify(/* glsl */`
#pragma glslify: snoise3 = require(glsl-noise/simplex/3d);
varying vec2 vUv;
varying vec3 v_normal;
uniform float time;
uniform float noise;
uniform float nData;
uniform float displacement;

void main () {
  v_normal = normal;
  vec3 newPosition = position + normal * vec3(displacement);
  gl_Position = projectionMatrix * modelViewMatrix * vec4(vec3(newPosition.xyz), 1.0);
}
`);

const settings = {
  // Make the loop animated
  animate: true,
  fps: 1,
  dimensions: [ 1024, 1024 ], 
  // Get a WebGL canvas rather than 2D
  context: 'webgl',
  // Turn on MSAA
  attributes: { antialias: true }
};

const makeShaderBox = (size, index, fftSize, analyser) => {
  const loader = new THREE.TextureLoader();
  const mesh = new THREE.Mesh(
    new THREE.BoxGeometry(size,size,size),
    new THREE.ShaderMaterial({
      fragmentShader: frag,
      vertexShader: vert,
      uniforms: {
        time: { 
          value: 0
        },
      }
    })
  )
  return mesh
}

const getCamera = (kind="perspective") => {
  switch (kind) {
    case 'orthagonal':
      let orth = new THREE.OrthographicCamera(
        l,r,t,b, 1, 1000
      )
      orth.position.set(0, 0, 5);
      return orth
    case 'perspective':
      let pers = new THREE.PerspectiveCamera(
        45, 1, 1, 1000
      )
      pers.position.set(-180, 25, 25);
      return pers
    default:
    return null
  }
}

const makeShaderSphere = (size, index, x, y) => {
  const loader = new THREE.TextureLoader();
  var uniforms = {
    displacement: { type: 'f', value: [] },
    time: { value: index },
  }

  var sphere = new THREE.Mesh(
    new THREE.SphereGeometry( size, 32, 32 ),
    new THREE.ShaderMaterial({
      fragmentShader: frag,
      vertexShader: vert,
      uniforms,
    })
   );
   return sphere
}

const makeBox = (size) => {
  const mesh = new THREE.Mesh(
    new THREE.BoxGeometry(size, size, size),
    new THREE.MeshPhysicalMaterial({
      color: 'white',
      roughness: 0.75,
      flatShading: true
    })
  );
  return mesh;
}

const handleEvent = (event) => {
  let key = event.key
  console.log(key)

  switch (key) {
    case 'm':
      sound.play( false )
      break;
  
    default:
      break;
  }
  
}

const sketch = ({ context }) => {
  // add event listener
  window.addEventListener("keydown", handleEvent, false);
  // global meshes
  const meshes = []

  // create renderer
  const renderer = new THREE.WebGLRenderer({
    context
  });
  
  // WebGL background color
  backgroundColor = random.pick(palette)
  // renderer.setClearColor(random.pick(palette), 0); // transparent
  renderer.setClearColor(backgroundColor, 1);
  renderer.setClearColor('black',1);

  // Setup a camera
  // const camera = new THREE.PerspectiveCamera(45, 1, 0.01, 100);
  
  // const l,r,t,b = [-5,5,5,-5]

  let l = -5
  let r = 5
  let t = 5
  let b = -5
  const origin = new THREE.Vector3()

  // Audio Listener

  var listener = new THREE.AudioListener()
  
  // Camera Settings
  
  const camera = getCamera('perspective');
  
  camera.lookAt(origin);
  camera.add(listener)
  
  // Audio Configuration
  
  // var sound = new THREE.Audio( listener )
  // var fftSize = 2048;
  
  // var audioLoader = new THREE.AudioLoader()
  // audioLoader.load( '2011-feb01.mp3', ( buffer ) => {
  //   sound.setBuffer( buffer );
  //   sound.setLoop( true );
  //   sound.setVolume( 0.5 );
  //   sound.autoplay = true
  //   sound.play();
  //   console.log(sound)
  // })

  // var analyser = new THREE.AudioAnalyser( sound, 512)
  var audioAnalyser = audioContext.createAnalyser()
  audioAnalyser.smoothingTimeConstant = 0.85;
  audioAnalyser.fftSize = 2048
  dataArray = new Uint8Array(audioAnalyser.fftSize)

  var audioSource

  navigator.mediaDevices.getUserMedia({ audio: true, video: false })
  .then(stream => {
      audioContext = window.audioContext
      audioSource = audioContext.createMediaStreamSource(stream)
      audioAnalyser.connect(audioContext.destination)
      
      var bufferSize = audioAnalyser.frequencyBinCount
      var dataArray = new Uint8Array(bufferSize);
      console.log("audio context gotten")
    }
  )

  // Setup camera controller
  const controls = new THREE.OrbitControls(camera);

  // Setup your scene
  const scene = new THREE.Scene();

  const backdrop = new THREE.Mesh(
    new THREE.BoxGeometry(10,10,1),
    new THREE.MeshBasicMaterial({
      color: backgroundColor,
      reflectivity: 0,
      // roughness: 1,
    })
  )
  backdrop.position.set(0,0,-5)
  // scene.add(backdrop)

  
  let gridSize = 0

  // for (let x = -gridSize; x <= gridSize; x++) {
  //   for (let y = -gridSize; y <= gridSize; y++) {
    for (let i = 0; i < dataArray.length; i++) {
      const element = dataArray[i];
      const offset = dataArray.length / 2
      let index = meshes.length
      // let mesh = makeShaderSphere(0.5, index, x, y)
      let mesh = makeShaderBox(0.5, index, audioAnalyser.fftSize, audioAnalyser, i)
      // let mesh = makeBox(0.75)

      mesh.i = index
      mesh.noise = random.noise1D(i)
      mesh.position.set(i - 128,0, 0)
      // mesh.y = y
      // mesh.noise = random.noise2D(x,y)
      mesh.rand = random.gaussian()
      
      // if(random.value() > 0.5) {
        meshes.push(mesh)
        scene.add(mesh)
      // }

    }

  //   }
  // }
  
  // Specify an ambient/unlit colour
  scene.add(new THREE.AmbientLight(random.pick(palette)));

  // Add some light
  // const light = new THREE.PointLight('#45caf7', 1, 15.5);
  const light = new THREE.PointLight(random.pick(palette), 1, 15.5, 2);
  light.position.set(3, 3, 5).multiplyScalar(1.5);
  scene.add(light);









  // draw each frame
  return {
    // Handle resize events here
    resize ({ pixelRatio, viewportWidth, viewportHeight }) {
      renderer.setPixelRatio(pixelRatio);
      renderer.setSize(viewportWidth, viewportHeight);
      camera.aspect = viewportWidth / viewportHeight;
      camera.updateProjectionMatrix();
    },

    // Update & render your scene here
    render ({ time }) {
      
      if (audioAnalyser) {
        // audioAnalyser.fftSize = 512;
        // var bufferLength = audioAnalyser.fftSize;
        // var dataArray = new Uint8Array(bufferLength);

        audioAnalyser.getByteTimeDomainData(dataArray)
        console.log(dataArray[dataArray.length/ 2])
      }
      renderer.render(scene, camera);
      return
      
      analyser.getFrequencyData();
      
      light.power = Math.max(Math.sin(time * Math.PI / 2) * 4 * Math.PI, 0)
      backdrop.position.z = Math.sin(time) * 0.2 - 0.4
      
      meshes.forEach(mesh => {
        let nData = analyser.data[mesh.i] / 256
        // mesh.scale.z = Math.abs(Math.cos(mesh.i + time + mesh.noise) * 1)
        // mesh.scale.y = random.noise2D(mesh.noise, time * 0.1)
        mesh.material.uniforms.tAudioData.value.needsUpdate = true;
        mesh.material.uniforms.time.value = (mesh.i + time)
        mesh.material.uniforms.nData.value = nData
        mesh.rotation.x = nData * Math.PI + (Math.PI * 0.25)
        
        mesh.position.y = analyser.data[mesh.i] / 128
        // mesh.rotation.y += Math.abs(random.noise2D(mesh.noise + mesh.i, time * 0.05) * 0.05) // * Math.PI
        // mesh.rotation.x += Math.sin(mesh.i + time64S * 0.3 + mesh.rand) * 0.01
        mesh.scale.y = analyser.data[mesh.i]/6
        mesh.scale.z = analyser.data[mesh.i]/12
        mesh.scale.x = analyser.data[mesh.i]/64
      })

      controls.update();
      renderer.render(scene, camera);
    },
    // Dispose of events & renderer for cleaner hot-reloading
    unload () {
      controls.dispose();
      renderer.dispose();
    }
  };
};

canvasSketch(sketch, settings);
