const canvasSketch = require('canvas-sketch');
const math = require('canvas-sketch-util/math')
const random = require('canvas-sketch-util/random')
const niceColorPalletes = require('nice-color-palettes')
const myPalletes = require('../../modules/palettes')
// const gatewayLog = require('../../modules/gateway_log')

const generate = require('../../modules/generate')
const utils = require('../../modules/utils')
const maf = require('../../modules/maf')
let seed = "562581"

// random.setSeed(random.getRandomSeed())
random.setSeed('976296')

palette = random.pick(niceColorPalletes)
reducedPalette = palette.slice(0,3)
background = palette.slice(3,5)

const backgroundFill = background[0]

const settings = {
  suffix: `__seed_${random.getSeed()}`,
  // dimensions: 'a4', // 
  // dimensions: [ 9, 16 ],
  // dimensions: [ 2048, 2048 ], // instagram
  dimensions: [ 4096, 4096 ], // instagram
  // orientation: 'landscape',
  // units: 'in',
  // pixelsPerInch: 300,
  // animate: true,
};

const sketch = () => {
  const size = 5
  const randomRadius = true 
  const radius = 25
  console.log("Random Seed =>",random.getSeed())

  // window.addEventListener("keydown", utils.newSeed, false);
  
  const gradient = (context, start, end, width, height, left_x=0, top_y=0) => { 
    fill = context.createLinearGradient(left_x, top_y, width, height)
    fill.addColorStop(0, start)
    fill.addColorStop(1, end)
    return fill
  }

  points = generate.grid({ size, randomRadius, radius })//.filter(() => random.value() > 0.2)
  points = utils.shuffle(points)

  const click = () => {

  }

  return ({ context, width, height, time }) => {

    let state = {
      rotation: 0
    }

    const margin = width / 8
    let count = 0
    // context.fillStyle = gradient(context, background[0], background[1], width/3, height)
    context.fillStyle = backgroundFill // 'white'
    context.fillRect(0,0,width,height)
    
    // context.lineCap = 'round'
    // context.lineJoin = 'round'

    context.shadowColor = 'rgba(0,0,0,0.5)'
    context.shadowBlur = 1000
    // context.translate(width/4,height/4)

    points.forEach(point => {
      
      // point = points[335]
      
      let x = maf.lerp({min: margin, max: width - margin, t: point.position.u})
      let y = maf.lerp({min: margin, max: height - margin, t: point.position.v})
      let fontSize = 200
      let length = 200

      if (count % size/4 == 0) {
        context.fillStyle = backgroundFill + '66'
        context.fillRect(0,0,width,height)
      }

      context.strokeStyle = point.color
      context.lineWidth = 100
      context.fillStyle = point.color
      context.save()
      context.translate(x,y)//Math.sin(time) * (y / 4000) * height/4 + height/2)
      // context.moveTo(1,0)
      context.rotate(point.radius * 1000 + 100)
      context.beginPath()
      context.moveTo(0,0)
      context.lineTo(0,-width/10)
      context.lineTo(-width/5,0)
      context.lineTo(width/5,0)
      context.lineTo(0,width/10)
      context.arc(0,0,width/4 * point.radius * 100,Math.PI/2,Math.PI * 3,false)
      context.lineTo(0,0)
      context.stroke()
      context.restore()
      // context.translate(0,0)
      count += 1
    });

    // context.restore()
    // generate.DarkNoise(context, width, height, background[1])
    // console.log("> rendered!")
  };
};

canvasSketch(sketch, settings);
