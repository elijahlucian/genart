const canvasSketch = require('canvas-sketch');
const random = require('canvas-sketch-util/random')
const niceColorPalletes = require('nice-color-palettes')
// const gatewayLog = require('./gateway_log')

const generate = require('../../modules/generate')
const utils = require('../../utils/utils')
let seed = "562582"

random.setSeed(random.getRandomSeed())
// random.setSeed(seed)

palette = random.pick(niceColorPalletes)
reducedPalette = palette.slice(0,2)
background = palette.slice(3,5)

const backgroundFill = background[0]

const settings = {
  suffix: `__seed_${random.getSeed()}`,
  // dimensions: 'a4', // 
  // dimensions: [ 9, 16 ],
  dimensions: [ 2048, 2048 ], // instagram
  // dimensions: [ 4096, 4096 ], // instagram
  // orientation: 'landscape',
  // units: 'in',
  // pixelsPerInch: 300,
  animate: true,
};

const sketch = () => {
  const size = 15
  const randomRadius = true 
  const radius = 25

  console.log("Random Seed =>",random.getSeed())


  // window.addEventListener("keydown", utils.newSeed, false);
  
  const gradient = (context, start, end, width, height, left_x=0, top_y=0) => { 
    fill = context.createLinearGradient(left_x, top_y, width, height)
    fill.addColorStop(0, start)
    fill.addColorStop(1, end)
    return fill
  }

  points = generate.grid({ size, randomRadius, radius }).filter(() => random.value() > 0.8)
  console.log(size*size)

  const click = () => {

  }

  return ({ context, width, height, time }) => {

    let state = {
      rotation: 0
    }

    const margin = width / 8
    let count = 0
    // context.fillStyle = gradient(context, background[0], background[1], width/3, height)
    context.fillStyle = backgroundFill // 'white'
    context.fillRect(0,0,width,height)
    
    context.lineCap = 'round'

    context.shadowColor = 'rgba(0,0,0,0.5)'
    context.shadowBlur = 10
    context.translate(width/4,height/4)

    points.forEach(point => {
      
      context.fillStyle = point.color
      context.save()
      
      let x = utils.lerp(0, width/2 - margin, point.position.u)
      let y = utils.lerp(0, height/2 - margin, point.position.v)
      let fontSize = 200
      let length = 200

      context.beginPath()
      
      context.moveTo(x,y)
      context.lineTo(10,10)
      context.translate(width/2,height/2)
      context.lineTo(-10,20)
      // context.arc(x,y,(point.index) + 1,0,Math.PI*2,false)
      
      context.font = `${fontSize}px "Inconsolata"`
      // context.fillText("A", x-fontSize/10, y+ fontSize/3)
      context.fill()
      context.rotate(time * 0.001 + 10)
      context.restore()
      count += 1
    });

    // context.restore()
    
    // generate.DarkNoise(context, width, height, background[1])

    // console.log("> rendered!")

  };
};

canvasSketch(sketch, settings);
