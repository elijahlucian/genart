const canvasSketch = require('canvas-sketch');
const createShader = require('canvas-sketch-util/shader');
const random = require('canvas-sketch-util/random')
const palettes = require('nice-color-palettes')
const glsl = require('glslify');


// Setup our sketch
const settings = {
  context: 'webgl',
  // dimensions: [ 512, 512 ],
  dimensions: [ 2048, 2048 ],
  // dimensions: [ 6500, 6500 ],
  // dimensions: [ 1080, 1920 ],
  // dimensions: [ 11000, 11000 ],
  animate: true,
};

// Your glsl code
const frag = glsl(/* glsl */`
  precision highp float;
  uniform float aspect;
  uniform float time;
  varying vec2 vUv;

  #pragma glslify: noise = require('glsl-noise/simplex/2d');
  #pragma glslify: hsl2rgb = require('glsl-hsl2rgb');


  void main () {
  	float t = time * 1.1;
    vec2 uv = vUv;
    float u=uv.x;
    float v=uv.y;
    
    float l=length(uv-vec2(0.33,0.5));
    float d=distance(sin(0.1*t+uv*12.)*0.2,vec2(sin(l*2.+sin(u)+t),cos(l*15.)*1.4));
  
    float circles=sin(dot(sin(t)+8.,l*50.));
    
    float shape=circles-d;
    
    vec3 color=vec3(u,v,u*v+sin(t*0.5)*.5+.8);
    
    vec3 col=vec3(shape+color*.8);
  
    gl_FragColor = vec4(col,1.0);
    
  }
`);

// Your sketch, which simply returns the shader
const sketch = ({ gl }) => {
  // Create the shader and return it
  return createShader({
    // Pass along WebGL context
    clearColor: false,
    gl,
    // Specify fragment and/or vertex shader strings
    frag,
    // Specify additional uniforms to pass down to the shaders
    uniforms: {
      // Expose props from canvas-sketch
      time: ({ time }) => time,
      aspect: ({ width, height }) => width / height
    }
  });
};

canvasSketch(sketch, settings);
