const canvasSketch = require('canvas-sketch');
import Unsplash from 'unsplash-js'
import BezierEasing from 'bezier-easing'
import simplexNoise from 'simplex-noise'

import {shuffle, lerp} from '../../modules/utils'
const maf = require('../../modules/maf')

const simplex = new simplexNoise()
const unsplash = new Unsplash({
  applicationId: "c39e003c8d00  8e3fd82b87ffce2330e600998c35f0c188f90a48b60e26ad9ab2",
  secret: "4fd595ad0acc6a5b6374b98dd0366370b450e72b8e435c9bc8d2088aa5b96146",
  // callbackUrl: "urn:ietf:wg:oauth:2.0:oob",
  // bearerToken: "{USER_BEARER_TOKEN}"
});

// unsplash.photos.listPhotos(2,15,"latest")
//   .then(res => res.json())
//   .then(data => {
//     // console.log(data)
//   })
  
var mouse = {
  x: 0,
  y: 0,
}

const img = new Image()
img.src = '../images/moon.png'

const settings = {
  dimensions: [ 2048, 2048 ],
  // dimensions: [ 1024, 1024 ],
  // dimensions: [ 1920, 1080 ],
  animate: true,
  canvas: document.createElement('canvas')
};

const sketch = ({context, width, height}) => {
  let step = 0.1
  let steps = 10
  let blockSize = width / steps
  let margin = 500

  document.body.style.backgroundColor = '#000'
  document.body.children[2]
  let imageData = []

  const onMouseMove = (event) => {
    mouse.x = event.clientX - width / 2
    mouse.y = event.clientY - height / 2
  }

  window.addEventListener('mousemove', onMouseMove)

  img.onload = () => {
    console.log(img.naturalWidth)
    context.drawImage(img, 0,0,width,height)
    context.fillStyle = '#000000';
    
    for(let u = 0; u < 1; u+=step) {
      for(let v = 0; v < 1; v+=step) {
        let x = u * width
        let y = v * height
        let n = simplex.noise2D(u, v)
        
        imageData.push({
          i: imageData.length,
          n,
          u,
          v,
          data: context.getImageData(x,y,1,1).data
        })
      }
    }
    imageData = shuffle(imageData)
    context.fillRect(0,0,width,height)
  }

  return ({ time, context, width, height }) => {
    let t = time * 0.2

    context.fillStyle = '#0000000f';
    context.fillRect(0,0,width,height)

    if(imageData.length < 1) return
    
    imageData.forEach(pixel => {
      let {i,u,v,n,data} = pixel
      let x = maf.lerp({min: margin, max: width, t: u})
      let y = maf.lerp({min: margin, max: height, t: v})
      
      let n3 = simplex.noise3D(u * 5,v * 5,t)
      
      if(n3 < 0) return

      let easing = BezierEasing(1,.29,.85,.65)

      context.save()
      context.translate(
        x + -mouse.x * n * 0.4, 
        y + -mouse.y * n * 0.4
      )
      context.rotate(n3 * Math.PI * 2 + time + i)
      context.rotate(n * easing(v))

      let [r,g,b,a] = data
      context.fillStyle = `rgba(
        ${r + (n3)},
        ${g + (n3)},
        ${b + (n3)},
        ${n3 / 0.7}
      )`
    
      context.strokeStyle = `rgba(
        ${r + 50 * (n3)},
        ${g + 50 * (n3)},
        ${b + 50 * (n3)},
        ${n3 / 1.4}
      )`
      context.lineWidth = 10

      let shapeSize = n3 * 200 // step * width * 0.8 // / 4 + (n3 / 0.7) * 50
      
      context.beginPath()
      context.rect(-shapeSize / 2,-shapeSize / 2,shapeSize,shapeSize)
      context.stroke()

      context.beginPath()
      context.arc(0,0,shapeSize, 0, Math.PI * 4 * (n3))
      context.clip()
      // context.drawImage(img, -u * img.naturalWidth / 2, -v * img.naturalHeight / 2)
      // context.fill()
      context.stroke()

      context.restore()
    })
  }
};

canvasSketch(sketch, settings);
