const canvasSketch = require('canvas-sketch');
const random = require('canvas-sketch-util/random')
const niceColorPalletes = require('nice-color-palettes')


const settings = {
  // dimensions: 'a4', // 
  // dimensions: [ 9, 16 ],
  // dimensions: [ 2048, 2048 ], // instagram
  dimensions: [ 4096, 4096 ], // instagram
  // orientation: 'landscape',
  // units: 'in',
  pixelsPerInch: 600,
};

const sketch = () => {

  let seed = ""

  // random.setSeed(seed)
  const size = 20
  const randomRadius = true 
  const radius = 25

  const newSeed = (e) => {
    console.log(e)
    seed += e.key
    random.setSeed(seed)
    points = createGrid({
      size,
      randomRadius,
      radius,
    })
    window.requestAnimationFrame(() => {
      // console.log("a callback? wtf", seed)
    })
    e.preventDefault()

  }

  window.addEventListener("keydown", newSeed, false);

  palette = random.pick(niceColorPalletes)
  reducedPalette = palette.slice(0,3)
  background = palette.slice(3,5)

  const gradient = (context, start, end, width, height, left_x=0, top_y=0) => { 
    fill = context.createLinearGradient(left_x, top_y, width, height)
    fill.addColorStop(0, start)
    fill.addColorStop(1, end)
    return fill
  }
  
  const lerp = (min, max, t) => { 
    return (max - min) * t + min
  }

  const mando = (z, c, n=40) => {
    
  }

  const createGrid = (args) => {
    const points = []
    const size = args.size
    
    for (let x = 0; x < size; x++) {
      for (let y = 0; y < size; y++) {
    
        const u = x / (size - 1)
        const v = y / (size - 1)
        // const radius = Math.max(0, random.noise2D(u,v) + 1) * 0.01
        
        points.push({
          color: random.pick(reducedPalette),
          position: {u, v},
          radius,
        })
      }
    }
    return points
  }

  // points = createGrid({ size, randomRadius, radius })//.filter(() => random.value() > 0.5)
  
  const create_n_rectangles = (context, width, height, n) => {
    for (let i = 0; i < n; i++) {
      context.save()
      context.fillStyle = random.pick(palette)
      context.translate(width/i, height/i)
      context.fillRect(0,0, width/i,height/i)
      context.restore()
    }
  }

  const square = (c,x,y,s) => {
    // c.fillStyle = random.pick(palette)
    
  }
  
  return ({ context, width, height }) => {
    const margin = width / 10
    
    context.fillStyle = gradient(context, '#f4225a', '#f4b800', width/3, height)
    // context.fillStyle = random.pick(background)
    context.fillRect(0,0,width,height)
    
    for (let i = 0; i < 5; i++) {
      context.lineWidth = 40;
      context.strokeStyle = 'white'
      context.save()
      context.translate(width/(i*i), height/(i*i))
      let x2 = width/i
      let y2 = height/i
      let size = width/i
      let fractal_size = width/(i*2)
      let offset = fractal_size/2
      // square(context,0,0,size)
      context.strokeRect(0,0,x2,y2)
      
      for (let j  = 0; j < i*4; j++) {
        // context.fillStyle = random.pick(palette)
        context.translate(0,0)
        context.strokeRect(0-offset,0-offset,fractal_size,fractal_size)
        context.strokeRect(0-offset,x2-offset,fractal_size,fractal_size)
        context.strokeRect(y2-offset,0-offset,fractal_size,fractal_size)
        context.strokeRect(y2-offset,x2-offset,fractal_size,fractal_size)
        
      }
      context.restore()
      
    }

  };
};

canvasSketch(sketch, settings);
