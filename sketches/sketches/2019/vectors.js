const canvasSketch = require('canvas-sketch');
const math = require('canvas-sketch-util/math')
const random = require('canvas-sketch-util/random')
const niceColorPalletes = require('nice-color-palettes')
const myPalletes = require('../../modules/palettes')

const generate = require('../../modules/generate')
const utils = require('../../modules/utils')
// let seed = "956508" // && null

// window.fetch('http://www.colourlovers.com/api/palettes/top?format=json&numResults=100').then(data => console.log(data))

seed = {
  // set: "209725",
  generate: random.getRandomSeed()
}

random.setSeed(seed.set || random.getRandomSeed())
// random.setSeed(seed)

const settings = {
  suffix: `__seed_${random.getSeed()}`,
  // dimensions: 'a4', // 
  // dimensions: [ 9, 16 ],
  // // dimensions: [ 512, 512 ], // instagram
  // dimensions: [ 1024, 1024 ], // instagram
  dimensions: [ 2048, 2048 ], // instagram
  // dimensions: [ 4096, 4096 ], // instagram
  // orientation: 'landscape',
  // animate: true,
  fps: 24,
  // units: 'in',
  // pixelsPerInch: 300,
};

// code 0047

const sketch = () => {
  const size = 4
  const randomRadius = true 
  const radius = 25

  console.log("Random Seed =>",random.getSeed())

  palette = random.pick(niceColorPalletes)
  foreground = palette.slice(0,3)
  background = palette.slice(3,5)

  // window.addEventListener("keydown", utils.newSeed, false);
  
  const gradient = (context, start, end, width, height, left_x=0, top_y=0) => { 
    fill = context.createLinearGradient(left_x, top_y, width, height)
    fill.addColorStop(0, start)
    fill.addColorStop(1, end)
    return fill
  }

  const lerp = (min, max, n) => { 
    return (max - min) * n + min
  }

  const createGrid = (args) => {
    const points = []
    const size = args.size
    
    for (let y = 0; y < size; y++) {
      for (let x = 0; x < size; x++) {
    
        const u = x / (size - 1)
        const v = y / (size - 1)

        vectorScalar = 5
        let i = (Math.sin(u) + 1) / 2
        let j = (Math.cos(v) + 1) / 2
        let vectorNoise = random.noise2D(i,j)
        console.log(vectorNoise)

        const vector = {
          i,
          j,
          n: vectorNoise,
        }

        const index = points.length
        
        points.push({
          color: random.pick(foreground),
          // color: foreground[index % 3],
          rotation: 0,
          vector,
          x,
          y,
          index,
          position: {u, v},
          rotation: random.noise2D(u,v)
        })
      }
    }
    return points
  }
  
  points = createGrid({ size, randomRadius, radius })//.filter(() => random.value() > 0.8)

  const foregroundColor = random.pick(foreground)
  const backgroundColor = random.pick(background)
  
  return ({ context, width, height, time }) => {
    // bgColor = gradient(context, background[0], background[1], width/3, height)
    context.fillStyle = backgroundColor // 'white'
    // context.globalCompositeOperation = 'multiply'
    context.fillRect(0,0,width,height)
    
    let state = {
      rotation: 0
    }

    const stepWidth = width / 10
    const stepHeight = height / 6
    const margin = width / 8
    const lineWidth = height/100
    const lineLength = width / 10

    let count = 0

    context.strokeStyle = foregroundColor
    
    var lastX = 0
    var lastY = 0
    
    points.forEach(point => {
      
      let {u,v} = point.position
      const {i,j,n} = point.vector
      
      // context.lineCap = 'round'
      context.lineWidth = lineWidth
      context.strokeStyle = point.color
      
      let lineX = ((i)*width) 
      let lineY = ((j)*height) 
      
      context.beginPath()
      context.lineWidth = n * 10 + 10
      
      context.moveTo(lastX, lastY)
      context.lineTo(lineX, lineY)
      // context.moveTo(-lineX,-lineY)
      context.stroke()

      lastX, lastY = lineX, lineY
      // context.beginPath()
      // context.lineTo(lineX,lineY)
    });
    
    // context.restore()

    console.log("> rendered!")

  };
};

canvasSketch(sketch, settings);
