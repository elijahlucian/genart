const canvasSketch = require('canvas-sketch');
import simplexNoise from 'simplex-noise'
const simplex = new simplexNoise()
  
const img = new Image()
img.src = '../images/mountain.jpg' // "https://unsplash.com/photos/Aca6sQjoddw/download"

const settings = {
  // dimensions: [ 2048, 2048 ],
  dimensions: [ 1920, 1080 ],
  animate: true,
  canvas: document.createElement('canvas')
};

const sketch = () => {
  // document.body.style.backgroundColor = '#000'
  let image
  let drawn = false

  img.onload = () => {
    console.log(img.naturalWidth)
    image = img
  }

  return ({ time, context, width, height }) => {
    if(drawn) return
    let t = time * 0.14
    let step = width * 0.000007
    let imgW = width * step
    let imgH = height * step
    let margin = width * 0.03

    context.fillStyle = '#000';
    context.fillRect(0,0,width,height)

    if(image) {
      drawn = true
      for(let u = 0; u < 1; u+=step) {
        for(let v = 0; v < 1; v+=step) {
          let i = u + v
          let x = width * u
          let y = height * v
          let n = simplex.noise2D(u,v)
          let n3 = Math.abs(simplex.noise3D(u * 3,v * 3,t)) + 0.2
          let n3b = Math.abs(simplex.noise3D(u * 3,v * 3,time)) + 0.2

          context.save()
          context.translate(x,y)
          // context.rotate((n3 * Math.PI * 2 + 1))
          context.drawImage(
            image,
            n3b * 2 + image.naturalWidth * u,
            n3b * 100 + image.naturalHeight * v,
            0.1, //step * image.naturalWidth,
            1, //step * image.naturalHeight,
            0,
            0,
            imgW - margin * (n3 * 0.01) + 1 ,
            40, imgH - margin * (n3 * 5),
          )
          context.restore()
        }
      }
    }
  };
};

canvasSketch(sketch, settings);
