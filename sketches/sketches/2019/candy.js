const canvasSketch = require('canvas-sketch');
const math = require('canvas-sketch-util/math')
const random = require('canvas-sketch-util/random')
const niceColorPalletes = require('nice-color-palettes')

const settings = {
  // dimensions: 'a4', // 
  // dimensions: [ 9, 16 ],
  // dimensions: [ 2048, 2048 ], // instagram
  dimensions: [ 4096, 4096 ], // instagram
  // orientation: 'landscape',
  // units: 'in',
  pixelsPerInch: 600,
};

const sketch = () => {

  let seed = ""
  seed = random.setSeed(seed)

  const size = 20
  const randomRadius = true 
  const radius = 25

  const newSeed = (e) => {
    console.log(e)
    seed += e.key
    random.setSeed(seed)
    points = createGrid({
      size,
      randomRadius,
      radius,
    })
    window.requestAnimationFrame(() => {
      // console.log("a callback? wtf", seed)
    })
    e.preventDefault()

  }

  window.addEventListener("keydown", newSeed, false);

  palette = random.pick(niceColorPalletes)
  reducedPalette = palette.slice(0,3)
  background = palette.slice(3,5)

  const gradient = (context, start, end, width, height, left_x=0, top_y=0) => { 
    fill = context.createLinearGradient(left_x, top_y, width, height)
    fill.addColorStop(0, start)
    fill.addColorStop(1, end)
    return fill
  }
  
  const lerp = (min, max, t) => { 
    return (max - min) * t + min
  }

  const createGrid = (args) => {
    const points = []
    const size = args.size
    
    for (let x = 0; x < size; x++) {
      for (let y = 0; y < size; y++) {
    
        const u = x / (size - 1)
        const v = y / (size - 1)
        const radius = Math.max(0, random.noise2D(u,v) + 1) * 0.01
        
        points.push({
          color: random.pick(reducedPalette),
          position: {u, v},
          radius,
        })
      }
    }
    return points
  }

  points = createGrid({ size, randomRadius, radius })//.filter(() => random.value() > 0.5)
  
  return ({ context, width, height }) => {
    const margin = width / 10

    // context.fillStyle = gradient(context, '#f4225a', '#f4b800', width/3, height)
    context.fillStyle = random.pick(background) // 'white'
    context.fillRect(0,0,width,height)

    points.forEach(point => {
      let x = lerp(margin, width - margin, point.position.u)
      let y = lerp(margin, height - margin, point.position.v)
      
      // TODO - 2019-01-23 16:09 - EL - Idea!
      // DIYarc(context, x, y, 0, Math.PI *2, true, 'white') 
      
      context.beginPath()
      context.lineWidth = 10
      context.fillStyle = point.color
      context.arc(x, y , point.radius * width / 2 + width / 250, 0, Math.PI * 1, false)
      context.fill()
      
      // context.beginPath()
      // gradientFill = gradient(context, 'hsla(180, 0%, 20%, 50%)', 'hsla(180, 0%, 0%, 10%)', x + margin/10, y + margin/10, x - margin/10, y -margin/10)
      // context.fillStyle = 'hsla(180, 0%, 0%, 20%)'
      // context.fillStyle = gradientFill
      // context.arc(x, y, point.radius * width, offs = 5.2, offs + Math.PI, false)
      // context.arc(x, y, point.radius * width * 4, 0, Math.PI * 2, false)
      // context.fill()
    });
      
  };
};

canvasSketch(sketch, settings);
