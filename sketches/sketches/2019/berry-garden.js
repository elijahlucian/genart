const canvasSketch = require('canvas-sketch');
const random = require('canvas-sketch-util/random')

const BezierEasing = require('bezier-easing');
const { palettes }  = require('../../modules/nice-color-palettes')
const { paletteSeeds } = require('../../seeds/palettes')

const utils = require('../../modules/utils')
const maf = require('../../modules/maf')
const { BPM, Tempo, musicalTime } = require('../../classes/music')
const { dimensions } = require('../../modules/dimensions')

const { FracTree } = require('../../classes/shapes')
const { Img, Mouse, Keyboard, Controls } = require('../../classes/utils')
const { AudioAnalyser, AudioLoader } = require('../../classes/audio')
const { Caption } = require('../../classes/typeface')
// GLOBALS
var zeroOffset = 0
var dt = 0

// const bpm = new BPM({
//   bpm: 105,
// })

const bez = new BezierEasing(.22,.92,.73,.63)

let seed = {
  // palette: '314962',
  // determined: 'dank',
  random: Date.now(), //random.getRandomSeed()
}

// let img = new Img({src: '../images/mountains-06.png'})

// paletteSeed
random.setSeed(seed.palette || seed.defined || seed.random)
console.log("Palette Seed =>", random.getSeed())

var palette = [...random.pick(palettes)]

const rIndex = Math.floor(Math.random() * palette.length)

const bg = palette[rIndex]
palette.splice(rIndex,1)

const mainColor = random.pick(palette)
console.log(palette)
// algoSeed
random.setSeed(seed.determined || seed.random)
console.log("Algo Seed =>", random.getSeed())

document.body.style.backgroundColor = bg
document.title = "Elijah Lucian - Generative Berry Garden"

// const canvasSize = 2048
const settings = {
  suffix: `__seed_${random.getSeed()}`,
  // dimensions: [canvasSize,canvasSize],
  dimensions: [6500,6500],
  // dimensions: dimensions.square.insta,
  // dimensions: dimensions.desktop.fhd,
  // orientation: 'landscape',
  // units: 'in'
  // pixelsPerInch: 300,
  animate: true,
};

// TODO: WEBCAM
// TODO: Image Loader

const sketch = (params) => {
  console.log("SKETCH PARAMS =>",params)
  let { context, width, height } = params

  context.lineCap = "square" // || "round" || "square" || "butt"
  context.lineJoin = "square" // || "bevel" || "miter" || "square"

  const longEdge = utils.getLongEdgeFromArray([width,height])
  const shortEdge = utils.getShortEdgeFromArray([width,height])

  const config = {
    speed: 0.7,
    agent_count: 5,
    agent_limit: 1000,
    trails: true,
    margin: shortEdge / 20,
    lineWidth: shortEdge / 150,
    shapeSize: shortEdge / 300,
    bandwidthMin: 120,
    bandwidthMax: 10000,
    audio: {
      gain: -0.8
    }
  }

  var mouse = new Mouse({width,height})
  var controls = new Controls()
  // var analyser = new AudioAnalyser() // live audio from mic source
  var caption = new Caption({
    font: 'Inconsolata', 
    size: 3,
    // caption: 'just some drums and keys',
    color: random.pick(palette),
  })

  var shapes = []
  console.log("MAINCOLOR", mainColor)

  for (let i = 0; i < config.agent_count; i++) {
    let z = i / config.agent_count - 1
    shapes.push(

      new FracTree({
        i,
        z,
        u: 0.33,
        v: 1,
        angle: 0,
        color: mainColor, // random.pick(palette),
        baseSize: config.shapeSize,
        life: 30,
      })
    )
  }
  context.fillStyle = bg // + (config.trails ? '33' : 'ff')
  context.fillRect(0, 0, width, height);
  
  caption.update({time: 0, width, height})
  caption.draw({context})
  
  var trees = 0
  var growTime = 0

  return ({ time, context, width, height }) => {
    growTime += 0.1
    
    caption.update({time, width, height})

    dt = time
    const t = time * config.speed
    
    shapes.forEach((shape, i) => {
      if(shape.v < 0) return
      if(shape.dead) return
      shape.update({
        time: time, // bpm.getMusicalTime(), 
        width, 
        height, 
      })
      shape.draw({context})
      if(shape.spawn) {
        let { u, v, angle, baseSize, i } = shape
        let angle_delta = 12.5
        if(shapes.length > config.agent_limit) { 
          shape.kill()
          return
        }
        for (let index = -1; index <= 1; index+=2) {
          if(shapes.length > 1) { if(random.chance(.3)) continue }
          else if(shapes.length > 10) { if(random.chance(0.45)) continue }
          else if(shapes.length > 35) { if(random.chance(0.55)) continue }
          else if(shapes.length > 70) { if(random.chance(0.85)) continue }
          else if(shapes.length > 120) { if(random.chance(0.99)) continue }
          // if(index===1) {
          //   if(random.boolean()) continue
          // }
          let hardAngle = 0 // (random.chance(0.15) ? 45 : 0)
          shapes.push(new FracTree({
            u, 
            v, 
            angle: angle + angle_delta * index,
            color: shape.color,
            baseSize: baseSize * 0.85,
            index: shapes.length,
            i: i + 1,
            life: shape.og_hp * random.range(0.85,0.95),
          }))  
        }
        shape.kill()
      }
    });
    if(growTime > 100) {
      if(trees > 5) {
        palette = random.pick(palettes)
        trees = 0
        context.fillStyle = palette.shift()
        context.fillRect(0,0,width,height)
      }
      shapes = []
      shapes.push(
        new FracTree({
          i: 0,
          z: 0,
          u: random.range(0.1,width / height - 0.1),
          v: 1,
          angle: 0,
          color: random.pick(palette),
          baseSize: config.shapeSize,
          life: 30,
        }))
      trees += 2
      growTime = 0
    }
    // caption.draw({context})
  };  
};

canvasSketch(sketch, settings);