const canvasSketch = require('canvas-sketch');
const math = require('canvas-sketch-util/math')
const random = require('canvas-sketch-util/random')
const niceColorPalletes = require('nice-color-palettes')
const myPalletes = require('../../modules/palettes')

const generate = require('../../modules/generate')
const utils = require('../../modules/utils')
// let seed = "956508" // && null

// window.fetch('http://www.colourlovers.com/api/palettes/top?format=json&numResults=100').then(data => console.log(data))

seed = {
  // set: "209725",
  generate: random.getRandomSeed()
}

random.setSeed(seed.set || random.getRandomSeed())
// random.setSeed(seed)

const settings = {
  suffix: `__seed_${random.getSeed()}`,
  // dimensions: 'a4', // 
  // dimensions: [ 9, 16 ],
  // // dimensions: [ 512, 512 ], // instagram
  // dimensions: [ 1024, 1024 ], // instagram
  dimensions: [ 2048, 2048 ], // instagram
  // dimensions: [ 4096, 4096 ], // instagram
  // orientation: 'landscape',
  // animate: true,
  // fps: 24,
  // units: 'in',
  // pixelsPerInch: 300,
};

const sketch = () => {
  console.log("Random Seed =>",random.getSeed())

  palette = random.pick(niceColorPalletes)
  foreground = palette.slice(0,3)
  background = palette.slice(3,5)

  // window.addEventListener("keydown", utils.newSeed, false);
  
  const config = {
    x: 3,
    y: 3,
    sx: 3,
    sy: 3,
  }

  const lerp = (min, max, n) => { 
    return (max - min) * n + min
  }

  const transform = (margin, span, n) => { 
    return (span - margin) * n + margin
  }

  const createGrid = (config) => {
    const points = []
    const {x,y,sx,sy} = config
    var position = 0;
    for (let u = -3; u <= 3; u += (x/sx)) {
      for (let v = -3; v <= 3; v += (y/sy)) {
        
        let vector = {
          i: u,
          j: v,
        }

        let n = random.noise2D(u,v)

        let mapping = {
          u: Math.sin(n),
          v: Math.cos(n),
          n
        }

        points.push({
          position,
          vector,
          mapping,
        })
        position++
      }
    }
    return points
  }
  
  points = createGrid(config)//.filter(() => random.value() > 0.8)

  const backgroundColor = random.pick(background)
  
  return ({ context, width, height, time }) => {
    
    let state = {
      rotation: 0,
      margin: width/5,
      lineWidth: width/50,
      lineLength: 0,
      fontSize: width/50,
    }
    
    context.fillStyle = backgroundColor // 'white'
    context.fillRect(0,0,width,height)
    
    context.lineCap = 'round'
    
    points.forEach(point => {
      const foregroundColor = random.pick(foreground)
      context.strokeStyle = foregroundColor
      
      var {i,j,n} = point.vector
      var {u,v} = point.mapping
      
      context.lineWidth = state.lineWidth // + (n * 1)
      u = random.noise2D(i + u * 0.1, 2) / 2 + 0.5
      v = random.noise2D(v - v * 0.1, 1) / 2 + 0.5

      let x = transform(state.margin, width, u)
      let y = transform(state.margin, height, v)
      
      context.font = `${state.fontSize}px "Inconsolata"` // `${margin/10}px`

      context.save()
      context.beginPath()
      context.translate(x,y)
      context.moveTo(0,0)
      context.lineTo(u * state.lineLength, v * state.lineLength)
      context.stroke()

      context.fillStyle = foregroundColor
      context.restore()

    });
    
    console.log("> rendered!")

  };
};

canvasSketch(sketch, settings);
