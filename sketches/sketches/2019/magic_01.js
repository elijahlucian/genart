const canvasSketch = require('canvas-sketch')
const random = require('canvas-sketch-util/random')

const palettes = require('nice-color-palettes')

palette = random.pick(palettes)

// Ensure ThreeJS is in global scope for the 'examples/'
global.THREE = require('three')

// Include any additional ThreeJS examples below
// require('three/examples/js/controls/OrbitControls');

const settings = {
  // Make the loop animated
  animate: true,
  dimensions: [1048, 1048],
  // Get a WebGL canvas rather than 2D
  context: 'webgl',
  // Turn on MSAA
  attributes: { antialias: true },
}

const makeBox = (size) => {
  const mesh = new THREE.Mesh(
    new THREE.BoxGeometry(size, size, size),
    new THREE.MeshPhysicalMaterial({
      color: 'white',
      // roughness: 0.75,
      // flatShading: true,
    }),
  )
  return mesh
}

const makeHeartShape = () => {
  // var heartShape = new THREE.Shape()

  // heartShape.moveTo(2.5, 2.5)
  // heartShape.bezierCurveTo(2.5, 2.5, 2.0, 0, 0, 0)
  // heartShape.bezierCurveTo(3.0, 0, 3.0, 3.5, 3.0, 3.5)
  // heartShape.bezierCurveTo(3.0, 5.5, 1.0, 7.7, 2.5, 0.95)
  // heartShape.bezierCurveTo(6.0, 7.7, 8.0, 5.5, 8.0, 3.5)
  // heartShape.bezierCurveTo(8.0, 3.5, 8.0, 0, 5.0, 0)
  // heartShape.bezierCurveTo(3.5, 0, 2.5, 2.5, 2.5, 2.5)

  var extrudeSettings = {
    depth: 0.8,
    bevelEnabled: true,
    bevelSegments: 2,
    steps: 2,
    bevelSize: 1,
    bevelThickness: 1,
  }

  var geometry = new THREE.ExtrudeGeometry(heartShape, extrudeSettings)

  var mesh = new THREE.Mesh(geometry, new THREE.MeshPhongMaterial())
  return mesh
}

const sketch = ({ context }) => {
  const renderer = new THREE.WebGLRenderer({
    context,
  })

  backgroundColor = random.pick(palette)
  renderer.setClearColor(backgroundColor, 1)

  let l = -5
  let r = 5
  let t = 5
  let b = -5
  const origin = new THREE.Vector3()

  const camera = new THREE.PerspectiveCamera(45, 1, 1, 1000)
  camera.position.set(0, 0, 12)

  camera.lookAt(origin)

  // const controls = new THREE.OrbitControls(camera);

  const scene = new THREE.Scene()
  const backdrop = new THREE.Mesh(
    new THREE.BoxGeometry(10, 10, 1),
    new THREE.MeshBasicMaterial({
      color: random.pick(palette),
      reflectivity: 0,
    }),
  )
  backdrop.position.set(0, 0, -5)
  scene.add(backdrop)

  const meshes = []
  for (let x = -3; x <= 3; x++) {
    for (let y = -3; y <= 3; y++) {
      let mesh = makeBox(0.75)
      mesh.noise = random.noise2D(x, y)
      mesh.rand = random.gaussian()
      mesh.position.set(x, y, 0)
      mesh.rotation.x -= Math.PI / 7 + random.noise1D(x) + random.noise1D(y)
      meshes.push(mesh)
      scene.add(mesh)
    }
  }

  const heart = makeHeartShape()
  heart.rotation.z = Math.PI
  heart.position.z = 3
  heart.scale.set(0.25, 0.25, 0.25)
  // scene.add(heart)

  // Specify an ambient/unlit colour
  scene.add(new THREE.AmbientLight(random.pick(palette)))

  // Add some light
  // const light = new THREE.PointLight('#45caf7', 1, 15.5);
  const light = new THREE.PointLight(random.pick(palette), 1, 15.5, 2)
  light.position.set(3, 3, 5).multiplyScalar(1.5)
  // scene.add(light);

  // draw each frame
  return {
    // Handle resize events here
    resize({ pixelRatio, viewportWidth, viewportHeight }) {
      renderer.setPixelRatio(pixelRatio)
      renderer.setSize(viewportWidth, viewportHeight)
      camera.aspect = viewportWidth / viewportHeight
      camera.updateProjectionMatrix()
    },
    // Update & render your scene here
    render({ time }) {
      // light.power = Math.max(Math.sin(time * Math.PI / 2) * 4 * Math.PI, 0)

      backdrop.position.z = Math.sin(time) * 0.2 - 0.4

      meshes.forEach((mesh) => {
        mesh.scale.z = Math.cos(time + mesh.noise) * 1
        mesh.position.z = random.noise2D(mesh.noise, time * 0.1) * 0.4
        mesh.rotation.y += Math.abs(random.noise2D(mesh.noise, time) * 0.05) // * Math.PI
        mesh.rotation.x += Math.sin(time + mesh.rand) * 0.01
      })

      // controls.update()
      renderer.render(scene, camera)
    },
    // Dispose of events & renderer for cleaner hot-reloading
    unload() {
      // controls.dispose()
      renderer.dispose()
    },
  }
}

canvasSketch(sketch, settings)
