const canvasSketch = require('canvas-sketch');
const math = require('canvas-sketch-util/math')
const random = require('canvas-sketch-util/random')
const niceColorPalletes = require('nice-color-palettes')
const myPalletes = require('../../modules/palettes')



const generate = require('../../modules/generate')
let seed = "194892"

random.setSeed(random.getRandomSeed())
// random.setSeed(seed)

const size = 4096
const step = size / 32

const randomPalette = random.pick(niceColorPalletes)
const lineColor = random.pick(niceColorPalletes)

const settings = {
  suffix: `__seed_${random.getSeed()}`,
  // dimensions: 'a4', // 
  // dimensions: [ 9, 16 ],
  dimensions: [ size, size ],
  // dimensions: [ 2048, 2048 ], // instagram
  // dimensions: [ 4096, 4096 ], // instagram
  // orientation: 'landscape',
  // units: 'in',
  // pixelsPerInch: 300,
  animate:true,
};

const sketch = () => {


  var AudioContext = window.AudioContext
  var audioCtx = new AudioContext()
  var listener = audioCtx.listener

  console.log("Random Seed =>",random.getSeed())
  const background = random.pick(randomPalette)

  var lines = []

  for (let i = 0; i <= size; i += step) {
    var line = []
    for (let j = 0; j <= size; j += step ) {
      let center = size / 2
      let offset = Math.abs(j - center) / (size / 10)
      let randomY = random.value() * size / 20
      let point = {x: j, y: i + randomY * offset * -1}
      line.push(point)
    }
    lines.push(line)
  }

  let lineColor = random.pick(randomPalette)
  
  return ({ context, width, height, time }) => {

    context.translate(width,0)
    context.rotate(Math.PI/2)
    // context.fillStyle = 'white';
    context.fillStyle = background;
    context.fillRect(0, 0, width, height);
    
    for (let y = 10; y < size; y+= 64) {
      let middle = size/2
          
      // context.save()
      // context.globalCompositeOperation = 'multiply'
    
      context.lineWidth = width/200 //*y/2000
      context.lineJoin = 'round'
      context.beginPath();
      context.moveTo(0,middle)
      context.strokeStyle = lineColor
      context.quadraticCurveTo(
        0 + Math.abs(Math.sin(time) * size / 4, 0),
        y + Math.cos(time / 2 + y / 8) * middle / 2,
        middle + Math.cos(time) * middle/2 + middle /4 ,
        middle // + Math.sin(time) * middle
      )
      context.quadraticCurveTo(
        middle + Math.sin(time + y / 100) * middle / 50,
        middle - Math.cos(time + y / 15) * middle,
        width - Math.sin(time),
        y
      )
      // context.globalCompositeOperation = 'mask'
      context.stroke()
      // context.restore()
      
    }
    
    // context.fillStyle = 'white';
    context.save()
    context.globalCompositeOperation = 'overlay'
    context.fillStyle = background;
    context.fillRect(0, 0, width, height);
    context.lineWidth = width/200
    context.restore()


  };
  
};

canvasSketch(sketch, settings);
