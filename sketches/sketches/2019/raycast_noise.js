const canvasSketch = require('canvas-sketch')
const palettes = require('nice-color-palettes')
const random = require('canvas-sketch-util/random')

const palette = random.pick(palettes)

console.log(palette)

// Ensure ThreeJS is in global scope for the 'examples/'
global.THREE = require('three')

var raycaster = new global.THREE.Raycaster()
var painter = new global.THREE.Vector2()

// Include any additional ThreeJS examples below
// require('three/examples/js/controls/OrbitControls');

const settings = {
  // Make the loop animated
  animate: true,
  // Get a WebGL canvas rather than 2D
  context: 'webgl',
  fps: 2,
  // Turn on MSAA
  attributes: { antialias: true },
}

const params = {
  width: 50,
  height: 50,
  depth: 0,
  get offsetX() {
    return this.width / 2
  },
  get offsetY() {
    return this.height / 2
  },
  get offsetZ() {
    return this.depth / 2
  },
  spacingX: 0.01,
}

const simpleBox = (vec3) => {
  let mesh = new THREE.Mesh(
    new THREE.BoxGeometry(0.3, 0.3, 0.3),
    new THREE.MeshPhysicalMaterial({
      color: 'white',
      roughness: 0.75,
      flatShading: true,
    }),
  )
  mesh.position.copy(vec3)
  return mesh
}

const drawSphere = () => {
  var geometry = new THREE.SphereGeometry(0.25, 32, 32)
  var material = new THREE.MeshBasicMaterial({ color: '#ffffff' })
  var sphere = new THREE.Mesh(geometry, material)
  return sphere
}

const drawBox = (params, i, j, k) => {
  let mesh = new THREE.Mesh(
    new THREE.BoxGeometry(0.5, 0.5, 0.1),
    new THREE.MeshPhysicalMaterial({
      color: 'white',
      roughness: 0.75,
      flatShading: true,
    }),
  )

  // let variance = rando/m.noise3D(i,j,k) * 0.2

  mesh.i = i
  mesh.j = j
  mesh.k = k

  mesh.v3 = new THREE.Vector3(i, j, k)
  mesh.noise = random.noise3D(i, j, k)

  mesh.position.set(
    i - params.offsetX, // * (i - params.offsetX),
    j - params.offsetY,
    k - params.offsetZ,
  )
  mesh.rotation.x = random.noise2D(i, mesh.noise)
  mesh.rotation.y = random.noise2D(j, mesh.noise)
  mesh.rotation.z = random.noise2D(k, mesh.noise)
  return mesh
}

function getVec2(obj) {
  // calculate mouse position in normalized device coordinates
  // (-1 to +1) for both components

  painter.x = (obj.x / window.innerWidth) * 2 - 1
  painter.y = -(obj.y / window.innerHeight) * 2 + 1
}

const toScreenXY = (obj, camera) => {
  var vector = obj.clone()

  vector.project(camera)
  vector.z = 0
  return vector
}

const sketch = ({ context }) => {
  // Create a renderer
  const renderer = new THREE.WebGLRenderer({
    context,
  })

  // WebGL background color
  renderer.setClearColor(random.pick(palette), 1)

  // Setup a camera
  const camera = new THREE.PerspectiveCamera(45, 1, 0.01, 100)
  camera.position.set(0, 0, 80)
  camera.lookAt(new THREE.Vector3())

  // Setup camera controller
  // // const controls = new THREE.OrbitControls(camera);

  // Setup your scene
  const scene = new THREE.Scene()
  const meshes = []

  for (let i = 0; i <= params.width; i++) {
    for (let j = 0; j <= params.height; j++) {
      for (let k = 0; k <= params.depth; k++) {
        mesh = drawBox(params, i, j, k)
        meshes.push(mesh)
        scene.add(mesh)
      }
    }
  }

  // Specify an ambient/unlit colour
  scene.add(new THREE.AmbientLight(random.pick(palette), 0.4))
  scene.add(new THREE.AmbientLight('#ffffff', 1))

  // Add some light
  const lightBack = new THREE.PointLight(random.pick(palette), 2, 200)
  lightBack.position.set(-6, -6, -10).multiplyScalar(1.5)
  scene.add(lightBack)

  lightpos = new THREE.Vector3(-4, 6, -5)
  // scene.add(simpleBox(lightpos))

  const floatyLight = new THREE.PointLight(random.pick(palette), 1, 9)
  floatyLight.position.copy(lightpos).multiplyScalar(1.5)
  // scene.add(floatyLight);

  let sphere = drawSphere()
  // scene.add(sphere)

  // draw each frame
  return {
    // Handle resize events here
    resize({ pixelRatio, viewportWidth, viewportHeight }) {
      renderer.setPixelRatio(pixelRatio)
      renderer.setSize(viewportWidth, viewportHeight)
      camera.aspect = viewportWidth / viewportHeight
      camera.updateProjectionMatrix()
    },
    // Update & render your scene here
    render({ time }) {
      // window.addEventListener( 'mousemove', onMouseMove, false );

      raycaster.setFromCamera(toScreenXY(floatyLight.position, camera), camera)

      var intersects = raycaster.intersectObjects(scene.children)

      for (var i = 0; i < intersects.length; i++) {
        // intersects[ i ].object.material.color.set( random.pick(palette) );
      }

      floatyLight.position.y = Math.sin(time) * 5
      floatyLight.position.x = Math.sin(time) * 6

      let v3 = new THREE.Vector3(
        Math.sin(time * 0.3) * 10,
        Math.sin(time * 0.5) * 20,
        Math.sin(time) * 6,
      )

      floatyLight.position.copy(v3)
      sphere.position.copy(v3)

      meshes.forEach((mesh) => {
        mesh.rotation.y += Math.abs(mesh.noise * 0.1) //* (10 * Math.PI / 180);
        mesh.rotation.x += Math.abs(mesh.noise * 0.1)
        mesh.rotation.z += Math.abs(mesh.noise * 0.1)
        // if(mesh.i === 1) {
        // }
        let lightDiff = mesh.position.distanceTo(floatyLight.position)

        let scale = lightDiff > 4 ? 2 : 0
        scale = 2

        mesh.scale.x = scale // Math.abs(lightDiff - 10)
        mesh.scale.y = scale // Math.abs(lightDiff - 10)
        mesh.scale.z = scale // Math.abs(lightDiff - 10)
        // mesh.rotation.y = scale
      })

      // lightWhite.position.x = Math.sin(time * 0.3) * 6
      // lightWhite.position.y = Math.sin(time * 0.5) * 5
      // lightWhite.position.z = Math.sin(time) * -5

      // controls.update();
      // renderer.render(scene, camera);
    },
    // Dispose of events & renderer for cleaner hot-reloading
    unload() {
      // controls.dispose();
      renderer.dispose()
    },
  }
}

canvasSketch(sketch, settings)
