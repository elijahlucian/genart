const canvasSketch = require('canvas-sketch');
const createShader = require('canvas-sketch-util/shader');
const random = require('canvas-sketch-util/random')
const palettes = require('nice-color-palettes')
const glsl = require('glslify');


// Setup our sketch
const settings = {
  context: 'webgl',
  dimensions: [ 512, 512 ],
  // dimensions: [ 11000, 11000 ],
  animate: true,
};

const toRGB = (hex) => {
  const n = parseInt(hex.split(/#/)[1], 16);
  return [(Math.floor(n / 256 / 256) %256)/255, (Math.floor(n / 256) % 256) /255, (n % 256) / 255]
}

const palette = random.pick(palettes)

const color1 = toRGB(random.pick(palette))
const color2 = toRGB(random.pick(palette))
console.log(color1)

// Your glsl code
const frag = glsl(/* glsl */`
  precision highp float;
  uniform float aspect;
  uniform float time;
  varying vec2 vUv;

  #pragma glslify: noise = require('glsl-noise/simplex/2d');
  #pragma glslify: hsl2rgb = require('glsl-hsl2rgb');

  float dankSin(float v,float x,float t){
    return sin(v*x+t)*.5+.5;
  }
  
  float nsin(float v){
    return sin(v)*.5+.5;
  }
  
  float ncos(float v){
    return cos(v)*.5+.5;
  }
  

  void main () {
    vec2 uv=vUv;
  
    float t=time*.3;
    float u=uv.x;
    float v=uv.y;
    float PI=3.14;
    float TAU=PI*2.;
    float mult=5.5;
    
    // VARS
    float su=dankSin(u,TAU*mult,0.);
    float sv=dankSin(v,TAU*mult,0.);
    
    float d=distance(uv,vec2(.5,.5));
    float du=distance(u,.5);
    float dv=distance(v,.5);
      
    // SHAPES
  
    float waves=mod(
      distance(
        su*sin(.05*t*TAU+nsin(v*10.+t+u*3.)*2.),
        su+sin(.05*t*3.+cos(u*10.+t*2.))
      ),
      .4
    );
  
  
    vec3 circles = vec3(distance(9. * du * d, sin(v)));
    //circles = mod(t - circles * circles * 15., 1); 
    vec3 shape = mod(t - 2. * circles * circles,1.) + circles * waves;
  //  shape = mod(t - shape * shape * 15., 1.);
      
    // COLORS
    float r=1.;
    float g=.2;
    float b=.5;
    
    vec3 color=vec3(u*r,g,b);
      
    vec3 final = color * shape * 5.;
      
    gl_FragColor=vec4(final,1.);
    
  }
`);

// Your sketch, which simply returns the shader
const sketch = ({ gl }) => {
  // Create the shader and return it
  return createShader({
    // Pass along WebGL context
    clearColor: false,
    gl,
    // Specify fragment and/or vertex shader strings
    frag,
    // Specify additional uniforms to pass down to the shaders
    uniforms: {
      // Expose props from canvas-sketch
      time: ({ time }) => time,
      aspect: ({ width, height }) => width / height
    }
  });
};

canvasSketch(sketch, settings);
