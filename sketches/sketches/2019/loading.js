const canvasSketch = require('canvas-sketch');
const math = require('canvas-sketch-util/math')
const random = require('canvas-sketch-util/random')

const niceColorPalletes = require('nice-color-palettes')
const myPalletes = require('../../modules/palettes')
// const gatewayLog = require('./gateway_log')
const utils = require('../../modules/utils')
const maf = require('../../modules/maf')
const drawingUtils = require('../../modules/drawingUtils')
const mathUtils = require('../../modules/mathUtils')
const genearteSettings = require('../../modules/generate')
const generate = require('../../modules/generate')
const { palettes } = require('../../modules/palettes')

let seed = "562582"

random.setSeed(random.getRandomSeed())

const settings = {
  animate: true,
  dimensions: [1024,1024],
  fps: 30,
}

const sketch = () => {
  const size = 80
  const randomRadius = true 
  const radius = 25

  console.log("Random Seed =>",random.getSeed())

  // palette = random.pick(niceColorPalletes)
  palette = palettes.tambo
  reducedPalette = palette.slice(0,3)
  background = random.pick(palette)

  // window.addEventListener("keydown", utils.newSeed, false);
  
  const gradient = (context, start, end, width, height, left_x=0, top_y=0) => { 
    fill = context.createLinearGradient(left_x, top_y, width, height)
    fill.addColorStop(0, start)
    fill.addColorStop(1, end)
    return fill
  }

  const lerp = (min, max, t) => { 
    return (max - min) * t + min
  }

  points = drawingUtils.createGrid({ size, randomRadius, radius }).filter(() => random.value() > 0.8)
  
  document.body.style.backgroundColor = background

  return ({ context, width, height, time }) => {

    let state = {
      rotation: 0
    }

    // const margin = width / 8
    const margin = -100
    let count = 0
    // context.fillStyle = gradient(context, background[0], background[1], width/3, height)
    context.fillStyle = background
    context.fillRect(0,0,width,height)

    points.forEach(point => {
      
      let x = maf.lerp({min: margin, max: width - margin, t: point.position.u})
      let y = maf.lerp({min: margin, max: height - margin, t: point.position.v})
      let fontSize = point.radius * width * 10
      let length = 200
      
      let noiseX = Math.sin(time * 0.2) * width / 10 * point.noise + 0
      let noiseY = 0 + random.noise2D(time * 0.2,point.position.u) * height/40
      // TODO - 2019-01-23 16:09 - EL - Idea!

      context.fillStyle = point.color
      
      context.save()
        context.translate(x,y)
        context.rotate(Math.PI * point.noise * 0.1)
        context.fillRect(noiseX,noiseY,width / 15,width / 15)
      context.restore()
      
      count += 1
    });
    context.save()

      context.translate(width/1.5,height / 1.8)
      context.fillStyle = '#00000033'
      context.font = `42px "Inconsolata"` // `${margin/10}px`
      context.fillText("@ELI7VH", 400, 400)
      context.translate(-width/550,-height/250)
      context.fillStyle = background
      context.font = `42px "Inconsolata"` // `${margin/10}px`
      context.fillText("@ELI7VH", 400, 400)
    context.restore()

    // console.log("> rendered!")

  };
};

canvasSketch(sketch, settings);
