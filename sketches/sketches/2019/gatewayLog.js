const canvasSketch = require('canvas-sketch');
const random = require('canvas-sketch-util/random')
const niceColorPalletes = require('nice-color-palettes')
const gatewayLog = require('../../data/gateway_log')


const settings = {
  // dimensions: 'a4', // 
  // dimensions: [ 9, 16 ],
  // dimensions: [ 2048, 2048 ], // instagram
  // dimensions: [ 6500, 6500 ], // instagram
  dimensions: [ 11100, 11100 ], // instagram
  // orientation: 'landscape',
  // units: 'in',
  pixelsPerInch: 300,
};

const sketch = () => {

  let seed = "1"

  random.setSeed(seed)
  const size = 80
  const randomRadius = true 
  const radius = 25

  const newSeed = (e) => {
    console.log(e)
    seed += e.key
    random.setSeed(seed)
    points = createGrid({
      size,
      randomRadius,
      radius,
    })
    window.requestAnimationFrame(() => {
      // console.log("a callback? wtf", seed)
    })
    e.preventDefault()

  }

  window.addEventListener("keydown", newSeed, false);

  palette = random.pick(niceColorPalletes)
  reducedPalette = palette.slice(0,3)
  background = palette.slice(3,5)

  const gradient = (context, start, end, width, height, left_x=0, top_y=0) => { 
    fill = context.createLinearGradient(left_x, top_y, width, height)
    fill.addColorStop(0, start)
    fill.addColorStop(1, end)
    return fill
  }
  
  const lerp = (min, max, t) => { 
    return (max - min) * t + min
  }

  const createGrid = (args) => {
    const points = []
    const size = args.size
    
    for (let y = 0; y < size - 30; y++) {
      for (let x = 0; x < size; x++) {
    
        const u = x / (size - 1)
        const v = y / (size - 31)
        const radius = Math.max(0, random.noise2D(u,v) + 1) * 0.01
        
        points.push({
          color: random.pick(reducedPalette),
          position: {u, v},
          radius,
        })
      }
    }
    return points
  }

  points = createGrid({ size, randomRadius, radius })//.filter(() => random.value() > 0.5)
  
  return ({ context, width, height }) => {
    const margin = width / 10
    let count = 0
    // context.fillStyle = gradient(context, '#f4225a', '#f4b800', width/3, height)
    context.fillStyle = random.pick(background) // 'white'
    context.fillRect(0,0,width,height)
    
    random.setSeed()

    const startIndex = random.rangeFloor(0,gatewayLog.text.length)
    points.forEach(point => {
      
      let x = lerp(0, width, point.position.u)
      let y = lerp(-105, height + 15, point.position.v)
      
      // TODO - 2019-01-23 16:09 - EL - Idea!
      // DIYarc(context, x, y, 0, Math.PI *2, true, 'white') 
      context.fillStyle = point.color
      context.font = `bold ${margin/7}px "Fira Code"` // `${margin/10}px`
      context.fillText(gatewayLog.text[startIndex + count], x, y)
      
      count += 1
    });
      
  };
};

canvasSketch(sketch, settings);
