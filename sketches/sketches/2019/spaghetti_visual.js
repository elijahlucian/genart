const canvasSketch = require('canvas-sketch');
const math = require('canvas-sketch-util/math')
const random = require('canvas-sketch-util/random')
const niceColorPalletes = require('nice-color-palettes')
const myPalletes = require('../../modules/palettes')
const BezierEasing = require('bezier-easing')

const utils = require('../../modules/utils')
const maf = require('../../modules/maf')
const generate = require('../../modules/generate')

const canvasSize = 4096

const settings = {
  dimensions: [ canvasSize, canvasSize ], // instagram
  animate: true,
};

let seeds = utils.paletteSeeds

// let seed = seeds.midnight
random.setSeed(random.getRandomSeed())
// random.setSeed(seed)

console.log("Random Seed =>",random.getSeed())

palette = random.pick(niceColorPalletes)
reducedPalette = palette.slice(0,3)
backgroundPalette = palette.slice(3,5)

var backgroundColor = random.pick(backgroundPalette)

const sketch = ({context, canvas}) => {
  
  const size = 20
  const randomRadius = true 
  const radius = 25

  const newSeed = (e) => {
    console.log(e)
    seed += e.key
    random.setSeed(seed)

    // points = createGrid({
    //   size,
    //   randomRadius,
    //   radius,
    // })
    // window.requestAnimationFrame(() => {
      // console.log("a callback? wtf", seed)
    // })
    // e.preventDefault()
  }
  window.addEventListener("keydown", newSeed, false);
  
  const gradient = (context, start, end, width, height, left_x=0, top_y=0) => { 
    fill = context.createLinearGradient(left_x, top_y, width, height)
    fill.addColorStop(0, start)
    fill.addColorStop(1, end)
    return fill
  }

  const lerp = (min, max, t) => { 
    return (max - min) * t + min
  }

  const reDraw = () => {
    random.setSeed(random.getRandomSeed())
    
    palette = random.pick(niceColorPalletes)
    reducedPalette = palette.slice(0,3)
    backgroundPalette = palette.slice(3,5)

    backgroundColor = random.pick(backgroundPalette)

  }

  const createGrid = (args) => {
    const points = []
    const size = args.size
    let index = 0
    
    for (let y = 0; y < size; y++) {
      for (let x = 0; x < size; x++) {
        index += 1
        const u = x / (size - 1)
        const v = y / (size - 1)
        const radius = Math.max(0, random.noise2D(u,v) + 1) * 0.01
        
        points.push({
          color: random.pick(reducedPalette),
          index,
          position: {u, v},
          radius,
          rotation: random.noise2D(u,v)
        })
      }
    }
    return points
  }

  points = createGrid({ size, randomRadius, radius }).filter(() => random.value() > 0.8)
  points = utils.shuffle(points)

  let transparentThing =  backgroundColor + "11"
  
  var state = {
    rotation: 0
  }
  
  // var image_data = context.createImageData(canvasSize.w, canvasSize.h); // create image data
  // var buffer32 = new Uint32Array(image_data.data.buffer);  // get 32-bit view
  
  // var gl = canvas.getContext("webgl")
  // console.log('webgl => ', gl)

  let sineTime = 0
  let easeFn = BezierEasing(.28, .85, .43, 1)

  return ({ context, width, height, time }) => {

    const margin = width / 8
    let count = 0
    // context.fillStyle = gradient(context, background[0], background[1], width/3, height)
    context.fillStyle = backgroundColor // 'white'
    context.fillRect(0,0,width,height)

    sineTime = Math.sin(time / 5 ) * 2
    
    points.forEach(point => {
      let x = maf.lerp({min: margin, max: width - margin, t: point.position.u})
      let y = maf.lerp({min: margin, max: height - margin, t: point.position.v})
      let fontSize = point.radius * width * 10
      let length = 8000

      // TODO - 2019-01-23 16:09 - EL - Idea!
      // DIYarc(context, x, y, 0, Math.PI *2, true, 'white') 
      context.fillStyle = point.color
      context.font = `${fontSize}px "Inconsolata"` // `${margin/10}px`
      
      context.save()
          
      // context.shadowColor = 'rgba(0,0,0,0.2)'
      // context.shadowBlur = 20
      context.translate(x,y)
      state.rotation += 1.0005
      // context.rotate(state.rotation)
      context.rotate(point.rotation * easeFn(sineTime))
      // context.fillRect(0,0,canvasSize/100,canvasSize/100)

      
      context.fillRect(
        -canvasSize.w*1.5,
        0 - Math.sin(point.index + time),
        canvasSize.w*10,
        canvasSize.w/52.5 + Math.sin(point.index + time)*2
      )
      // context.fillText("☠", 0-fontSize/2, 0+ fontSize/3)
      context.restore()
      
      context.save()
      context.globalCompositeOperation = "multiply"
      context.fillStyle = transparentThing
      context.fillRect(0,0,1920,1080)
      context.restore()

      count += 1
    });

    // context.restore()
    
    // let osc = generate.darkNoise(document, context, width, height, backgroundColor, time)
    // console.log("> rendered!")
    // generate.bufferNoise(context, buffer32, image_data)
    // console.log(buffer32)
    // context.putImageData(image_data, 0, 0, 0,0,width,height);
    // console.log(image_data)
  };
};

canvasSketch(sketch, settings);
