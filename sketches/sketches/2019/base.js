const canvasSketch = require('canvas-sketch');
const math = require('canvas-sketch-util/math')
const random = require('canvas-sketch-util/random')
const niceColorPalletes = require('nice-color-palettes')
const myPalletes = require('./palletes')
// const gatewayLog = require('./gateway_log')
const utils = require('./utils')
const drawingUtils = require('./utils/drawingUtils')
const mathUtils = require('./utils/mathUtils')
const settings = require('./settings/general')
const generate = require('./generate')
const dopeSeeds = require('./helpers/dopeSeeds')
const dopePalettes = require('./helpers/dopePalettes')

let seed = "562582"

random.setSeed(random.getRandomSeed())
// random.setSeed(seed)

const settings = {
  suffix: `__seed_${random.getSeed()}`,
  // dimensions: 'a4', // 
  // dimensions: [ 9, 16 ],
  animate: true,
  dimensions: [1920,1080],
  antialias: true,
  // dimensions: [ 2048, 2048 ], // instagram
  // dimensions: [ 4096, 4096 ], // instagram
  // dimensions: [ 8192, 8192 ], // instagram
  // orientation: 'landscape',
  // units: 'in',
  // pixelsPerInch: 300,
};

const sketch = () => {
  const size = 80
  const randomRadius = true 
  const radius = 25

  console.log("Random Seed =>",random.getSeed())

  // palette = random.pick(niceColorPalletes)
  palette = dopePalettes.tambo
  reducedPalette = palette.slice(0,3)
  background = random.pick(palette)

  // window.addEventListener("keydown", utils.newSeed, false);
  
  const gradient = (context, start, end, width, height, left_x=0, top_y=0) => { 
    fill = context.createLinearGradient(left_x, top_y, width, height)
    fill.addColorStop(0, start)
    fill.addColorStop(1, end)
    return fill
  }

  const lerp = (min, max, t) => { 
    return (max - min) * t + min
  }

  const createGrid = (args) => {
    const points = []
    const size = args.size
    
    for (let y = 0; y < size; y++) {
      for (let x = 0; x < size; x++) {
    
        const u = x / (size - 1)
        const v = y / (size - 1)
        const radius = Math.max(0, random.noise2D(u,v) + 1) * 0.01
        const noise = random.noise2D(u,v)

        points.push({
          color: random.pick(palette),
          position: {u, v},
          radius,
          rotation: noise,
          noise: noise,

        })
      }
    }
    return points
  }

  points = createGrid({ size, randomRadius, radius }).filter(() => random.value() > 0.8)
  
  document.body.style.backgroundColor = background

  return ({ context, width, height, time }) => {

    let state = {
      rotation: 0
    }

    // const margin = width / 8
    const margin = -100
    let count = 0
    // context.fillStyle = gradient(context, background[0], background[1], width/3, height)
    context.fillStyle = background
    context.fillRect(0,0,width,height)

    context.strokeStyle = 'white'
    context.lineWidth = 10
    let h = height / 2
    let collection = [1,2,3,4]
    collection.forEach(i => {
      let offset = i * 50
      let day = Math.PI * 2
      let hour = width / 24
      context.moveTo(0, h + offset)
      for(let x = 0; x <= width; x += hour) {
        let y = Math.sin(x / width * day * 10) * height / 4 + h
        x / width * day <= i ? context.lineTo(x, y + offset) : context.lineTo(x,h + offset)
      }  
      context.stroke()
    });
    

    context.save()

      context.translate(width/1.5,height / 1.8)
      context.fillStyle = '#00000033'
      context.font = `42px "Inconsolata"` // `${margin/10}px`
      context.fillText("@ELI7VH", 400, 400)
      context.translate(-width/550,-height/250)
      context.fillStyle = background
      context.font = `42px "Inconsolata"` // `${margin/10}px`
      context.fillText("@ELI7VH", 400, 400)
    context.restore()

    // console.log("> rendered!")

  };
};

canvasSketch(sketch, settings);
