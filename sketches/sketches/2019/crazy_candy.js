const canvasSketch = require('canvas-sketch');
const math = require('canvas-sketch-util/math')
const random = require('canvas-sketch-util/random')
const niceColorPalletes = require('nice-color-palettes')
// const gatewayLog = require('./gateway_log')

// let seed = "956508" // && null

// window.fetch('http://www.colourlovers.com/api/palettes/top?format=json&numResults=100').then(data => console.log(data))

seed = {
  set: "241057",
  generate: random.getRandomSeed()
}

random.setSeed(seed.set || random.getRandomSeed())
// random.setSeed(seed)

const settings = {
  suffix: `__seed_${random.getSeed()}`,
  // dimensions: 'a4', // 
  // dimensions: [ 9, 16 ],
  // // dimensions: [ 512, 512 ], // instagram
  // dimensions: [ 1024, 1024 ], // instagram
  // dimensions: [ 2048, 2048 ], // instagram
  dimensions: [ 6500, 6500 ], // instagram
  // orientation: 'landscape',
  animate: true,
  fps: 24,
  // units: 'in',
  // pixelsPerInch: 300,
};

// code 0047

const sketch = () => {
  const size = 20
  const randomRadius = true 
  const radius = 25

  
  console.log("Random Seed =>",random.getSeed())
  
  // random.setSeed('893158')

  palette = random.pick(niceColorPalletes)
  foreground = palette.slice(0,3)
  background = palette.slice(3,5)

  // window.addEventListener("keydown", utils.newSeed, false);
  
  const gradient = (context, start, end, width, height, left_x=0, top_y=0) => { 
    fill = context.createLinearGradient(left_x, top_y, width, height)
    fill.addColorStop(0, start)
    fill.addColorStop(1, end)
    return fill
  }

  const lerp = (min, max, n) => { 
    return (max - min) * n + min
  }

  const createGrid = (args) => {
    const points = []
    const size = args.size
    
    for (let y = 0; y < size; y++) {
      for (let x = 0; x < size; x++) {
    
        const u = x / (size - 1)
        const v = y / (size - 1)

        vectorScalar = 5
        let vectorNoise = 2 * Math.PI * random.noise2D(u * vectorScalar,v * vectorScalar)

        const vector = {
          i: Math.sin(vectorNoise),
          j: Math.cos(vectorNoise),
          n: vectorNoise,
        }

        const i = points.length
        
        points.push({
          // color: random.pick(foreground),
          color: foreground[i % 3],
          rotation: 0,
          vector,
          x,
          y,
          i,
          position: {u, v},
          rotation: random.noise2D(u,v)
        })
      }
    }
    return points
  }
  
  points = createGrid({ size, randomRadius, radius })//.filter(() => random.value() > 0.8)
  const foregroundColor = random.pick(foreground)
  
  return ({ context, width, height, time }) => {
    // bgColor = gradient(context, background[0], background[1], width/3, height)
    context.fillStyle = background[0] // 'white'
    context.fillRect(0,0,width,height)
    
    let state = {
      rotation: 0
    }

    const margin = width / 8
    const lineLength = width / 40

    let count = 0

    context.strokeStyle = foregroundColor

    points.forEach(point => {
      
      let {u,v} = point.position
      const {i,j} = point.vector
      
      
      u = random.noise2D(u/4, time/20) + 1
      v = random.noise2D(v/4, time/20) + 1

      u = Math.sin(point.i / 7.3 + time/4 + u) /2 + 0.5
      v = Math.cos(point.i / 10 + time/4 + v) /2 + 0.5

      context.save()
      
      context.translate(
        margin / 2 + ((u) * (width - margin)), 
        margin / 2 + ((v) * (height - margin))
      )

      context.rotate(point.rotation += u /100 )

      context.lineCap = 'round'
      context.lineWidth = v * width * 0.05
      context.strokeStyle = point.color

      let lineX = i*lineLength / 2
      let lineY = j*lineLength / 2

      context.beginPath()
      context.moveTo(-lineX,-lineY)
      context.lineTo(lineX,lineY)
      context.stroke()
      
      context.restore()

    });

    console.log("> rendered!")

  };
};

canvasSketch(sketch, settings);
