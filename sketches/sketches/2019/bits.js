const canvasSketch = require('canvas-sketch')
const math = require('canvas-sketch-util/math')
const random = require('canvas-sketch-util/random')
const niceColorPalletes = require('nice-color-palettes')

random.setSeed(random.getRandomSeed())

random.setSeed('253703') // '484082')
console.log('Random Seed =>', random.getSeed())

const palette = random.pick(niceColorPalletes)

random.setSeed(random.getRandomSeed())
// random.setSeed(random.getRandomSeed())

const settings = {
  suffix: `__seed_${random.getSeed()}`,
  // dimensions: 'a4', /
  // dimensions: [ 9, 16 ],
  dimensions: [12000, 12000], // instagram
  // dimensions: [ 4096, 4096 ], // instagram
  // orientationW: 'landscape',
  // units: 'in',
  // pixelsPerInch: 300,
}

const sketch = ({ width, height }) => {
  const size = 20
  const randomRadius = true
  const radius = 25

  const reducedPalette = palette.slice(0, 3)
  const background = palette.slice(3, 5)

  const createGrid = (args) => {
    const { width, height } = args

    let count = 0
    const points = []

    const gap = 13

    for (let y = 0; y < height; y += gap * 9) {
      for (let x = 0; x < width; x += gap * 2) {
        const u = x / width
        const v = y / height

        points.push({
          color: random.pick(reducedPalette),
          position: { u, v },
        })
        count++
      }
    }

    console.log(points)
    return points
  }

  const points = createGrid({ width, height, randomRadius, radius }).filter(
    () => random.value() > 0.96,
  )

  return ({ context, width, height }) => {
    const widthMargin = 0 //width * 0.12
    const heightMargin = 0 //height * 0.18
    let count = 0
    // context.fillStyle = gradient(context, background[0], background[1], width/3, height)
    context.fillStyle = background[0] // 'white'
    context.fillRect(0, 0, width, height)

    points.forEach((point) => {
      let x = math.lerp(widthMargin, width - widthMargin, point.position.u)
      const gap = 100
      x = Math.floor(x / gap) * gap
      const y = math.lerp(heightMargin, height - heightMargin, point.position.v)

      const fontSize = point.radius * width * 10
      const length =
        width * 0.1 +
        Math.abs(
          (random.noise2D(point.position.u, point.position.v) * width) / 50,
        )

      // TODO - 2019-01-23 16:09 - EL - Idea!
      // DIYarc(context, x, y, 0, Math.PI *2, true, 'white')
      context.fillStyle = point.color
      context.font = `${fontSize}px "Inconsolata"` // `${margin/10}px`

      context.save()
      context.translate(x, y)

      context.fillRect(-length / 2, 0, length, height * 0.01)

      context.restore()

      count += 1
    })

    // context.restore()

    // generate.DarkNoise(context, width, height, background[1])

    console.log('> rendered!')
  }
}

canvasSketch(sketch, settings)
