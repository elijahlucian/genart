const canvasSketch = require('canvas-sketch')
const random = require('canvas-sketch-util/random')

import {palettes} from '../../modules/nice-color-palettes'
import utils from '../../modules/utils'
import dimensions from '../../modules/dimensions'

const { FracTree2 } = require('../../classes/fractrees')
const { Mouse } = require('../../classes/utils')

let seed = {
  // palette: 1577062540360,
  // determined: 1577068381203,
  random: Date.now(), //random.getRandomSeed()
}

// paletteSeed
random.setSeed(seed.palette || seed.defined || seed.random)
console.log('Palette Seed =>', random.getSeed())

const palette = random.pick(palettes)
const bg = palette.shift()
const mainColor = random.pick(palette)

let tree

// algoSeed
random.setSeed(seed.determined || seed.random)
console.log('Algo Seed =>', random.getSeed())

document.body.style.backgroundColor = bg
document.body.style.backgroundColor = '#333'
document.title = 'JS Sketches'

// const canvasSize = 2048
const settings = {
  suffix: `__seed_${random.getSeed()}`,
  // dimensions: [canvasSize,canvasSize],
  dimensions: [5000, 5000],
  // dimensions: dimensions.print.displate,
  // orientation: 'landscape',
  // units: 'in'
  // pixelsPerInch: 300,
  animate: true,
}

// TODO: WEBCAM
// TODO: Image Loader

let lines = []

const sketch = params => {
  console.log('SKETCH PARAMS =>', params)
  let { context, width, height } = params

  context.lineCap = 'square' // || "round" || "square" || "butt"
  context.lineJoin = 'square' // || "bevel" || "miter" || "square"

  const longEdge = utils.getLongEdgeFromArray([width, height])

  const config = {
    speed: 0.7,
    agent_count: 1,
    agent_limit: 250,
    trails: true,
    margin: longEdge / 20,
    lineWidth: longEdge / 150,
    shapeSize: longEdge / 80,
    bandwidthMin: 120,
    bandwidthMax: 10000,
    audio: {
      gain: -0.8,
    },
  }

  var mouse = new Mouse({ width, height, context })

  console.log('MAINCOLOR', mainColor)

  tree = new FracTree2({
    u: 0.5,
    v: 1,
    angle: 0,
    // color: mainColor,
    color: random.pick(palette),
    palette,
    transparency: random.value(),
    baseSize: longEdge / 500,
    life: 5.5,
    context,
    rand: random,
    limit: 40,
  })

  // context.fillStyle = bg // + (config.trails ? '33' : 'ff')
  context.fillStyle = '#333'
  context.fillRect(0, 0, width, height)

  // context.globalCompositeOperation = 'lighten'

  let stopped = false

  return ({ time, width, height }) => {
    if (mouse.isPressed()) {
      stopped = !stopped
    }
    if (stopped) return
    if (tree.dead) return
    tree.update({ time, width, height, speed: 2 })
    tree.draw({ fill: true, stroke: false })
  }
}

canvasSketch(sketch, settings)
