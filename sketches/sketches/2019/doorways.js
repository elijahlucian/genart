const canvasSketch = require('canvas-sketch');
const random = require('canvas-sketch-util/random')

const BezierEasing = require('bezier-easing');
const glslify = require('glslify');
const Simplex = require('simplex-noise')
const palettes = require('nice-color-palettes')

const utils = require('../../modules/utils')
const { PI, TAU } = require('../../modules/maf')
const { Tempo, musicalTime } = require('../../modules/music')
const { BPM } = require('../../modules/music')

const bpm = new BPM()
const bez = new BezierEasing(.22,.92,.73,.63)

let seed = {
  determined: '237948',
  randomized: random.getRandomSeed()
}

random.setSeed(seed.determined || seed.randomized)
console.log("Random Seed", seed.determined || seed.randomized)

const palette = random.pick(palettes)
const bg = palette.shift()

var zeroOffset = 0
var dt = 0

const settings = {
  dimensions: [ 12000, 12000 ],
  // dimensions: [ 2048, 2048 ],
  // dimensions: [ 1024, 1024 ],
  // animate: true,
};

document.body.style.backgroundColor = bg

const handleKeyDown = (event) => {
  let time = dt
  bpm.tap({time})   
  zeroOffset = dt % 1  
}

window.addEventListener('keydown', handleKeyDown)

class FunShape {
  constructor({size, color}) {
    this.size = size
    this.color = color
    this.rounded_corner = random.pick([2])
    this.algo = [
      {u:-1, v:1},
      {u:1, v:1},
      {u:1, v:-1},
      {u:-1, v:-1}
    ]
  }

  draw({context, time}) {
    context.fillStyle = this.color
    context.beginPath()
    this.algo.forEach(({u,v},i) => {
      if(i == this.rounded_corner) {
        context.lineTo(
          u*this.size,
          v*this.size
        )
        context.quadraticCurveTo(
          u * this.size,
          v * this.size * 2,
          this.algo[i+1].u * this.size,
          this.algo[i+1].v * this.size * 2
        )
      } else {
        context.lineTo(
          u * this.size,
          v * this.size * 2
        )
      }
      
    });
  
    context.fill()
    context.closePath()
    context.stroke()
  }
}

const sketch = ({width, height}) => {
  const funShape = new FunShape({size: width / 25, color: random.pick(palette)})

  const canvas = document.createElement('canvas')
  const { w, h } = utils.dimensions({width,height})

  // instantiate classes here 

  return ({ time, context, width, height }) => {
    
    dt = time
    // context.fillStyle = '#000000';
    context.fillStyle = bg
    context.fillRect(0, 0, width, height);
    const margin = 200
    const speed = 0.5
    const t = time * speed
    
    context.lineCap = "butt" // || "round" || "square" || "butt"
    context.lineJoin = "square" // || "bevel" || "miter" || "square"
    context.strokeStyle = bg
    context.lineWidth = width / 110

    // make this more atomic and shit    
    let mt
    if(bpm) {
     mt = musicalTime({
        zeroOffset,
        time,
        bpm: bpm.bpm
      })
    } else { mt = time }
    let b = mt % 1
    let q = mt * 2 % 1
    // 

    let ln = width / 50
    let length = 3

    let o = 5
    
    for (let j = 0; j <= o; j++) {
      for (let i = 0; i <= o; i++) {
        
        let u = i / (o - 1)
        let v = j / (o - 1)
        context.save()
        
        if(j%2==0) {
          context.translate(u *  width, (v) * height)  
        } else {
          context.translate(u *  width + (width / o / 2), (v) * height)  
        }
        

        for (let i = 0; i < length; i++) {
          let index = j+i+i
          funShape.draw({
            context,
            time,
            index
          })
          context.translate(
            ln,
            ln
          )
        }
        context.restore()
        
      }
    }
    context.restore()
    
  };  
};

canvasSketch(sketch, settings);