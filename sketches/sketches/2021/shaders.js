import canvasSketch from 'canvas-sketch'
import SimplexNoise from 'simplex-noise'

const seed = undefined // 1236

const noise = new SimplexNoise(seed)
import { Random, MersenneTwister19937 } from 'random-js'
const r = new Random(MersenneTwister19937.seedWithArray([seed || +new Date()]))

import { forU, mapU, mapXY } from '@dank-inc/lewps'
import { scl, iu } from '../../utils'
import { hsl } from '@dank-inc/sketchy/lib/helpers/color'
import { createCtx, dot, nhsl, xy } from './textures/basic'
import { cos, sin } from '@dank-inc/sketchy/lib/maff'

import palettes from 'nice-color-palettes'

import { fragmentShader, vertexShader } from './shaders/class'
import { BufferGeometry, MeshDepthMaterial } from 'three'

<<<<<<< HEAD
const palette = palettes[1]
=======
const palette = r.pick(palettes)
>>>>>>> 2cd0486ef5dd883721ca23a5450b93abb1533a6e

import * as THREE from 'three'
global.THREE = THREE

const settings = {
  animate: true,
  // dimensions: [1200, 1200],
  context: 'webgl',
  attributes: { antialias: true },
}

require('three/examples/js/controls/OrbitControls')

document.body.style.backgroundColor = '#111'

const sketch = ({ context, width, height }) => {
  const renderer = new THREE.WebGLRenderer({ context })
  renderer.setClearColor('#000', 0)

  const camera = new THREE.PerspectiveCamera(35, 1, 0.01, 100)
  // const c = width < height ? height / width : width / height
  // const camera = new THREE.OrthographicCamera(-c, c, -c, c)
  camera.position.set(0, 2, -4)
  camera.lookAt(new THREE.Vector3())

  const scene = new THREE.Scene()
<<<<<<< HEAD

  const controls = new THREE.OrbitControls(camera, renderer.domElement)
  // controls.autoRotate = true
=======
  const controls = new THREE.OrbitControls(scene, renderer.domElement)

  controls.autoRotate = true
>>>>>>> 2cd0486ef5dd883721ca23a5450b93abb1533a6e

  const matStandard = new THREE.MeshBasicMaterial({
    color: palette[1],
  })
  const matStandard2 = new THREE.MeshBasicMaterial({
    color: palette[3],
  })

  const material = new THREE.ShaderMaterial({
    fragmentShader,
    vertexShader,
    uniforms: {
      time: { value: 0 },
<<<<<<< HEAD
      color: { value: new THREE.Color(palette[3]) },
=======
      color: { value: new THREE.Color(palette[4]) },
>>>>>>> 2cd0486ef5dd883721ca23a5450b93abb1533a6e
    },
  })

  const u = 0.5
  const v = 0.5

  const cube = new THREE.BoxGeometry(2, 2, 2)
  const sphere = new THREE.SphereGeometry(1, 64, 32)
<<<<<<< HEAD
  const icosphere = new THREE.IcosahedronGeometry(1, 0)

  // const bufferGeo = icosphere.toBufferGeometry()

  // const bufferGo = new BufferGeometry().fromGeometry(icosphere)

  console.log(icosphere.isBufferGeometry)

  // for (const point of points) {
  //   console.log(point)
  // }

  // const meshes = mapXY(6, 2, (u, v) => {
  const mesh = new THREE.Mesh(icosphere, material)
=======
  const icosphere = new THREE.IcosahedronGeometry(0.7)

  const points = icosphere.vertices

  const mesh = new THREE.Mesh(sphere, matStandard2)

  for (const point of points) {
    const thing = new THREE.Mesh(sphere, matStandard)

    thing.scale.setScalar(0.2)

    thing.position.copy(point)

    thing.lookAt(mesh.position)

    scene.add(thing)
  }

  // const meshes = mapXY(6, 2, (u, v) => {
>>>>>>> 2cd0486ef5dd883721ca23a5450b93abb1533a6e

  mesh.position.x = v - 0.5
  mesh.position.z = scl(u - 0.5, 5)

  const s = scl(1, 0.5, 0.1)

  mesh.scale.setScalar(1)
  scene.add(mesh)
  // return mesh
  // })

  // return mesh

  scene.add(new THREE.AmbientLight('#fff', 0.1))

  const light = new THREE.PointLight('#ffffff', 2, 25.5)
  light.position.set(-2, 2, 2)
  scene.add(light)

  console.log(controls)

  return {
    resize({ pixelRatio, viewportWidth, viewportHeight }) {
      renderer.setPixelRatio(pixelRatio)
      renderer.setSize(viewportWidth, viewportHeight)
      camera.aspect = viewportWidth / viewportHeight
      camera.updateProjectionMatrix()
    },

    render({ time, deltaTime }) {
<<<<<<< HEAD
      mesh.material.uniforms.time.value += deltaTime
      mesh.rotation.y = -Math.PI // + time / 2 // deltaTime //* mesh.p
=======
      // mesh.material.uniforms.time.value = time
      mesh.rotation.y = -Math.PI / 4 + time // deltaTime //* mesh.p
>>>>>>> 2cd0486ef5dd883721ca23a5450b93abb1533a6e
      // mesh.rotation.x += 0.01
      // mesh.rotation.z += 0.005
      controls.update()
      renderer.render(scene, camera)
    },

    unload() {
      controls.dispose()
      renderer.dispose()
    },
  }
}

canvasSketch(sketch, settings)
