import { hsl } from '@dank-inc/sketchy/lib/helpers/color'

import Simplex from 'simplex-noise'
const n = new Simplex()

export const nhsl = (u) =>
  `hsl(${u * 40 + 220}, ${u * 30 + 70}%, ${u * 10 + 40}%)`

export const xy = (ctx, u, v) => {
  const { width, height } = ctx.canvas
  return [u * width, v * height]
}

export const dot = (ctx, uu) => {
  const { width, height } = ctx.canvas
  const u = uu || Math.random()
  const v = Math.random()
  const x = u * width
  const y = v * height
  const s = (Math.random() * height) / 100

  const nu = n.noise2D(Math.sin(u * Math.PI * 2) * 4, v * 8)
  ctx.fillStyle = nhsl(nu * 0.3 + 0.6, 0.7, 0.4)

  ctx.beginPath()
  ctx.arc(x, y, s, 0, Math.PI * 2)
  ctx.fill()
}

export const createCtx = (col = '#111', w = 256, h = 256) => {
  const ctx = document.createElement('canvas').getContext('2d')

  ctx.canvas.width = w
  ctx.canvas.height = h

  ctx.fillStyle = col
  ctx.fillRect(0, 0, w, h)

  return ctx
}
