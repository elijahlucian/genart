const canvasSketch = require('canvas-sketch')
const BezierEasing = require('bezier-easing')

const bez = BezierEasing(0.22, 0.92, 0.73, 0.63)

const CanvasSketch = require('canvas-sketch')

const settings = {
  // dimensions: [2048, 2048],
  dimensions: [1024, 1024],
  animate: true,
  fps: 30,
}

document.body.style.backgroundColor = '#000000'

const sketch = () => {
  const TAU = Math.PI * 2
  const PI = Math.PI

  return ({ time, context, width, height }) => {
    context.fillStyle = `hsl(180, 100%, 100%)`
    context.fillRect(0, 0, 50, 50)
  }
}

canvasSketch(sketch, settings)
