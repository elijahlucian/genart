const canvasSketch = require('canvas-sketch')
const BezierEasing = require('bezier-easing')

const bez = new BezierEasing(0.22, 0.92, 0.73, 0.63)

const settings = {
  // dimensions: [ 2048, 2048 ],
  dimensions: [1024, 1024],
  animate: true,
  fps: 30,
}

document.body.style.backgroundColor = '#000000'

const sketch = () => {
  const TAU = Math.PI * 2
  const PI = Math.PI

  return ({ time, context, width, height }) => {
    context.fillStyle = '#000000'
    context.fillRect(0, 0, width, height)

    const speed = 0.5
    const t = time * speed

    context.lineCap = 'round' // || "round" || "square" || "butt"
    context.lineJoin = 'round' // || "bevel" || "miter" || "square"

    context.strokeStyle = '#fff'
    context.lineWidth = width / 10
    let dt = t % 1

    let lineResolution = 0.01

    // chaos ring
    context.strokeStyle = `hsl(${
      Math.sin(Math.sin(0.01 * t * TAU)) * TAU * 180 + 180
    }, 100%, 50%)`
    context.lineWidth = width / 10
    context.beginPath()

    for (let u = 0; u <= TAU; u += lineResolution) {
      let x = (Math.sin(t + u * u * TAU) * width) / 4 + width / 2
      let y = (Math.cos(Math.sin(t * PI) + u * TAU) * height) / 4 + width / 2
      context.lineTo(x, y)
    }
    context.stroke()
    context.closePath()

    context.strokeStyle = '#fff'
    context.lineWidth = width / 20
  }
}

canvasSketch(sketch, settings)
