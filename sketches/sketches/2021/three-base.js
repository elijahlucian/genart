import canvasSketch from 'canvas-sketch'
import SimplexNoise from 'simplex-noise'
const noise = new SimplexNoise()

import { SuperMouse } from '@dank-inc/super-mouse'
import { forU, mapU, mapXY } from '@dank-inc/lewps'
import { scl, iu } from '../../utils'
import { hsl } from '@dank-inc/sketchy/lib/helpers/color'
import { createCtx, dot, nhsl, xy } from './textures/basic'

global.THREE = require('three')
const THREE = global.THREE

const settings = {
  // Make the loop animated
  animate: true,
  // dimensions: [1200, 1200],
  // Get a WebGL canvas rather than 2D
  context: 'webgl',
  // Turn on MSAA
  attributes: { antialias: true },
}

require('three/examples/js/controls/OrbitControls')

document.body.style.backgroundColor = '#000'

const sketch = ({ context }) => {
  const renderer = new THREE.WebGLRenderer({ context })
  renderer.setClearColor('#000', 1)

  // const camera = new THREE.PerspectiveCamera(35, 1, 0.01, 100)
  const c = 1.5
  const camera = new THREE.OrthographicCamera(c, -c, c, -c)
  camera.position.set(0, 0, 2)
  camera.lookAt(new THREE.Vector3())

  const scene = new THREE.Scene()
  const mouse = new SuperMouse({
    logging: false,
  })

  const ctx = createCtx('#000', 512 * 4, 256 * 4)
  // const n = noise.noise2D(0.5, 0.5) + 0.5

  mapXY(512 / 4, 256 / 4, (u, v) => {
    const [x, y] = xy(ctx, u, v)
    const n =
      Math.abs(noise.noise2D(Math.sin(u * Math.PI * 2) * 4, v * 8)) + 0.5
    ctx.fillStyle = hsl(n, 0.5, 0.5)
    ctx.fillRect(x, y, 4, 4)
  })

  console.log(ctx)

  const map = new THREE.CanvasTexture(ctx.canvas)

  // const material = new THREE.MeshBasicMaterial({
  const material = new THREE.MeshStandardMaterial({
    map,
    normalMap: map,
    flatShading: true,
    metalness: 0,
    roughness: 1,
  })

  map.needsUpdate = true

  // const cube = new THREE.BoxGeometry(2, 2, 2)
  const geometry = new THREE.SphereGeometry(2, 4, 3)

  const u = 0.5

  const mesh = new THREE.Mesh(geometry, material)

  const p = scl(u, 2, -1)

  mesh.position.set(p, 0, 0)

  const s = scl(u, 1, 0.1)
  mesh.u = u
  mesh.scale.set(s, s, s)
  scene.add(mesh)

  // return mesh

  scene.add(new THREE.AmbientLight('#fff', 0.1))

  const light = new THREE.PointLight('#ffffff', 2, 25.5)
  light.position.set(-2, -2, 1.5)
  scene.add(light)

  return {
    resize({ pixelRatio, viewportWidth, viewportHeight }) {
      renderer.setPixelRatio(pixelRatio)
      renderer.setSize(viewportWidth, viewportHeight)
      camera.aspect = viewportWidth / viewportHeight
      camera.updateProjectionMatrix()
    },

    render({ time, deltaTime }) {
      mouse.update()

      // for (const mesh of meshes) {
      // forU(30, (u) => {
      //   dot(u + time)
      // })
      map.needsUpdate = true
      mesh.rotation.y -= deltaTime * (0.5 * iu(mesh.u))
      // }

      // controls.update()
      renderer.render(scene, camera)
    },

    unload() {
      // controls.dispose()
      renderer.dispose()
    },
  }
}

canvasSketch(sketch, settings)
