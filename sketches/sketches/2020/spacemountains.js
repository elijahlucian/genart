import canvasSketch from 'canvas-sketch'
import * as THREE from 'three'
import SimplexNoise from 'simplex-noise'
import { Vector3, Vector2 } from 'three'
import Delaunator from 'delaunator'
import { forN } from '@dank-inc/lewps'

const simplex = new SimplexNoise()

// Ensure ThreeJS is in global scope for the 'examples/'
global.THREE = require('three')
// Include any additional ThreeJS examples below
require('three/examples/js/controls/OrbitControls')

const settings = {
  animate: true,
  // dimensions: [1080, 1080],
  // margin: 0,
  // dimensions: [1920,1080],
  // dimensions: [3840,2160],
  // dimensions: [1920*4,1080*4],
  context: 'webgl',
  // Turn on MSAA
  attributes: { antialias: true },
}

const sketch = ({ context }) => {
  const renderer = new THREE.WebGLRenderer({ context })
  const color1 = 256 // Math.floor(Math.random() * 360)
  const offset = Math.random() > 0.5 ? 120 : 180
  const color2 = color1 + offset * Math.random() > 0.5 ? 1 : -1

  console.log('BGCOLOR => ', color1)
  const backgroundColor = `hsl(${color1},30%, 20%)`

  renderer.setClearColor(backgroundColor, 1)

  const camera = new THREE.PerspectiveCamera(15, 1, 0.01, 500)
  camera.position.set(0, 1, 2)
  camera.lookAt(new THREE.Vector3(0, 0, 0))

  const scene = new THREE.Scene()
  // scene.fog = new THREE.FogExp2(backgroundColor, 0.008);

  // const controls = new THREE.OrbitControls(camera)
  const SIZEX = 1
  const SIZEZ = SIZEX // 0.5
  const peak = SIZEX
  const points = []

  const TOTAL_POINTS = 1000

  forN(TOTAL_POINTS, (i) => {
    const r = THREE.Math.randFloatSpread(2)
    const x = Math.sin(i) * 0.7 * r
    const z = Math.cos(i) * 0.7 * r

    // let x = THREE.Math.randFloatSpread(SIZEX)
    // let z = THREE.Math.randFloatSpread(SIZEZ)

    // console.log("THINGS =>", x,z)

    const u = x / SIZEX
    const w = z / SIZEZ

    const noiseWeight = 5 // TOTAL_POINTS / 10

    const n2 = simplex.noise2D(u * noiseWeight, w * noiseWeight)
    const nx = 0 // simplex.noise2D(u*2,w*2) * 0.2
    const n1 = simplex.noise2D(nx + u + n2 * 0.1, nx + w)

    // get closeness to edges
    // as gets closer to edge, reduce / increse peak

    // const edgedist = distFromRectEdge(u,w,1,1)

    const a = new Vector2(x, z)
    const b = new Vector2()

    const edgedist = b.distanceTo(a)

    const edgemod = Math.max(1 - edgedist - 0.5, simplex.noise2D(x, z) * 0.02)

    const y = (1 + n1) * edgemod * peak // simplex.noise2D(u,w) * v * 15 - v * 15

    points.push(new Vector3(x, y < 0.3 ? y : 0.3 + y * 0.1, z))
  })

  const geometry = new THREE.BufferGeometry().setFromPoints(points)

  // const cloud = new THREE.Points(geometry, new THREE.PointsMaterial({color: '#ffffff', size: 0.1}))
  // scene.add(cloud)

  const indexDelaunay = Delaunator.from(points.map(({ x, z }) => [x, z]))

  // const meshIndex = indexDelaunay.triangles.map(t => t)
  var meshIndex = [] // delaunay index => three.js index
  for (let i = 0; i < indexDelaunay.triangles.length; i++) {
    meshIndex.push(indexDelaunay.triangles[i])
  }

  geometry.setIndex(meshIndex)
  geometry.computeVertexNormals()

  const material = new THREE.MeshPhysicalMaterial({
    color: new THREE.Color('#fff'),
    // wireframe: true,
    // side: THREE.DoubleSide,
    metalness: 0.5,
    flatShading: true,
  })
  const plane = new THREE.Mesh(geometry, material)

  scene.add(plane)

  const rightLight = new THREE.DirectionalLight(
    `hsla(${color2}, 50%, 50%)`,
    0.7,
  )
  rightLight.position.set(2, 2, 5).multiplyScalar(1.5)
  scene.add(rightLight)

  const leftLight = new THREE.DirectionalLight(`hsla(${color1}, 80%, 50%)`, 1.9)
  leftLight.position.set(-1, 2, -3).multiplyScalar(1.5)
  scene.add(leftLight)

  // const composer = addFx(renderer, scene, camera, width, height)

  return {
    resize({ pixelRatio, viewportWidth, viewportHeight }) {
      renderer.setPixelRatio(pixelRatio)
      renderer.setSize(viewportWidth, viewportHeight)
      camera.aspect = viewportWidth / viewportHeight
      camera.updateProjectionMatrix()
    },

    render({ time }) {
      // console.log("time", time)
      const speed = 1 / 6
      const t = time * speed * 0.2

      const u = Math.sin(t * Math.PI * 2)
      const v = Math.cos(t * Math.PI * 2)

      camera.position.set(u * 4, peak * 4, v * 4)
      camera.lookAt(new Vector3(0, 0, 0))

      // controls.update()
      // composer.render(time);
      renderer.render(scene, camera)
    },

    unload() {
      // controls.dispose()
      renderer.dispose()
    },
  }
}

canvasSketch(sketch, settings)
