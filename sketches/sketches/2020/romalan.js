const canvasSketch = require('canvas-sketch')
const SimplexNoise = require('simplex-noise')
const simplex = new SimplexNoise()

const settings = {
  // dimensions: [1024, 1024],
  // dimensions: [6500, 6500],
  dimensions: [11000, 11000],
  // animate: true,
}
const palette = ['#d79878', '#4E1D2A', '#989AAD', '#ba8e7e', '#3D365F']


document.body.style.backgroundColor = '#333'

let t = 0
const squares = []
const squareLimit = 75
const noiseScale = 1
margin = 5 // at 1024

document.addEventListener('keypress', ({key}) => {
  console.log("t", t)
  t += 1
})

const sketch = ({ context, width, height }) => {
  const w = width / 2
  const h = height / 2

  for (let j = 0; j <= squareLimit; j++) {
    const row = []
    for (let i = 0; i <= squareLimit; i++) {
      
      let u = i / squareLimit
      let v = j / squareLimit
      let square = { i, j, u, v }
      
      row.push(square)
      // squares.push(square)
    }
    squares.push(row)
  }

  context.strokeStyle = '#333'
  context.lineWidth = 40

  return ({ time, context, width, height }) => {
    context.fillStyle = '#0B041E'
    context.fillRect(0, 0, width, height)

    // time = time * 0.2
 
    time = t

    for (const row of squares) {
      for (const square of row) {


        let { u, v, i, j} = square
        
        // check all neighbours
        let rectSize = (Math.random() * 0.30 + 0.4) * width / squareLimit
        // let rectSize = width / squareLimit
        
        let n = simplex.noise3D(u*noiseScale,v*noiseScale,t)
        let hue = Math.sin(time + n) * 180 + 180
        context.fillStyle = palette[Math.floor(Math.random() * palette.length)]

        // slightly hue shift

        // context.fillStyle = `hsl(${hue}, 100%, 50%)`
        
        context.save()
        context.translate(u * width, v * height)
        
        context.fillRect(-rectSize / 2, -rectSize /2, rectSize, rectSize)
        context.restore()
      }
    }
  }
}

canvasSketch(sketch, settings)
