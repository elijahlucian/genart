
const canvasSketch = require('canvas-sketch');
const SimplexNoise = require('simplex-noise')

const simplex = new SimplexNoise()

// Ensure ThreeJS is in global scope for the 'examples/'
global.THREE = require('three');
const THREE = global.THREE
const BOX_COUNT = 10
// Include any additional ThreeJS examples below
require('three/examples/js/controls/OrbitControls');

const settings = {
  // Make the loop animated
  animate: true,
  // Get a WebGL canvas rather than 2D
  context: 'webgl',
  // Turn on MSAA
  attributes: { antialias: true }
};

const forN = (n, fn) => { for (let i = 0; i < n; i++) fn(i) }

const sketch = ({ context, width, height }) => {
  const renderer = new THREE.WebGLRenderer({ context });
  renderer.setClearColor('#000', 1);

  const camera = new THREE.PerspectiveCamera(35, 1, 0.01, 100);
  camera.position.set(2, 2, -2);
  camera.lookAt(new THREE.Vector3());
  const controls = new THREE.OrbitControls(camera)
  
  const scene = new THREE.Scene();

  const boxSize = 1/16
  const box = new THREE.BoxGeometry(boxSize, boxSize / 2, boxSize)

  const heightMax = 0.04
  
  const corner2corner = () => {
    box.vertices[0].y += heightMax / 2;
    box.vertices[1].y += 0.00;
    box.vertices[4].y += heightMax / 2;
    box.vertices[5].y += heightMax ;
  }

  const edge2edge = () => {
    box.vertices[0].y += heightMax / 2;
    // box.vertices[1].y += 0.00;
    // box.vertices[4].y += heightMax / 2;
    box.vertices[5].y += heightMax / 2;
  }

  // edge2edge()

  corner2corner()

  const meshes = []

  const w = 16
  const h = 16

  forN(w, x => {
    let rot = Math.PI * x
    forN(h, y => {

      const u = x / w
      const v = y / h

      console.log(x,u,y,v)

      const mesh = new THREE.Mesh(
        box,
        new THREE.MeshPhysicalMaterial({
          color: 'white',
          roughness: 0.75,
          flatShading: true
        }))

        mesh.rotation.y = (rot)
        rot += Math.random() < 0.5 ? Math.PI : Math.PI * 2



        mesh.position.x = (u - 0.5) * 1
        mesh.position.z = (v - 0.5) * 1

        scene.add(mesh);
        meshes.push(mesh)

        const ns = 2

        const s = Math.sin(1 + (u * v) * 7) * 0.5 + 0.5
        let n = simplex.noise2D(u,v) * 0.5 + 0.5
        let n3 = simplex.noise3D(0.1*u*ns*0.2,v*ns,0.1) * 0.5 + 0.5

        const blockHeight = 5
        
        n3 = Math.random() * 0.2 + 0.2

        // mesh.scale.y = n3 * blockHeight
        // mesh.position.y = n3 * 1/BOX_COUNT * blockHeight / 2

        mesh.material.color.set(new THREE.Color(`hsl(${30 + n3 * 60},80%,60%)`))

    })
  })
  scene.add(new THREE.AmbientLight('#59314f', 0.5));
  const light = new THREE.PointLight('#ffffff', 2, 15.5);
  light.position.set(1, 2, -4).multiplyScalar(1.5);
  scene.add(light);

  return {
    resize ({ pixelRatio, viewportWidth, viewportHeight }) {
      renderer.setPixelRatio(pixelRatio);
      renderer.setSize(viewportWidth, viewportHeight);
      camera.aspect = viewportWidth / viewportHeight;
      camera.updateProjectionMatrix();
    },

    render () {
      controls.update()
      renderer.render(scene, camera);
    },

    
    unload () { 
      controls.dispose()
      renderer.dispose() 
    }
  };
};

canvasSketch(sketch, settings);
