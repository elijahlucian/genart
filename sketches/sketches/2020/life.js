const canvasSketch = require('canvas-sketch')
const SimplexNoise = require('simplex-noise')
const { randomBytes } = require('crypto')
const simplex = new SimplexNoise()

const [width,height] = [1024, 1024]
// const [width,height] = [2048, 2048]
// const [width,height] = [11000, 11000]
// const [width,height] = [6500, 6500]

const settings = {
  // dimensions: [1024, 1024],
  // dimensions: [6500, 6500],
  dimensions: [width, height],
  animate: true,
}

document.body.style.backgroundColor = '#333'

let t = 0
const squares = []
const squareLimit = 15
const noiseScale = 1.5
margin = width / 100

const sketch = ({ context, width, height }) => {
  const w = width / 2
  const h = height / 2

  for (let j = 0; j < squareLimit; j++) {
    const row = []
    for (let i = 0; i < squareLimit; i++) {
      
      let u = i / squareLimit
      let v = j / squareLimit
      let square = { i, j, u, v }
      
      row.push(square)
      // squares.push(square)
    }
    squares.push(row)
  }

  context.strokeStyle = '#333'
  context.lineWidth = 40

  return ({ time, context, width, height }) => {
    context.fillStyle = '#333'
    // context.fillRect(0, 0, width, height)

    // time = time * 0.2
 
    time = t

    for (const row of squares) {
      for (const square of row) {


        let { u, v, i, j} = square
        
        // check all neighbours
        let rectSize = width / squareLimit
        
        let n = simplex.noise3D(u*noiseScale,v*noiseScale,t)

        let hue = Math.sin(time + n) * 40 + (n > 0 ? 140 : 20)
        context.fillStyle = `hsl(${hue}, 100%, 50%)`
        
        context.save()
        context.translate(u * width, v * height)
        
        context.fillRect(margin, margin, rectSize - margin, rectSize - margin)
        context.restore()
      }
    }
  }
}

document.addEventListener('keypress', ({key}) => {
  console.log("t", t)
  t += 1
})


canvasSketch(sketch, settings)
