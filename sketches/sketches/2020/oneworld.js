import { forN } from '../../utils'
import { Unit } from './classes/unit'

const canvasSketch = require('canvas-sketch')
const SimplexNoise = require('simplex-noise')

const simplex = new SimplexNoise()

const settings = {
  dimensions: [1024, 1024],
  // dimensions: [6500, 6500],
  // animate: true,
}

document.body.style.backgroundColor = '#333'

let t = 0
const squares = []
const squareLimit = 6
const noiseScale = 1
const margin = 5 // at 1024

document.addEventListener('keypress', ({key}) => {
  console.log("t", t)
  t += 1
})

const sketch = ({ context, width, height }) => {
  const w = width / 2
  const h = height / 2

  forN(squareLimit + 1, (j) => {
    const row =[]
    forN(squareLimit + 1, i => {

      const unit = new Unit({
        i,
        j,
        u: i / squareLimit,
        v: j / squareLimit, 
      })
      row.push(unit)
    })
    
    squares.push(row)
  })

  context.strokeStyle = '#333'
  context.lineWidth = 40

  return ({ time, context, width, height }) => {
    context.fillStyle = '#333'

    context.fillRect(0, 0, width, height)
    time = time * 0.1
    // time = t

    for (const row of squares) {
      for (const unit of row) {
     
        let { u, v, i, j, s} = unit
    
        context.fillStyle = `rgb(${unit.a * 255}, ${unit.b * 255}, 0.5)`

        context.save()
        context.translate(u * width, v * height)
        
        const p = -s/2

        unit.interactWith(squares)

        context.fillRect(p, p, s, s)
        context.restore()
    
      }
    }
  }
}

canvasSketch(sketch, settings)
