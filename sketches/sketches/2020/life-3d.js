import canvasSketch from 'canvas-sketch'
import { Color } from 'three'
import * as THREE from 'three'
import SimplexNoise from 'simplex-noise'
import { SuperMouse } from '@dank-inc/super-mouse'

let seed
const setSeed = () => (seed = Math.floor(Math.random() * 1000000))

// seed = 813654
const simplex = new SimplexNoise(seed || setSeed())

console.log('Random Seed =>', seed)

// Ensure ThreeJS is in global scope for the 'examples/'
global.THREE = require('three')

const BOX_COUNT = 15 // 14
// Include any additional ThreeJS examples below
require('three/examples/js/controls/OrbitControls')

const settings = {
  // animate: true,
  suffix: `life3d_${seed}`,
  // dimensions: [1024, 1024],
  animate: true,
  context: 'webgl',
  attributes: { antialias: true },
}

const sketch = ({ context }) => {
  const renderer = new THREE.WebGLRenderer({ context })
  renderer.setClearColor('#111', 1)

  const camera = new THREE.PerspectiveCamera(125, 1, 0.01, 100)
  // camera.position.set(3.5, 3, -3.5);
  camera.position.set(1, 0.6, 0)
  camera.lookAt(new THREE.Vector3())

  const scene = new THREE.Scene()

  const boxSize = 1 / BOX_COUNT
  const box = new THREE.BoxGeometry(boxSize, boxSize, boxSize)

  const world = []
  const clouds = []

  const step = 1 / BOX_COUNT

  const border = new THREE.BoxGeometry(2.15, 0.31, 2.15)
  const mesh = new THREE.Mesh(
    border,
    new THREE.MeshPhysicalMaterial({ color: '#ffffff' }),
  )
  mesh.position.y = -0.24
  scene.add(mesh)

  let i = 0
  for (let u = -1; u <= 1; u += step) {
    for (let v = -1; v <= 1; v += step) {
      const mesh = new THREE.Mesh(
        box,
        new THREE.MeshPhysicalMaterial({
          color: 'white',
          roughness: 0.75,
          flatShading: true,
        }),
      )

      const meta = { u, v, i }

      mesh.meta = meta
      mesh.position.x = u
      mesh.position.z = v

      scene.add(mesh)
      world.push(mesh)
      i++
    }
  }

  const sphere = new THREE.SphereGeometry(0.1, 5, 5)

  i = 0
  for (let u = -1; u <= 1; u += step) {
    for (let v = -1; v <= 1; v += step) {
      const mesh = new THREE.Mesh(
        sphere,
        new THREE.MeshPhysicalMaterial({
          color: 'white',
          roughness: 0.75,
          flatShading: true,
          // opacity: 0.01
        }),
      )

      mesh.position.x = u
      mesh.position.z = v
      mesh.position.y = 0.8

      const meta = { u, v, i }
      mesh.meta = meta

      scene.add(mesh)
      clouds.push(mesh)
      i++
    }
  }

  scene.add(new THREE.AmbientLight('#59314f', 0.5))
  const light = new THREE.PointLight('#ffffff', 2, 15.5)
  light.position.set(1, 2, -4).multiplyScalar(1.5)
  scene.add(light)

  const state = {
    time: 0,
    offset: 0,
  }

  const mouse = new SuperMouse({})

  return {
    resize({ pixelRatio, viewportWidth, viewportHeight }) {
      renderer.setPixelRatio(pixelRatio)
      renderer.setSize(viewportWidth, viewportHeight)
      camera.aspect = viewportWidth / viewportHeight
      camera.updateProjectionMatrix()
    },

    render({ time, deltaTime }) {
      state.time += deltaTime // += mouse.inertia * 0.1
      state.offset += mouse.inertia * 0.1
      mouse.update()
      const t = state.time * 0.9 + state.offset

      for (const cloud of clouds) {
        const { u, v } = cloud.meta
        const n3 = simplex.noise3D(0.2 * t + u, 0.1 * t + v, t * 0.1 + 2)

        if (n3 > 0.5) {
          let n = (n3 - 0.2) * 1.25
          cloud.material.visible = true
          cloud.scale.x = n * n * 2
          cloud.scale.y = n * n * 2
          cloud.scale.z = n * n * 2

          cloud.meta.active = true
          cloud.meta.n = n

          cloud.material.color.set(new Color('#dddddd'))
        } else {
          cloud.meta.active = false
          cloud.material.visible = false
        }
      }

      for (const mesh of world) {
        const ns = 1

        const { u, v, i } = mesh.meta
        const s = Math.sin(-time + u * v * 7) * 0.5 + 0.5
        const n3 = simplex.noise2D(0.2 * t + u * ns, v * ns)
        const n = n3 * 0.5 + 0.5

        let h = n3 > 0 ? 5 * n : 1
        mesh.scale.y = (n3 > 0 ? n3 * h + 1 : 1) + 6
        mesh.position.y = (n3 > 0 ? (((n3 * 1) / BOX_COUNT) * h) / 2 : 0) - 0.2

        const lightness = clouds[i].meta.active
          ? Math.floor(60 - clouds[i].meta.n * 50)
          : 50

        mesh.material.color.set(
          new Color(
            `hsl(${n * 30 + (n3 > 0 ? 90 : 200)},${
              lightness + 10
            }%,${lightness}%)`,
          ),
        )
      }

      renderer.render(scene, camera)
    },

    unload() {
      renderer.dispose()
    },
  }
}

canvasSketch(sketch, settings)
