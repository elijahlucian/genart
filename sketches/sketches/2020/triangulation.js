import canvasSketch from 'canvas-sketch'
import * as THREE from 'three'
import SimplexNoise from 'simplex-noise'
import { Vector3 } from 'three'
import Delaunator from 'delaunator'

const simplex = new SimplexNoise()

// Ensure ThreeJS is in global scope for the 'examples/'
global.THREE = require('three')
// Include any additional ThreeJS examples below
// require('three/examples/js/controls/OrbitControls');

const settings = {
  // Make the loop animated
  animate: true,
  // Get a WebGL canvas rather than 2D
  dimensions: [1920, 1080],
  // dimensions: [3840,2160],
  // dimensions: [1920*4,1080*4],
  context: 'webgl',
  // Turn on MSAA
  antialias: true,
  attributes: { antialias: true },
}

const forN = (n, fn) => {
  for (let i = 0; i < n; i++) {
    fn(i)
  }
}

const sketch = ({ context }) => {
  const renderer = new THREE.WebGLRenderer({ context })

  const backgroundColor = 'hsl(20,30%, 20%)'

  renderer.setClearColor(backgroundColor, 1)

  const camera = new THREE.PerspectiveCamera(5, 1, 0.01, 500)
  camera.position.set(0, 30, 95)
  camera.lookAt(new THREE.Vector3(2, -5, 20))

  const scene = new THREE.Scene()
  scene.fog = new THREE.FogExp2(backgroundColor, 0.008)

  // // const controls = new THREE.OrbitControls(camera)
  const TOTAL_POINTS = 50000
  const SIZEX = 150
  const SIZEZ = 100
  const scaleX = 6
  const scaleZ = 12

  const points = []

  forN(TOTAL_POINTS, () => {
    let x = THREE.Math.randFloatSpread(SIZEX)
    let z = THREE.Math.randFloatSpread(SIZEZ)

    const u = (x / SIZEX) * scaleX
    const w = (z / SIZEZ) * scaleZ

    const v = (z - SIZEZ * 0.5) / SIZEZ

    let y = Math.sin(w * 1.5 + u * 3 + simplex.noise2D(u * 2, w * 2)) * 2 // simplex.noise2D(u,w) * v * 15 - v * 15

    points.push(new Vector3(x, z > SIZEZ - 20 ? 0 : y, z))
  })

  const geometry = new THREE.BufferGeometry().setFromPoints(points)

  // const cloud = new THREE.Points(geometry, new THREE.PointsMaterial({color: '#ffffff', size: 0.1}))
  // scene.add(cloud)

  const indexDelaunay = Delaunator.from(points.map(({ x, z }) => [x, z]))

  // const meshIndex = indexDelaunay.triangles.map(t => t)
  var meshIndex = [] // delaunay index => three.js index
  for (let i = 0; i < indexDelaunay.triangles.length; i++) {
    meshIndex.push(indexDelaunay.triangles[i])
  }

  geometry.setIndex(meshIndex)
  geometry.computeVertexNormals()

  const material = new THREE.MeshPhysicalMaterial({
    color: new THREE.Color('#fff'),
    // wireframe: true,
    side: THREE.DoubleSide,
    flatShading: true,
    clearCoat: 0.6,
    metalness: 0.5,
    clearCoatRoughness: 0.35,
  })
  const plane = new THREE.Mesh(geometry, material)

  scene.add(plane)

  const TOTAL_STARS = 0

  const starsMat = new THREE.MeshBasicMaterial({
    color: 'hsla(20, 20%, 80%)',
  })

  forN(TOTAL_STARS, () => {
    const starsGeo = new THREE.SphereGeometry(
      THREE.Math.randFloat(0.2, 0.4),
      THREE.Math.randInt(2, 3),
      THREE.Math.randInt(2, 3),
    )
    const star = new THREE.Mesh(starsGeo, starsMat)
    star.rotation.x = THREE.Math.randFloat(0, Math.PI * 2)
    star.rotation.y = THREE.Math.randFloat(0, Math.PI * 2)
    star.rotation.z = THREE.Math.randFloat(0, Math.PI * 2)
    star.position.x = Math.random() * 100 - 50
    star.position.z = Math.random() * 50 - 100
    star.position.y = Math.random() * 30 + 10

    scene.add(star)
  })

  const rightLight = new THREE.DirectionalLight(`hsla(190, 50%, 50%)`, 0.7)
  rightLight.position.set(2, 2, 5).multiplyScalar(1.5)
  scene.add(rightLight)

  const leftLight = new THREE.DirectionalLight(`hsla(20, 80%, 50%)`, 1.9)
  leftLight.position.set(-1, 2, -3).multiplyScalar(1.5)
  scene.add(leftLight)

  return {
    resize({ pixelRatio, viewportWidth, viewportHeight }) {
      renderer.setPixelRatio(pixelRatio)
      renderer.setSize(viewportWidth, viewportHeight)
      camera.aspect = viewportWidth / viewportHeight
      camera.updateProjectionMatrix()
    },

    render() {
      // move all points
      // recalculate shit
      // update

      // forN(plane.geometry.vertices.length, i => {
      //   console.log(i)
      // })
      //   const vector = plane.geometry.vertices[i].clone()
      //   const {x,y} = plane.geometry.vertices[i]
      //   vector.applyMatrix4( plane.matrixWorld );

      //   // console.log(vector.x)
      //   const s = 1

      //   const xs = s * 0.4
      //   const ys = s * 0.06
      //   const u = x * xs
      //   const v = y * ys

      //   const yn = y * 0.01

      //   const n = simplex.noise2D(u,v + yn)
      //   const z = n * 0.05 * Math.abs(vector.x * vector.x * 0.2) + Math.random() * 0.05
      //   plane.geometry.vertices[i].z = z < 0 ? z : z * 0.1
      // }

      // plane.geometry.verticesNeedUpdate = true

      // controls.update()
      renderer.render(scene, camera)
    },

    unload() {
      controls.dispose()
      renderer.dispose()
    },
  }
}

canvasSketch(sketch, settings)
