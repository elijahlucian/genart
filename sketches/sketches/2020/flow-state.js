import canvasSketch from 'canvas-sketch'
import SimplexNoise from 'simplex-noise'

import { SuperMouse } from '@dank-inc/super-mouse'

// const superMouse = new SuperMouse()

const SN = new SimplexNoise()

const settings = {
  // dimensions: [1024, 1024],
  // dimensions: [6500, 6500],
  animate: true,
}

let blades = []

const makeblade = (i, n, blades, t) => {
  const v = SN.noise2D(i / n / 2, t) / 2 + 0.5
  const s = SN.noise2D(i / n, t)
  blades.push({ u: i / n, v, h: 1, s })
}

const sketch = ({ context, width, height }) => {
  context.strokeStyle = '#333'
  context.lineWidth = 40

  const n = 80

  const state = {
    blade: {
      gap: 0.04,
      next: 0.04,
    },
    scene: {
      gap: 6,
      next: 6,
    },
  }

  makeblade(blades.length, n, blades, 0)

  return ({ time, context, width, height }) => {
    context.fillStyle = '#333'
    context.fillRect(0, 0, width, height)
    context.fillStyle = '#fff'

    if (time > state.scene.next) {
      blades = []
      state.scene.next += state.scene.gap
      console.log(state.scene)
    }

    if (time > state.blade.next) {
      state.blade.next += state.blade.gap

      makeblade(blades.length, n, blades, time * 0.1)
    }

    for (const blade of blades) {
      const s = 3

      blade.h += 0.05 + blade.s * 0.3

      const h = s * blade.h
      const w = s

      const x = blade.u * width
      const y = blade.v * height - s * blade.h

      context.fillRect(x - w / 2, y, w, h)
    }
  }
}

canvasSketch(sketch, settings)
