import canvasSketch from 'canvas-sketch'
import * as THREE from 'three'
import SimplexNoise from 'simplex-noise'
import { Mesh } from 'three'

const simplex = new SimplexNoise()

// Ensure ThreeJS is in global scope for the 'examples/'
global.THREE = require('three')
// Include any additional ThreeJS examples below
require('three/examples/js/controls/OrbitControls')

const settings = {
  // Make the loop animated
  // animate: true,
  // Get a WebGL canvas rather than 2D
  dimensions: [1920, 1080],
  // dimensions: [1920*4,1080*4],
  context: 'webgl',
  // Turn on MSAA
  attributes: { antialias: true },
}

const sketch = ({ context }) => {
  const renderer = new THREE.WebGLRenderer({ context })
  renderer.setClearColor('#000', 1)

  const camera = new THREE.PerspectiveCamera(35, 1, 0.01, 100)
  camera.position.set(2, 1.5, 0)
  camera.lookAt(new THREE.Vector3(0, 0.75, 0))

  const scene = new THREE.Scene()
  // const controls = new THREE.OrbitControls(camera)

  const geometry = new THREE.PlaneGeometry(6, 6, 30, 30)
  const material = new THREE.MeshPhongMaterial({
    color: new THREE.Color(`hsla(200, 50%, 50%)`),
    side: THREE.DoubleSide,
    flatShading: true,
  })
  const plane = new THREE.Mesh(geometry, material)

  plane.position.x = -1
  plane.rotation.x = Math.PI / 2
  plane.updateMatrixWorld()
  scene.add(plane)

  // const boxgeo = new THREE.BoxGeometry(0.05,0.05,0.05)
  // const boxmat = new THREE.MeshStandardMaterial({color: 0xffffff, emissive: 0xffffff})
  // const helperbox1 = new THREE.Mesh(boxgeo,boxmat)
  // scene.add(helperbox1)

  scene.add(new THREE.AmbientLight('#59314f', 0.2))
  const light = new THREE.DirectionalLight('#ffffff', 0.6)
  light.position.set(2, 1, -3).multiplyScalar(1.5)
  scene.add(light)

  return {
    resize({ pixelRatio, viewportWidth, viewportHeight }) {
      renderer.setPixelRatio(pixelRatio)
      renderer.setSize(viewportWidth, viewportHeight)
      camera.aspect = viewportWidth / viewportHeight
      camera.updateProjectionMatrix()
    },

    render({ time }) {
      const vertices = plane.geometry.vertices
        ? plane.geometry.vertices.length
        : 0

      console.log(plane.geometry.vertices)
      for (let i = 0; i < vertices; i++) {
        const vector = plane.geometry.vertices[i].clone()
        const { x, y } = plane.geometry.vertices[i]
        vector.applyMatrix4(plane.matrixWorld)

        // console.log(vector.x)
        const s = 2
        const u = x * s
        const v = y * s

        const n = simplex.noise2D(u, v)
        const z = n * 0.3 * Math.abs(vector.x * vector.x)
        plane.geometry.vertices[i].z = z > -0.5 ? z : z * 0.5
      }

      plane.geometry.verticesNeedUpdate = true

      // controls.update()
      renderer.render(scene, camera)
    },

    unload() {
      // controls.dispose()
      renderer.dispose()
    },
  }
}

canvasSketch(sketch, settings)
