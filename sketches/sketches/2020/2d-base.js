import canvasSketch from 'canvas-sketch'
import SimplexNoise from 'simplex-noise'
import palettes from 'nice-color-palettes'

import { random } from 'canvas-sketch-util'

import { SuperMouse } from '@dank-inc/super-mouse'

import { forN } from '../../utils/index'
import { Blade } from '../../classes/blade'
import { convertToSVGPath } from 'canvas-sketch-util/penplot'

// const superMouse = new SuperMouse()

const SN = new SimplexNoise()

const settings = {
  // dimensions: [1024, 1024],
  // dimensions: [2400, 2400],
  // dimensions: [6500, 6500],
  animate: true,
}

// random.setSeed('1')

const palette = random.pick(palettes)

const c1 = palette.splice(random.rangeFloor(0, palette.length), 1)
const c2 = palette.splice(random.rangeFloor(0, palette.length), 1)
const c3 = palette.splice(random.rangeFloor(0, palette.length), 1)
const c4 = palette.splice(random.rangeFloor(0, palette.length), 1)

let blades = []

const makeblade = (i, n, blades, t) => {
  // const v = SN.noise2D(i / n / 2, t) / 2 + 0.5
  const speed = SN.noise2D(i / n, t)
  const blade = new Blade({ u: i / n, v: 1, h: 0, speed })

  blades.push(blade)
}

const sketch = ({ context, width, height }) => {
  context.strokeStyle = '#333'
  context.lineWidth = 40

  const n = 80

  const state = {
    blade: {
      gap: 0.04,
      next: 0.04,
    },
    scene: {
      gap: 6,
      next: 6,
    },
  }

  const mouse = new SuperMouse({})

  forN(n, (i) => {
    makeblade(i, n, blades, 0)
  })

  return ({ time, context, width, height }) => {
    context.fillStyle = c1
    context.fillRect(0, 0, width, height)

    // console.log(mouse)

    for (const blade of blades) {
      if (blade.spawn) {
        //
        blade.spawn = false

        const flake = new Blade({
          u: blade.u,
          v: Math.abs(1 - blade.cutAt),
          h: blade.h - blade.cutAt,
          s: 10,
          color: c4,
          child: true,
          speed: random.gaussian(),
        })
        blades.push(flake)
      }

      context.fillStyle = blade.cut ? c2 : c3

      const s = 3

      if (!blade.child) {
        blade.grow(time)
        blade.check(mouse)
      } else {
        blade.fall()
      }

      const h = blade.cut ? blade.cutAt : blade.h
      const w = s
      const x = blade.u * width
      const y = blade.v * height

      context.save()
      context.translate(x, y)

      context.rotate(blade.r)
      context.fillRect(0, 0, w, -h * height)
      context.restore()
      // context.pop()
    }
  }
}

canvasSketch(sketch, settings)
