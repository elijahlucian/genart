import { Color, Vector3 } from 'three'

const THREE = require('three')

export class Transport {
  constructor({ startingCoords }) {
    this.destination = new Vector3(-8, -10, -6)
    this.a = 0.003
    const shipSize = 0.12
    const shipGeo = new THREE.BoxGeometry(shipSize, shipSize, shipSize)
    this.mesh = new THREE.Mesh(
      shipGeo,
      new THREE.MeshPhongMaterial({
        color: new Color('#777'),
      }),
    )

    // this.mesh.material.color.set(new THREE.Color(`hsl(${this.hue},10%,20%)`))
    this.mesh.position.set(startingCoords.x, startingCoords.y, startingCoords.z)
  }

  update() {
    const nv = new THREE.Vector3(
      this.destination.x - this.mesh.position.x,
      this.destination.y - this.mesh.position.y,
      this.destination.z - this.mesh.position.z,
    )

    nv.normalize()

    this.mesh.position.add(nv.setScalar(-this.a))
    this.a *= 1.005
  }
}
