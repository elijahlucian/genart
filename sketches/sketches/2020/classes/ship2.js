import { Transport } from './transport';

const THREE = require('three');

export class Ship {
  constructor({i}) {
    this.fuel = 0
    this.i = i
    this.flyIn = true
    this.supplied = false
    this.dispatchReady = false
    this.dispatched = false

    const shipSize = 0.1 + Math.random() * 0.05
    const shipGeo = new THREE.BoxGeometry(shipSize, shipSize, shipSize * 3)

    this.mesh = new THREE.Mesh( shipGeo, new THREE.MeshBasicMaterial( 0xff0000 ) );
    this.y = (Math.random() - 0.5) * 2
    this.hue = Math.floor(Math.random() * 360)
    
    this.mesh.material.color.set(new THREE.Color(`hsl(${this.hue},70%,80%)`))
    this.mesh.position.x = (Math.random() - 0.5) * 2
    this.mesh.position.y = this.y
    this.mesh.position.z = -105 + Math.random()
    this.transports = []
  }

  update({deltaTime}) {
    if(this.flyIn) {
      // flying in animation
      this.mesh.scale.z = 10
      this.mesh.position.z += 4.75
      
      // ship accelerating decreasing
      // checvk if in space 
      if(this.mesh.position.z > -1 && this.flyIn) {
        this.flyIn = false
        this.mesh.scale.z = 1
        this.mesh.material.color.set(new THREE.Color(`hsl(${this.hue},70%,100%)`))
      }
      return
    } 
    // this.mesh.scale.z = 0.5
    // standing still login
    // get fuel


    // release pods
    this.getFuel(deltaTime)
    this.mesh.position.y = this.y + Math.sin(this.fuel + this.y) * 0.02

    if(!this.dispatchReady && !this.dispatched) {
      this.dispatchReady = true
    }

    if(this.fuel > 1) {
      // start flying away

      const a = Math.abs(this.fuel - 1) * 3
      this.mesh.position.z += a * 2
      this.mesh.scale.z = a * 15
    }

    if(this.mesh.position.z > 7) {

      this.kill = true
    }
  }

  getFuel(n) {
    this.fuel += n
  }

  isDispatchReady() {
    if(this.dispatchReady) {
      this.dispatchReady = false
      this.dispatched = true
      return true
    }

    return false
  }

  confirmDispatch() {
    this.dispatched = true
  }

  releasePods() {
    // spawn 10 children
    const ships = []

    ships.push(new Transport())
  }

}
