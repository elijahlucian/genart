const canvasSketch = require('canvas-sketch')
global.THREE = require('three')
const SimplexNoise = require('simplex-noise')
const {
  ChromaticAberrationEffect,
  NoiseEffect,
  BloomEffect,
  EffectPass,
  RenderPass,
  EffectComposer,
  BlendFunction,
  BlendMode,
} = require('postprocessing')
const { Ship } = require('./classes/ship2')
const { Transport } = require('./classes/transport')
const THREE = global.THREE
const clock = new THREE.Clock({ autostart: true })
const simplex = new SimplexNoise()

const INTERVAL = 0.3
let nextShip = INTERVAL

// Include any additional ThreeJS examples below
require('three/examples/js/controls/OrbitControls')

const settings = {
  animate: true,
  context: 'webgl',
  attributes: { antialias: true },
}

const state = {
  ships: [],
  transports: [],
}

// TODO: Spaceport
// - have little ships go out and resupply the incoming veseles
// -- maybe they can meet the incomng ships right after they arrive
// - show shuttles leaving the craft and going down to the surface
// - wireframe planet for extreme retro
// - stars in the bg
// - milky way?
// - space dust and crap

// TODO: Battle for Rokinon
// - add opposing ships
// - launch lasers and shit from each side, blowin up ships
// - if opposing team has too many, use WMD to even out the score
// -
// -

const sketch = ({ context, width, height }) => {
  const renderer = new THREE.WebGLRenderer({ context })
  renderer.setClearColor('#000', 1)

  const camera = new THREE.PerspectiveCamera(55, 1, 0.01, 1000)
  camera.position.set(2, 0.5, 4)
  camera.lookAt(new THREE.Vector3(-4, 0, -2))

  const scene = new THREE.Scene()
  console.log(renderer)
  const controls = new THREE.OrbitControls(camera, renderer.domElement)

  const ship = new Ship({ i: 0 })
  scene.add(ship.mesh)
  state.ships.push(ship)

  var sphere = new THREE.SphereBufferGeometry(8, 10, 8)
  var planet = new THREE.Mesh(
    sphere,
    new THREE.MeshStandardMaterial({
      color: 0x4f748c,
      // wireframe: true,
      flatShading: true,
    }),
  )

  planet.position.y = -8
  planet.position.x = -10
  planet.position.z = -6

  planet.rotation.z = Math.PI * 0.125

  scene.add(planet)

  // var boxHelp = new THREE.BoxHelper( planet, 0xffff00 );
  // scene.add( boxHelp );

  let directionalLight = new THREE.DirectionalLight('#fff', 0.8)
  directionalLight.position.set(0, 10, 10)
  scene.add(directionalLight)

  const composer = new EffectComposer(renderer)
  composer.setSize(width, height)

  const bloomEffect = new BloomEffect({
    resolutionScale: 0.85,
    distinction: 5.2,
  })

  bloomEffect.blendMode.opacity.value = 0.1
  const ca = new ChromaticAberrationEffect({
    offset: new THREE.Vector2(0.0014, 0),
  })

  ca.blendMode.opacity.value = 1

  const film = new NoiseEffect()

  film.blendMode = new BlendMode(BlendFunction.SOFT_LIGHT)

  const effectPass = new EffectPass(camera, ca, bloomEffect)
  const renderPass = new RenderPass(scene, camera)

  // const shaderPass = new ShaderPass( new THREE.ShaderMaterial())

  effectPass.renderToScreen = true

  composer.addPass(renderPass)
  composer.addPass(effectPass)

  return {
    resize({ pixelRatio, viewportWidth, viewportHeight }) {
      renderer.setPixelRatio(pixelRatio)
      renderer.setSize(viewportWidth, viewportHeight)
      camera.aspect = viewportWidth / viewportHeight
      camera.updateProjectionMatrix()
    },

    render({ time, deltaTime }) {
      renderer.render(scene, camera)
      const speed = 0.025
      planet.rotateY(deltaTime * speed)

      if (nextShip < time) {
        const n = simplex.noise2D(1, time)
        nextShip += n + 0.8

        const ship = new Ship({ id: 1 })

        scene.add(ship.mesh)
        state.ships.push(ship)
      }

      for (const ship of state.ships) {
        ship.update({ deltaTime })

        if (ship.isDispatchReady()) {
          const transport = new Transport({
            startingCoords: ship.mesh.position,
          })
          scene.add(transport.mesh)
          state.transports.push(transport)
          ship.confirmDispatch()
        }

        if (ship.kill) {
          scene.remove(ship.mesh)
        }
      }

      for (const transport of state.transports) {
        transport.update({ deltaTime })
      }

      state.ships = state.ships.filter((ship) => !ship.kill)
      composer.render(clock.getDelta())

      controls.update()
    },

    unload() {
      renderer.dispose()
    },
  }
}

canvasSketch(sketch, settings)
