const canvasSketch = require('canvas-sketch')
const SimplexNoise = require('simplex-noise')
const simplex = new SimplexNoise()
import { SuperMouse } from '@dank-inc/super-mouse'

// Ensure ThreeJS is in global scope for the 'examples/'
global.THREE = require('three')
const THREE = global.THREE
const BOX_COUNT = 10
// Include any additional ThreeJS examples below
require('three/examples/js/controls/OrbitControls')

const settings = {
  // Make the loop animated
  animate: true,
  // Get a WebGL canvas rather than 2D
  context: 'webgl',
  // Turn on MSAA
  attributes: { antialias: true },
}

const sketch = ({ context }) => {
  const renderer = new THREE.WebGLRenderer({ context })
  renderer.setClearColor('#000', 1)

  const camera = new THREE.PerspectiveCamera(35, 1, 0.01, 100)
  camera.position.set(2, 4, -4)
  camera.lookAt(new THREE.Vector3())

  const scene = new THREE.Scene()

  const boxSize = 1 / BOX_COUNT
  const box = new THREE.BoxGeometry(boxSize, boxSize, boxSize)
  const meshes = []

  const step = 1 / BOX_COUNT

  for (let u = -1; u <= 1; u += step) {
    for (let v = -1; v <= 1; v += step) {
      const mesh = new THREE.Mesh(
        box,
        new THREE.MeshPhysicalMaterial({
          color: 'white',
          roughness: 0.75,
          flatShading: true,
        }),
      )

      const meta = { u, v }
      mesh.meta = meta

      mesh.position.x = u
      mesh.position.z = v

      scene.add(mesh)
      meshes.push(mesh)
    }
  }

  scene.add(new THREE.AmbientLight('#59314f', 0.5))
  const light = new THREE.PointLight('#ffffff', 2, 15.5)
  light.position.set(1, 2, -4).multiplyScalar(1.5)
  scene.add(light)

  const mouse = new SuperMouse({})

  const state = {
    tO: 0,
  }

  return {
    resize({ pixelRatio, viewportWidth, viewportHeight }) {
      renderer.setPixelRatio(pixelRatio)
      renderer.setSize(viewportWidth, viewportHeight)
      camera.aspect = viewportWidth / viewportHeight
      camera.updateProjectionMatrix()
    },

    render({ time, deltaTime }) {
      const { inertia } = mouse

      state.tO += inertia * 0.1
      const t = time * 0.5 + state.tO
      mouse.update()

      for (const mesh of meshes) {
        // mesh.rotation.y = time * (10 * Math.PI / 180);

        const ns = 2

        const { u, v } = mesh.meta
        const s = Math.sin(-time + u * v * 7) * 0.5 + 0.5
        const n = simplex.noise2D(t + u, v) * 0.5 + 0.5
        const n3 =
          simplex.noise3D(0.1 * t + u * ns * 0.2, v * ns, t * 0.1) * 0.5 + 0.5

        const h = 5
        mesh.scale.y = n3 * h
        mesh.position.y = (((n3 * 1) / BOX_COUNT) * h) / 2

        const hueOffset = mouse.scrollY * 0.1
        mesh.material.color.set(
          new THREE.Color(`hsl(${(n3 * 360 + hueOffset) % 360},100%,50%)`),
        )
      }

      renderer.render(scene, camera)
    },

    unload() {
      renderer.dispose()
    },
  }
}

canvasSketch(sketch, settings)
