
import canvasSketch from 'canvas-sketch'
import * as THREE from 'three';
import SimplexNoise from 'simplex-noise'

const simplex = new SimplexNoise()

// Ensure ThreeJS is in global scope for the 'examples/'
global.THREE = require('three');
// Include any additional ThreeJS examples below
require('three/examples/js/controls/OrbitControls');

const settings = {
  // Make the loop animated
  animate: true,
  // Get a WebGL canvas rather than 2D
  dimensions: [1200,1200],
  // dimensions: [6500,6500],
  // dimensions: [1920*4,1080*4],
  context: 'webgl',
  // Turn on MSAA
  antialias: true,
  attributes: { antialias: true }
};

const sketch = ({ context }) => {
  const renderer = new THREE.WebGLRenderer({ context });

  const backgroundColor = 'hsl(20,30%, 5%)'

  renderer.setClearColor(backgroundColor, 1);

  const camera = new THREE.PerspectiveCamera(55, 1, 0.01, 100);
  camera.position.set(4, 5, 0);
  camera.lookAt(new THREE.Vector3(0, 5,0));
  
  const scene = new THREE.Scene();
  // const controls = new THREE.OrbitControls(camera)

  scene.fog = new THREE.FogExp2(backgroundColor, 0.045);

  const resoX = 150
  const resoY = 10

  window.backgroundColor = '#333'

  // const background = new THREE.P

  const geometry = new THREE.PlaneGeometry(70, 30, resoX, resoY)
  const material = new THREE.MeshPhongMaterial({
    color: new THREE.Color('#fff'), 
    side: THREE.DoubleSide,
    flatShading: true
  })
  const plane = new THREE.Mesh(geometry, material)

  plane.position.x = -5
  plane.rotation.x = Math.PI / 2
  plane.updateMatrixWorld()
  scene.add(plane)
  
  const TOTAL_STARS = 400;
  const starsGeo = new THREE.SphereGeometry(Math.random() * 0.02 + 0.02)
  const starsMat = new THREE.MeshBasicMaterial({
    color: 'hsla(0, 0%, 100%)', 
  })
  

  for(let i = 0; i < TOTAL_STARS; i++) {
    const stars = new THREE.Mesh(starsGeo, starsMat)
    stars.position.x = Math.random() * -20 - 40
    stars.position.z = Math.random() * 50 - 25
    stars.position.y = Math.random() * 15


    scene.add(stars)
  }


  const ambience = new THREE.HemisphereLight(`hsla(190, 50%, 50%)`, `hsla(20, 80%, 50%)`, 0.5)
  // scene.add(ambience)

  const rightLight = new THREE.DirectionalLight(`hsla(190, 50%, 50%)`, 0.7);
  rightLight.position.set(2, 2, 5).multiplyScalar(1.5);
  scene.add(rightLight);
  
  const leftLight = new THREE.DirectionalLight(`hsla(20, 80%, 50%)`, 1.9);
  leftLight.position.set(-1, 2, -3).multiplyScalar(1.5);
  scene.add(leftLight);



  return {
    resize ({ pixelRatio, viewportWidth, viewportHeight }) {
      renderer.setPixelRatio(pixelRatio);
      renderer.setSize(viewportWidth, viewportHeight);
      camera.aspect = viewportWidth / viewportHeight;
      camera.updateProjectionMatrix();
    },

    render ({ time }) {
      for (let i = 0; i < plane.geometry.vertices.length; i++) {
        const vector = plane.geometry.vertices[i].clone()
        const {x,y} = plane.geometry.vertices[i]
        vector.applyMatrix4( plane.matrixWorld );

        // console.log(vector.x)
        const s = 1

        const xs = s * 0.4
        const ys = s * 0.06
        const u = x * xs
        const v = y * ys

        const yn = y * 0.01

        const n = simplex.noise3D(0.2 * time + u * 0.4, Math.sin(v) + yn, u * 0.04) 
        const z = n * 0.05 * Math.abs(vector.x * vector.x * 0.2) 
        plane.geometry.vertices[i].z = z < 0 ? z : z * 0.1
      }
      
      
      plane.geometry.verticesNeedUpdate = true

      // controls.update()
      renderer.render(scene, camera);

    },

    
    unload () { 
      // controls.dispose()
      renderer.dispose() 
    }
  };
};

canvasSketch(sketch, settings);
