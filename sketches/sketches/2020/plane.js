
const canvasSketch = require('canvas-sketch');
const SimplexNoise = require('simplex-noise');
const { Vector3 } = require('three');

const simplex = new SimplexNoise()

// Ensure ThreeJS is in global scope for the 'examples/'
global.THREE = require('three');
const THREE = global.THREE
// Include any additional ThreeJS examples below
require('three/examples/js/controls/OrbitControls');

const settings = {
  // Make the loop animated
  animate: true,
  dimensions: [2048, 2048],
  name: 'plane',
  // Get a WebGL canvas rather than 2D
  context: 'webgl',
  // Turn on MSAA
  attributes: { antialias: true }
};

const sketch = ({ context }) => {
  const renderer = new THREE.WebGLRenderer({ context });
  renderer.setClearColor('#000', 1);

  const camera = new THREE.PerspectiveCamera(55, 1, 0.01, 100);
  camera.position.set(6, 2, 0);
  camera.lookAt(new THREE.Vector3(0,-0.5,0));
  const scene = new THREE.Scene();

  const s = 7
  const scl = s * 5
  const plane = new THREE.PlaneGeometry(s*2,s,scl,scl)
  const meshes = []

  // mesh.geometry.rotateX(90)
  
  const hue = Math.floor(Math.random() * 360)
  const mesh = new THREE.Mesh(
    plane,
    new THREE.MeshPhysicalMaterial({

      color: `hsl(${hue}, 50%, 50%)`,
      // side: THREE.DoubleSide,
      // wireframe: true,
      roughness: 0.75,
      flatShading: true
    })
  );

  mesh.rotation.x = -Math.PI / 2

  scene.add(mesh);
  meshes.push(mesh)

  
  scene.add(new THREE.AmbientLight('#59314f', 0.5));
  const light = new THREE.PointLight('#ffffff', 2, 15.5);
  light.position.set(1, 2, -4).multiplyScalar(1.5);
  scene.add(light);

  return {
    resize ({ pixelRatio, viewportWidth, viewportHeight }) {
      renderer.setPixelRatio(pixelRatio);
      renderer.setSize(viewportWidth, viewportHeight);
      camera.aspect = viewportWidth / viewportHeight;
      camera.updateProjectionMatrix();
    },

    render ({ time }) {

        for(let i = 0; i < mesh.geometry.vertices.length; i++) {
          const x = mesh.geometry.vertices[i].x
          const y = mesh.geometry.vertices[i].y
          const scl = 1

          const n = simplex.noise2D(0.1 * -time + x * scl, y * scl) * 0.5 + 0.5
          // console.log(n)
          // mesh.geometry.vertices[i].z = n * 1 // + Math.sin(time + x * 2)
          // const r = Math.random() * 0.3 * Math.abs(y)
          mesh.geometry.vertices[i].z = n * Math.abs(y)//n * 0.1 //+ Math.random() * 0.01
        } 

        // const t = time * 0.5
        // const ns = 2
        // const u = Math.random()
        // const v = Math.random()
        // const s = Math.sin(-time + (u * v) * 7) * 0.5 + 0.5
        // const n = simplex.noise2D(t+u,v) * 0.5 + 0.5
        // const n3 = simplex.noise3D(0.1*t+u*ns*0.2,v*ns,t*0.1) * 0.5 + 0.5

        // const h = 5
        // // mesh.scale.y = n3 * h
        // mesh.position.y = n3 * 1/BOX_COUNT * h / 2

        

        mesh.geometry.verticesNeedUpdate = true
      renderer.render(scene, camera);
    },

    unload () { renderer.dispose() }
  };
};

canvasSketch(sketch, settings);
