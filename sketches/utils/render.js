import * as THREE from 'three';
import { EffectComposer, BloomEffect, ChromaticAberrationEffect, NoiseEffect, BlendMode, EffectPass, RenderPass, BlendFunction } from "postprocessing";

export const addFx = (renderer, scene, camera, width, height) => {
  const composer = new EffectComposer(renderer)
  composer.setSize(width,height)
  
  const bloomEffect = new BloomEffect({
    resolutionScale: 0.85,
    distinction: 5.2,
  })

  bloomEffect.blendMode.opacity.value = 0.1
  const ca = new ChromaticAberrationEffect({
    offset: new THREE.Vector2(0.002, 0),
  })
  
  ca.blendMode.opacity.value = 1
  
  const film = new NoiseEffect()

  film.blendMode = new BlendMode(BlendFunction.SOFT_LIGHT)

  const effectPass = new EffectPass(camera, ca, bloomEffect)
  const renderPass = new RenderPass(scene, camera)
  
  // const shaderPass = new ShaderPass( new THREE.ShaderMaterial())

  effectPass.renderToScreen = true 
  
  composer.addPass(renderPass)
  composer.addPass(effectPass)


  return composer
};