export const range = n => {
  return [...new Array(n).keys()]
}

export const shuffle = array => {
  var currentIndex = array.length
  var temporaryValue, randomIndex

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {
    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex)
    currentIndex -= 1

    // And swap it with the current element.
    temporaryValue = array[currentIndex]
    array[currentIndex] = array[randomIndex]
    array[randomIndex] = temporaryValue
  }
  return array
}

export const distFromRectEdge = (x, y, sx, sy) => 
  Math.min(sx - Math.abs(x),sy - Math.abs(y))


export const lerp = (margin, max, v) => {
  return (max - margin) * v + margin / 2
}

export const forN = (n, loopBlock) => { for(let i = 0; i < n; i++) loopBlock(i) }