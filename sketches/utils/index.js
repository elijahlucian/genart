export const forN = (n, fn) => {
  for (let i = 0; i < n; i++) fn(i)
}

export const forKernel = (n, fn) => {
  for (let i = -n; i <= n; i++) {
    for (let j = -n; j <= n; j++) {}
  }
}

export const scl = (n, s = 1, o = 0) => n * s + o

/*
  invert u
*/
export const iu = (u) => Math.abs(u - 1)

export const r = (s = 1, o = 0) => {
  return (Math.random() - 0.5) * 2 * s + o
}
