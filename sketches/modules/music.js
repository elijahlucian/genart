class BPM {
  constructor(params) {
    this.timeslices = []
    this.bpm = 120
    this.lastTime = -10
    this.dt = 0
  }
  tap({time}) {
   
    if(time - this.lastTime > 4) {
      console.log('resetting...')
      this.dt = 0
      this.timeslices = []
    } else {
      this.dt = time - this.lastTime 
      this.timeslices.push(this.dt)
    }

    if(this.timeslices.length > 4) {
      let avg = this.timeslices.reduce((t,v) => t+v) / this.timeslices.length
      console.log(this.timeslices)
      this.bpm = (avg) * 60
      console.log("BPM...", this.bpm, 'AVG...', avg)
    }

    this.lastTime = time
    console.log("delta:", this.dt)
  }
}

class Tempo {
  constructor({bpm}) {
    this.musicalTime = 0
    this.bpm = 120
  }
  
  zero() {
    // set time to 0
  }

  reset() {
    // reset bpm?
  }

  update({time}) {
    this.musicalTime = time * (this.bpm / 120)
  }

  beat() {
    return this.musicalTime % 1
  }

  quarter() {
    return this.musicalTime * 2 % 1
  }

  eighth() {
    return this.musicalTime * 4 % 1
  }

  sixteenth() {
    return this.musicalTime * 8 % 1
  }
}

const calculateBPMFromDelta = (n) => {
  return 60 / n
}

const musicalTime = ({zeroOffset, time, bpm}) => {
  return zeroOffset + time * (bpm / 120)
}

const tapTempo = (time, delta) => {
  // get keypress time
  let offset = time - delta
  let bpmDelta = offset 
}



module.exports = { Tempo, BPM, calculateBPMFromDelta, musicalTime }