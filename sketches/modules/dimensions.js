export const dimensions =  {
  desktop: {
    hd: [1280, 720],
    fhd: [1920, 1080],
    fourk: [3840, 2160],
    eightk: [7680, 4320],
  },
  square: {
    insta: [2048, 2048],
    large: [4096, 4096],
    huge: [8192, 8192],
    max: [11180, 11180],
  },
  print: {
    eightbyten: [1500, 3000],
    fivebyfour: {
      small: [8 * 300, 10 * 300],
      medium: [16 * 300, 20 * 300],
      large: [24 * 300, 30 * 300],
    },
    threebyfour: [],
    threebytwo: [],
    a5: [],
    poster: [],
    displate: [2900, 4060], // margin 181px min
    moo: {
      standard: [1038, 696],
      square: [813, 813],
      mini: [874, 378],
    },
  },
}

export const doubleSize = ([x, y]) => {
  return [x * 2, y * 2]
}
export const quadSize = ([x, y]) => {
  return [x * 4, y * 4]
}

export const flipOrientation = ([x, y]) => {
  return [y, x]
}
