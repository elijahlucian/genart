const PI = Math.PI
const TAU = PI * 2

const lerp = (args, context={}) => {
	let {max, min, t} = args
  return (max - min) * t + min
}

const clamp = ({n, min, max}) => {

}

const vectors = [
  {u: 0,v: 1},
  {u: 1,v: 1},
  {u: 1,v: 0},
  {u: 1,v: -1},
  {u: 0,v: -1},
  {u: -1,v: -1},
  {u: -1,v: 0},
  {u: -1,v: 1},
]

export { PI, TAU, lerp, clamp, vectors }