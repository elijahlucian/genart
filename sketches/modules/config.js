const config = {
  speed: 0.5,
  trails: true,
  margin: longEdge / 20,
  lineWidth: longEdge / 100,
}

const three = {
  speed: 0.24,
  shape: {
    size: 1,
    edges: 30,
  },
  grid: {
    size: 1,
    steps: 2,
    scale: 9,
    margin: 2,
  },
  seed: {
    palette: '418861',
    // algorithm: '',
    random: random.getRandomSeed()
  },
  shader: { enabled: false },
  dimensions: dimensions.square.insta
}

three.camera = {
  orthographic: {
    fov: config.grid.scale + config.grid.margin * 2,
    depth: 100,
  },
  perspective: {
  }
}

export { config, three }