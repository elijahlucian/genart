// drawing.js
const random = require('canvas-sketch-util/random')


const createGrid = (args) => {
	const points = []
	const size = args.size
	
	for (let y = 0; y < size; y++) {
		for (let x = 0; x < size; x++) {
	
			const u = x / (size - 1)
			const v = y / (size - 1)

			const radius = Math.max(0, random.noise2D(u,v) + 1) * 0.01
			const noise = random.noise2D(u,v)

			points.push({
				color: random.pick(palette),
				position: {u, v},
				radius,
				rotation: noise,
				noise: noise,

			})
		}
	}
	return points
}

const createCenteredGrid = (args) => {

}

const create3dGrid = (args) => {

}

export { createGrid, createCenteredGrid, create3dGrid }