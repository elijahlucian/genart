export const dimensions = ({ width, height } = params) => {
  return {
    w: width / 2,
    h: height / 2,
  }
}

export const range = n => {
  return [...new Array(n).keys()]
}

export const sequence = n => {
  return range(n)
}

export const shuffle = array => {
  var currentIndex = array.length
  var temporaryValue, randomIndex

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {
    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex)
    currentIndex -= 1

    // And swap it with the current element.
    temporaryValue = array[currentIndex]
    array[currentIndex] = array[randomIndex]
    array[randomIndex] = temporaryValue
  }
  return array
}

export const createGrid = ({ size, color, rotation }) => {
  const points = []

  for (let y = 0; y < size; y++) {
    for (let x = 0; x < size; x++) {
      const u = x / (size - 1)
      const v = y / (size - 1)
      const radius = Math.max(0, (u, v) + 1) * 0.01

      points.push({
        color,
        position: { u, v },
        radius,
        rotation,
      })
    }
  }
  return points
}

export const getLongEdge = (x, y) => {
  return x > y ? x : y
}
export const getShortEdge = (x, y) => {
  return x < y ? x : y
}

export const getLongEdgeFromArray = xy => {
  let [x, y] = xy
  return getLongEdge(x, y)
}

export const getShortEdgeFromArray = xy => {
  let [x, y] = xy
  return getLongEdge(x, y)
}

export const getLongEdgeFromObject = (uv, u, v) => {
  let x = uv[u]
  let y = uv[v]
  return getLongEdge(x, y)
}

export const rotateDimensions = xy => {
  let [x, y] = xy
  return [y, x]
}

export const get3dPosition = (angle, radius, coordinate) => {
  if (coordinate == 'x') {
    return Math.sin(angle * Math.PI * 2) * radius
  } else if (coordinate == 'z') {
    return Math.cos(angle * Math.PI * 2) * radius
  }
}

export const create3dUV = params => {
  let { step } = params
  let uv = []
  let row = 1
  let col = 1
  console.log('Step size', step)
  for (let u = -1; u <= 1; u += step) {
    col = 0
    for (let v = -1; v <= 1; v += step) {
      uv.push({ u, v, row, col })
      col++
    }
    row++
  }
  return uv
}

export const create3dUVW = params => {
  let { size, step } = params
  let uv = []

  for (let u = -1; u <= 1; u += step) {
    for (let v = -1; v <= 1; v += step) {
      for (let w = -1; w <= 1; w += step) {
        uv.push({ u, v, w })
      }
    }
  }
  return uv
}

export const create2dUV = params => {
  let { step } = params
  let uv = []
  let row = 0
  let col = 0
  for (let u = 0; u <= 1; u += step) {
    for (let v = 0; v <= 1; v += step) {
      uv.push({ u, v, row, col })
      col++
    }
    row++
  }
  return uv
}

// const createXY = () => { }

export const hexToNumber = () => {}

export default {
  dimensions,
  range,
  sequence,
  shuffle,
  createGrid,
  getLongEdge,
  getShortEdge,
  getLongEdgeFromArray,
  getShortEdgeFromArray,
  getLongEdgeFromObject,
  rotateDimensions,
  get3dPosition,
  create3dUV,
  create3dUVW,
  create2dUV,
  hexToNumber,
}
