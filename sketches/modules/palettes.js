export const palettes = {
  synthwave:        [ '#f4225a', '#f4b800', '2de2e6', '#261447', '#0d0221'],
  cool:             ['#4ad3b8', "#dd4381", '#643b7a', '#b04bc4', '#4a73ad'],
  shutterAndFlash:  ['#048ba8', '#13505b', '#040404', '#d7d9ce', '#f0a202'],
  tambo:            ['#75cfbd', '#ffaf87', '#ff8e72', '#ed6a5e', '#377771'],
  tambo2:           ['#000000', '#6ec1b1', '#75cfbd', '#7ddbc8', '#ffffff'],
  rain:             ['#75cfbd', '#595959', '#7f7f7f', '#cccccc', '#f2f2f2'],
  chill:            ['#75cfbd', '#b0a1ba', '#abc8c7', '#b8e2c8', '#bff0d4'],
  vintage:          ['#255d83', '#735983', '#cc6686', '#ff687e', '#ffaf87'],
  kitchen:          ['#eff0eb', '#c5c1bb', '#484e4a', '#eacb48', '#00a099'],
  wind:             ['#e0ef4d', '#3f3fbc', '#223982', '#f2edf0', '#3a3a3a'],
  trusted:          ['#c6c4c4', '#f7f6f6', '#51c5dd',  '#f04c63', '#0a4f70'],
  swanky:           ['#001f23', '#007a75', '#5b9489', '#fcbe8e', '#ff0021'],
  autumn:           ['#fbd0cf', '#e6a89a', '#d1a6c7', '#274338', '#1b1d1c'],
  sophisticated:    ['#b7d8d6', '#789e9e', '#4d6466', '#eef3db', '#fe615a'],
  leveling:         ['#f99227', '#ebcc72', '#2b85be', '#e1dfdb', '#2a2728'],
  kidsroom:         ['#1c4755', '#00a08f', '#efc458', '#ff9f52', '#f77747'],
  dusk:             ['#f0d06b', '#d95a40', '#bf2742', '#501e31', '#577c80'],
  riverbed:         ['#2b4d59', '#39998e', '#ffdc7c', '#ffaa67', '#da674a'],
  neopolitan:       ['#faeee6', '#ffe5d8', '#ffcad4', '#f3acb6', '#9e8a89'],
  pbj:              ['#a6284b', '#d03a63', '#ffe2c5', '#ffba75', '#c29261'],
  lilac:            ['#502d55', '#8e4b71', '#e7c8b7', '#e6e0da', '#95ae8d'],
  lavendar:         ['#ffcbb1', '#ffb4a0', '#e5989b', '#b4838f', '#6a6876'],
  synthwave2:       ['#f1e678', '#ed8f60', '#bf406c', '#682a6e', '#2b1f46'],
  treefrog:         ['#1e486c', '#26a250', '#b2db35', '#f2ac34', '#fe7634'],
  beachhouse:       ['#fff3d9', '#ffe6b3', '#47aad9', '#2e6e8d', '#153240'],
  thunderstorm:     ['#fffbee', '#8872ad', '#32558c', '#163460', '#021c33'],
  koi:              ['#f9ce10', '#fd710f', '#f43000', '#040f3c', '#fcf5eb'],
  eighties:         ['#f0eaf4', '#251738', '#24ddd8', '#ffa4cd', '#f9d673'],
  romulan:          ['#d79878', '#4E1D2A', '#989AAD', '#ba8e7e', '#3D365F']

  // empty: ['#', '#', '#', '#', '#'],
}

export const getIndex = (i) => {
  const [name, palette] = Object.entries(i)
  return { name, palette }
}
