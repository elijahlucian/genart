const gradient = (context, start, end, width, height, left_x=0, top_y=0) => { 
  fill = context.createLinearGradient(left_x, top_y, width, height)
  fill.addColorStop(0, start)
  fill.addColorStop(1, end)
  return fill
}

export { gradient }