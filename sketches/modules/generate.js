const random = require('canvas-sketch-util/random')
const utils = require('./utils')
const { lerp } = require('./maf')
const DIYarc = (context,x,y,r,s,e,fill,color) => {
  // fix this
  context.beginPath()
  context.lineWidth = 10
  context.fillStyle = color
  context.arc(x, y, r, s, e, false)
  fill ? context.fill() : context.stroke()
}

const grid = (args) => {
  const points = []
  const size = args.size
  
  for (let y = 0; y < size; y++) {
    for (let x = 0; x < size; x++) {
  
      const u = x / (size - 1)
      const v = y / (size - 1)
      const radius = Math.max(0, random.noise2D(u,v) + 1) * 0.01
      const index = random.value() * 10
      
      points.push({
        color: random.pick(palette),
        position: {u, v},
        radius,
        index,
        rotation: random.noise2D(u,v)
      })
    }
  }
  return points
}

const diagonal_line = (context, x, y, step_x, step_y) => {
  if(random.value() > 0.5){
    context.moveTo(x, y)
    context.lineTo(x + step_x, y + step_y)
    context.stroke()
  } else {
    context.moveTo(x, y + step_y)
    context.lineTo(x + step_x, y)
  }
}

const buildings = (context, margin, width, height, pairs = 1) => {
  new_points = []
  for (let i = 0; i < pairs * 2; i++) {
    new_points.push(random.pick(points))
  }
  
  for (let i = 0; i < pairs; i++) {
    context.beginPath()
    context.strokeStyle = backFill
    context.fillStyle = 'rgba(255,255,255,0.5)'
    context.lineWidth = margin / 10

    vertex_pair = new_points.slice(i*2, i*2+2)
    
    let start_x = lerp(margin, width - margin, vertex_pair[0].position.u)
    let end_x = lerp(margin, width - margin, vertex_pair[1].position.u) 
    
    context.moveTo(start_x,height)
    vertex_pair.forEach(point => {
      let x = lerp(margin, width - margin, point.position.u)
      let y = lerp(margin, height - margin, point.position.v)
      context.lineTo(x,y)
    });    
    context.lineTo(end_x,height)
    
    context.fill()
    context.stroke()
  }
}

const bufferNoise = (context, buffer32, image_data) => {
  // pix = image_data.data
  // for (let i = 0; i < pix.length; i++) {
  //   pix[i] = Math.sin(i) * 255
  // }
  var len = buffer32.length - 1;
  while(len--) buffer32[len] = Math.random() < 0.5 ? 0 : -1>>0;
  
}

const darkNoise = ({document, mainContext, width, height, color}) => {
  let resolution = 2
  // console.log("noise!")
  // context.globalCompositeOperation = "multiply"

  mainContext.offscreenCanvas = document.createElement('canvas')
  mainContext.offscreenCanvas.width = width
  mainContext.offscreenCanvas.height = height
  mainContext.offscreenContext = mainContext.offscreenCanvas.getContext('2d')

  for (let x = 0; x < width; x += resolution) {
    for (let y = 0; y < height; y += resolution) {
      darkness = random.rangeFloor(0,2) / 40
      mainContext.offscreenContext.beginPath()
    
      mainContext.offscreenContext.fillStyle = `rgba(0,0,0,${darkness})`
      mainContext.offscreenContext.fillRect(x,y,resolution,resolution)
      mainContext.offscreenContext.arc(x,y,resolution/2,0,6,false)
      mainContext.offscreenContext.fill()
    }
  }

  return mainContext.offscreenCanvas
}

module.exports = { 
  DIYarc, 
  darkNoise, 
  grid, 
  buildings, 
  diagonal_line, 
  bufferNoise 
}
