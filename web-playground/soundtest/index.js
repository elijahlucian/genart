const c = document.querySelector('#stage')
c.width = 500
c.height = 500

var sound1 = 'sfx'

const loadSound = () => {
	createjs.Sound.registerSound('./sounds/sfx2.wav', sound1)
}

const playSound = () => {
	createjs.Sound.play(sound1)
}
loadSound()

const draw = () => {
	var stage = new createjs.Stage('stage')
	var circle = new createjs.Shape()
	circle.graphics.beginFill('DeepSkyBlue').drawCircle(0, 0, 50)
	circle.x = 100
	circle.y = 100
	stage.addChild(circle)
	stage.update()
	createjs.Tween.get(circle, {loop: true})
		.to({x: 400}, 1000, createjs.Ease.getPowInOut(4))
	createjs.Ticker.setFPS(144)
	createjs.Ticker.addEventListener('tick', stage)
}
const init = () => {
	draw()
}