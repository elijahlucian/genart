const W = 1200
const H = 1200
const palette = PALETTES.reds

let qt
let points = []

function setup() {
  createCanvas(W, H)
  // background(10)
  frameRate(60)
  // strokeWeight(3)
  // blendMode(HARD_LIGHT)
  rectMode(CENTER)
  // noFill()
  noStroke()
  // noLoop()

  for (let i = 0; i < AGENT_COUNT; i++) {
    let u = noise(i * 10) * 10
    let x = u * width
    let y = noise(u * 10) * height
    let point = new Point(x, y, u)
    points.push(point)
  }

  console.log(qt)
}

function draw() {
  background(20, 100)
  let t = frameCount * 0.01

  let boundary = new Rectangle(width / 2, height / 2, width / 2, height / 2)
  qt = new QuadTree(boundary, 4, palette)

  // noStroke()
  // fill(255, 20)
  for (let point of points) {
    point.update(t)

    // circle(point.x, point.y, 10)
    qt.insert(point)
  }

  let u = noise(t)
  let x = u * width * 1.5
  let y = noise(u, t) * height * 1.5

  if (frameCount % 4 == 0) {
    let point = new Point(x, y)
    // qt.insert(point)
  }

  qt.show()

  // fill(255)
  // circle(x, y, 10)

  noFill()
  stroke(255)
}

function mouseMoved() {
  let point = new Point(mouseX, mouseY)
  points.push(point)
  // console.log('adding point', point)
  // qt.insert(point)
  // console.log(qt)
  draw()
}

function mousePressed() {
  let u = noise(points.length * 10) * 10
  let point = new Point(mouseX, mouseY, u)
  points.push(point)
  // console.log('adding point', point)
  // qt.insert(point)
  // console.log(qt)
  draw()
}

function keyPressed({ key }) {
  if (key == 'Enter') location.reload()
  if (key == 's') save('quadthingy')
  if (key == 'a') noLoop()
}
