class Agent {
  constructor(params) {
    Object.assign(this, params)
    this.total = 0
  }
  update({ t, last, next }) {
    let rad_size = this.radius

    if (this.i > 0) {
      if (last) rad_size += last.radius
      this.u = sin(this.a) * rad_size + last.u
      this.v = cos(this.a) * rad_size + last.v
    } else {
      // this.a += t * 2
      // this.u += (noise(this.u) -0.5) * 0.01
      // this.v += (noise(this.v) -0.5) * 0.01
    }

    if (this.total > Math.PI * 2) this.dead = true

    this.a += (noise(this.i * 0.1, t) - 0.5) * 0.01 + 0.005

    this.total += 0.005
  }
  show({ t }) {
    if (this.dead) {
    }
    this.n = noise(this.u * 10, this.v * 10, t)

    let noisyRad = this.n - 0.1

    circle(
      this.u * width,
      this.v * height,
      this.radius * 2 * width + noisyRad * width * 0.01
    )

    if (this.i > 0) {
    }
    if (this.color) {
      fill(this.color + '11')
    } else {
      fill(this.n * 80 + 160, 50, 100, 0.1)
    }
  }
}
