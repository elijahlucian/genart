class Point {
  constructor(x, y, u) {
    Object.assign(this, { x, y, u })
  }

  update(t) {
    this.x = noise(this.u, t * 0.1) * width
    this.y = noise(-this.u * 2, t * 0.2) * height
  }
}

class Rectangle {
  constructor(x, y, w, h) {
    Object.assign(this, { x, y, w, h })
  }

  contains(p) {
    let { x, y, w, h } = this
    let ruling = p.x > x - w && p.x < x + w && p.y > y - h && p.y < y + h
    return ruling
  }
}

class QuadTree {
  constructor(boundary, capacity, palette) {
    Object.assign(this, { boundary, capacity, palette })
    this.c = random(this.palette)
    this.points = []
    this.divided = false
    this.edges = {
      tl: random() > 0.5 ? boundary.w : 0,
      tr: random() > 0.5 ? boundary.w : 0,
      bl: random() > 0.5 ? boundary.w : 0,
      br: random() > 0.5 ? boundary.w : 0,
    }
  }

  insert(point) {
    if (!this.boundary.contains(point)) return

    if (this.points.length < this.capacity) {
      this.points.push(point)
    } else {
      if (!this.divided) {
        this.subdivide()
      }
      this.northwest.insert(point)
      this.northeast.insert(point)
      this.southwest.insert(point)
      this.southeast.insert(point)
    }
  }

  subdivide() {
    let { x, y, w, h } = this.boundary
    let bh = w / 2
    let bw = h / 2

    let nw_boundary = new Rectangle(x - bw, y - bh, bw, bh)
    this.northwest = new QuadTree(nw_boundary, this.capacity, this.palette)

    let ne_boundary = new Rectangle(x + bw, y - bh, bw, bh)
    this.northeast = new QuadTree(ne_boundary, this.capacity, this.palette)

    let sw_boundary = new Rectangle(x - bw, y + bh, bw, bh)
    this.southwest = new QuadTree(sw_boundary, this.capacity, this.palette)

    let se_boundary = new Rectangle(x + bw, y + bh, bw, bh)
    this.southeast = new QuadTree(se_boundary, this.capacity, this.palette)

    this.divided = true
  }

  show() {
    fill(this.c)
    // noStroke()
    // circle(x, y, w * 2)
    stroke(255, 100)
    noFill()

    let { tl, tr, bl, br } = this.edges
    rect(
      this.boundary.x,
      this.boundary.y,
      this.boundary.w * 2,
      this.boundary.h * 2
    )
    if (this.divided) {
      this.northeast.show()
      this.northwest.show()
      this.southeast.show()
      this.southwest.show()
    }
    // stroke(255, 40)
    // strokeWeight(10)
    // for (let p of this.points) {
    //   point(p.x, p.y)
    // }
  }
}
