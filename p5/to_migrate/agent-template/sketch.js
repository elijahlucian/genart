const agents = []
let deviation, total_temps
temperatures = []

function setup() {
  createCanvas(1200, 1200)
  colorMode(HSB)
  stroke(200)
  background(10)
  frameRate(60)
  strokeWeight(3)
  // noStroke()
  textSize(48)

  let x = 0 / 23
  let y = sin(x * Math.PI * 2)
  console.log(y)

  for (var i = 0; i <= 23; i++) {
    let u = i / 23
    let v = sin(u * Math.PI * 2)

    temperatures.push(v)
    agents.push({ u, v })
  }

  total_temps = temperatures.reduce((val, temp) => {
    return val + abs(temp)
  })

  other_total = 0
  for (const t of temperatures) {
    other_total += t
  }

  let mean = total_temps / temperatures.length
  let other_mean = other_total / temperatures.length
  console.log(Math.round(other_mean * 1000000) / 1000000)
}

function draw() {
  stroke(255)
  for (const p of agents) {
    const { u, v } = p
    const x = u * width
    const y = (v * height) / 2 + height / 2
    point(x, y)
    line(x, height / 2, x, y)
  }
  let t = frameCount * 0.01
  noStroke()
  fill(255)

  let mean = Math.floor((total_temps / temperatures.length) * 100) / 100
  text(mean, 0, 48)
  text(Math.floor(total_temps * 100) / 100, 0, 100)
  text(temperatures.length, 0, 150)
}
