// noise.js
const agent_count_squared = 1
const confetti_count = 200
const line_width_div = 2
const noise_level = 10
let saved = false
let agents = []
let trunk

let confetti = []

function setupSketch() {
  let rando = (frameCount / Math.PI) * millis()
  // randomSeed(rando)
  console.log(rando)
  agents = []
  background(20)

  trunk = new Agent({
    u: 0.2,
    v: 1,
    a: 0,
    i: 0,
    palette: ['#E63C94', '#B72475'],
    limit: height / 6.5,
    depth: 3,
    generation: 0,
  })

  for (let i = 0; i < confetti_count; i++) {
    confetti.push(
      new Confetti({
        u: random(-0.1, 1.1),
        v: random(-1, 0),
        i,
        size: random(0, width / 100),
      })
    )
  }
}

function setup() {
  createCanvas(2400, 2400)
  background(20)
  // noStroke()
  // noLoop()
  noFill()

  setupSketch()
  // agents = shuffle(agents)
  console.log(trunk)
}

function draw() {
  background('#001F2E')
  var time = frameCount

  // stroke('#FFE4B0')
  // noStroke()
  strokeWeight(width / line_width_div)
  strokeCap(ROUND)

  confetti.forEach(c => c.updraw({ width, height, time }))

  // agents.forEach(agent => {
  trunk.updraw({ width, height, time })
  // })
}
