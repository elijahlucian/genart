class Agent {
  constructor(params) {
    Object.assign(this, params)
    this.life = 0
    // transform angle into uu,vv
    this.children = []
    this.color = random(this.palette)
    this.uu = sin(this.a)
    this.vv = cos(this.a)
    this.x2 = this.x - this.uu * this.life
    this.y2 = this.y - this.vv * this.life
    this.imperfection = this.generation / this.depth / 2
    this.mature = false
    this.matureChildren = 0
  }

  offScreen() {
    return this.u < 0 || this.u > 1 || this.v < 0 || this.v > 1
  }

  sprout() {
    if (this.generation <= this.depth) {
      for (let i = 0; i < 3; i++) {
        if (random() > 0.9 - this.imperfection) continue
        let branch = new Agent({
          limit: this.limit * (0.87 - random(this.imperfection / 4)),
          life: 0, //this.life * 0.75 - (random(this.imperfection) * this.life) / 4,
          u: 0,
          v: 0,
          i: this.i + i + 1,
          a: this.a + (random(-1, 0.2) * Math.PI) / (8 - this.imperfection * 5),
          depth: this.depth,
          generation: this.generation + 1,
          parent: this,
          palette: this.palette,
        })
        this.children.push(branch)
      }
    }
  }

  drawSelf() {
    stroke(this.color)
    strokeWeight(this.life / 30)
    beginShape()
    // draw jagged line over this one
    vertex(this.x, this.y)
    vertex(this.x2, this.y2)

    endShape()
  }

  updraw({ time, width, height }) {
    this.drawSelf()
    push()
    translate(this.x2, this.y2)
    this.children.forEach(branch => {
      branch.updraw({ time, width, height })
    })
    pop()

    if (this.sprouted) return

    // this.life += random(0.01, 0.2)

    let t = time * 0.05
    let n = noise(this.generation, this.i * 0.1, time * 0.01)
    // this.a += sin(t + n + this.i) * 0.01 * (this.generation / this.depth) // (n - 0.5) * 0.02

    this.x = this.u * width
    this.y = this.v * height
    this.uu = sin(this.a)
    this.vv = cos(this.a)
    this.x2 = this.x - this.uu * this.life
    this.y2 = this.y - this.vv * this.life
    // draw self

    if (this.life > this.limit) {
      this.sprout()
      this.sprouted = true
      if (this.parent) console.log(this.i, 'child mature')
    }

    this.life += 12
  }
}

class Confetti {
  constructor(params) {
    Object.assign(this, params)
  }

  updraw({ time, width, height }) {
    this.v += 0.001 + this.i / confetti_count / 1000 // random(-0.001, 0.01)
    this.x = this.u * width + (noise(this.i, this.v * Math.PI * 2) * width) / 10
    this.y = this.v * height
    if (this.v > 1) {
      this.v = -this.size / height
    }
    fill(random(['#9B8A18', '#BDBE32']))
    noStroke()
    circle(this.x, this.y, this.size)
    noFill()
  }
}
