const palettes = {
  synthwave: ['f4225a', 'f4b800', '2de2e6', '261447', '0d0221'],
  cool: ['4ad3b8', "dd4381", '643b7a', 'b04bc4', '4a73ad'],
  shutterAndFlash: ['048ba8', '13505b', '040404', 'd7d9ce', 'f0a202'],
  tambo: ['75cfbd', 'ffaf87', 'ff8e72', 'ed6a5e', '377771'],
  tambo2: ['000000', '6ec1b1', '75cfbd', '7ddbc8', 'ffffff'],
  rain: ['75cfbd', '595959', '7f7f7f', 'cccccc', 'f2f2f2'],
  chill: ['75cfbd', 'b0a1ba', 'abc8c7', 'b8e2c8', 'bff0d4'],
}

IDEAS = [
  'dot plot',
  'statistical table view',
]

const palette = palettes[0]

let cols = 5
let rows = 5
let grid = new Array(cols)
let size = 500

function preload() {}

function setup() {
  createCanvas(size, size)

  for (let i = 0; i < cols; i++) {
    grid[i] = new Array(rows)
  }

  console.log(grid)

}

function draw() {
  background(0)
  noSmooth()
}