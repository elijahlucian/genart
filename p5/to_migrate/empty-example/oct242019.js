reDrawInterval = Math.PI * 4
var reDrawAt = reDrawInterval
const size = 500
const margin = size / 50

const agent = new Agent({ u: .5, v: .5, size: 20, parent: true, margin })

function setup() {
  createCanvas(size, size)
  background('#111')
  noStroke()
  colorMode(HSB)
  // noLoop()
}

function draw() {
  const t = frameCount * 0.02
  if (t > reDrawAt) {
    reDrawAt += reDrawInterval
    agent.flipColor()
  }
  agent.update({ t })
  agent.show()
}

