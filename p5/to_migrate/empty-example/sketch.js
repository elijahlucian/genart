const palettes = {
  synthwave: ['f4225a', 'f4b800', '2de2e6', '261447', '0d0221'],
  cool: ['4ad3b8', 'dd4381', '643b7a', 'b04bc4', '4a73ad'],
  shutterAndFlash: ['048ba8', '13505b', '040404', 'd7d9ce', 'f0a202'],
  tambo: ['75cfbd', 'ffaf87', 'ff8e72', 'ed6a5e', '377771'],
  tambo2: ['000000', '6ec1b1', '75cfbd', '7ddbc8', 'ffffff'],
  rain: ['75cfbd', '595959', '7f7f7f', 'cccccc', 'f2f2f2'],
  chill: ['75cfbd', 'b0a1ba', 'abc8c7', 'b8e2c8', 'bff0d4'],
}

const palette = palettes[pickAtRandom(Object.keys(palettes))]

let mapping = []
let size = 50

function setup() {
  createCanvas(size, size)
  pixelDensity(1)
  frameRate(4)

  for (let x = 0; x < width; x++) {
    for (let y = 0; y < height; y++) {
      let i = (x + y) / (size * size)
      mapping.push(
        new Pixel({
          x,
          y,
          a: random(),
          b: random(),
          i,
          index: x * size + y,
        })
      )
    }
  }
}

function draw() {
  background(0)
  noSmooth()
  loadPixels()

  mapping.forEach((coord, index) => {
    let pix = (coord.x + coord.y * width) * 4
    for (let p = 0; p < 3; p++) {
      pixels[pix + p] = coord.c * 255
    }
    pixels[pix + 3] = 255
    // coord.bleed(pickAtRandom(mapping))
    coord.a *= surroundingCells(coord) + 0.2
    coord.update()
  })
  console.log(mapping[0].c)
  updatePixels()
  // noLoop()
}

function pickAtRandom(collection) {
  let normalizedIndex = Math.random()
  let actualIndex = Math.floor((collection.length - 1) * normalizedIndex)
  return collection[actualIndex]
}

function surroundingCells({ index }) {
  cells = []
  let { x, y } = getXYFromIndex(index)
  for (let u = -1; u <= 1; u++) {
    for (let v = -1; v <= 1; v++) {
      let uu = -Math.abs(u)
      let vv = -Math.abs(v)
      let weight = (uu + vv) / 2
      let ix = x + u
      let iy = y + v

      let i = xyToIndex(ix, iy)
      coord = mapping[i]
      if (coord == undefined) continue
      cells.push((coord.a *= weight)) + 0.2
    }
  }
  let avg =
    cells.reduce((v, i) => {
      return v + i
    }) / cells.length
  return avg
}

function xyToIndex(x, y) {
  return x * size + y
}

function getXYFromIndex(index) {
  let x = Math.floor(index / size)
  let y = index % size
  return { x, y }
}

function colorFromHex() {
  return {
    r: parseInt(c.slice(0, 2), 16) / 255,
    g: parseInt(c.slice(2, 4), 16) / 255,
    b: parseInt(c.slice(4, 6), 16) / 255,
    a: 1,
  }
}

class Pixel {
  constructor({ x, y, index, c, a, b, i }) {
    // pixel position
    this.x = x
    this.y = y
    // normalized amount of shit in thingy
    this.a = a
    this.b = b
    this.i = i
    this.index = index
    this.diffusion_rate = 0.1
    this.feed_rate = 0.5
    this.kill_rate = 1
    this.laplace = () => {}
    this.last = { a, b }
    this.grid = { a, b }
    this.next = { a, b }

    this.c = ((Math.sin(i * x * Math.PI * 2 * 100) + 1) / 2) * 255
  }

  update() {
    this.c = (this.c + this.a) % 1
  }

  getc(p) {
    return this.c[p] * 255
  }
}
