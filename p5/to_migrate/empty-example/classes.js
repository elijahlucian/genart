class Dot {
  constructor(params = {}) {
    this.u = params.u || 0.5
    this.v = params.v || 1
    this.stackCount = params.stackCount || 0
    this.totalV = this.v - this.stackCount / 10
    this.realV = this.v
    this.life = 1
    this.color = `#${params.color}` || '#fff'
    this.throb = 0
    this.i = params.i
  }

  update(t) {
    // this.v = (Math.sin(t + this.u * Math.PI) / 10) + 0.8
    this.throb = (Math.sin(t + this.i) + 1.5) * 10
    if (this.realV > this.totalV - 0.01) {
      this.realV -= 0.01
    }
  }

  draw({ w, h }) {
    let c = color(this.color)
    fill(c)
    circle(
      w * this.u,
      h * this.realV,
      w / 20 - (w / 20) * this.realV + this.throb
    )
  }
}

class Line {}

class Agent {
  constructor(params) {
    Object.assign(this, { ...params })
    this.color = true
  }

  update({ t }) {
    const { x, y } = this
    if (this.lastAgent) {
      this.lastAgent.setUV({ x, y })
    }
    let dw = sin(t / 2) * (width - margin * 2)
    let dh = sin(t / 2) * (height - margin * 2)
    if (this.parent) {
      this.x = sin(t) * this.u * dw + (width / 2 + margin)
      this.y = cos(t) * this.v * dh + (height / 2 + margin)
    }
    this.scale = sin(t) * 10
    if (this.color) {
      fill((10 * t * PI * 2) % 360, 100, 255)
      this.sizeOffset = 0
    } else {
      fill('#111')
      this.sizeOffset = 2
    }
  }

  show() {
    circle(this.x, this.y, this.size + this.scale + this.sizeOffset)
  }

  flipColor() {
    this.color = !this.color
  }
}
