reDrawInterval = Math.PI * 4
var reDrawAt = reDrawInterval
const size = 500
const margin = size / 50

class Thing {
  constructor(params = {}) {
    let { u, v } = params
    Object.assign(this, { u, v })
  }
  update = ({ t }) => {}
  show = () => {
    let { u, v } = this
    circle(u * width, v * height, 10)
  }
}

let agent = new Thing({ u: 0.5, v: 0.5 })

function setup() {
  createCanvas(size, size)
  background('#111')
  noStroke()
  colorMode(HSB)
  // noLoop()
}

function draw() {
  const t = frameCount * 0.02
  agent.update({ t })
  agent.show()
}
