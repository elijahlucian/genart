const palettes = {
  synthwave: ['f4225a', 'f4b800', '2de2e6', '261447', '0d0221'],
  cool: ['4ad3b8', 'dd4381', '643b7a', 'b04bc4', '4a73ad'],
  shutterAndFlash: ['048ba8', '13505b', '040404', 'd7d9ce', 'f0a202'],
  tambo: ['75cfbd', 'ffaf87', 'ff8e72', 'ed6a5e', '377771'],
  tambo2: ['000000', '6ec1b1', '75cfbd', '7ddbc8', 'ffffff'],
  rain: ['75cfbd', '595959', '7f7f7f', 'cccccc', 'f2f2f2'],
  chill: ['75cfbd', 'b0a1ba', 'abc8c7', 'b8e2c8', 'bff0d4'],
}

IDEAS = ['dot plot', 'statistical table view']

const palette = palettes['cool']
// const palette = pickobj(palettes)

console.log(palette)
let count = 0
let cols = 50
let rows = 10
let size = 1024

let dots = new Array(cols)

function preload() {}

const plot = () => {
  dots = []
  let list = []
  for (var i = 0; i < cols; i++) {
    let stackCount = 0
    let r = Math.floor(Math.random() * rows)
    list.push(r)
    list.forEach(li => {
      if (li === r) stackCount += 1
    })
    let dot = new Dot({
      u: r / rows,
      stackCount: stackCount,
      color: palette[r % 5],
      i: i,
    })
    dots[i] = dot
  }
  // console.log(dots)
}

function mousePressed() {
  plot()
}

function keyPressed(e) {
  switch (e.keyCode) {
    case 37:
      cols -= 5
      break
    case 38:
      cols += 1
      break
    case 39:
      cols += 5
      break
    case 40:
      cols -= 1
      break
    case 32:
      plot()
  }
  console.log('cols =>', cols)
}

function setup() {
  createCanvas(size, size)
  frameRate(30)
  noStroke()
  plot()
}

const update = t => {
  for (let dot of dots) {
    dot.update(t)
  }
}

function draw() {
  let t = (count += 0.1)
  update(t)
  background(50)

  for (let dot of dots) {
    dot.draw({
      w: size,
      h: size,
    })
  }
}
