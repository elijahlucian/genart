// noise.js

function setup() {}

function draw() {
  background(20);
  var t = frameCount * 0.1;
  stroke(255);
  noFill();
  beginShape();
  for (var u = 0; u <= 1; u += 0.01) {
    vertex(u * width, noise(t + u * 10) * height);
  }
  endShape();
}
