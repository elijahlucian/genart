const newPoint = (agent, width, height0) => {
  const x = agent.u * width
  const y = agent.v * height
  const radius = agent.radius
  const color = agent.color

  return { x,y,radius,color }
}

const AGENT_COUNT = 1000
const MAX_SIZE = 0.02
const MAX_TRIES = 3

let agents = []
const points = []

// const ITERATIONS = Math.PI * 2 // * number of frames

const ITERATIONS = 1300


function setup() {

  const width = 6500 
  const height = 6500
  createCanvas(width, height)
  colorMode(HSB)
  blendMode(HARD_LIGHT)
  stroke(200, 0.1)
  background(10)
  frameRate(60)
  strokeWeight(3)
  noStroke()
  noLoop()



  for (let i = 0; i < AGENT_COUNT; i++) {
    let radius = (MAX_SIZE / 2) * random(0.1, 1)

    let u = random()
    let v = random()

    let a = random() * Math.PI * 2

    if (i > 0) {
      a = (0.5 - noise(i)) * 0.1 + agents[i - 1].a
    }

    agents.push(
      new Agent({
        i,
        a,
        u,
        v,
        radius,
        color: random(PALETTES.cute),
      })
    )
  }
  console.log(`total agents => ${agents.length}`)

  let f = 0


  for (let n = 0; n < ITERATIONS; n++) {
    let t = f * 0.01 // normalize dis
    // how many frames does it take for a total circle.
    // depends on agent behavior

    for(const agent of agents) {
    
      last = agents[agent.i - 1]
      next = agents[agent.i + 1]
      agent.update({ t, last, next, width })
      // agent.show({ t })
      points.push(newPoint(agent, width, height))
    }
    f+=1
  }

  console.log(`${points.length} points pointed`)
}

function draw() {
  for (let {x,y,radius,color} of points) {
    fill(color + '11')
    circle(x,y,radius * width)
  }

  console.log(`art has been arted!`)
}
