function setup() {
  createCanvas(1200, 1200)
  colorMode(HSB)
  blendMode(HARD_LIGHT)
  stroke(200, 0.1)
  background(10)
  frameRate(60)
  strokeWeight(3)
  noStroke()
  for (let i = 0; i < AGENT_COUNT; i++) {
    let radius = (MAX_SIZE / 2) * random(0.1, 1)

    let u = random()
    let v = random()

    let a = random() * Math.PI * 2

    if (i > 0) {
      a = (0.5 - noise(i)) * 0.1 + agents[i - 1].a
    }

    agents.push(
      new Agent({
        i,
        a,
        u,
        v,
        radius,
        color: random(PALETTES.cute),
      })
    )
  }
  console.log(`total agents => ${agents.length}`)
}

function draw() {
  // background(00,0.5);
  let t = frameCount * 0.01
  for (let agent of agents) {
    last = agents[agent.i - 1]
    next = agents[agent.i + 1]
    agent.update({ t, last, next })
    agent.show({ t })
  }
}
