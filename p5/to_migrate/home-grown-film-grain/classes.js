class Agent {
  constructor(params) {
    Object.assign(this, params)
  }

  updraw({ time, width, height }) {
    let t = time * 0.1
    this.n = this.noise(this.u, this.v, t)
    this.a = (this.n + this.a) / 2
    this.rad = this.a * Math.PI * 4

    this.u += Math.sin(this.rad) * this.speed
    this.v += Math.cos(this.rad) * this.speed

    let x =
      (sin(Math.sqrt(this.u * Math.PI) * 2000) * width) / 10 + this.u * width
    let y =
      t +
      Math.sqrt(cos(this.v * Math.PI * 3000) * height) / 10 +
      this.v * height

    this.size = this.n * this.baseSize
    fill(this.n * 128 + 128, 30)
    circle(x, y, this.size)
  }
}
