const qs = document.querySelector.bind(document)
const qsa = document.querySelectorAll.bind(document)

const nav = qs('.navigation') //document.createElement('DIV')

const loadSketch = ({ target: { value } }) => {
  const sketch = qs('#sketch')
  sketch.remove()

  const newSketch = document.createElement('script')
  newSketch.src = value + '.js'
  newSketch.type = 'text/javascript'
  newSketch.id = 'sketch'

  const canvas = qs('.p5Canvas')
  if (canvas) canvas.remove()

  console.log(value, sketch, newSketch)
  document.body.appendChild(newSketch)
}

navItems = ['basic']
for (const item of navItems) {
  const navItem = document.createElement('BUTTON')
  navItem.innerHTML = item
  navItem.onclick = loadSketch
  navItem.value = item
  nav.appendChild(navItem)
}

// <script id="sketch" src="sketch.js"></script>
