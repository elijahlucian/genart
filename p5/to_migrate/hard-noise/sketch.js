// noise.js

let saved = false
let agents = []
function setupSketch() {
  let rando = (frameCount / Math.PI) * millis()
  randomSeed(rando)
  console.log(rando)
  agents = []
  background(20)
  for (let u = 0; u <= 1; u += 0.02) {
    for (let v = 0; v <= 1; v += 0.02) {
      agents.push(
        new Agent({
          u,
          v,
          a: noise(u, v),
          noise,
          speed: 0.001,
          baseSize: width * 0.001,
        })
      )
    }
  }
}

function setup() {
  createCanvas(2400, 2400)
  background(20)
  // blendMode(HARD_LIGHT)
  noStroke()
  setupSketch()
}

function draw() {
  // background(20, 4)
  var time = frameCount * 0.005
  agents.forEach(agent => {
    agent.updraw({ width, height, time })
  })

  // if (millis() > 30_000) {
  //   if (!saved) {
  //     save('noise.png')
  //     console.log('saving image')
  //     saved = true
  //   }
  //   if (millis() > 40_000) window.location.reload()
  // }
}
