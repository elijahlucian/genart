class Agent {
  constructor(params) {
    Object.assign(this, params)
    this.life = 0
  }

  offScreen() {
    return this.u < 0 || this.u > 1 || this.v < 0 || this.v > 1
  }

  cull() {
    this.culled = true
  }

  updraw({ time, width, height }) {
    let t = time * 1
    if (this.life < 1) this.life += 0.01

    if (this.offScreen()) {
      if (this.culled) return
      this.life = 0
      this.u = random()
      this.v = random()
    }

    let scale = 10

    this.n = this.noise(
      Math.sqrt(30 * this.u + (Math.cos(t) + this.v) * scale),
      Math.floor(this.v * scale + (t % 1)),
      t
    )
    this.a = (this.n + this.a) / 2
    this.rad = this.a * Math.PI * 4

    this.u += Math.sin(this.rad) * this.speed
    this.v += Math.cos(this.rad) * this.speed //+ Math.sin(this.u) * 10

    let x = this.u * width
    let y = this.v * height

    this.size = this.life * this.n * this.baseSize
    fill(this.n * 60 - 20, 100, 50) //, this.life * (37 + this.n * 4))
    circle(x, y, this.size)
  }
}
