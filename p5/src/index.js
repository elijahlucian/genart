import p5 from 'p5'
import React from 'react'
import { render } from 'react-dom'
import { Interface } from './components/Interface'

import { sketch as followers } from './sketches/followers'
import { sketch as tree } from './sketches/tree'
import { sketch as ml5 } from './sketches/ml5'
import { sketch as glsl } from './sketches/glsl'
import { sketch as slowassrain } from './sketches/slowassrain'
import { sketch as mouserain } from './sketches/mouserain'
import { sketch as bouncybox } from './sketches/bouncybox'

const qs = document.querySelector.bind(document)

const app = qs('#app')

const canvasi = []

const sketches = {
  followers,
  tree,
  ml5,
  mouserain,
  slowassrain,
  bouncybox
}

const loadSketch = (sketch, params) => {
  let last
  while ((last = canvasi.pop())) {
    last.remove()
  }
  console.log('loading sketch =>', sketch)
  
  let c = new p5(p => sketches[sketch](p, params), 'canvas')
  canvasi.push(c)
}

render(<Interface sketches={Object.keys(sketches)} loadSketch={loadSketch} />, app)
