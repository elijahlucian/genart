interface Agent {
  x: number
  y: number
}

const abs = Math.abs

export const checkCollisions = (
  { x: aX, y: aY }: Agent,
  { x: gX, y: gY }: Agent,
  r: number
) => {
  return abs(aX - gX) < r && abs(aY - gY) < r
}
