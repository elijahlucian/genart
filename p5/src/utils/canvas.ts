export const isOnCanvas = (u: number, v: number) => {
  return u > 0 || u < 1 || v > 0 || v < 1
}
