export const lerp = (u: number, size: number, margin: number) => {
  return u * (size - margin * 2) + margin
}
