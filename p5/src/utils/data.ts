const r = Math.random
import { Dataset } from '../types/types'
import * as tf from '@tensorflow/tfjs'

export const dataGen = (ln): Dataset[] => {
  return [...new Array(ln)].map((v, i) => {
    return {
      x: {
        u: i / ln,
        v: i / ln,
      },
      y: {
        turn: r(),
      },
    }
  })
}

export const tensorify = (ds: Dataset[]) => {
  // takes normalized datasets
  // returns tensors?

  tf.util.shuffle(ds)

  let x1 = ds.map(d => d.x.u)
  let x2 = ds.map(d => d.x.u)

  let y = ds.map(d => d.y.turn)

  let inputs = tf.tensor2d([x1, x2], [x1.length + x2.length, 1])
  let labels = tf.tensor2d(y, [y.length, 1])

  return { inputs, labels }
}

export const normalify = (feature: Dataset[], min: number, max: number) => {}
