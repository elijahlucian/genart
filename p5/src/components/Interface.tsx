import React, { useState, useEffect, useRef } from 'react'
import { SketchParams } from '../types/types'

export interface Param {
  type: 'string' | 'number'
  name: string
  default: string | number
}

type Props = {
  sketches: string[]
  loadSketch: (id: string, params: any) => void 
  extraInputs: React.ReactNode
}

export const Interface = ({ sketches, loadSketch }: Props) => {
  const [agentCount, setAgentCount] = useState<number>(parseInt(localStorage.getItem('agentCount')) || 42)
  const [seed, setSeed] = useState<string | undefined>(localStorage.getItem('seed'))
  const [width, setWidth] = useState<number>(parseInt(localStorage.getItem('width')) || 600)
  const [height, setHeight] = useState<number>(parseInt(localStorage.getItem('height')) || 600)
  const [sketch, setSketch] = useState<string>(localStorage.getItem('sketch') || sketches[sketches.length - 1])
  const [params, setParams] = useState<SketchParams>({
    seed,
    agentCount,
    width,
    height,
  })

  useEffect(() => {
    const params: SketchParams = { seed, agentCount, width, height }
    setParams(params)

    window.localStorage.setItem('agentCount', agentCount.toString())
    window.localStorage.setItem('seed', seed)
    window.localStorage.setItem('params', JSON.stringify(params))
  }, [seed, agentCount, width, height])

  const updateSketch = (e: React.MouseEvent<HTMLButtonElement>) => {
    window.localStorage.setItem('sketch', e.currentTarget.value)
    setSketch(e.currentTarget.value)

    // if form valid, launch
    // @ts-ignore
    launch()
  }

  const launch = e => {
    if(e) e.preventDefault()
    if (!sketch) return
    loadSketch(sketch, params)
  }

  // TODO: Add dynamically generated params
  // TODO: after selecting sketch, change focus to form
  // TODO: Palette Picker!

  console.log("sketches", sketches)

  return (
    <>
      <div className="interface-row">
        <h3>Parcel-React-P5js-ML5</h3>
        <p>TODO</p>

        {sketches.map((name) => (
          <button key={`button-${name}`} value={name} onClick={updateSketch}>
            {name}
          </button>
        ))}
      </div>


      <form className="interface-row" onSubmit={launch}>
        <h3>Sketch: {sketch}</h3>
        <label>
          Agents
          <input
            value={agentCount}
            onChange={({ currentTarget }) =>
              setAgentCount(parseInt(currentTarget.value) || 0)
            }
          />
        </label>
        <label>
          Seed
          <input
            value={seed}
            onChange={({ currentTarget }) =>
              setSeed(currentTarget.value)
            }
          />
        </label>
        <label>
          Width
          <input
            value={width}
            onChange={e => setWidth(parseInt(e.currentTarget.value) || 0)}
          />
        </label>
        <label>
          Height
          <input
            value={height}
            onChange={e => setHeight(parseInt(e.currentTarget.value) || 0)}
          />
        </label>
        <button type="submit">GO</button>
      </form>
    </>
  )
}
