import p5 from 'p5'
import { MouseDropping } from '../classes/mousedropping'
import { isOnCanvas } from '../utils/canvas'
import { SketchParams } from '../types/types'

let droppings: MouseDropping[] = []

export const sketch = (p: p5, params: SketchParams) => {
  const interval = 0.05
  let next = 0

  p.setup = () => {
    droppings = []
    p.createCanvas(params.width, params.height)
    p.noStroke()
    p.blendMode('hard-light')
    p.colorMode('hsl', 1)
    p.background(0)
  }

  p.draw = () => {
    let t = p.frameCount / 60

    const mu = p.mouseX / p.width
    const mv = p.mouseY / p.height

    if (isOnCanvas(mu, mv)) {
      droppings.push(new MouseDropping({ u: mu, v: mv }))
      //   next = t + Math.random() * 0.001 + 0.125
    }

    for (const dropping of droppings) {
      const v = Math.abs(dropping.velocity)
      const a = 2 - v * 500

      if (a < 0.3) dropping.dead = true

      p.fill(dropping.r * 0.1 + 0.3 + dropping.n * 0.5, 0.5 + v, a - 0.7, a)
      dropping.update(p, t)
      dropping.draw(p)
    }

    droppings = [...droppings.filter(drop => !drop.dead)]
  }

  p.mouseClicked = () => {
    p.save('mouserain')
  }
}
