import { Agent } from '../classes/tree'

export const sketch = (p, { width, height, agentCount, seed }) => {
  const line_width_div = 2
  let trunk

  p.setup = () => {
    if (seed) p.randomSeed(seed)
    p.createCanvas(width, height)
    p.background(20)
    p.noFill()
    trunk = new Agent({
      p,
      u: 0.2,
      v: 1,
      a: 0,
      i: 0,
      palette: ['#E63C94', '#B72475'],
      limit: height / 6.5,
      depth: agentCount,
      generation: 0,
    })
  }

  p.draw = () => {
    p.background('#001F2E')
    var time = p.frameCount

    p.strokeWeight(width / line_width_div)
    p.strokeCap(p.ROUND)

    trunk.updraw({ p, width, height, time })
  }
}
