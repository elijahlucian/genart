import blah from '../shaders/test'

export const sketch = (p, { width = 500, height = 500 }) => {
  p.preload = () => {
    // p.fun = p.loadShader('shaders/test.vert', 'shaders/test.frag')
  }

  p.setup = () => {
    p.createCanvas(500, 500, p.WEBGL)
    p.noStroke()
    const { vert, frag } = blah

    p.fun = p.createShader(vert, frag)
  }

  p.draw = () => {
    let t = p.frameCount / 30

    p.shader(p.fun)

    p.fun.setUniform('resolution', [width, height])
    p.fun.setUniform('mouse', [p.mouseX / width, p.mouseY / height])
    p.fun.setUniform('time', p.frameCount * 0.01)

    p.rect(0, 0, 20, 20)
  }
}
