import p5 from 'p5'
import { SketchParams } from '../types/types'
import { Drop } from '../classes/SlowRain'


export const sketch = (p: p5, params: SketchParams) => {
  const drops = []
  const limit = params.agentCount

  p.setup = () => {
    p.colorMode('hsb')
    p.blendMode('hard-light')
    p.createCanvas(800, 800)

    for (let i = 0; i < limit; i++) {
      const drop = new Drop({
        i,
        z: i / limit,
        p,
      })
      drops.push(drop)
    }
    p.noFill()
    p.stroke(150)
    p.strokeWeight(2)
  }

  let time = 0

  p.draw = () => {
    time += 0.001
    p.background(0, 0.2)
    for (const drop of drops) {
      drop.updraw(p, time)
    }
  }
}
