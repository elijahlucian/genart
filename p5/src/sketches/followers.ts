import { Agent } from '../classes/follower'
import { checkCollisions } from '../utils/collisions'
import { lerp } from '../utils/draw'
import p5 from 'p5'
import { SketchParams } from '../types/types'




type Letter = { u: number; v: number }
type Goal = Letter & { x: number; y: number }

export const sketch = (
  p: p5,
  { seed, width, height, agentCount }: SketchParams
) => {
  const marginX = width / 5
  const marginY = height / 5

  let goal: Goal | undefined
  let agent: Agent | undefined
  let points = 2

  const columns = 4
  const maxPoints = 3

  let letters: Letter[] = []
  const followers = []
  const followerCount = agentCount

  const getNextGoal = (
    goals: Letter[],
    w: number,
    h: number,
    mx: number,
    my: number
  ): Goal | undefined => {
    if (!goals.length) return

    const { u, v } = goals.shift()

    return {
      u,
      v,
      x: lerp(u, w, mx),
      y: lerp(v, h, my),
    }
  }

  p.setup = () => {
    if (seed) p.randomSeed(seed)
    p.createCanvas(width, height)
    p.rectMode(p.CENTER)
    p.colorMode(p.HSB)
    p.noStroke()
    p.background(10)

    letters = []

    for (let i = 0; i <= columns; i++) {
      for (let j = 0; j <= p.random(2, maxPoints); j++) {
        const u = i / columns + p.random(-0.05, 0.05)
        const v = p.random()

        letters.push({ u, v })
      }
    }



    const nextGoal = getNextGoal(letters, p.width, p.height, marginX, marginY)

    agent = new Agent({
      x: nextGoal.x - 10,
      y: nextGoal.y - 10,
      i: 0,
      size: width / 50,
      lightness: 1,
    })

    for (let i = 0; i < followerCount; i++) {
      const follower = new Agent({
        x: agent.x - p.random(5, 185),
        y: agent.y - p.random(15, 25),
        i,
        size: width / 100,
        lightness: 500,
        speed: 0.5,
      })
      followers.push(follower)
    }
  }

  p.draw = () => {

    for(const {u,v} of letters) {
      const x = lerp(u, p.width, marginX)
      const y = lerp(v, p.height, marginY)
      // p.circle(x,y, 3)

    }

    if (!goal) {
      goal = getNextGoal(letters, p.width, p.height, marginX, marginY)
    }

    if (!goal) return

    let time = ((p.frameCount / 60) * 0.1 * p.width) / 2400

    let speed = 0.5

    agent.update({ speed })
    agent.steer(goal.x, goal.y)

    for (const follower of followers) {
      follower.steer(agent.x, agent.y)
      follower.update({ time })
      follower.draw({ p })
    }

    if (checkCollisions(agent, goal, 2)) {
      goal = undefined
      points += 1
    }
  }
}
