import p5 from "p5";
import { Boundary } from "../classes/boundaries";
import { SketchParams } from "../types/types";
import { forN } from '../utils/stuff'

export const sketch = (
  p: p5,
  { seed, width, height, agentCount }: SketchParams
) => {
  
  const agent = {
    u: 0.5,
    v: 0.5,
    vu: Math.random() /100,
    vv: Math.random() /100,
  }

  const boundary = new Boundary()

  const update = (agent) => {
    agent.u += agent.vu
    agent.v += agent.vv

    if (agent.u > 1 || agent.u < 0) agent.vu *= -1
    if (agent.v > 1 || agent.v < 0) agent.vv *= -1
  }

  p.setup = () => {
    p.createCanvas(width, height)
    p.noStroke()
    p.noLoop()

  }


  p.draw = () => {

    const x = agent.u * width 
    const y = agent.v * height

    p.circle(x,y,20)

    forN(4, (i) => {
      
      
    })

    update(agent)

    // draw 4 dots
    // draw boundaries
    // spawn point at middle

  }

}