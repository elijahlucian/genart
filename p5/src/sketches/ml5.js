import { SmartAgent } from '../classes/smartAgent'

export const sketch = p => {
  const agent = new SmartAgent({ name: 'HAL' })

  p.setup = () => {
    p.createCanvas(500, 500)
    p.noLoop()
  }

  p.draw = () => {
    p.background(10)
    p.rect(250, 250, 125, 125)
    p.text(agent.name, 100, 100)
  }
}
