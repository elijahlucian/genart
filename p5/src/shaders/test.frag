#ifdef GL_FRAGMENT_PRECISION_HIGH
precision highp float;
#else
precision mediump float;
#endif

uniform float time;
uniform vec2 resolution;

void main(void){
  float mx=max(resolution.x,resolution.y);
  vec2 uv=gl_FragCoord.xy/mx;
  vec3 color=vec3(uv,.25+.5*sin(3.14*uv.y+time));
  
  float su=0.;
  
  vec3 hole=vec3(sin(1.5-distance(time+99.*uv.x+sin(5.*cos(20.*3.14*uv.x+time*2.+uv.y*3.14)+99.*uv.y),8.)));
  
  color=mix(color,hole,-.5);
  
  gl_FragColor=vec4(color,1.);
}