export type Dataset = {
  x: { u: number; v: number }
  y: { turn: number }
}

export type SketchParams = {
  seed: string | undefined
  width: number
  height: number
  agentCount: number
}

export type Vec2 = {
  x: number
  y: number
}