import p5 from 'p5'
import { Howl } from 'howler'
const soundbank = [
  require('../audio/rain/normalized/1a.wav'),
  require('../audio/rain/normalized/1b.wav'),
  require('../audio/rain/normalized/1c.wav'),
  require('../audio/rain/normalized/2a.wav'),
  require('../audio/rain/normalized/2b.wav'),
  require('../audio/rain/normalized/2c.wav'),
  require('../audio/rain/normalized/3a.wav'),
  require('../audio/rain/normalized/3b.wav'),
  require('../audio/rain/normalized/3c.wav'),
  require('../audio/rain/normalized/4a.wav'),
  require('../audio/rain/normalized/4b.wav'),
  require('../audio/rain/normalized/4c.wav'),
  require('../audio/rain/normalized/5a.wav'),
  require('../audio/rain/normalized/5b.wav'),
  require('../audio/rain/normalized/5c.wav'),
  require('../audio/rain/normalized/6a.wav'),
  require('../audio/rain/normalized/6b.wav'),
  require('../audio/rain/normalized/6c.wav'),
  require('../audio/rain/normalized/7a.wav'),
  require('../audio/rain/normalized/7b.wav'),
  require('../audio/rain/normalized/7c.wav'),
  require('../audio/rain/normalized/8a.wav'),
  require('../audio/rain/normalized/8b.wav'),
  require('../audio/rain/normalized/8c.wav'),
  require('../audio/rain/normalized/9a.wav'),
  require('../audio/rain/normalized/9b.wav'),
  require('../audio/rain/normalized/9c.wav'),
  require('../audio/rain/normalized/10a.wav'),
  require('../audio/rain/normalized/10b.wav'),
  require('../audio/rain/normalized/10c.wav'),
]

interface Params {
  i: number
  z: number
  p: p5
}

export interface Drop extends Params {
  u: number
  v: number
  hit: boolean
  w: number
  h: number
  life: number
  floor: number
  sound: Howl
}
export class Drop {
  constructor({ i, z, p }: Params) {
    this.i = i
    this.z = z

    this.u = p.random(0.15, 0.75)
    this.v = p.random(-2, 0)
    this.hit = false
    this.w = 3
    this.h = 3
    this.life = 10
    this.floor = p.random(0.5, 0.9)
    this.sound = new Howl({
      src: p.random(soundbank), 
    })
    this.p = p
  }

  triggerHit() {
    this.hit = true
    const pan = this.p.constrain((this.u * 2 - 1) * -1 * 1.3,-1 ,1)
    const volume = (this.floor - 0.45) * 0.3

    this.sound.volume(volume)
    this.sound.stereo(pan)
    this.sound.play()

    this.sound = new Howl({
      src: this.p.random(soundbank), 
    })
  }

  updraw(p: p5, time: number) {
    if (this.hit) {
      this.h += 0.2 * 10
      this.w += 0.4 * 10
      this.life *= 0.9
    } else {
      this.v += (this.floor - 0.25) * 0.01
    }

    if (this.v > this.floor && !this.hit) {
      this.triggerHit()
    }

    if (this.life < 0.1) {
      this.v = 0
      this.u = p.random(0.1, 0.9)
      this.w = 3
      this.h = 3
      this.hit = false
      this.life = 10
      this.floor = p.random(0.5, 0.9)
    }

    const depth = (this.floor - 0.5) * 2.5 // 0 - 1

    const nOfY = p.noise(this.v * 1.51, this.floor * 10, time)

    this.u = this.hit ? this.u : (nOfY - 0.5) / 500 + this.u

    const x = this.u * p.width
    const y = this.v * p.height
    const w = this.hit
      ? this.w
      : p.sin(this.v * 10 + this.z * 10) * this.w * 0.5 + this.w
    const h = this.hit ? this.h * (this.floor - 0.25) * 2 : this.h

    p.stroke(
      0.5 * this.floor * this.v * 360,
      100,
      100,
      (this.life / 10) * depth
    )

    p.ellipse(x, y, w * depth, h * depth)
  }
}
