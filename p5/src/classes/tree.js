export class Agent {
  constructor(params) {
    Object.assign(this, params)
    this.life = 0
    // transform angle into uu,vv
    this.children = []
    this.color = this.p.random(this.palette)
    this.uu = this.p.sin(this.a)
    this.vv = this.p.cos(this.a)
    this.x2 = this.x - this.uu * this.life
    this.y2 = this.y - this.vv * this.life
    this.imperfection = this.generation / this.depth / 2
    this.mature = false
    this.matureChildren = 0
  }

  offScreen() {
    return this.u < 0 || this.u > 1 || this.v < 0 || this.v > 1
  }

  sprout() {
    if (this.generation <= this.depth) {
      for (let i = 0; i < 3; i++) {
        if (this.p.random() > 0.9 - this.imperfection) continue
        let branch = new Agent({
          p: this.p,
          limit: this.limit * (0.87 - this.p.random(this.imperfection / 4)),
          life: 0, //this.life * 0.75 - (random(this.imperfection) * this.life) / 4,
          u: 0,
          v: 0,
          i: this.i + i + 1,
          a:
            this.a +
            (this.p.random(-1, 0.2) * Math.PI) / (8 - this.imperfection * 5),
          depth: this.depth,
          generation: this.generation + 1,
          parent: this,
          palette: this.palette,
        })
        this.children.push(branch)
      }
    }
  }

  drawSelf() {
    this.p.stroke(this.color)
    this.p.strokeWeight(this.life / 30)
    this.p.beginShape()
    this.p.vertex(this.x, this.y)
    this.p.vertex(this.x2, this.y2)

    this.p.endShape()
  }

  updraw({ time, width, height }) {
    this.drawSelf()
    this.p.push()
    this.p.translate(this.x2, this.y2)
    this.children.forEach(branch => {
      branch.updraw({ time, width, height })
    })
    this.p.pop()

    if (this.sprouted) return

    // this.life += random(0.01, 0.2)

    let t = time * 0.05
    let n = this.p.noise(this.generation, this.i * 0.1, time * 0.01)
    // this.a += sin(t + n + this.i) * 0.01 * (this.generation / this.depth) // (n - 0.5) * 0.02

    this.x = this.u * width
    this.y = this.v * height
    this.uu = this.p.sin(this.a)
    this.vv = this.p.cos(this.a)
    this.x2 = this.x - this.uu * this.life
    this.y2 = this.y - this.vv * this.life
    // draw self

    if (this.life > this.limit) {
      this.sprout()
      this.sprouted = true
      // if (this.parent) console.log(this.i, 'child mature')
    }

    this.life += 12
  }
}
