import p5 from 'p5'

type Params = {
  u: number
  v: number
}

export interface MouseDropping extends Params {
  x: number
  y: number
  velocity: number
  angle: number
  dead: boolean
  size: number
  r: number
  n: number
}

export class MouseDropping {
  constructor(params: Params) {
    Object.assign(this, params)
    this.velocity = 0.004 * Math.random()
    this.angle = Math.random() * Math.PI * 2
    this.size = Math.random()
    this.r = Math.random()
  }

  update(p: p5, time: number) {
    if (this.v > 1) this.dead = true

    this.n = (p.noise(this.v * 100, this.u * 100, time) - 0.5) / 200

    this.velocity *= 1.025
    this.v = this.v + this.velocity
    this.u += this.n

    this.x = this.u * p.width
    this.y = this.v * p.height
  }

  draw(p: p5) {
    p.circle(this.x, this.y, this.size * 2 + 2)
  }
}
