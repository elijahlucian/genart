export class Agent {
  u: number
  v: number
  vu: number
  vv: number
  x: number
  y: number
  rot: number
  trained: boolean
  maxSpeed: number

  constructor({ u, v }: { u: number; v: number }) {
    Object.assign(this, { u, v })
    this.maxSpeed = 0.005
    this.vu = this.maxSpeed
    this.vv = this.maxSpeed
    this.rot = 0
    this.trained = false
  }

  upDraw = ({ p, width, height, time }) => {
    // update coords, physics
    // this.decide()
    if (this.u > 1) this.u = 0.1
    if (this.u < 0) this.u = 0.9
    if (this.v > 1) this.v = 0.1
    if (this.v < 0) this.v = 0.9

    let n = p.noise(this.u * 10, this.v * 10, time * 100) - 0.5
    let a = n * p.PI * 2

    this.vu = p.sin(a) / 2000 + this.vu
    this.vv = p.cos(a) / 2000 + this.vv

    if (this.vu > this.maxSpeed) this.vu = this.maxSpeed
    if (this.vu < -this.maxSpeed) this.vu = -this.maxSpeed
    if (this.vv > this.maxSpeed) this.vv = this.maxSpeed
    if (this.vv < -this.maxSpeed) this.vv = -this.maxSpeed

    this.decide()
    this.u += this.vu
    this.v += this.vv

    this.x = this.u * width
    this.y = this.v * height

    // translate & draw
    p.push()
    p.translate(this.x, this.y)
    p.rotate((this.rot += 0.1))
    p.rect(0, 0, 50, 50)
    p.pop()
  }

  init = async () => {
    // init brain and shit
  }

  decide = async () => {}

  learn = async ({ inputs, labels }) => {
    // expect normalized inputs and outputs

    this.trained = true
  }
}

const js = {
  this: 42,
}
js.this

const py = {
  this: 42,
}
py['this']
