export interface Params {
  i: number
  x: number
  y: number
  speed?: number
  lightness: number
  size: number
}
export interface Agent extends Params {
  vX: number
  vY: number
  maxSpeed: number
  maxForce: number
}

export class Agent {
  constructor({ x, y, i, size, lightness, speed = 1 }: Params) {
    Object.assign(this, { x, y, i, size, lightness })
    this.vX = 0
    this.vY = 0
    this.maxSpeed = -5 * 1
    this.maxForce = 0.08 // Math.random() * 0.2 + 0.21
  }

  update({ speed }) {
    this.x += this.vX // * speed
    this.y += this.vY // * speed
  }

  draw({ p }) {
    p.fill(
      240 + p.abs(this.vX + this.vY) * 10,
      p.abs(this.vX + this.vY) * 20 + 20,
      this.lightness || 50
    )
    p.circle(this.x, this.y, this.size)
  }

  steer(gX, gY) {
    // add ability to noise this up later

    const dX = this.x - gX
    const dY = this.y - gY
    const hyp = Math.sqrt(dX ** 2 + dY ** 2)

    // normalized vector
    const nX = (dX / hyp) * this.maxSpeed
    const nY = (dY / hyp) * this.maxSpeed

    // const steering = normalize(desired) * this.maxSpeed
    const fX = nX - this.vX
    const fY = nY - this.vY
    // normalize hypotenuse of force direction and limit if needed
    const fHyp = Math.sqrt(fX ** 2 + fY ** 2)

    // apply
    this.vX += (fX / fHyp) * this.maxForce
    this.vY += (fY / fHyp) * this.maxForce
  }

  arrive(gX, gY) {
    // find distance to goal
  }
}
