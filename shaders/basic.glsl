#ifdef GL_ES
precision mediump float;
#endif

uniform vec2 u_resolution;
uniform vec2 u_mouse;
uniform float u_time;

void main(){
  float t=u_time*2.1;
  vec2 uv=gl_FragCoord.xy/u_resolution.y*2.;
  float u=uv.x;
  float v=uv.y;
  
  float l=length(uv-vec2(1.,1.));
  float d=distance(sin(t+uv),vec2(sin(l*10.+sin(u)+t),cos(l*5.)));
  
  float circles=sin(dot(sin(t)+10.,l*10.));
  
  float shape=circles-d;
  
  vec3 color=vec3(u,v,u*v+sin(t*3.)*.5+.5);
  
  vec3 col=vec3(shape+color*.6);
  
  gl_FragColor=vec4(col,1.);
}