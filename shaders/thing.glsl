#ifdef GL_ES
precision mediump float;
#endif

#define PROCESSING_COLOR_SHADER

uniform vec2 u_resolution;
uniform vec3 u_mouse;
uniform float u_time;

void main() {
    float rate = 2.;
    float time = u_time * rate;
    vec2 uv = gl_FragCoord.xy/u_resolution;

    float waves = (mod(7. * cos(1. * sin(20. * cos(0.1 * uv.y + time * 0.05)) + 1.15) + 7. + tan(0.1 * uv.y + uv.x + (sin(time) * .2 + 1.9) * uv.x + u_time * 0.0001) * 3., 4.5) - 3.5);

    vec3 color = vec3(
      waves * 0.7, 
      sin(waves + time + uv.y) * 0.1,
      sin(waves + time + uv.x) * 0.2
    );

    gl_FragColor = vec4(color,1.0);
}