#ifdef GL_ES
precision mediump float;
#endif

#define PROCESSING_COLOR_SHADER

uniform vec2 u_resolution;
uniform vec3 u_mouse;
uniform float u_time;

void main() {
    float rate = 2.;
    float time = u_time * rate;
    vec2 uv = gl_FragCoord.xy/u_resolution.x;
    
    float s = sin(time);
    float c = cos(time);

    float y = mod(uv.x, 0.5);

    vec3 color = vec3(
      mod(ceil(time + 20. * uv.y - 1.), 1.2)
    );

    gl_FragColor = vec4(color,1.0);
}