const canvasSketch = require('canvas-sketch');
const random = require('canvas-sketch-util/random')
const utils = require('../utils')
const dopePalettes = require('../helpers/dopePalettes')

let seed = "562582"

random.setSeed(random.getRandomSeed())

const settings = {
  suffix: `__seed_${random.getSeed()}`,
  animate: true,
  dimensions: [1920,1080],
};

const sketch = () => {
  const size = 80
  const randomRadius = true 
  const radius = 25

  console.log("Random Seed =>",random.getSeed())

  palette = dopePalettes.tambo
  reducedPalette = palette.slice(0,3)
  background = random.pick(palette)

  const gradient = (context, start, end, width, height, left_x=0, top_y=0) => { 
    fill = context.createLinearGradient(left_x, top_y, width, height)
    fill.addColorStop(0, start)
    fill.addColorStop(1, end)
    return fill
  }

  const lerp = (min, max, t) => { 
    return (max - min) * t + min
  }

  const createGrid = (args) => {
    const points = []
    const size = args.size
    
    for (let y = 0; y < size; y++) {
      for (let x = 0; x < size; x++) {
    
        const u = x / (size - 1)
        const v = y / (size - 1)
        const radius = Math.max(0, random.noise2D(u,v) + 1) * 0.01
        const noise = random.noise2D(u,v)

        points.push({
          color: random.pick(palette),
          position: {u, v},
          radius,
          rotation: noise,
          noise: noise,

        })
      }
    }
    return points
  }

  points = createGrid({ size, randomRadius, radius }).filter(() => random.value() > 0.8)
  
  document.body.style.backgroundColor = background
  document.body.style.margin = 0
  document.body.style.padding = 0

  return ({ context, width, height, time }) => {

    let state = {
      rotation: 0
    }

    const margin = -100
    let count = 0
    context.fillStyle = background
    context.fillRect(0,0,width,height)

    points.forEach(point => {
      
      let x = utils.lerp(margin, width - margin, point.position.u)
      let y = utils.lerp(margin, height - margin, point.position.v)
      let fontSize = point.radius * width * 10
      let length = 200
      
      let noiseX = Math.sin(time * 0.2) * width / 10 * point.noise + 0
      let noiseY = 0 + random.noise2D(time * 0.2,point.position.u) * height/40
      // TODO - 2019-01-23 16:09 - EL - Idea!

      context.fillStyle = point.color
      
      context.save()
        context.translate(x,y)
        context.rotate(Math.PI * point.noise * 0.1)
        context.fillRect(noiseX,noiseY,width / 15,width / 15)
      context.restore()
      
      count += 1
    });
    context.save()

      context.translate(width/1.5,height / 1.8)
      context.fillStyle = '#00000033'
      context.font = `42px "Inconsolata"` // `${margin/10}px`
      context.fillText("@ELI7VH", 400, 400)
      context.translate(-width/550,-height/250)
      context.fillStyle = background
      context.font = `42px "Inconsolata"` // `${margin/10}px`
      context.fillText("@ELI7VH", 400, 400)
    context.restore()

    // console.log("> rendered!")

  };
};

canvasSketch(sketch, settings);
