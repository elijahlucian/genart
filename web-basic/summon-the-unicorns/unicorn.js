class Unicorn {
	constructor(div, i) {
		this.a = Math.random() * Math.PI * 2

		this.i = i
		this.u = 0
		this.v = 0
		this.x = 0
		this.y = 0
		this.div = div
		this.speed = 0.01 * Math.random() + 0.01
		this.dead = false
		this.fontSize = 50
	}
	
	update(t) {
		this.u = this.u += this.speed
		this.u = this.v += this.speed
		this.div.style.fontSize = `${this.fontSize * (1 - this.u)}pt`
		this.div.style.transform = `rotate(${this.a * this.i + this.u * 360}deg)`

		if(this.u > 0.99) this.dead = true

		this.x = Math.cos(this.a) * (this.u * 50) + 46
		this.y = Math.sin(this.a) * (this.v * 50) + 46

		this.div.style.left = `${this.x}%`
		this.div.style.top = `${this.y}%`
		this.div.style.filter = `blur(${this.u * 2}px)`
	}
	
	kill() {
		this.dead = true
	}

	draw() {
	}
}