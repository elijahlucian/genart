const bg = document.querySelector('.rainbow')
var bgOpacity = 1


const limit = 10
herd = []

console.log(bg)

const summonTheUnicorns = () => {
	bgOpacity = 1
	for(let i = 0; i < limit; i++) {
		let uni = document.createElement('div')
		uni.className = 'unicorn'
		uni.innerHTML = '🦄'
		uni.style.zIndex = 0
		document.body.appendChild(uni)

		herd.push(new Unicorn(uni, i))
	}
	console.log(herd)
}

var f = 0

const draw = () => {
	if(bgOpacity > 0) bgOpacity -= 0.02
	bg.style.opacity = bgOpacity

	f += 1
	let t = f * 0.01
	for(i in herd) {
		let unicorn = herd[i]

		if(unicorn.dead) {
			herd.splice(i,1)
			unicorn.div.remove()
		}
		unicorn.update(t)
	}
		
	window.requestAnimationFrame(draw)
}



draw()