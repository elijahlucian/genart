coords = {
    x: 0,
    y: 0,
    z: 100,
}

materials = [
    'wireframe',
    'meshNormalMaterial',
    'meshStandardMaterial',
]

var renderer = new THREE.WebGLRenderer()
renderer.setSize( window.innerWidth, window.innerHeight )
document.body.appendChild( renderer.domElement )

var camera = new THREE.PerspectiveCamera( 45, window.innerWidth / window.innerHeight, 1, 500) 
camera.position.set( 0, 0, 100 )
camera.lookAt( new THREE.Vector3( 0, 0, 0 ))


var scene = new THREE.Scene()

var material = new THREE.LineBasicMaterial( {color: 0xffffff} )

var geometry = new THREE.BufferGeometry()

var positions

var geometry = new THREE.Geometry()
geometry.vertices.push( new THREE.Vector3( -10, 0, 0 ))
geometry.vertices.push( new THREE.Vector3( 10, 10, 10  ))
geometry.vertices.push( new THREE.Vector3( -20, -30, -40 ))

var line = new THREE.Line( geometry, material )
scene.add( line )

renderer.render( scene, camera )

var animate = () => {
    coords.x += 0.1
    coords.z -= 0.1
    camera.position.set( coords.x, coords.y, coords.z )
}

animate()