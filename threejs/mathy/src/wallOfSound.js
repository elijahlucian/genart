var square_size = 0.1;
var renderer,
  scene,
  camera,
  distance,
  raycaster,
  projector,
  directionalLight,
  ambientLight;
let cubeLimit = 20;
let width = 3;
let offset = width / 2;
let speed = 0.002;
let cubes = [];
let ycubes = [];

function init() {
  scene = new THREE.Scene();
  camera = new THREE.PerspectiveCamera(
    95,
    window.innerWidth / window.innerHeight,
    0.1,
    900000000
  );

  camera.position.y = 0;
  camera.position.x = 0;
  camera.position.z = 2;

  renderer = new THREE.WebGLRenderer();
  renderer.setSize(window.innerWidth - 5, window.innerHeight - 5);

  document.body.appendChild(renderer.domElement);

  directionalLight = new THREE.DirectionalLight({
    color: 0xff0000,
    intensity: 2
  });
  ambientLight = new THREE.AmbientLight({ color: 0x2404040 });

  var geometry = new THREE.BoxGeometry(
    square_size / 4,
    square_size / 4,
    square_size * 2
  );
  var material = new THREE.MeshPhongMaterial({
    color: 0x9dd9d2,
    transparent: true,
    opacity: 0.7,
    side: THREE.DoubleSide
  });

  for (let y = 0; y < cubeLimit * 1.5 + 1; y++) {
    for (let i = 0; i < cubeLimit + 1; i++) {
      let cube = new THREE.Mesh(geometry, material);
      cube.position.x = width * (i / cubeLimit) - offset;
      cube.position.y = 1.2 * width * (y / cubeLimit) - offset * 1.5;
      cube.i = i + i * y;

      cube.scale.z = Math.sin(cube.i * 0.01) + 1;
      cube.scale.x = Math.cos(cube.i * 0.01) + 1;
      cube.scale.y = Math.tan(cube.i * 0.01) + 1;
      cube.rotation.y = (y + i) / 2;

      cubes.push(cube);
      scene.add(cube);
    }
  }

  var geometry = new THREE.BoxGeometry(3, 3, 0.3);
  var material = new THREE.MeshPhongMaterial({
    color: 0xff5555,
    transparent: true,
    opacity: 0.7,
    side: THREE.DoubleSide
  });
  let border = new THREE.Mesh(geometry, material);

  border.position.z = -0.1;

  // scene.add( border )
  scene.add(ambientLight);
  scene.add(directionalLight);
}

function createCubes() {}
function createSpheres() {}

function rotateCube(cube) {
  rotationOffset = 2.5;
  cube.rotation.y = Math.sin(cube.position.x / offset);
  // cube.rotation.y = cube.position.x + cube.position.y;
  return cube;
}

let frame = 0;

function animate() {
  const t = frame / 60;
  cubes.forEach(cube => {
    cube.position.y = Math.sin(t + cube.i * 0.1);
    // fade out cubes would be cool
    cube.rotation.x += 0.02;
    cube.rotation.z -= 0.001;
    cube.rotation.y +=
      Math.PI / 100 + Math.sin(0.01 * t + cube.i * 0.01) * 0.001;
    cube.position.x =
      Math.sin(t + cube.i / (cubeLimit + Math.sin(t + cube.i * 0.001))) / 1;
    // cube.position.y = cube.position.x ** 3;
    cube.position.z = -Math.sin(t + cube.position.y * 2) / 2;
  });

  renderer.render(scene, camera);
  requestAnimationFrame(animate);
  frame += 1;
}

init();
animate();
