var square_size = 0.1

var scene = new THREE.Scene()
var camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 0.1, 900000000 )
// camera.fov = 35
// camera.focus = 0

camera.rotation.x -= 0.4
camera.position.y += 0.5
camera.position.z = 5;

var renderer = new THREE.WebGLRenderer()
renderer.setSize( window.innerWidth - 5, window.innerHeight -5 )

document.body.appendChild( renderer.domElement )


let cubeLimit = 33
let width = 3
let offset = width / 2
let speed = 0.002

let cubes = []
let ycubes = []
let directionalLight = new THREE.DirectionalLight({color: 0xff0000, intensity: 5})
let ambientLight = new THREE.AmbientLight({color: 0xf404040})

for(let z = 0; z < cubeLimit; z++) {
  for(let i = 0; i < cubeLimit; i++) {
    
    var geometry = new THREE.BoxGeometry( square_size/4, square_size/2, square_size*2 );
    var material = new THREE.MeshPhongMaterial( {
      color: 0x9DD9D2,
      transparent: true,
      opacity: 0.7,
      side: THREE.DoubleSide,
    } );
    let cube = new THREE.Mesh( geometry, material );
    cube.position.x = width * (i/cubeLimit) - offset;
    cube.position.z = width * (z/4) - offset;
    cube.i = i
    
    cubes.push(cube)
    scene.add( cube )

  }
}

scene.add(ambientLight)
scene.add(directionalLight)

function rotateCube(cube) {
	rotationOffset = 2.5
	cube.rotation.y = Math.sin(cube.position.x/offset);
	// cube.rotation.y = cube.position.x + cube.position.y;
	return cube
}

function animate() {
	requestAnimationFrame( animate )


	cubes.forEach(cube => {
		let x = cube.position.x
		// fade out cubes would be cool
		if(x > offset) {
			cube.position.x = -offset
		}
		cube = rotateCube(cube)
		cube.position.x += speed
		cube.position.y = cube.position.x ** 3;
		// cube.position.z = -Math.abs(cube.position.y) + 1
	})

	renderer.render( scene, camera )
}

animate()

