var square_size = 0.1;

var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera(
  75,
  window.innerWidth / window.innerHeight,
  0.1,
  900000000
);
// camera.fov = 35
// camera.focus = 0
var renderer = new THREE.WebGLRenderer();
renderer.setSize(window.innerWidth - 5, window.innerHeight - 5);

document.body.appendChild(renderer.domElement);

let cubeLimit = 33;
let width = 3;
let offset = width / 2;
let speed = 0.001;

let cubes = [];
let ycubes = [];

let directionalLight = new THREE.DirectionalLight({
  color: 0xff0000,
  intensity: 5
});
// let ambientLight = new THREE.AmbientLight({color: 0xf404040})

for (let i = 0; i < cubeLimit; i++) {
  var geometry = new THREE.BoxGeometry(square_size, square_size, square_size);
  var material = new THREE.MeshPhongMaterial({
    color: 0x9dd9d2,
    transparent: true,
    opacity: 1,
    side: THREE.DoubleSide
  });
  let cube = new THREE.Mesh(geometry, material);
  cube.position.x = width * (i / cubeLimit) - offset;
  cube.i = i;
  cubes.push(cube);
  scene.add(cube);
}

for (let i = 0; i < cubeLimit; i++) {
  var geometry = new THREE.BoxGeometry(square_size, square_size, square_size);
  var material = new THREE.MeshPhongMaterial({
    color: 0xf45866,
    transparent: true,
    opacity: 1,
    side: THREE.DoubleSide
  });
  let cube = new THREE.Mesh(geometry, material);
  cube.position.y = -width * (i / cubeLimit) + offset;
  cube.i = i;
  ycubes.push(cube);
  scene.add(cube);
}

// scene.add(ambientLight);
scene.add(directionalLight);
camera.position.z = 4;

function rotateCube(cube) {
  rotationOffset = 2.5;
  cube.rotation.x = cube.position.x * cube.position.z;
  cube.rotation.y = cube.position.x * cube.position.y;
  return cube;
}

function animate() {
  requestAnimationFrame(animate);

  ycubes.forEach(cube => {
    let y = cube.position.y;
    if (y < -offset) {
      cube.position.y = offset;
    }
    cube = rotateCube(cube);
    cube.position.y -= speed;
    cube.position.x = cube.position.y ** 3;
    cube.position.z = Math.abs(cube.position.y * 2) - 1;
  });

  cubes.forEach(cube => {
    let x = cube.position.x;
    // fade out cubes would be cool
    if (x > offset) {
      cube.position.x = -offset;
    }
    cube = rotateCube(cube);
    cube.position.x += speed;
    cube.position.y = cube.position.x ** 3;
    cube.position.z = -Math.abs(cube.position.y) + 1;
  });

  renderer.render(scene, camera);
}

animate();
