const loader = () => {
  const canvas = document.getElementById('renderCanvas');
  const engine = new BABYLON.Engine(canvas, true)

  const createScene = () => {
    const scene = new BABYLON.Scene(engine)
    const camera = new BABYLON.UniversalCamera('UniversalCamera', new BABYLON.Vector3(0,5,-10), scene)
    // const camera = new BABYLON.FreeCamera('camera', new BABYLON.Vector3(0,5,-10), scene)
    camera.setTarget(BABYLON.Vector3.Zero());
    camera.attachControl(canvas, false);

    const light = new BABYLON.HemisphericLight('light1', new BABYLON.Vector3(0,1,0), scene);
    const sphere = BABYLON.MeshBuilder.CreateSphere('sphere', {segments:16, diameter:2}, scene);
    sphere.position.y = 1;

    const ground = BABYLON.MeshBuilder.CreateGround('ground1', {height:6, width:6, subdivisions: 2}, scene);

    return scene
  }

  scene = createScene()

  engine.runRenderLoop(() => scene.render())
  
}


window.addEventListener('DOMContentLoaded', loader)