export type Params = {
  fps: number
  deltaTime: number
  duration: number
  playhead: number
  width: number
  height: number
  time: number
  context: CanvasRenderingContext2D
  frame: 407
  totalFrames: number
  timeScale: 1
  canvas: HTMLCanvasElement
  started: boolean
  exporting: boolean
  playing: boolean
  recording: boolean
  settings: Params
  bleed: 0
  pixelRatio: 1
  dimensions: [number, number]
  units: 'px'
  scaleX: number
  scaleY: number
  pixelsPerInch: 72
  viewportWidth: number
  viewportHeight: number
  canvasWidth: number
  canvasHeight: number
  trimWidth: number
  trimHeight: number
  styleWidth: number
  styleHeight: number
}
