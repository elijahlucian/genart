import React, { useEffect, useState } from 'react'
import { ReactSketchy } from '@dank-inc/react-sketchy'
import sketches from './sketches'
import './app.css'

type State = {
  index: number
}

export const App = () => {
  const [state, setState] = useState<State | null>(null)

  useEffect(() => {
    const controls = (e: KeyboardEvent) => {
      if (e.key === 'e') next()
      if (e.key === 'q') prev()
    }
    document.addEventListener('keypress', controls)

    return () => document.removeEventListener('keypress', controls)
  })

  useEffect(() => {
    const index = localStorage.getItem('genart-index')
    setState({ ...state, index: index ? JSON.parse(index) : 0 })
  }, [])

  const prev = () => {
    if (!state) return
    if (state.index > 0) setState({ ...state, index: state.index - 1 })
  }
  const next = () => {
    if (!state) return

    if (state.index < Object.keys(sketches).length - 1)
      setState({ ...state, index: state.index + 1 })
  }

  if (!state)
    return (
      <div className="app">
        <div className="header">
          <h1>Loading...</h1>
        </div>
      </div>
    )

  const sketch = sketches[state.index]

  console.log('asdf', sketch)

  return (
    <div className="app">
      <div className="header">
        <button onClick={prev}>Prev</button>
        <h1>wut</h1>
        <p>
          index: {state.index + 1} / {sketches.length}
        </p>
        <button onClick={next}>Next</button>
      </div>
      <ReactSketchy
        className="sketch"
        dimensions={[600, 600]}
        animate
        sketch={sketch}
      />
    </div>
  )
}
