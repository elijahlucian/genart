import sketches from './sketches'

import { loadSketch, createParams } from '@dank-inc/sketchy'

const params = createParams({
  containerId: 'root',
  // animate: true,
})

const sketch = sketches[process.env.SKETCH || 'hazy']

loadSketch(sketch, params)
