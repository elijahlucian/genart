import { Vec2 } from 'types'

type DimensionMap = {
  desktop: Record<string, Vec2>
  square: Record<string, Vec2>
  print: Record<string, Vec2 | Record<string, Vec2>>
}

export const dimensions: DimensionMap = {
  desktop: {
    hd: [1280, 720],
    fhd: [1920, 1080],
    fourk: [3840, 2160],
    eightk: [7680, 4320],
  },
  square: {
    insta: [2048, 2048],
    large: [4096, 4096],
    huge: [8192, 8192],
    max: [11180, 11180],
  },
  print: {
    eightbyten: [1500, 3000],
    fivebyfour: {
      small: [8 * 300, 10 * 300],
      medium: [16 * 300, 20 * 300],
      large: [24 * 300, 30 * 300],
    },
    threebyfour: [3, 4],
    threebytwo: [3, 2],
    a5: [5, 5],
    poster: [8, 10],
    displate: [2900, 4060], // margin 181px min
    moo: {
      standard: [1038, 696],
      square: [813, 813],
      mini: [874, 378],
    },
  },
}

export const doubleSize = ([x, y]: Vec2) => {
  return [x * 2, y * 2]
}
export const quadSize = ([x, y]: Vec2) => {
  return [x * 4, y * 4]
}

export const flipOrientation = ([x, y]: Vec2) => {
  return [y, x]
}
