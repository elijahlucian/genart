export class TheNet {
  constructor(params) {
    Object.assign(this, params)
    this.z = this.u
    this.size = this.baseSize
    this.dead = false
    this.mature = false
    this.iterations = 0
    this.delay = this.rand.value() * 20
  }

  checkNormalBounds() {
    if (this.u > 1.05) this.u = 0
    if (this.u < -0.05) this.u = 1
    if (this.v < -0.05) this.v = 1
  }

  update({ time, width, height, speed }) {
    if (time < this.delay) return
    // grow the current branch in general direction
    // if reaches EOL, branch out if total agent count is not done.

    this.life -= time * 0.01

    if (this.life < -1) this.dead = true
    let xRatio = 1 / width
    let yRatio = 1 / height

    let n = this.rand.noise3D(this.u * 2, this.v, time * 0.005)
    this.u += Math.sin(this.angle + n * this.v * 5) * xRatio * (speed + n)
    this.v += Math.cos(this.angle + n * this.v * 5) * yRatio * (speed + n)

    if (this.v > 1.05) {
      this.dead = true //this.v = 0
    }

    this.size = this.baseSize * Math.abs(n) + 2 + Math.sin(time) * 2
    this.x = this.u * width
    this.y = this.v * height
  }

  draw({ fill, stroke }) {
    // draw shape
    this.context.fillStyle = this.color
    // this.context.fillRect(0, 0, this.x, this.y)

    this.context.beginPath()
    this.context.arc(this.x, this.y, this.size, 0, Math.PI * 2)

    if (fill) this.context.fill()
    if (stroke) this.context.stroke()
  }
}
