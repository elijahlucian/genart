// on construction, create settings
// fillstyle
// 

const events = {
  docs: 'https://developer.mozilla.org/en-US/docs/Web/Events',
  
  clipboard: {
    cut:	'The selection has been cut and copied to the clipboard',
    copy:	'The selection has been copied to the clipboard',
    paste:	'The item from the clipboard has been pasted',
  },
  mouse: {
    auxclick:	'A pointing device button (ANY non-primary button) has been pressed and released on an element.',
    click:	'A pointing device button (ANY button; soon to be primary button only) has been pressed and released on an element.',
    contextmenu:	'The right button of the mouse is clicked (before the context menu is displayed).',
    dblclick:	'A pointing device button is clicked twice on an element.',
    mousedown:	'A pointing device button is pressed on an element.',
    mouseenter:	'A pointing device is moved onto the element that has the listener attached.',
    mouseleave:	'A pointing device is moved off the element that has the listener attached.',
    mousemove:	'A pointing device is moved over an element. (Fired continously as the mouse moves.)',
    mouseover:	'A pointing device is moved onto the element that has the listener attached or onto one of its children.',
    mouseout:	'A pointing device is moved off the element that has the listener attached or off one of its children.',
    mouseup:	'A pointing device button is released over an element.',
    pointerlockchange:	'The pointer was locked or released.',
    pointerlockerror:	'It was impossible to lock the pointer for technical reasons or because the permission was denied.',
    select:	'Some text is being selected.',
    wheel:	'A wheel button of a pointing device is rotated in any direction.',
  },
  keyboard: {
    keydown:	'ANY key is pressed',
    keypress:	'ANY key except Shift, Fn, CapsLock is in pressed position. (Fired continously.)',
    keyup:	'ANY key is released',
  },
  drag_and_drop: {
    drag:	'An element or text selection is being dragged (Fired continuously every 350ms).',
    dragend:	'A drag operation is being ended (by releasing a mouse button or hitting the escape key).',
    dragenter:	'A dragged element or text selection enters a valid drop target.',
    dragstart:	'The user starts dragging an element or text selection.',
    dragleave:	'A dragged element or text selection leaves a valid drop target.',
    dragover:	'An element or text selection is being dragged over a valid drop target. (Fired continuously every 350ms.)',
    drop:	'An element is dropped on a valid drop target.',
  },
  web_vr: {
    vrdisplayactivate:	'When a VR display is able to be presented to, for example if an HMD has been moved to bring it out of standby, or woken up by being put on.',
    vrdisplayblur:	'when presentation to a VRDisplay has been paused for some reason by the browser, OS, or VR hardware — for example, while the user is interacting with a system menu or browser, to prevent tracking or loss of experience.',
    vrdisplayconnect:	'when a compatible VRDisplay is connected to the computer.',
    vrdisplaydeactivate:	'When a VRDisplay can no longer be presented to, for example if an HMD has gone into standby or sleep mode due to a period of inactivity.',
    vrdisplaydisconnect:	'When a compatible VRDisplay is disconnected from the computer.',
    vrdisplayfocus:	'When presentation to a VRDisplay has resumed after being blurred.',
    vrdisplaypresentchange:	'The presenting state of a VRDisplay changes — i.e. goes from presenting to not presenting, or vice versa.',
  }
}