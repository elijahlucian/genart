import { thisExpression } from '@babel/types'

const materials = require('../three-basics/materials')
const random = require('canvas-sketch-util/random')
const { PI, TAU } = require('../modules/maf')
const Bezier = require('bezier-easing')
const bezier = Bezier(0, 0.2, 0.6, 0.9)

class Base {
  constructor(params) {
    let {
      u,
      v,
      ui,
      index,
      size = 2,
      color = '#fff',
      edges = 10,
      scale = 10,
      normalized_index,
      row,
      col,
      stepSize,
      bezier,
    } = params
    Object.assign(this, {
      u,
      v,
      ui,
      scale,
      size,
      edges,
      color,
      index,
      normalized_index,
      row,
      col,
      stepSize,
      bezier,
    })

    this.n = random.noise2D(u, v)
    this.u = this.u * scale
    this.v = this.v * scale
  }
}

class Text extends Base {}

class Triangle extends Base {
  constructor(params) {
    super(params)

    this.speed = 0.5

    let shape = new THREE.Shape()

    shape.lineTo(this.size * 0.5, this.size, 0)
    shape.lineTo(-this.size * 0.5, this.size, 0)

    this.extrude = {
      depth: 0.01,
      bevelEnabled: true,
      bevelSegments: 2,
      steps: 1,
      bevelSize: 0.1,
      bevelThickness: 0.1,
    }

    let geometry = new THREE.ExtrudeGeometry(shape, this.extrude)
    this.shape = shape
    this.mesh = new THREE.Mesh(geometry, materials.basic({ color: this.color }))
    this.mesh.position.set(this.u, this.v - this.size / 2, 0)
    this.mesh.geometry.center()
    this.mesh.position.x -= this.size / 2

    if (this.col % 2 == 0) {
      this.mesh.rotation.z += Math.PI
      this.mesh.position.x += (this.stepSize * this.scale) / 2
      this.mesh.position.y += this.stepSize * 2
    }
    // console.log(this.mesh)
  }

  update({ time }) {
    let noise = random.noise3D(this.u, this.v, time)
    let t = 10 + this.speed * time + this.n
    let bez = bezier(t % 1)
    this.mesh.rotation.y = bez * Math.PI
    this.mesh.scale.y = bez
    // this.mesh.position.set(this.u * noise,noise + this.v - this.size / 2,0)

    // this.mesh.scale.z = bez * 2
    // this.geometry.extrude.depth = Math.sin(time * Math.PI * 2) * 1
  }
}

class Square extends Base {
  constructor(params) {
    super(params)
    this.shape = new THREE.Shape()
    this.color = params.color || '#fff'
    this.size = params.size || 0.2
    this.index = params.index
    this.scale = 1
    this.bing = false
    this.note = params.note
    this.notePlayed = false
    this.rotationVector = 0

    this.points = [
      [-this.size, this.size],
      [this.size, this.size],
      [this.size, -this.size],
      [-this.size, -this.size],
    ]
    this.shape.moveTo(this.u, this.v)
    this.points.forEach(point => {
      let [x, y] = point
      this.shape.lineTo(this.u + x, this.v + y + 10)
    })

    var extrudeSettings = {
      depth: 2,
      bevelSegments: 0,
      steps: 2,
      bevelSize: 1,
      bevelThickness: 1,
    }
    var geometry = new THREE.ExtrudeGeometry(this.shape, extrudeSettings)

    this.mesh = new THREE.Mesh(geometry, materials.basic({ color: this.color }))
    this.mesh.position.set(this.u, this.v, 0)
    this.mesh.geometry.center()
    this.noteOn = false
  }
  update({ time, beats }) {
    let mt = time % 8

    if (this.index == Math.floor(mt)) {
      if (this.noteOn) {
        this.rotationVector = time
        // this.mesh.rotation.z = this.rotationVector
        this.scale *= 0.9
        this.mesh.rotation.z =
          (Math.sin(time * 75 * this.scale * 2) / 4) * this.scale
        this.mesh.material.color = new THREE.Color(
          `hsl(0, 0%, ${Math.floor(90 * this.scale + 10)}%)`
        )
      }
      this.mesh.scale.z = this.scale
      // this.mesh.material.color.set('#fff')
      this.noteOn = true
      if (this.notePlayed === false) {
        this.bing = true
        this.notePlayed = true
      }
    } else {
      this.rotationVector = 0
      if (!this.noteOn && this.scale >= 1) {
        this.scale *= 0.9
        // this.mesh.rotation.z *= 0.8
      }
      this.scale = 1
      this.mesh.scale.z = 1
      this.mesh.rotation.z = 0
      this.mesh.material.color.set(this.color)
      this.noteOn = false
      this.notePlayed = false
    }
  }
}

class Arc extends Base {
  constructor(params) {
    super(params)
    this.rotationDirection = random.boolean() ? 1 : -1
    this.speed = random.value()
    this.clickInterval = 5 + 5 * random.value(0.75, 1.25)
    this.nextClick = this.clickInterval
    this.clickDelta = this.noisy ? 0 : 5

    let shape = new THREE.Shape()

    shape.moveTo(
      this.u + Math.sin(0) * this.size,
      this.v + Math.cos(0) * this.size
    )
    for (let i = 1; i <= this.edges; i++) {
      shape.lineTo(
        this.u + Math.sin((i / this.edges) * TAU) * this.size,
        this.v + Math.cos((i / this.edges) * TAU) * this.size
      )
    }
    this.extrude = {
      depth: 0.01,
      bevelEnabled: true,
      bevelSegments: 2,
      steps: 1,
      bevelSize: 0.1,
      bevelThickness: 0.1,
    }

    let geometry = new THREE.ExtrudeGeometry(shape, this.extrude)

    this.shape = shape
    this.mesh = new THREE.Mesh(geometry, materials.basic({ color: this.color }))

    this.mesh.position.set(this.u, this.v, 0)
    this.mesh.geometry.center()
    this.originalVertices = this.mesh.geometry.vertices
  }

  update({ time }) {}
}

class KungFuWap extends Arc {
  constructor(params) {
    super(params)
    this.noisy = params.noisy || random.boolean()
  }

  update({ time }) {
    this.clickDelta += 0.01
    if (this.clickDelta > this.nextClick) this.click()

    let t = time * this.speed
    this.mesh.rotation.y = Math.sin(this.index + time) * 0.5
    this.mesh.rotation.x = Math.sin(this.speed + this.index + time) * 0.5
    this.mesh.rotation.z += this.speed * 0.5 * 0.01 * this.rotationDirection

    if (!this.noisy) return

    this.mesh.geometry.computeVertexNormals()
    this.mesh.geometry.verticesNeedUpdate = true
    this.mesh.geometry.normalsNeedUpdate = true

    let verticesLength = this.mesh.geometry.vertices.length
    for (let i = 0; i < verticesLength; i++) {
      let n = random.noise4D(
        this.mesh.position.x * 0.3,
        this.mesh.position.y * 0.2,
        this.mesh.position.z * 0.3,
        t + i
      )

      // this.mesh.geometry.vertices[i].copy(
      //   sphereGeoTemp.vertices[i].multiplyScalar(1 + 0.3 * n)
      // )
      this.mesh.geometry.vertices[i]
        .normalize()
        .multiplyScalar(this.size + Math.abs(n * 2.7))
    }
  }

  click() {
    this.clickDelta = 0
    this.noisy = random.boolean()
    if (!this.noisy) {
      this.mesh.geometry = new THREE.ExtrudeGeometry(this.shape, this.extrude)
      this.mesh.position.set(this.u, this.v, 0)
      this.mesh.geometry.center()
    }
  }
}

class Blade {
  constructor(params) {
    let { u, v, index } = params
    this.u = u
    this.v = v
    this.index = index
  }

  update({ time }) {}
}
class Sphere {
  constructor(params) {
    let { u, v, size, x, y, index, speed = 0.5, color = '#fff' } = params // defaults here.
    this.u = u
    this.v = v
    this.size = size * 0.5
    this.x = x
    this.y = y
    this.index = index
    this.speed = speed
    this.color = color

    this.n = random.noise2D(this.u, this.v)

    this.mesh = new THREE.Mesh(
      new THREE.SphereGeometry(this.size, 2, 3),
      materials.physical({ color: color })
    )

    this.mesh.position.x = x
    this.mesh.position.y = y
    this.mesh.scale.x = 1.5
  }

  update({ time }) {
    let t = time * this.speed
    this.mesh.rotation.y = t + this.n + this.index * 0.01
    // this.mesh.position.z = Math.sin(this.index + t * Math.PI * 2)
  }

  draw(params) {
    // even need this with threejs?
  }
}

class CustomShape {
  constructor(params = null) {
    throw 'CustomShape requires params { u, v, index, size, color }'
    let { u, v, index, size, color } = params
  }
}

class Flame extends Base {
  constructor(params) {
    super(params)
    this.shape = new THREE.Shape()

    this.geometry = new THREE.Geometry(this.shape)

    this.mesh = new THREE.Mesh()
  }

  update() {}
}

export { Sphere, CustomShape, Arc, Triangle, KungFuWap, Square, Text, Flame }
