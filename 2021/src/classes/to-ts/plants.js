import { thisExpression, throwStatement } from '@babel/types'

const { validate } = require('./validations')
const { PI, TAU, vectors } = require('../modules/maf')
const { range } = require('../modules/utils')
const random = require('canvas-sketch-util/random')

class PlantBase {
  constructor(params) {
    let { i, u, v, size, limit, color, angle, palette } = validate(params)
    Object.assign(this, { i, u, v, size, limit, color, angle, palette })
    this.context = params.context
    this.age = 0
    this.points = []
    this.branches = []
    this.growing = true
    this.sprouted = false
    this.x = 0
    this.y = 0
  }

  sprout() {}

  getU(i) {}

  getV(i) {
    // use v value of where thingy is
    // calculate where to sprout from
  }

  getParams() {
    let { i, u, v, x, y, size, limit, color, palette, angle } = this
    return {
      i: i + this.branches.length,
      u,
      v,
      x,
      y,
      size,
      limit,
      color,
      palette,
      angle,
    }
  }

  grow() {
    if (this.matured) return
    this.age += 0.001
    if (this.age > 1)
      this.matured = console.log(this.name, 'finished growing') || true
  }

  updatePoints(params) {
    let { time, width, height } = params
    this.x = this.u * width
    this.y = this.v * height
    // if (this.matured) return
    this.points = []
    for (let i = -1; i <= 1.01; i += 0.02) {
      let u = i * Math.sin(time + Math.abs(this.i - 10) * Math.abs(i) * 2 * TAU) // (i * this.age) / 2
      let v = (Math.abs(i) - 1) * 1.04 // * this.age
      let n = 1 // random.noise3D(this.age, this.i, v * 5) * 4
      let x = u * this.size
      let y = v * this.limit
      this.points.push([x, y])
    }
    // this.matured = true
  }

  drawChildren(context) {
    for (let branch of this.branches) {
      branch.draw(context)
    }
  }

  drawSelf(context) {
    context.fillStyle = this.color
    context.strokeStyle = this.color // '#fff'
    context.lineWidth = 5
    context.save()
    context.translate(this.x, this.y)
    context.rotate((this.angle / 360) * TAU)
    context.beginPath()
    for (let point of this.points) {
      let [x, y] = point
      context.lineTo(x, y)
    }
    context.closePath()
    context.fill()
    // context.stroke()
    context.restore()
  }
}

class Tree extends PlantBase {
  constructor(params) {
    super(params)
    this.name = 'Tree'
  }

  update(params) {
    this.grow()
    this.trunk.update(params)
  }

  sprout() {
    if (this.sprouted) return
    this.trunk = new Trunk(this.getParams())
    this.sprouted = true
  }

  draw(context) {
    this.trunk.draw(context)
  }
}

class Trunk extends PlantBase {
  constructor(params) {
    super(params)
    this.name = 'Trunk'
    this.branches = []
    this.branchLimit = 100
  }

  update(params) {
    // if (random.chance(0.05) && this.branches.length < this.branchLimit)
    this.grow()
    this.sprout()
    this.updatePoints(params)
    for (let branch of this.branches) {
      branch.update(params)
    }
  }

  draw(context) {
    this.drawChildren(context)
    this.drawSelf(context)
  }

  sprout() {
    if (this.sprouted) return
    let params = this.getParams()
    params.v = random.value()
    params.angle = this.angle + (random.boolean() ? -65 : 65)
    params.limit = (this.limit / 2) * params.v
    params.size = (this.size / 4) * params.v
    params.color = random.pick(this.palette)
    this.branches.push(new Branch(params))
    if (this.branches.length > this.branchLimit) this.sprouted = true
  }
}

class Branch extends PlantBase {
  constructor(params) {
    super(params)
    this.name = 'Branch'
    this.branchLimit = 5
  }

  update(params) {
    this.grow()
    this.updatePoints(params)
    this.sprout()
  }

  draw(context) {
    this.drawSelf(context)
    this.drawChildren(context)
  }

  sprout() {
    if (this.sprouted) return
    console.log('Growing a Twig!')
    let params = this.getParams()
    params.angle = this.angle + random.boolean ? 90 : -90
    params.v = random.value()
    params.limit = (this.limit / 2) * params.v
    params.size = (this.size / 2) * params.v

    this.branches.push(new Branch(params))

    if (this.branches.length > this.branchLimit) this.sprouted = true
  }
}

class Twig extends PlantBase {
  constructor(params) {
    super(params)
  }
  update(params) {
    this.grow()
    this.updatePoints(params)
  }
  draw(context) {
    this.drawSelf(context)
    // this.drawChildren(context)
  }
}

class Leaf extends PlantBase {
  constructor(params) {
    super(params)
  }
}

export { Tree }
