const { PI, TAU } = require('../modules/maf')
const random = require('canvas-sketch-util/random')

class Fluff {
  constructor({u,v,time}) {
    this.u = u
    this.v = v
    this.n = random.noise3D(u,v,time)
    this.vu = random.noise2D(u,time)
    this.vv = random.noise2D(v,time)
    this.floatSpeed = 0.0007
    
    this.life = 0.1
    this.fluffize = 0
    this.dead = false
    this.mature = false 
  }

  update({time}) {
    if(this.dead)return
    // increase size to threshold, then decay
    if(this.mature) {
      this.fluffSize = ((this.n + 0.8)) * this.life
      this.life -= 0.003
    } else {
      this.fluffSize = ((this.n + 0.8)) * this.life
      this.life += 0.01
      if(this.life >= 1) this.mature = true
    }
    this.u += this.vu * this.floatSpeed
    this.v += this.vv * this.floatSpeed
    if(this.fluffSize < 0) this.dead = true
  }

  draw({context,width,height}) {
    if(this.dead) return
    let x = this.u * width 
    let y = this.v * height
    
    context.save()
    context.beginPath()
    context.strokeStyle = '#fff7'
    context.lineWidth = width / 1000
    context.fillStyle = `hsla(${(this.n + 0.5) * 360},100%,80%,0.9)`

    context.translate(x,y)
    context.arc(9,0,this.fluffSize * width / 90,0,TAU)
    context.fill()
    context.stroke()
    context.closePath()
    context.restore()
  }
}

class Ship {
  constructor() {
    this.force = 0.0045
    this.rotation = 0
    this.u = 0.5
    this.v = 0.5
    this.level = 0
    this.levelUpAt = 0
    this.leveledUp = false
  }
  update({keys, time}) {
    let t = random.noise1D(time)
    let {u,v,rotation} = this
    let n = random.noise1D(time * 0.05)
    this.u += Math.cos(this.rotation) * this.force
    this.v += Math.sin(this.rotation) * this.force
    this.rotation = TAU * n
    if(u > 1) {
      this.u = 0
      this.v = random.valueNonZero() / 2 + 0.25
      this.levelUp({time})
    }
    if(u < 0) {
      this.u = 1
      this.v = random.valueNonZero() / 2 + 0.25
      this.levelUp({time})
    }
    if(v > 1) {
      this.v = 0
      this.u = random.valueNonZero() / 2 + 0.25
      this.levelUp({time})
    }
    if(v < 0) {
      this.v = 1
      this.u = random.valueNonZero() / 2 + 0.25
      this.levelUp({time})
    }

    return

    if(keys['w']){
      console.log("THRUST")
      this.u += Math.cos(this.rotation) * 0.002
      this.v += Math.sin(this.rotation) * 0.002
    }
    if(keys['s']){
      console.log("BRAKE")
    }
    if(keys['a']){
      this.rotation -= PI * 0.015
    }
    if(keys['d']){
      this.rotation += PI * 0.015
    }
  }
  levelUp({time}){
    this.level += random.value()
    this.levelUpAt = time
    this.leveledUp = true
  }
  draw({context, width, height}) {
    let o = width / 100
    context.strokeStyle = '#fff'
    context.lineWidth = width / 150
    context.save()
    context.beginPath()
    context.translate(this.u * width, this.v * height)
    context.rotate(this.rotation + PI * 0.6 + PI / 2)
    context.moveTo(0,0)
    for(let i = 0; i < 3; i ++) {
      context.lineTo(o,o)
      context.rotate(TAU/3)
    }
    context.closePath()
    context.fillStyle = '#fff'
    context.fill()
    context.stroke()
    context.restore()
  
  }
}

class Asteroid {
  constructor({u,v,w,sides,time}) {
    this.u = u || 0.5
    this.v = v || 0.5
    this.w = w || 0.5
    this.sides = sides || 5
    this.n = random.noise3D(u,v,time || 0)
  }

  draw() {
    // noisy circle would make asteroid thingy
    
  }
}

export { Fluff, Ship, Asteroid }