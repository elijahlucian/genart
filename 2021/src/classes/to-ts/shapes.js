// a 2d shape class library

const { PI, TAU, vectors } = require('../modules/maf')
const { range } = require('../modules/utils')
const random = require('canvas-sketch-util/random')

class Base {
  constructor(params) {
    let { u, v, i, z, color } = params
    Object.assign(this, { u, v, i, z, color })
  }
}

class BaseShape {
  constructor(params) {
    let { i, z, baseSize, palette } = params
    this.palette = palette
    this.i = i
    this.z = z
    this.u = 0.5
    this.v = 0.5
    this.color = params.color
    this.baseSize = baseSize || 100
    this.lineWidth = params.lineWidth
    this.data = params.data
    this.speed = 0.5
    this.a = random.range(0, TAU)
    this.n = 1
    this.size = params.size || 100
    this.stroke = params.stroke || false
    this.strokeStyle = params.strokeStyle || '#fff'
    this.clockwise = params.clockwise || true
    this.radius = params.radius || 1
    this.time = 0
    this.changeInterval = 1
    this.nextChange = this.changeInterval
  }

  sorted() {
    return this
  }

  update({ time, width, height, audio, music, beats }) {
    if (!audio) audio = { low: 1, mid: 1, high: 1 }
    this.time = time
    if (this.time > this.nextChange) {
      this.nextChange += this.changeInterval
      this.color = random.pick(this.palette)
    }
    let t = time * this.speed
    let w = width / 2
    let h = height / 2
    let s = Math.sin(t + this.z * TAU)
    let c = Math.cos(t + this.z * TAU)
    this.size = this.baseSize + Math.abs(music) * this.baseSize * 4
    this.x =
      this.u * (music * Math.sin(t + this.z * TAU) * Math.sin(time * TAU)) * w +
      w
    this.y = this.v * (music / 2 + c) * h + h / 1.2
  }

  draw({ context }) {
    context.beginPath()
    context.fillStyle = this.color
    context.lineWidth = this.lineWidth
    context.strokeStyle = this.strokeStyle
    context.arc(this.x, this.y, this.size * this.n, 0, TAU)
    if (this.stroke) {
      context.stroke()
    }
    context.fill()
  }
}

class Box extends BaseShape {
  draw({ context }) {
    context.save()
    context.translate(this.x, this.y)
    context.rotate(this.i)
    context.strokeStyle = this.color
    context.beginPath()
    let points = [
      [-1, 1],
      [1, 1],
      [1, -1],
      [-1, -1],
    ]
    points.forEach(point => {
      let [u, v] = point
      context.lineTo(u * this.size, v * this.size)
    })
    context.stroke()
    context.restore()
  }
}

class Leaf extends Base {
  constructor(params) {
    super(params)
    this.angle = params.angle // vector of leaf
    this.u = 0.5
    this.v = 0.5
    this.length = params.length || 20
    this.points = []
    this.translated_points = []
    this.generatePoints = this.generatePoints.bind(this)
    this.generatePoints()
  }

  generatePoints() {
    console.log('making the leaf')

    for (let i = -1; i <= 1; i += 0.1) {
      let u, v
      if (i < 0) {
        u = (this.u + -i) * 0.4 * Math.sin(i * 100) * 0.2 + 0.2
        v = (this.v + -i) * 0.5 * Math.abs(i) + 0.1
      } else {
        u = (this.u + i) * 0.3 * Math.cos(i * 20) * 0.5 + 0.4
        v = (this.u + i) * 0.3 * Math.sin(i * 100) * 0.2 + 0.2
      }
      this.points.push({ u, v })
    }
  }

  update({ time, width, height }) {
    this.translated_points = []
    this.points.forEach(point => {
      this.translated_points.push({
        x: point.u * width,
        y: point.v * height,
      })
    })
  }

  draw({ context }) {
    context.fillStyle = '#fff'
    context.beginPath()
    this.translated_points.forEach(point => {
      context.lineTo(point.x, point.y)
    })
    context.closePath()
    context.fill()
  }
}
class Vine extends Base {
  constructor(params) {
    super(params)
    this.segments = params.segments || 10
    this.lineWidth = params.lineWidth
    this.points = []
  }

  update({ time, width, height }) {
    this.points = []
    let t = time * 0.1
    for (let i = 0; i < this.segments; i++) {
      let z = i / (this.segments - 1)
      let x =
        this.u * width +
        Math.sin(4 * time + z * 10 * (Math.cos(time) + 5)) * 100
      let y = z * height + time
      let size = Math.abs(
        Math.sin(4 * time + z * 10 * (Math.cos(time) + 5)) * this.lineWidth * 2
      )
      this.points.push({ x, y, size })
    }
  }

  draw({ context }) {
    // context.beginPath()
    context.lineWidth = this.lineWidth
    context.fillStyle = this.color
    context.strokeStyle = this.color
    // context.moveTo(this.x,0)
    this.points.forEach(point => {
      context.beginPath()
      let { x, y, size } = point
      context.arc(x, y, size, 0, TAU)
      // context.lineTo(x,y)
      context.stroke()
    })
  }
}

class FracTree extends Base {
  constructor(params) {
    super(params)

    let { angle, baseSize, color, life, opacity, index = 0 } = params
    Object.assign(this, { angle, baseSize, color, life, opacity, index })
    this.og_hp = this.life < 5 ? 5 : this.life
    this.life = this.og_hp * (random.boolean() ? 1 : 0.75)
    this.speed = 0.0025
    this.opacity = opacity
  }

  decay() {
    if (this.dead) return
    this.life -= this.speed * 200
    if (this.life < 0) {
      this.spawn = true
      return
    }
  }

  update({ time, width, height }) {
    if (this.dead) return
    this.life -= this.speed * 200
    if (this.life < 0) {
      this.spawn = true
      return
    }
    // if(this.u < 0 || this.u > 1) this.dead = true
    // if(this.v < 0) this.dead = true
    let n = random.noise2D(this.u * 10, this.v * 10)
    let g = random.gaussian()
    this.size = this.baseSize + Math.abs(n) * 5 + 1.7
    this.u -= Math.sin((this.angle / 360) * TAU) * this.speed
    this.v -= Math.cos((this.angle / 360) * TAU) * this.speed
    let rndSize = height / 200
    this.x = this.u * height + n * rndSize
    this.y = this.v * height + n * rndSize
    this.angle += (random.boolean() ? 0.2 : -0.2) * this.index //Math.sin((this.index + time)) * PI
  }

  draw({ context }) {
    let r = Math.floor(random.range(0.2, 0.5) * 256)
    if (this.dead) return
    context.lineWidth = this.baseSize / 2
    context.beginPath()
    context.arc(this.x, this.y, this.size, 0, TAU)
    context.fillStyle = this.color // + r.toString(16)
    context.fill()
  }

  kill() {
    this.dead = true
  }
}

class RealTree extends FracTree {
  constructor(params) {
    super(params)
    this.size = params.size || 20
    this.speed = 0.001
  }

  update({ time, width, height }) {
    this.u += Math.sin((this.angle / 360) * TAU) * this.speed
    this.v -= Math.cos((this.angle / 360) * TAU) * this.speed
    this.x = this.u * height
    this.y = this.v * height
  }
}

class FunShape {
  constructor({ size, color }) {
    this.size = size
    this.color = color
    this.rounded_corner = random.pick([2])
    this.algo = [
      { u: -1, v: 1 },
      { u: 1, v: 1 },
      { u: 1, v: -1 },
      { u: -1, v: -1 },
    ]
  }

  draw({ context, time }) {
    context.fillStyle = this.color
    context.beginPath()
    this.algo.forEach(({ u, v }, i) => {
      if (i == this.rounded_corner) {
        context.lineTo(u * this.size, v * this.size)
        context.quadraticCurveTo(
          u * this.size,
          v * this.size * 2,
          this.algo[i + 1].u * this.size,
          this.algo[i + 1].v * this.size * 2
        )
      } else {
        context.lineTo(u * this.size, v * this.size * 2)
      }
    })

    context.fill()
    context.closePath()
    context.stroke()
  }
}

export { BaseShape, FunShape, Box, FracTree, Vine, Leaf, RealTree }
