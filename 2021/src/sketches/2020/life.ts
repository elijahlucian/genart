import { createSketch } from '@dank-inc/sketchy'

const SimplexNoise = require('simplex-noise')
const simplex = new SimplexNoise()

document.body.style.backgroundColor = '#333'

export default createSketch(({ context, width, height }) => {
  let t = 0
  type Square = Record<'i' | 'j' | 'u' | 'v', number> // { i:, j, u, v }
  const squares: Square[][] = []

  const squareLimit = 15
  const noiseScale = 1.5
  const margin = width / 100

  const w = width / 2
  const h = height / 2

  for (let j = 0; j < squareLimit; j++) {
    const row = []
    for (let i = 0; i < squareLimit; i++) {
      let u = i / squareLimit
      let v = j / squareLimit
      let square = { i, j, u, v }

      row.push(square)
      // squares.push(square)
    }
    squares.push(row)
  }

  context.strokeStyle = '#333'
  context.lineWidth = 40

  // document.addEventListener('keypress', ({ key }) => {
  //   t += 0.11
  // })

  return ({ t, context, width, height }) => {
    context.fillStyle = '#111'
    context.fillRect(0, 0, width, height)
    // time = time * 0.2

    for (const row of squares) {
      for (const square of row) {
        let { u, v } = square

        // check all neighbours
        let rectSize = width / squareLimit

        let n = simplex.noise3D(u * noiseScale, v * noiseScale, t())

        let hue = Math.sin(t() + n) * 40 + (n > 0 ? 140 : 20)
        context.fillStyle = `hsl(${hue}, 80%, 40%)`

        context.save()
        context.translate(u * width, v * height)

        context.fillRect(margin, margin, rectSize - margin, rectSize - margin)
        context.restore()
      }
    }
  }
})
