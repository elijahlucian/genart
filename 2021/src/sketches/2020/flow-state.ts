import { createSketch } from '@dank-inc/sketchy'
import SimplexNoise from 'simplex-noise'

const SN = new SimplexNoise()

type Blade = { u: number; v: number; h: number; s: number }
const makeblade = (i: number, n: number, blades: Blade[], t: number) => {
  const v = SN.noise2D(i / n / 2, t) / 2 + 0.5
  const s = SN.noise2D(i / n, t)
  blades.push({ u: i / n, v, h: 1, s })
}

export default createSketch(({ context, width, height }) => {
  let blades: Blade[] = []

  context.strokeStyle = '#333'
  context.lineWidth = 40

  const n = 80

  const state = {
    blade: {
      gap: 0.04,
      next: 0.04,
    },
    scene: {
      gap: 6,
      next: 6,
    },
  }

  makeblade(blades.length, n, blades, 0)

  return ({ t, context, width, height }) => {
    context.fillStyle = '#333'
    context.fillRect(0, 0, width, height)
    context.fillStyle = '#fff'

    if (t() > state.scene.next) {
      blades = []
      state.scene.next += state.scene.gap
    }

    if (t() > state.blade.next) {
      state.blade.next += state.blade.gap

      makeblade(blades.length, n, blades, t() * 0.1)
    }

    for (const blade of blades) {
      const s = height / 100

      blade.h += 0.05 + blade.s * 0.3

      const h = s * blade.h
      const w = 2

      const x = blade.u * width
      const y = blade.v * height - s * blade.h

      context.fillRect(x - w / 2, y, w, h)
    }
  }
})
