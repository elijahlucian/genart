import { Sketch } from '@dank-inc/sketchy'

import Bezier from 'bezier-easing'
import Simplex from 'simplex-noise'

const TAU = Math.PI * 2

document.body.style.backgroundColor = '#000000'

const sketch: Sketch = () => {
  return ({ t, context, width, height }) => {
    context.fillStyle = '#000000'
    context.fillRect(0, 0, width, height)

    const margin = 200

    context.lineCap = 'round' // || "round" || "square" || "butt"
    context.lineJoin = 'round' // || "bevel" || "miter" || "square"

    context.strokeStyle = '#fff'
    context.lineWidth = width / 40

    let lineResolution = 0.001
    let start = false
    let lastX
    let lastY

    for (let u = 0; u <= 1.01; u += lineResolution) {
      context.beginPath()
      let x =
        (Math.sin(t() * 0.5 + u * TAU) * width) / 3.5 +
        width / 2 +
        Math.sin(t() + u * TAU * 20) *
          (width / 8) *
          Math.sin(t() * 0.1 * Math.PI)
      let y =
        (Math.cos(t() + u * TAU) * height) / 3.5 +
        height / 2 +
        Math.cos(t() * 2 + u * TAU * 10) *
          (width / 8) *
          Math.cos(t() * 0.1 * Math.PI)
      if (start) {
        context.moveTo(x, y)
        lastX = x
        lastY = y
        start = false
      } else {
        context.strokeStyle = `hsl(${
          (-t() * TAU * 50 + u * 360) % 360
        }, 100%, 50%)`
        context.moveTo(lastX as number, lastY as number)
        context.lineTo(x, y)
        lastX = x
        lastY = y
        context.stroke()
      }
    }
    context.closePath()
  }
}
export default sketch
