import { createSketch } from '@dank-inc/sketchy'
import { forN, mapU } from '@dank-inc/lewps'
import { Vec2 } from '../types'

const TAU = Math.PI * 2

export default createSketch(({ context, width, height }) => {
  const points = mapU<Vec2>(50, (u) => [u, 0.5])
  context.translate(width, 0)
  context.rotate(Math.PI / 2)

  return ({ context, width, height, time }) => {
    context.lineCap = 'butt'
    // time = 5.4

    // bg
    // context.fillStyle = `hsl(${time * 360}, 100%, 20%)`
    context.fillStyle = '#000'
    context.fillRect(0, 0, width, height)
    context.lineCap = 'round'

    forN(100, (_, uu) => {
      context.beginPath()
      // context.strokeStyle = `rgba(255,255,255,${0.1})`

      const h = Math.sin(time + uu) * 160 * 2
      const s = uu * uu * 60 + 30

      context.strokeStyle = `hsla(${h},${s}%, 50%, 0.8)`

      const nu = Math.abs(uu - 1)

      context.filter = `blur(${nu * 100}px)`

      context.lineWidth = (width / 50) * nu + width / 100 // 20

      for (let [u, v] of points) {
        v = Math.sin(TAU * u + time) * 0.1 + 0.5

        const x =
          (u * width) / 2 + Math.sin(1 * time + uu * Math.PI) * 200 + width / 4

        const y = v * height + Math.cos(time + 10 * u + uu * Math.PI * 2) * 200 //+ width * 0.4

        context.lineTo(x, y)
      }
      context.stroke()
    })
  }
})
