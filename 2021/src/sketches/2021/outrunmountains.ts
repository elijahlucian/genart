import Simplex from "simplex-noise";
import { create3dSketch } from "../lib";
import sun from "./shaders/outrunSun";
import plane from "./shaders/outrunSun";
// import { Random, MersenneTwister19937 } from "random-js";

// const random = new Random();

import * as THREE from "three";

const simplex = new Simplex();

export default create3dSketch(({ scene }) => {
  var sunShader = new THREE.ShaderMaterial({
    vertexShader: sun.vert,
    fragmentShader: sun.frag,
    uniforms: {
      time: { value: 0 },
    },
  });
  var sunGeometry = new THREE.SphereGeometry(1, 32, 32);
  var sunMesh = new THREE.Mesh(sunGeometry, sunShader);
  sunMesh.position.z = 30;
  sunMesh.position.y = -15;
  sunMesh.scale.x = 40;
  sunMesh.scale.y = 40;
  sunMesh.scale.z = 0.1;
  scene.add(sunMesh);

  var groundShader = new THREE.ShaderMaterial({
    fragmentShader: plane.frag,
    vertexShader: plane.vert,
    transparent: true,
    // depthWrite: false,
    // depthTest: false,
    side: THREE.DoubleSide,
    blending: THREE.NormalBlending,
    uniforms: {
      time: { value: 0 },
    },
  });

  var planeGeo = new THREE.PlaneGeometry(40, 350, 20, 20);
  var planeMat = new THREE.MeshBasicMaterial({
    color: 0xffffff,
    side: THREE.DoubleSide,
    wireframe: true,
  });
  var planeMesh = new THREE.Mesh(planeGeo, groundShader);

  planeMesh.geometry.center();
  planeMesh.rotation.x = Math.PI / 2;
  planeMesh.rotation.z = Math.PI / 2;
  planeMesh.position.y = -5;
  scene.add(planeMesh);

  return () => {};
});
