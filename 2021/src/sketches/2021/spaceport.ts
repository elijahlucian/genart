import * as THREE from 'three'
import { mapXY } from '@dank-inc/lewps'
import { useGeometry, useCircle } from '../lib/helpers/geometry'
import { useLight } from '../lib/helpers/light'
import { v3 } from '../lib/helpers/maff'
import { useMaterial } from '../lib/helpers/material'
import { useMesh } from '../lib/helpers/mesh'
import { create3dSketch } from '../lib/sketchy-3d'
import { Ship } from './classes/ship2'
import { Transport } from './classes/transport'
import Simplex from 'simplex-noise'
import { Vector3 } from 'three'

export default create3dSketch(({ renderer, camera, scene, abs, TAU, PI }) => {
  const state = {
    scroll: 0,
    camera: { x: 0, y: 0 },
    ships: [],
    transports: [],
  }
  const viewer =
    new URLSearchParams(window.location.search).get('viewer') ||
    'tz1SLpSREWeSSdHhMTVgt1ygi9pkgeBZFGRG'

  const hasharr = viewer.split('').map((char) => char.charCodeAt(0))

  const ghettoCrunch = (arr: number[]) => {
    return arr.map((n) => n / 255).reduce((a, b) => a + b)
  }

  const viewerMeta = {
    x: ghettoCrunch(hasharr.slice(0, 12)),
    y: ghettoCrunch(hasharr.slice(12, 24)),
    z: ghettoCrunch(hasharr.slice(24, 36)),
  }
  console.log(viewerMeta)

  const simplex = new Simplex(viewer)

  const INTERVAL = 0.3
  let nextShip = INTERVAL

  const originZ = -2
  const origin = v3(-4, 0, originZ)

  const stars = []

  camera.position.set(3, 0.5, 5)
  camera.lookAt(origin)
  ;(camera as THREE.PerspectiveCamera).fov = 70
  ;(camera as THREE.PerspectiveCamera).updateProjectionMatrix()

  const ship = new Ship({ i: 0 })
  scene.add(ship.mesh)
  state.ships.push(ship)

  var planet = useMesh(
    useGeometry('sphere-buffer', [8, 10, 8]),
    useMaterial('standard', 0xffffff),
    // useShader({ vert, frag }, [
    //   ['time', 0],
    //   ['color', new THREE.Color(planetColor)],
    //   ['heat', 0.5],
    // ]),
  )

  planet.position.y = -8
  planet.position.x = -10
  planet.position.z = -6

  planet.rotation.z = Math.PI * 0.125

  scene.add(planet)

  // var boxHelp = new THREE.BoxHelper(planet, 0xffff00)
  // scene.add(boxHelp)

  let directionalLight = useLight('#fff', 0.8)
  directionalLight.position.set(0, 10, 10)
  scene.add(directionalLight)

  renderer.domElement.addEventListener('mousemove', (e) => {
    const el = renderer.domElement
    const mu = (e.clientX / el.clientWidth - 0.5) * 2
    const mv = (e.clientY / el.clientHeight - 0.5) * 2
    const nmv = abs(1 - e.clientY / el.clientHeight)

    // planet.material.color =

    camera.position.x = 3 + -mu
    camera.position.z = 5 + mu
    camera.lookAt(origin.setZ(originZ + -mu * 0.5))
    // planet.material.uniforms.heat.value = nmv
  })

  renderer.domElement.addEventListener('wheel', (e) => {
    state.scroll += e.deltaY * 0.001
  })

  mapXY(10, 20, (u, v) => {
    const size = 0.3 + simplex.noise2D(u, v) * 0.3
    const starGeo = useCircle(size, 8)
    const starMat = useMaterial('basic', 0x333333)
    const mesh = useMesh(starGeo, starMat)

    const creator = new URLSearchParams(window.location.search).get('creator')

    // mesh.material.side = THREE.DoubleSide
    const n = simplex.noise3D(u, v, viewerMeta.z)
    const sy = simplex.noise2D(u, v) * 200 - 10
    mesh.position.y = sy
    scene.add(mesh)
    stars.push({ mesh, u, v, n, size })
  })

  const updateStars = (time = 0) => {
    for (const star of stars) {
      const planetOrigin = new Vector3()
      planetOrigin.copy(planet.position)

      const { x, z } = planetOrigin
      const { n, size } = star

      const scale = 100
      const sx = x + Math.sin(time + n * TAU) * scale + 5 * size
      const sz = z + Math.cos(time + n * TAU) * scale + 5 * size

      const sn = simplex.noise3D(sx, time * 100, sz) + 0.5

      star.mesh.material.color = new THREE.Color(
        `hsl(0, 0%, ${Math.floor(sn * 50 + 50)}%)`,
      )

      star.mesh.position.set(sx, star.mesh.position.y, sz)
      star.mesh.lookAt(planet.position)
    }
  }

  updateStars()

  return ({ time, dt }) => {
    renderer.render(scene, camera)
    const speed = 0.025

    updateStars(-time * 0.004)
    const inertia = state.scroll * 0.03
    state.scroll -= inertia
    const l = Math.floor(Math.abs(inertia * 200) + 70)
    planet.material.color = new THREE.Color(`hsl(0, 10%, ${l}%)`)

    planet.rotateY(dt * speed + inertia)
    // planet.material.uniforms.time.value = time

    if (nextShip < time) {
      const n = simplex.noise2D(1, time) + 0.3
      nextShip += n + 0.5

      const ship = new Ship({ i: 1 })

      scene.add(ship.mesh)
      state.ships.push(ship)
    }

    for (const ship of state.ships) {
      ship.update({ dt })

      if (ship.isDispatchReady()) {
        const transport = new Transport({
          startingCoords: ship.mesh.position,
        })
        scene.add(transport.mesh)
        state.transports.push(transport)
        ship.confirmDispatch()
      }

      if (ship.kill) {
        scene.remove(ship.mesh)
      }
    }

    for (const transport of state.transports) {
      transport.update({ dt })
    }

    state.ships = state.ships.filter((ship) => !ship.kill)

    // controls.update();
  }
})

const vert = `
varying vec2 vUv;

void main() {
  gl_Position = projectionMatrix * modelViewMatrix * vec4(position.xyz, 1.0);
}
`

const frag = `
#define PI 3.14

varying vec2 vUv;
uniform float time;
uniform float heat;

void main() {
  float tick = mod(time, 1.0);

  vec3 color = vec3(heat);

  gl_FragColor = vec4(color, 1.0);
}
`
