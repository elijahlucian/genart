import { useGeometry } from '../lib/helpers/geometry'
import { useLight } from '../lib/helpers/light'
import { v3 } from '../lib/helpers/maff'
import { useMaterial } from '../lib/helpers/material'
import { useMesh } from '../lib/helpers/mesh'
import { create3dSketch } from '../lib/sketchy-3d'
import { Ship } from './classes/ship2'
import { Transport } from './classes/transport'
import Simplex from 'simplex-noise'

export default create3dSketch(({ renderer, camera, scene }) => {
  const state = {
    ships: [],
    transports: [],
  }

  const simplex = new Simplex()

  const INTERVAL = 0.3
  let nextShip = INTERVAL

  const originZ = -2
  const origin = v3(-4, 0, originZ)

  camera.position.set(3, 0.5, 5)
  camera.lookAt(origin)

  renderer.domElement.addEventListener('mousemove', (e) => {
    const el = renderer.domElement
    const mu = (e.clientX / el.clientWidth - 0.5) * 2
    const mv = (e.clientY / el.clientHeight - 0.5) * 2

    camera.position.z = 3 + mu
    camera.lookAt(origin.setZ(originZ + -mu * 0.5))
  })

  const ship = new Ship({ i: 0 })
  scene.add(ship.mesh)
  state.ships.push(ship)

  var planet = useMesh(
    useGeometry('sphere-buffer', [8, 10, 8]),
    useMaterial('standard', 0x4f748c),
  )

  planet.position.y = -8
  planet.position.x = -10
  planet.position.z = -6

  planet.rotation.z = Math.PI * 0.125

  scene.add(planet)

  // var boxHelp = new THREE.BoxHelper( planet, 0xffff00 );
  // scene.add( boxHelp );

  let directionalLight = useLight('#fff', 0.8)
  directionalLight.position.set(0, 10, 10)
  scene.add(directionalLight)

  return ({ time, dt }) => {
    renderer.render(scene, camera)
    const speed = 0.025
    planet.rotateY(dt * speed)

    if (nextShip < time) {
      const n = simplex.noise2D(1, time)
      nextShip += n + 0.8

      const ship = new Ship({ i: 1 })

      scene.add(ship.mesh)
      state.ships.push(ship)
    }

    for (const ship of state.ships) {
      ship.update({ dt })

      if (ship.isDispatchReady()) {
        const transport = new Transport({
          startingCoords: ship.mesh.position,
        })
        scene.add(transport.mesh)
        state.transports.push(transport)
        ship.confirmDispatch()
      }

      if (ship.kill) {
        scene.remove(ship.mesh)
      }
    }

    for (const transport of state.transports) {
      transport.update({ dt })
    }

    state.ships = state.ships.filter((ship) => !ship.kill)

    // controls.update();
  }
})
