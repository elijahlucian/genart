import { forU } from '@dank-inc/lewps'
import { createSketch } from '@dank-inc/sketchy'

export const r = (s: number, o: number) => Math.random() * s + o

export default createSketch(
  ({ context, width, height, setFillStyle, lerp }) => {
    setFillStyle('#111')

    return ({ t }) => {
      setFillStyle('#ddd7')

      forU(20, () => {
        const s = 10
        const u = Math.random()
        const v = Math.random()

        const x = lerp(u, width)
        const y = lerp(v, height)

        context.translate(x, y)

        context.fillRect(0, 0, s, s)
      })
    }
  },
)
