import { forN, mapU, mapXY } from '@dank-inc/lewps'
import { createSketch, Vec2 } from '@dank-inc/sketchy'
import Simplex from 'simplex-noise'

const n = new Simplex()

const scl = (n: number, scale = 1, offset = 0) => n * scale + offset

export default createSketch(
  ({ context, setFillStyle, height, width, setStrokeStyle, r, lerp }) => {
    const thing: Vec2[] = mapU(100, (u) => [
      u,
      scl(n.noise2D(u * 4, 0), 0.5, 0.5),
    ])
    context.lineJoin = 'round'

    return ({ t }) => {
      setFillStyle('#333')
      context.fillRect(0, 0, width, height)

      context.strokeStyle = '#ccc'
      context.lineWidth = 10

      forN(7, (i) => {
        context.beginPath()

        for (const point of thing) {
          const [u, v] = point

          const ui = scl(i, 0.6)

          point[1] = scl(
            n.noise2D(5 * ui + t() + u * 4, t(0.2)) + i * 0.1,
            0.1,
            0.15,
          )

          const x = lerp(u, width)
          const y = lerp(v, height) // + i * height * 0.1
          context.lineTo(x, y)
        }

        context.stroke()
      })
    }
  },
)
