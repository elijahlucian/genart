import hazy from './hazy'
import infinilove from './2019/07-02-infinilove'
import dejaVu from './2019/07-08-deja-vu'
import flowState from './2020/flow-state'
import life from './2020/life'
import grewvy from './grewvy'

import { Sketch } from '@dank-inc/sketchy'

type SketchIndex = Record<string, Sketch>

export default {
  grewvy,
  hazy,
  infinilove,
  dejaVu,
  flowState,
  life,
} as SketchIndex
