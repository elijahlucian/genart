const normalize = (n: number): number => n * 0.66

export const sin = n => {
  const s = Math.sin(n) // -1.5 .. 1.5
  return normalize(s) // -.75 .. .75
}

export const cos = (n: number): number => {
  const s = Math.cos(n) // -1.5 .. 1.5
  return normalize(s) // -.75 .. .75
}

export const abs = (n: number) => Math.abs(n)

export const PI = Math.PI
export const TAU = PI * 2
