import { PerspectiveCamera, OrthographicCamera, Vector3 } from 'three'

export const makeOrthoCamera = (): OrthographicCamera => {
  return new OrthographicCamera(-3, 3, 3, -3)
}

type PerspectiveParams = {
  w: number
  h: number
  pos?: [number, number, number]
  lookAt?: [number, number, number]
  angle?: number
  near?: number
  far?: number
}
export const makeCamera = ({
  w,
  h,
  pos = [0, 0, 0],
  lookAt = [0, 0, 0],
  angle = 75,
  near = 0.001,
  far = 50,
}: PerspectiveParams): PerspectiveCamera => {
  const camera = new PerspectiveCamera(angle, w / h, near, far)
  camera.position.set(...pos)
  camera.lookAt(new Vector3(...lookAt))
  return camera
}
