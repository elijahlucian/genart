import { Scene, DirectionalLight, AmbientLight } from 'three'

type SceneParams = {
  lightColor: string
  lightBalance: number // 0-1 hard amount
  directionalPos?: [number, number, number]
}
/* 
 Makes a scene and Lights it!
*/
export const makeAScene = ({
  lightColor,
  lightBalance,
  directionalPos = [4, 4, 0],
}: SceneParams): Scene => {
  const scene = new Scene()
  const ambient = 1 - lightBalance
  const hard = lightBalance
  const directional = new DirectionalLight(lightColor, hard)
  directional.position.set(...directionalPos)

  scene.add(new AmbientLight(lightColor, ambient))

  scene.add(directional)
  return scene
}
