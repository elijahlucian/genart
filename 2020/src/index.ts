import {
  Clock,
  WebGLRenderer,
  Scene,
  SphereGeometry,
  Mesh,
  Camera,
  Renderer,
  ShaderMaterial,
  MeshToonMaterial,
  AmbientLight,
  DirectionalLight,
} from 'three'

import { makeOrthoCamera, makeCamera } from './utils/camera'

import { FilmPass } from 'three/examples/jsm/postprocessing/FilmPass'
import { BloomPass } from 'three/examples/jsm/postprocessing/BloomPass'
import { EffectComposer } from 'three/examples/jsm/postprocessing/EffectComposer'
import {
  BloomEffect,
  ChromaticAberrationEffect,
  RenderPass,
  EffectPass,
} from 'postprocessing'
import glsl from 'glslify'
import { Chance } from 'chance'
import { Vector3 } from 'three'
import { sketch } from './sketches/basic'
import fragmentShader from './sketches/basic/frag'
import vertexShader from './sketches/basic/vert'
import { ShaderPass } from 'three/examples/jsm/postprocessing/ShaderPass'
import { makeAScene } from './utils/scene'

// console.log(vertexShader)
// console.log(fragmentShader)

type Vec2 = {
  x: number
  y: number
}

type Params = {
  name: string
  size: Vec2
  el: Element
  agentCount: number
  seed: string

  sketch: (meshes: ExtendedMesh[], time: number, updateRate: number) => void
}

export type ExtendedMesh = Mesh & {
  meta?: {
    u: number
    v?: number
    w?: number
    color?: string
    i: number
  }
}

const t = { v: 0 }

class ThreeSketch {
  scene: Scene
  camera: Camera
  renderer: WebGLRenderer
  meshes: ExtendedMesh[]
  size: Vec2
  el: Element
  agentCount: number
  time: number
  chance: Chance.Chance
  clock: Clock
  sketch: (meshes: ExtendedMesh[], time: number, updateRate: number) => void
  composer: any

  constructor({ name, size, agentCount, seed }: Params) {
    Object.assign(this, { name, size, el, agentCount, sketch })
    this.time = 0
    this.chance = new Chance(seed)
  }

  init = () => {
    const canvas = document.querySelector('#sketch') as HTMLCanvasElement

    this.clock = new Clock()

    this.camera = makeCamera({
      w: this.size.x,
      h: this.size.y,
      pos: [-1, 1.5, 0],
      lookAt: [0, 0, -5],
    })
    this.scene = makeAScene({ lightColor: '#ffffff', lightBalance: 1 })

    var geometry = new SphereGeometry(0.5, 5, 3)

    // const material = new ShaderMaterial({
    //   vertexShader,
    //   fragmentShader,
    // })

    const material = new MeshToonMaterial()

    this.meshes = []

    for (let i = 0; i <= this.agentCount; i++) {
      // TODO: wrap mesh material creation in makeMesh function in sketch file.

      const u = (i / this.agentCount - 0.5) * 2
      const mesh: ExtendedMesh = new Mesh(geometry, material)

      const scale = 0.1
      mesh.scale.set(scale, scale, scale)
      mesh.meta = { i, u }

      mesh.position.x = this.chance.floating({ min: -4, max: 4 })
      mesh.position.y = this.chance.floating({ min: -4, max: 4 })
      mesh.position.z = this.chance.floating({ min: -25, max: 2 })

      this.meshes.push(mesh)
      this.scene.add(mesh)
    }

    // this.renderer = makeDank() // handles render and shader fx passes and shit.

    this.renderer = new WebGLRenderer({ antialias: true, canvas })
    this.composer = new EffectComposer(this.renderer)
    this.renderer.setSize(this.size.x, this.size.y)
    this.composer.setSize(this.size.x, this.size.y)

    this.composer.addPass(new RenderPass(this.scene, this.camera))
    this.composer.addPass(new BloomPass(0.9, 15, 0.7, this.size.x))
    this.composer.addPass(new FilmPass(1))

    this.el.appendChild(this.renderer.domElement)
  }

  animate = () => {
    const updateRate = this.clock.getDelta() * 0.55
    requestAnimationFrame(this.animate)
    this.time += updateRate

    sketch(this.meshes, this.time, updateRate)

    this.camera.position.x = Math.sin(this.time)
    this.camera.lookAt(new Vector3(0, 0, Math.cos(this.time) * -3 - 7))
    this.composer.render(this.clock.getDelta())
  }
}

const el = document.querySelector('#app')
const app = new ThreeSketch({
  name: 'threejs',
  size: { x: window.innerWidth, y: window.innerHeight },
  el,
  agentCount: 5000,
  seed: 'dank',
  sketch,
})
app.init()
app.animate()

console.log('hello world', app)
