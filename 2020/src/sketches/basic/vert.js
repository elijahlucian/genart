const glsl = require('glslify')

export default /* glsl */ `
  varying vec3 vUv;
  uniform float time;
  uniform float i;
  uniform float u;

  void main() {
    vUv = position;

    vec3 p = position;

    vec4 modelViewPosition = modelViewMatrix * vec4(p,1.0);

	  gl_Position = projectionMatrix * modelViewPosition;
  }
`
