import { ExtendedMesh } from '../../index'
import { sin, cos, PI, TAU, abs } from '../../utils/maff'

export const sketch = (
  meshes: ExtendedMesh[],
  time: number,
  updateRate: number
) => {
  for (const mesh of meshes) {
    const { u, i } = mesh.meta

    const nz = (mesh.position.z + 15) / 17

    // mesh.rotation.z += updateRate * 20
    mesh.scale.z = nz ** 2

    const offset = -(mesh.position.z * mesh.meta.i) / 3000

    mesh.position.y =
      Math.cos(-mesh.position.z + offset +  time ) *
    mesh.position.x =
      Math.sin(-mesh.position.z + offset + time ) *
      (25 / mesh.position.z)

    mesh.position.z += updateRate // + abs(u) * 0.01 // sin(sin(1 * time + i) + u * TAU) * u
    if (mesh.position.z > 2) mesh.position.z = -25
  }
}
