const glsl = require('glslify')

export default /* glsl */ `
  uniform float time;
  varying vec3 vUv; 
  uniform float i; 
  uniform float u;

  void main() {
    float r = 0.; // distance(time + vUv.x, .5);
    float g = .5; // distance(vUv.y, .8);
    float b = sin(time);

    gl_FragColor = vec4(r, g, b, .1);
  }
`
